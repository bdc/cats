// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_time.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_TIME_H
#define CATS_CATS_TIME_H

#include "../cats_defs.h"
#include <stdint.h>

#ifdef CATS_ON_WINDOWS
#include <winsock.h>
#else
#include <sys/time.h>
#endif

struct cats_time {

// monotonic time stamps, used for total elapsed time
#ifdef CATS_ON_WINDOWS
        LARGE_INTEGER clock_monotonic;
#else
        struct timespec clock_monotonic;
#endif

        struct timeval current_time;
};

double seconds_monotonic_since(struct cats_time *start_time);

void time_now(struct cats_time *time_info);

double get_elapsed_seconds_monotonic(struct cats_time *start_time, struct cats_time *end_time);

void get_time(struct timeval *tv);

#endif //CATS_CATS_TIME_H
