// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_time.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <time.h>
#include <stdio.h>
#include "cats_time.h"
#include <sys/time.h>
#ifdef CATS_ON_WINDOWS
#include <profileapi.h>
#endif


void get_time(struct timeval *tv)
{
#if defined(CATS_ON_WINDOWS) && ! defined(__MINGW32__)
        struct timespec ts;
        timespec_get(&ts, TIME_UTC);
        tv->tv_sec = (long) ts.tv_sec;
        tv->tv_usec = (long) ((double) ts.tv_nsec / 1000.0);
#else
        gettimeofday(tv, NULL);
#endif

}


void time_now(struct cats_time *time_info)
{
#ifdef CATS_ON_WINDOWS
        LARGE_INTEGER zero = {0};
        QueryPerformanceCounter(&time_info->clock_monotonic);
        if (time_info->clock_monotonic.QuadPart == zero.QuadPart) {
                fprintf(stderr, "%s: unable to get start time with QueryPerformanceCounter\n", __func__);
                exit(EXIT_FAILURE);
        }
#else
        clock_gettime(CLOCK_MONOTONIC, &time_info->clock_monotonic);
#endif

        get_time(&time_info->current_time);
}


double get_elapsed_seconds_monotonic(struct cats_time *start_time, struct cats_time *end_time)
{
#ifdef CATS_ON_WINDOWS
        LARGE_INTEGER frequency;
        QueryPerformanceFrequency(&frequency);
        return (double) (end_time->clock_monotonic.QuadPart  -  start_time->clock_monotonic.QuadPart) / frequency.QuadPart;
#else
        return (double) (end_time->clock_monotonic.tv_sec - start_time->clock_monotonic.tv_sec) +
               (double) (end_time->clock_monotonic.tv_nsec - start_time->clock_monotonic.tv_nsec) * 1e-9;
#endif

}


double seconds_monotonic_since(struct cats_time *start_time)
{
        struct cats_time now;
        time_now(&now);
        return get_elapsed_seconds_monotonic(start_time, &now);
}