// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_defs.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_DEFS_H
#define CATS_CATS_DEFS_H
#include "cats_windows.h"

#if defined(__GNUC__)
#define EXPECT(A, B) __builtin_expect((A), (B))
#else
#define EXPECT(A, B) __builtin_expect(A)
#endif

#if defined(WIN32) || defined (_WIN32)
#define DIRECTORY_SEPARATOR "\\"
#else
#define DIRECTORY_SEPARATOR "/"
#endif

#define str(s) #s
#define xstr(s) str(s)

#ifndef CATS_GIT_VERSION
#define VERSION str(UNKNOWN)
#else
#define VERSION xstr(CATS_GIT_VERSION)
#endif

#ifndef TIMESTAMP
#define COMPILATION_DATE str(UNKNOWN)
#else
#define COMPILATION_DATE xstr(TIMESTAMP)
#endif

#ifndef COMPILER_VERSION
#define COMPILER str(UNKNOWN)
#else
#define COMPILER xstr(COMPILER_VERSION)
#endif

#endif //CATS_CATS_DEFS_H
