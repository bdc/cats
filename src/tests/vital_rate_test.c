// SPDX-License-Identifier: GPL-3.0-or-later
//
// vital_rate_test.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include "inline_vital_rates.h"
#include "vital_rates/setup_rates.h"
#include "debug/debug_vital_rates.h"
#include "memory/cats_memory.h"
#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

struct cats_global global;
struct cats_debug_options cats_debug;

struct test_vital_rates {
        struct cats_vital_rate vr;
        struct cats_vital_rate_hybrid_function vital_dependency_registry[MAX_VITAL_RATES];
        struct cats_grid *grid;
        struct cats_configuration *conf;
        struct cats_environment *env;
        struct cats_species_param param;
        struct cats_vital_rate hybrid_vital_rate;
        int32_t MAX_SUIT_STEPS;
        int32_t MAX_POP;
        int32_t link_count;
};


int setup(void **state)
{
        logging_initialize(LOG_ERROR, NULL, NULL, true);
        struct test_vital_rates *test = calloc_or_die(1, sizeof(struct test_vital_rates));
        test->link_count = setup_default_links(test->vital_dependency_registry);
        test->env = minimal_suitability_environment();
        test->grid = minimal_grid(test->conf, test->env);
        test->grid->suitability = test->env;
        test->param.OT = 0.5;
        test->param.ZT = 0.5 / 2.0;
        test->MAX_POP = 1000;
        test->param.scale_factor = 0.5;
        test->MAX_SUIT_STEPS = 1000;
        test->param.demographic_slope = 15;
        test->hybrid_vital_rate.environment_set = test->env;
        test->hybrid_vital_rate.density = DENSITY_DEP_NEGATIVE;
        test->hybrid_vital_rate.density_ts = 0.5;
        test->hybrid_vital_rate.max_rate = 1.0;
        *state = test;
        return 0;
}


int teardown(void **state)
{
        return 0;
}


cats_dt_rates sfs[] = {0.1, 0.5, 0.9};
cats_dt_rates ots[] = {0.1, 0.5, 0.9};


void vital_rate_monotonic(void **state, cats_rate_function func, cats_dt_rates density)
{
        assert(density <= 1);
        assert(density == -1 || density >= 0);
        struct test_vital_rates *test = *state;
        struct cats_vital_rate rate = test->hybrid_vital_rate;
        struct cats_vital_rate rate_no_density = test->hybrid_vital_rate;

        cats_dt_rates value = 0;
        cats_dt_rates no_density_value = 0;
        cats_dt_rates old_value = 0;
        cats_dt_rates N = 0;

        if (density >= 0) {
                N = test->MAX_POP * density;
                rate.density = DENSITY_DEP_NEGATIVE;
                rate_no_density.density = NO_DENSITY_DEP;
        } else {
                N = 0;
                rate.density = NO_DENSITY_DEP;
        }


        for (int32_t s = 0; s < test->MAX_SUIT_STEPS + 1; s++) {
                cats_dt_rates suit = (cats_dt_environment) s / (cats_dt_environment) test->MAX_SUIT_STEPS;

                value = func(&rate, &test->param, suit, N, test->MAX_POP);
                assert_true(value >= old_value);
                assert_true(value >= 0);
                assert_true(value <= rate.max_rate);

                if (density != -1) {
                        no_density_value = func(&rate_no_density, &test->param, suit, 0, test->MAX_POP);
                        assert_true(no_density_value >= value);
                }

                old_value = value;
        }

}


void vital_rate_sigmoid_density_monotonic(void **state)
{
        struct test_vital_rates *test = *state;
        for (int32_t p = 0; p < test->MAX_POP + 1; p++) {
                cats_dt_rates density = (cats_dt_rates) p / (cats_dt_rates) test->MAX_POP;
                vital_rate_monotonic(state, sigmoid, density);
        }

}

void vital_rate_linear_density_monotonic(void **state)
{
        struct test_vital_rates *test = *state;
        for (int32_t p = 0; p < test->MAX_POP + 1; p++) {
                cats_dt_rates density = (cats_dt_rates) p / (cats_dt_rates) test->MAX_POP;
                vital_rate_monotonic(state, linear_rate, density);
        }

}

void vital_rate_constant_density_monotonic(void **state)
{
        struct test_vital_rates *test = *state;
        for (int32_t p = 0; p < test->MAX_POP + 1; p++) {
                cats_dt_rates density = (cats_dt_rates) p / (cats_dt_rates) test->MAX_POP;
                vital_rate_monotonic(state, constant_value_rate, density);
        }

}


void vital_rate_sigmoid_monotonic(void **state)
{
        vital_rate_monotonic(state, sigmoid, -1);
}


void vital_rate_linear_monotonic(void **state)
{
        vital_rate_monotonic(state, linear_rate, -1);
}


void vital_rate_constant_monotonic(void **state)
{
        vital_rate_monotonic(state, constant_value_rate, -1);
}


void vital_rate_sigmoid(void **state)
{
        cats_rate_function func = sigmoid;
        struct test_vital_rates *test = *state;
        struct cats_vital_rate *rate = &test->hybrid_vital_rate;
        cats_dt_rates zero_value = func(rate, &test->param, 0.0, 0, test->MAX_POP);
        cats_dt_rates one_value = func(rate, &test->param, 1.0, 0, test->MAX_POP);
        cats_dt_rates OT_value = func(rate, &test->param, test->param.OT, 0, test->MAX_POP);
        assert_true(zero_value < 1e-3);
        assert_true(zero_value >= 0);
        assert_true(one_value >= 1 - 1e-3);
        assert_true(one_value <= 1);
        assert_true(OT_value == test->param.scale_factor * rate->max_rate);

}


void vital_rate_linear(void **state)
{
        cats_rate_function func = linear_rate;
        struct test_vital_rates *test = *state;
        struct cats_vital_rate *rate = &test->hybrid_vital_rate;
        cats_dt_rates zero_value = func(rate, &test->param, 0.0, 0, test->MAX_POP);
        cats_dt_rates one_value = func(rate, &test->param, 1.0, 0, test->MAX_POP);
        cats_dt_rates OT_value = func(rate, &test->param, test->param.OT, 0, test->MAX_POP);
        assert_true(zero_value < 1e-3);
        assert_true(zero_value >= 0);
        assert_true(one_value >= 1 - 1e-3);
        assert_true(one_value <= 1);
        assert_true(OT_value == test->param.scale_factor * rate->max_rate);

}


void vital_rate_constant(void **state)
{
        cats_rate_function func = constant_value_rate;
        struct test_vital_rates *test = *state;
        struct cats_vital_rate rate = test->hybrid_vital_rate;
        rate.max_rate = 0.3;
        cats_dt_rates zero_value = func(&rate, &test->param, 0.0, 0, test->MAX_POP);
        cats_dt_rates one_value = func(&rate, &test->param, 1.0, 0, test->MAX_POP);
        cats_dt_rates OT_value = func(&rate, &test->param, test->param.OT, 0, test->MAX_POP);
        assert_true(zero_value == rate.max_rate);
        assert_true(one_value == rate.max_rate);
        assert_true(OT_value == rate.max_rate);

}


int main(int argc, char **argv)
{

        const struct CMUnitTest tests[] =
                {
                        //cmocka_unit_test(hybrid_vital_rate_zero_below_suitability_ts),
                        cmocka_unit_test(vital_rate_sigmoid_monotonic),
                        cmocka_unit_test(vital_rate_linear_monotonic),
                        cmocka_unit_test(vital_rate_constant_monotonic),
                        cmocka_unit_test(vital_rate_sigmoid),
                        cmocka_unit_test(vital_rate_linear),
                        cmocka_unit_test(vital_rate_constant),
                        cmocka_unit_test(vital_rate_sigmoid_density_monotonic),
                        cmocka_unit_test(vital_rate_linear_density_monotonic),
                        cmocka_unit_test(vital_rate_constant_density_monotonic),
                };


        int count_fail_tests = cmocka_run_group_tests (tests, setup, teardown);

        return count_fail_tests;

}