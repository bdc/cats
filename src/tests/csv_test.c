// SPDX-License-Identifier: GPL-3.0-or-later
//
// csv_test.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//
#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>
#include "memory/cats_memory.h"
#include "cats_strings/cats_strings.h"
#include "cats_csv/cats_csv.h"
#include <unistd.h>
#include <limits.h>

struct csv_test {
        FILE *csv_file;
        struct cats_csv *csv;
};
#define CSV_FILE "test.csv"

int setup(void **state)
{
        struct csv_test *test = calloc_or_die(1, sizeof(struct csv_test));
        test->csv_file = fopen(CSV_FILE, "r");
        test->csv = NULL;
        ENSURE_FILE_OPENED(test->csv_file, CSV_FILE);
        *state = test;
        return 0;
}


int teardown(void **state)
{
        return 0;
}

void test_csv_open(void **state)
{
        struct csv_test *test = *state;
        assert_non_null(test->csv_file);
        test->csv = csv_read_file(test->csv_file, 3);
        assert_non_null(test->csv);
        assert_int_equal(test->csv->data_row_count, 2);
        assert_int_equal(test->csv->column_count, 3);



}

void test_read_csv(void **state)
{
        struct csv_test *test = *state;
        struct cats_csv *csv = test->csv;
        double x0C = csv_get_double_field_name(csv, 0, "C");
        int32_t col_B = csv_get_field_idx(csv, "B");
        double x1B = csv_get_double_field_idx(csv, 1, 1);
        assert_true(col_B == 1);
        assert_true(3.0 == x0C);
        assert_true(5.0 == x1B);


}

void test_free_csv(void **state)
{
        struct csv_test *test = *state;
        struct cats_csv *csv = test->csv;
        csv_free(&csv);
        assert_null(csv);
}

int main(int argc, char **argv)
{

        const struct CMUnitTest tests[] =
                {
                        cmocka_unit_test(test_csv_open),
                        cmocka_unit_test(test_read_csv),
                        cmocka_unit_test(test_free_csv),

                };


        int count_fail_tests = cmocka_run_group_tests (tests, setup, teardown);

        return count_fail_tests;

}