// SPDX-License-Identifier: GPL-3.0-or-later
//
// register_module.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "configuration/configuration.h"
#include "cats_global.h"
#include "memory/cats_memory.h"
#include "modules/load_module.h"
#include "modules/module_header.h"
#include "second_file.h"

const char *module_name = "cats-test-module";

struct cats_global global;


struct cats_test_data {
        char *name;
        float x;
};


void *grid_init(struct cats_configuration *conf, struct cats_grid *grid, void *data)
{
        log_message(LOG_INFO, "grid init");
        return NULL;
}


void *grid_cleanup(struct cats_configuration *conf, struct cats_grid *grid, void *data)
{
        log_message(LOG_INFO, "grid cleanup");
        return NULL;
}


void cats_module_init(struct cats_configuration *conf)
{
        void *data = calloc_or_die(1, sizeof(struct cats_test_data));
        enum cats_module_flags flags = MODULE_NO_FLAGS;
        int32_t id = register_module(conf, module_name, data, flags);
        register_cats_grid_init_function(conf, grid_init, grid_cleanup);
        log_message(LOG_INFO, "Hello from '%s' (id: %d)", module_name, id);
        greeting2();

}

