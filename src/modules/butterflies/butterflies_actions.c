// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_actions.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; youi can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "actions/cats_actions.h"
#include "modules/module_header.h"
#include "butterflies_actions.h"
#include "butterflies_main.h"
#include "inline_overlays.h"
#include "butterflies_populations.h"

#include "butterflies_inline.h"
#include "grids/grid_wrapper.h"
#include "grids/gdal_save.h"
#include "paths/paths.h"
#include "populations/population.h"
#include "butterflies_generations.h"
#include "butterflies_dispersal.h"
#include "butterflies_overlays.h"
#include "butterflies_paths.h"
#include "inline.h"
#include "lambda/leslie_matrix.h"
#include "temporal/years.h"
#include "butterflies_initial_population.h"


enum action_status bf_action_stats_reset(struct cats_grid *grid, struct cats_configuration *conf)
{
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        int module_id = CATS_MODULE_ID;

        struct grid_data_butterflies *module_data = grid->grid_modules[module_id].module_data;
        if (conf->time.year_current == conf->time.year_start &&
            module_conf->generations_max == module_data->generation_current) {
                log_message(LOG_INFO, "SUMMARY: Scale factor: %Lf", grid->param.scale_factor);
                struct lambda_parameters l_param = {0};
                l_param.calculate_scale = true;
                l_param.suitability = grid->param.OT;
                l_param.N = 0;
                l_param.K = (cats_dt_population)
                        (get_vital_rate_maximum(&conf->param[grid->id].carrying_capacity) *
                         conf->param->max_adult_cc_fraction);
                l_param.grid = 0;
                l_param.row = 0;
                l_param.col = 0;
                l_param.param = &conf->param[grid->id];
                l_param.species_id = grid->id;
                bool print_rate = false;
                cats_dt_rates female_fraction = module_conf->female_fraction;
                cats_dt_rates stationary = module_conf->probability_to_stay;
                cats_dt_rates mobile = 1.0 - stationary;
                cats_dt_rates egg_fraction_source = module_conf->egg_fraction_source;
                cats_dt_rates eggs_per_female = calculate_rate_for_matrix(&module_conf->eggs_per_female, &l_param,
                                                                          print_rate);
                cats_dt_rates reproduction_rate = calculate_rate_for_matrix(&module_conf->reproduction_rate, &l_param,
                                                                            print_rate);
                cats_dt_rates K =
                        calculate_rate_for_matrix(&conf->param[grid->id].carrying_capacity, &l_param, print_rate) *
                        conf->param->max_adult_cc_fraction;
                cats_dt_rates local_eggs = (stationary + mobile * egg_fraction_source) * eggs_per_female;
                // female -> female
                // to achieve the target reproduction rate, the number of eggs per female laid in the cell
                // that survive and become adult has to be the reproduction rate divided by the female fraction divided by the number of eggs
                cats_dt_rates eggs_to_adults_rate =
                        bf_egg_to_adult_survival_rate(reproduction_rate, local_eggs) / module_conf->female_fraction;
                cats_dt_rates result = local_eggs * eggs_to_adults_rate * female_fraction;
                cats_dt_rates generations = calculate_rate_for_matrix(&module_conf->butterfly_generations, &l_param,
                                                                      print_rate);


                log_message(LOG_INFO, "SUMMARY: reproduction rate at OT: %Lf", reproduction_rate);
                log_message(LOG_INFO, "SUMMARY: eggs per female at OT: %Lf", eggs_per_female);
                log_message(LOG_INFO, "SUMMARY: local eggs at OT: %Lf", local_eggs);
                log_message(LOG_INFO, "SUMMARY: stationary females at OT: %Lf", stationary);
                log_message(LOG_INFO, "SUMMARY: eggs to adult rate at OT: %Lf", eggs_to_adults_rate);
                log_message(LOG_INFO, "SUMMARY: egg fraction source (non-stationary females) at OT: %Lf",
                            egg_fraction_source);
                log_message(LOG_INFO, "SUMMARY: carrying capacity at OT: %Lf", K);
                log_message(LOG_INFO, "SUMMARY: generations at OT: %Lf", generations);
                log_message(LOG_INFO, "SUMMARY: effective female to female rate at OT: %Lf\n", result);

                l_param.suitability = 1.0;
                eggs_per_female = calculate_rate_for_matrix(&module_conf->eggs_per_female, &l_param, print_rate);
                reproduction_rate = calculate_rate_for_matrix(&module_conf->reproduction_rate, &l_param, print_rate);
                K = calculate_rate_for_matrix(&conf->param[grid->id].carrying_capacity, &l_param, print_rate) *
                    conf->param->max_adult_cc_fraction;
                local_eggs = (stationary + mobile * egg_fraction_source) * eggs_per_female;
                eggs_to_adults_rate =
                        bf_egg_to_adult_survival_rate(reproduction_rate, local_eggs) / module_conf->female_fraction;
                result = local_eggs * eggs_to_adults_rate;
                generations = calculate_rate_for_matrix(&module_conf->butterfly_generations, &l_param, print_rate);

                log_message(LOG_INFO, "SUMMARY: reproduction rate at suitability 1: %Lf", reproduction_rate);
                log_message(LOG_INFO, "SUMMARY: eggs per female at suitability 1: %Lf", eggs_per_female);
                log_message(LOG_INFO, "SUMMARY: local eggs at suitability 1: %Lf", local_eggs);
                log_message(LOG_INFO, "SUMMARY: stationary females at suitability 1: %Lf", stationary);
                log_message(LOG_INFO, "SUMMARY: eggs to adult rate at suitability 1: %Lf", eggs_to_adults_rate);
                log_message(LOG_INFO, "SUMMARY: egg fraction source (non-stationary females) at suitability 1: %Lf",
                            egg_fraction_source);
                log_message(LOG_INFO, "SUMMARY: carrying capacity at suitability 1: %Lf", K);
                log_message(LOG_INFO, "SUMMARY: generations at suitability 1: %Lf", generations);
                log_message(LOG_INFO, "SUMMARY: effective female to female rate at suitability 1: %Lf\n", result);

        }

        return action_grid_stats_reset(grid, conf);
}


void grid_butterflies_maturation(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;
        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;

        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;


        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {
#ifdef BF_DEBUG
                        if (get_adult_population(grid, row, col)) {
                                printf("BFDBG::%s::adults at maturation::%d,%d,%d\n",
                                       __func__, row, col, get_adult_population(grid, row, col));
                        }
#endif
                        if (cell_excluded_by_overlay(conf, row, col)
                            || bf_cell_excluded_by_generation(grid, row, col)) {
#ifdef BF_DEBUG
                                if (get_adult_population(grid, row, col)) {
                                        printf("BFDBG::%s::adults at maturation::%d,%d,%d - excluded by generation\n",
                                               __func__, row, col, get_adult_population(grid, row, col));
                                }
#endif

                                continue;
                        }

                        bf_cell_maturation(grid, ts, row, col, false);
                }
        }

}


void bf_initial_population_to_eggs(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;
        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;

        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;


        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {

                        if (cell_excluded_by_overlay(conf, row, col)) {
                                continue;
                        }
                        set_population(grid, row, col, get_adult_population(grid, row, col));
                        butterflies_cell_dispersal(grid, ts, row, col, false, true);
                }
        }

}


void butterflies_area_dispersal(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;
        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;

        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;

        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {
                        if (cell_excluded_by_overlay(conf, row, col)
                            || bf_cell_excluded_by_generation(grid, row, col)) {
                                continue;
                        }

                        butterflies_cell_dispersal(grid, ts, row, col, false, false);
                }
        }
}


enum action_status bf_action_maturation(struct cats_grid *grid, struct cats_configuration *conf)
{
        threaded_action(&grid_butterflies_maturation, grid, conf, TS_DEFAULT);

        return ACTION_RUN;
}


enum action_status bf_action_save_grid(struct cats_grid *grid, struct cats_configuration *conf)
{
        if (!is_output_year(&conf->time)) return ACTION_NOT_RUN;
        int32_t id = grid->id;
        char *filename = bf_population_filename(conf, grid);
        struct grid_wrapper data = gridwrapper(grid->population, grid->dimension);
        save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
        free(filename);
        return ACTION_RUN;
}


enum action_status bf_action_save_overlay(struct cats_grid *grid, struct cats_configuration *conf)
{
        if (!is_output_year(&conf->time)) return ACTION_NOT_RUN;
        int32_t id = grid->id;

        char *filename = bf_population_overlay_filename(conf, grid);
        int module_id = CATS_MODULE_ID;

        struct grid_data_butterflies *module_data = grid->grid_modules[module_id].module_data;

        struct grid_wrapper data = gridwrapper(module_data->info_layer, grid->dimension);
        save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
        free(filename);
        return ACTION_RUN;
}


enum action_status bf_action_save_eggs_grid(struct cats_grid *grid, struct cats_configuration *conf)
{
        if (!is_output_year(&conf->time)) return ACTION_NOT_RUN;
        int32_t id = grid->id;
        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *module_data = grid->grid_modules[module_id].module_data;
        char *filename = bf_population_eggs_filename(conf, grid);
        struct grid_wrapper data = gridwrapper(module_data->eggs, grid->dimension);
        save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
        free(filename);
        return ACTION_RUN;
}


enum action_status bf_action_dispersal(struct cats_grid *grid, struct cats_configuration *conf)
{
        threaded_action(&butterflies_area_dispersal, grid, conf, TS_DISPERSAL);
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;

        for (enum butterfly_stats which = BF_RANDOM_WALK_DEPOSIT_COUNT; which < BF_OUTPUT_STAT_MAX; which++) {
                int64_t stat_id = module_conf->stat_ids[which];
                log_message(LOG_INFO, "STAT %s (%ld): %ld", bf_get_stats_field_name(which), stat_id,
                            grid->stats.custom_stats[stat_id]);
        }

        return ACTION_RUN;
}


enum action_status bf_action_stats_gather(struct cats_grid *grid, struct cats_configuration *conf)
{
        threaded_action(&bf_area_stats_gather, grid, conf, TS_DEFAULT);
        return ACTION_RUN;
}


enum action_status bf_action_stats_write(__attribute__((unused)) struct cats_grid *grid,
                                         __attribute__((unused)) struct cats_configuration *conf)
{
        bf_stats_write(conf, grid);

        return ACTION_RUN;
}


// only run at the start of the year
enum action_status bf_action_generation_update(struct cats_grid *grid, struct cats_configuration *conf)
{
        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        const int64_t egg_cells_id = module_conf->stat_ids[BF_CELLS_WITH_EGGS];
        const int64_t egg_cells_removed_id = module_conf->stat_ids[BF_CELLS_WITH_EGGS_REMOVED];
        grid->stats.custom_stats[egg_cells_id] = 0;
        grid->stats.custom_stats[egg_cells_removed_id] = 0;

        data->generation_current = module_conf->generations_max;
        log_message(LOG_IMPORTANT, "resetting generation to %d", module_conf->generations_max);

        if (grid->param.initial_population.adjusted == false) {
                bf_initial_population_adjustment(conf, grid);
                grid->param.initial_population.adjusted = true;
        }

        threaded_action(&bf_area_generation_update, grid, conf, TS_DEFAULT);

        int64_t cells_with_eggs = grid->stats.custom_stats[egg_cells_id];
        int64_t cells_with_eggs_removed = grid->stats.custom_stats[egg_cells_removed_id];
        log_message(LOG_INFO, "%ld cells with eggs left, %ld cells with eggs removed", cells_with_eggs,
                    cells_with_eggs_removed);

        return ACTION_RUN;
}


enum action_status bf_action_overlay_update(struct cats_grid *grid, struct cats_configuration *conf)
{
        return bf_grid_overlay_update(conf, grid);
}


enum action_status bf_action_generation_finish(struct cats_grid *grid, struct cats_configuration *conf)
{
        const int module_id = CATS_MODULE_ID;

        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        threaded_action(&bf_area_kill_adults, grid, conf, TS_DEFAULT);

        data->generation_current--;
        assert(data->generation_current >= 0);

        return ACTION_RUN;
}


enum action_status
bf_action_generation_start(struct cats_grid *grid, __attribute__((unused)) struct cats_configuration *conf)
{
#ifdef BF_DEBUG
        cats_dt_population rows = grid->dimension.rows;
        cats_dt_population cols = grid->dimension.cols;
        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        if (get_adult_population(grid, row, col)) {
                                printf("BFDBG::%s::adults at start of generation::%d,%d,%d\n",
                                       __func__, row, col, get_adult_population(grid, row, col));

                        }
                }
        }
#endif
        int module_id = CATS_MODULE_ID;

        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        log_message(LOG_INFO, "Starting generation %d", data->generation_current);
        assert(data->generation_current >= 0);
        return ACTION_RUN;
}
