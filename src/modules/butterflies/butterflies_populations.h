// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_populations.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

//
// Created by gattringera on 21/11/22.
//

#ifndef CATS_BUTTERFLIES_POPULATIONS_H
#define CATS_BUTTERFLIES_POPULATIONS_H
#include "data/cats_grid.h"
int64_t count_populated_cells(const struct cats_grid *grid);
void bf_cell_maturation(struct cats_grid *grid, struct cats_thread_info *ts, cats_dt_coord row, cats_dt_coord col, bool check_exclusion);
void bf_area_kill_adults(struct cats_grid *grid, struct cats_thread_info *ts);
cats_dt_rates bf_egg_to_adult_survival_rate(cats_dt_rates reproduction_rate, cats_dt_rates eggs);
cats_dt_rates bf_expected_local_eggs_per_female(struct cats_configuration *conf, struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);
int64_t butterflies_prune_invalid_cells(struct cats_grid *grid);
#endif //CATS_BUTTERFLIES_POPULATIONS_H
