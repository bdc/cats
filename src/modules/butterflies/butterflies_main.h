// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_main.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_BUTTERFLIES_MAIN_H
#define CATS_BUTTERFLIES_MAIN_H

#include "cats_global.h"
#include <stdint.h>
#include "data/cats_datatypes.h"
#include "vital_rates/vital_rates.h"
#include "butterflies_stats.h"


struct grid_data_butterflies {
    float **generations;
    int32_t generation_current;
    float **eggs;
    int32_t **info_layer;


};

enum butterfly_cell_info {
    BF_CELL_CLEAR = 0,
    BF_CELL_EXCLUDED = 1 << 1,
    BF_CELL_HABITAT_OK = 1 << 2,
    BF_CELL_RESOURCE_AVAILABLE = 1 << 3,
    BF_CELL_VALID_DISPERSAL_TARGET = 1 << 4


};

#define BF_DEBUG_ROW  4319
#define BF_DEBUG_COL 10502

struct conf_data_butterflies {

    // fixme -> move to grid data;

    int32_t generations_max;
    int32_t generations_min;
    int32_t animal_dispersal_max_radius; ///< maximal flight/dispersal distance
    cats_dt_rates probability_to_stay;
    bool debug_rw;
    FILE *debug_rw_file;
    char *debug_rw_filename;
    cats_dt_rates egg_fraction_source;
    cats_dt_rates egg_fraction_step;
    struct cats_vital_rate eggs_per_female;
    struct cats_vital_rate butterfly_egg_to_adult_survival;
    struct cats_vital_rate butterfly_generations;
    struct cats_vital_rate reproduction_rate;
    cats_dt_rates female_fraction;

    bool actions_added;

    int64_t stat_ids[BF_STAT_MAX];
    FILE *stats_file;


};

#endif //CATS_BUTTERFLIES_MAIN_H
