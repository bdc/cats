// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_overlays.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "paths/paths.h"
#include "butterflies_populations.h"
#include "modules/module_header.h"
#include "actions/cats_actions.h"

#include "butterflies_overlays.h"
#include "butterflies_main.h"
#include "inline_overlays.h"
#include "inline_population.h"


enum action_status bf_grid_overlay_update(const struct cats_configuration *conf, struct cats_grid *grid)
{
        if (conf->overlays.have_overlays == false) {
                return ACTION_NOT_RUN;
        }

        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;


        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;
        int64_t cells_habitat_ok = 0;
        int64_t cells_excluded = 0;
        int64_t cells_resource_ok = 0;
        int64_t cells_habitat_and_resource_ok = 0;
        int64_t cells_eggs_removed = 0;
        int64_t cells_adults_removed = 0;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {

                        data->info_layer[row][col] = BF_CELL_CLEAR;

                        if (cell_excluded_by_overlay(conf, row, col)) {
                                data->info_layer[row][col] |= BF_CELL_EXCLUDED;
                                if (data->eggs[row][col]) cells_eggs_removed += 1;
                                if (get_adult_population(grid, row, col)) cells_adults_removed += 1;
                                data->eggs[row][col] = 0;
                                set_population_ignore_cc(grid, row, col, 0);
                                cells_excluded += 1;
                                continue;
                        }


                        if (conf->overlays.overlay[OL_HABITAT_TYPE_CC].enabled &&
                            conf->overlays.habitat_cc->data[row][col] > 0) {
                                data->info_layer[row][col] |= BF_CELL_HABITAT_OK;
                                cells_habitat_ok += 1;
                        } else if (!conf->overlays.overlay[OL_HABITAT_TYPE_CC].enabled) {
                                data->info_layer[row][col] |= BF_CELL_HABITAT_OK;
                                cells_habitat_ok += 1;
                        }

                        if (conf->overlays.overlay[OL_RESOURCE].enabled &&
                            conf->overlays.resources->data[row][col] > 0) {
                                data->info_layer[row][col] |= BF_CELL_RESOURCE_AVAILABLE;
                                cells_resource_ok += 1;
                        } else if (!conf->overlays.overlay[OL_RESOURCE].enabled) {
                                data->info_layer[row][col] |= BF_CELL_RESOURCE_AVAILABLE;
                                cells_resource_ok += 1;
                        }

                        if ((data->info_layer[row][col] & BF_CELL_HABITAT_OK) &&
                            (data->info_layer[row][col] & BF_CELL_RESOURCE_AVAILABLE)) {
                                data->info_layer[row][col] |= BF_CELL_VALID_DISPERSAL_TARGET;
                                cells_habitat_and_resource_ok += 1;

                        } else {
                                if (data->eggs[row][col]) cells_eggs_removed += 1;
                                if (get_adult_population(grid, row, col)) cells_adults_removed += 1;
                                data->eggs[row][col] = 0;
                                set_population_ignore_cc(grid, row, col, 0);
                        }
                }
        }

        int64_t cells_with_eggs = 0;
        double total_eggs = 0;
        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        float eggs = data->eggs[row][col];
                        if (eggs > 0) {
                                cells_with_eggs += 1;
                                total_eggs += eggs;
                        }

                }
        }

        log_message(LOG_INFO,
                    "Overlay update: %ld excluded, %ld habitat ok, %ld resource ok, %ld habitat + resource ok",
                    cells_excluded, cells_habitat_ok, cells_resource_ok, cells_habitat_and_resource_ok);
        log_message(LOG_INFO, "Overlay update: %ld cells with eggs, total %f eggs, average %f eggs per cell",
                    cells_with_eggs, total_eggs, total_eggs / (float) cells_with_eggs);
        log_message(LOG_INFO, "Overlay update: %ld cells with eggs removed", cells_eggs_removed);
        log_message(LOG_INFO, "Overlay update: %ld cells with adults removed", cells_adults_removed);


        return ACTION_RUN;
}

