
// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_actions_setup.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "butterflies_actions_setup.h"
#include "configuration/configuration.h"
#include "actions/setup_actions.h"
#include "module.h"
#include "modules/module_header.h"
#include "butterflies_actions.h"
#include "butterflies_main.h"

void bf_add_generation_action(struct cats_configuration *conf, action_function function, const char *name,
                              int generation)
{
        char *result = NULL;
        int rc = asprintf(&result, "%s (generation %d)", name, generation);
        asprintf_check(rc);
        append_action(conf, function, ALL_STAGES, result, module_name);
        free(result);
}

void bf_register_actions(struct cats_configuration *conf)
{

        register_action_function(conf, bf_action_stats_reset, "butterfly_action_reset_stats",
                                 "resetting butterfly statistics");
        register_action_function(conf, bf_action_generation_update, "butterfly_action_update_generations",
                                 "update generations");

        register_action_function(conf, bf_action_generation_finish, "bf_action_generation_finish",
                                 "update generation");
        register_action_function(conf, bf_action_generation_start, "bf_action_generation_start",
                                 "start generation");


        register_action_function(conf, bf_action_maturation, "butterfly_action_egg_to_adult",
                                 "transition eggs to adults");

        register_action_function(conf, bf_action_overlay_update, "butterfly_action_overlay_update",
                                 "updating overlays");

        register_action_function(conf, bf_action_dispersal, "butterfly_action_dispersal", "egg dispersal");
        register_action_function(conf, bf_action_save_grid, "bf_action_save_grid", "output");
        register_action_function(conf, bf_action_save_eggs_grid, "bf_action_save_egg_grid", "egg output");
        register_action_function(conf, bf_action_save_overlay, "bf_action_save_overlay_grid", "overlay output");
        register_action_function(conf, bf_action_stats_gather, "bf_action_stats_gather", "gather stats");
        register_action_function(conf, bf_action_stats_reset, "bf_action_stats_reset", "reset stats");
        register_action_function(conf, bf_action_stats_write, "bf_action_stats_write", "write stats");
}


void bf_add_actions(struct cats_configuration *conf)
{
        struct conf_data_butterflies *data = CATS_MODULE_DATA;

        log_message(LOG_INFO, "Adding actions of %d generations", data->generations_max);


        append_action(conf, bf_action_stats_reset, ALL_STAGES, "resetting butterfly statistics", module_name);
        append_action_by_name(conf, "action_load_environments", ALL_STAGES, "environment update", module_name);
        append_action_by_name(conf, "action_overlay_update", ALL_STAGES, "overlay update", module_name);

        append_action(conf, bf_action_overlay_update, ALL_STAGES, "updating resource layer", module_name);
        append_action(conf, bf_action_generation_update, ALL_STAGES, "update generations", module_name);
        append_action(conf, bf_action_save_overlay, ALL_STAGES, "save overlay", module_name);

        for (int32_t generation = data->generations_max; generation > 0; generation--) {

                bf_add_generation_action(conf, bf_action_stats_reset, "reset stats", generation);
                bf_add_generation_action(conf, bf_action_generation_start, "start generation", generation);
                bf_add_generation_action(conf, bf_action_maturation, "transition eggs to adults",
                                         generation);
                bf_add_generation_action(conf, bf_action_save_grid, "output adults", generation);
                bf_add_generation_action(conf, bf_action_stats_gather, "gather stats", generation);
                bf_add_generation_action(conf, bf_action_dispersal, "dispersal", generation);
                bf_add_generation_action(conf, bf_action_save_eggs_grid, "output eggs", generation);
                bf_add_generation_action(conf, bf_action_stats_write, "write stats", generation);
                bf_add_generation_action(conf, bf_action_generation_finish, "finish generation", generation);

        }
}