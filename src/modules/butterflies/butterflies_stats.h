
// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_stats.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_BUTTERFLIES_STATS_H
#define CATS_BUTTERFLIES_STATS_H

#include <assert.h>
#include "stats/statistics.h"
#include "threading/threading.h"

enum butterfly_stats {
    BF_STAT_MIN = 0,
    BF_STAT_POPULATED_FIT = 0,
    BF_STAT_POPULATED_UNFIT,
    BF_STAT_UNPOPULATED_FIT,
    BF_STAT_UNPOPULATED_UNFIT,
    BF_STAT_EXCLUDED,
    BF_RANDOM_WALK_DEPOSIT_COUNT,
    BF_RANDOM_WALK_COUNT,
    BF_RANDOM_WALK_STEP_COUNT,
    BF_OUTPUT_STAT_MAX,
    BF_CELLS_WITH_EGGS,
    BF_CELLS_WITH_EGGS_REMOVED,
    BF_STAT_MAX
};

const char *bf_get_stats_field_name(enum butterfly_stats which);
void bf_area_stats_gather(struct cats_grid *grid, struct cats_thread_info *ts);
void bf_stats_write(struct cats_configuration *conf, struct cats_grid *grid);

static inline void increase_custom_stat(struct statistics *stats, int64_t stat_id, int64_t by)
{
        assert(stats != NULL);
        assert(stat_id >= 0 && stat_id < stats->custom_stat_count);
        stats->custom_stats[stat_id] += by;
}



#endif //CATS_BUTTERFLIES_STATS_H
