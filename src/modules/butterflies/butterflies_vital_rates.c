// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_vital_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "butterflies_vital_rates.h"

void bf_add_vital_rates(struct cats_configuration *conf, struct conf_data_butterflies *data)
{
        // number of generations
        register_module_vital_rate(conf, &data->butterfly_generations,"butterflies generations");
        set_vital_rate_name(&data->butterfly_generations, "butterflies generations");
        set_vital_rate_suitability_cutoff_hint(&data->butterfly_generations, HYBRID_SUIT_TS_ZT);
        set_vital_rate_link_hybrid_function(&data->butterfly_generations, conf, LINK_SUITABILITY_SIGMOID);
        set_vital_density(&data->butterfly_generations, NO_DENSITY_DEP);
        set_vital_rate_minimum(&data->butterfly_generations, 1.0);



        // adult to eggs
        register_module_vital_rate(conf, &data->eggs_per_female, "butterflies eggs per female");
        set_vital_rate_name(&data->eggs_per_female, "butterflies eggs per female");
        set_vital_rate_suitability_cutoff_hint(&data->eggs_per_female, HYBRID_SUIT_TS_ZT);
        set_vital_rate_link_hybrid_function(&data->eggs_per_female, conf, LINK_CONSTANT);

        // eggs to adults
        register_module_vital_rate(conf, &data->reproduction_rate, "butterflies reproduction rate");
        set_vital_rate_name(&data->reproduction_rate, "butterflies reproduction rate");
        set_vital_rate_link_hybrid_function(&data->reproduction_rate, conf, LINK_SUITABILITY_SIGMOID);
        set_vital_rate_suitability_cutoff_hint(&data->reproduction_rate, HYBRID_SUIT_TS_ZT);
}
