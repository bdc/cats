// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_generations.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "butterflies_populations.h"
#include "inline_overlays.h"
#include "butterflies_main.h"
#include "modules/module_header.h"
#include "actions/cats_actions.h"
#include "butterflies_generations.h"
#include "inline.h"

// only run at the start of the year
void bf_area_generation_update(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;

        struct cats_vital_rate *rate = &module_conf->butterfly_generations;

        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;
        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;

        struct statistics *stats = &ts->stats[grid->id];
        const cats_dt_rates suit_ts = module_conf->butterfly_generations.suitability_cutoff;

        const int64_t egg_cells_id = module_conf->stat_ids[BF_CELLS_WITH_EGGS];
        const int64_t egg_cells_removed_id = module_conf->stat_ids[BF_CELLS_WITH_EGGS];
        int64_t cells_with_eggs = 0;
        int64_t cells_with_eggs_removed = 0;

        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {

                        if (get_suitability(grid, row, col) < suit_ts)
                        {
                                if (data->eggs[row][col] > 0) cells_with_eggs_removed += 1;
                                data->eggs[row][col] = 0.0f;
                                data->generations[row][col] = 0.0f;
                                set_population_ignore_cc(grid, row, col, 0);
                                continue;
                        }

                        if (cell_excluded_by_overlay(conf, row, col)) {
                                if (data->eggs[row][col] > 0) cells_with_eggs_removed += 1;
                                data->eggs[row][col] = 0.0f;
                                data->generations[row][col] = 0.0f;
                                set_population_ignore_cc(grid, row, col, 0);
                                continue;
                        }

                        if ( ! (data->info_layer[row][col] & BF_CELL_VALID_DISPERSAL_TARGET)){
                                if (data->eggs[row][col] > 0) cells_with_eggs_removed += 1;
                                data->eggs[row][col] = 0.0f;
                                data->generations[row][col] = 0.0f;
                                set_population_ignore_cc(grid, row, col, 0);
                                continue;
                        }

                        cats_dt_rates gen = calculate_rate(rate, 0, conf->param, grid, row, col, NULL);

                        data->generations[row][col] = (float) gen;
                        if (data->eggs[row][col] > 0) cells_with_eggs += 1;

                }
        }

        stats->custom_stats[egg_cells_id] += cells_with_eggs;
        stats->custom_stats[egg_cells_removed_id] += cells_with_eggs_removed;


}