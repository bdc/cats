// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_paths.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "butterflies_generations.h"
#include "paths/paths.h"
#include "paths/output_paths.h"
#include "butterflies_populations.h"
#include "inline_overlays.h"
#include "butterflies_main.h"
#include "butterflies_actions.h"
#include "modules/module_header.h"
#include "actions/cats_actions.h"
#include "butterflies_paths.h"
#include "module.h"

void bf_add_directories(struct cats_configuration *conf)
{
        add_module_output_directory(conf, "butterfly-adults");
        if (conf->output.write_all) {
                add_module_output_directory(conf,"butterfly-eggs");
                add_module_output_directory(conf,"butterfly-overlay");
        }

}

char *bf_population_filename(struct cats_configuration *conf, struct cats_grid *grid)
{
        assert(grid != NULL);
        assert(conf != NULL);
        assert(conf->grid_count == 1);
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        int32_t max_generation = module_conf->generations_max;
        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;


        struct string_array *path = get_output_directory(conf, "butterfly-adults"); // FIXME MAKE DIRECTORY

        char *extension = get_extension(conf, "adults");
        struct string_array *name = standard_output_file_name(conf, NULL, NULL, NULL);
        string_array_add_int(name, max_generation - data->generation_current, "g%03d");

        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}

char *bf_population_eggs_filename(struct cats_configuration *conf, struct cats_grid *grid)
{
        assert(grid != NULL);
        assert(conf != NULL);
        assert(conf->grid_count == 1);
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        int32_t max_generation = module_conf->generations_max;
        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;


        struct string_array *path = get_output_directory(conf, "butterfly-eggs"); // FIXME MAKE DIRECTORY

        char *extension = get_extension(conf, "eggs");
        struct string_array *name = standard_output_file_name(conf, NULL, NULL, NULL);
        string_array_add_int(name, max_generation - data->generation_current, "g%03d");

        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}


char *bf_population_overlay_filename(struct cats_configuration *conf, struct cats_grid *grid)
{
        assert(grid != NULL);
        assert(conf != NULL);
        assert(conf->grid_count == 1);


        struct string_array *path = get_output_directory(conf, "butterfly-overlay"); // FIXME MAKE DIRECTORY

        char *extension = get_extension(conf, "overlay");
        struct string_array *name = standard_output_file_name(conf, NULL, NULL, NULL);
        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}


char *bf_stats_filename(struct cats_configuration *conf, struct cats_grid *grid)
{
        struct string_array *path = get_output_directory(conf, "stats");

        struct string_array *name = new_string_array();
        string_array_add(name, module_name);
        string_array_add(name, conf->run_name);
        string_array_add_int(name, conf->simulation.replicate, "r%03d");
        char *filename = assemble_filename(path, name, "_", "csv");
        free_string_array(&path);
        free_string_array(&name);

        return filename;
}