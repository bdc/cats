// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_scale.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

//
// Created by andreas on 03/07/23.
//

#ifndef CATS_BUTTERFLIES_SCALE_H
#define CATS_BUTTERFLIES_SCALE_H

#include <stdbool.h>
#include <stdint.h>
#include "configuration/configuration.h"
#include "lambda/leslie_matrix.h"

double *bf_leslie_matrix(struct cats_configuration *conf, struct lambda_parameters *l_param,
                         bool silent, int32_t *N_out);

#endif //CATS_BUTTERFLIES_SCALE_H
