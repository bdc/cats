// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_scale.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

//
// Created by andreas on 03/07/23.
//
#include "modules/module_header.h"
#include "butterflies_main.h"
#include "butterflies_scale.h"
#include "butterflies_populations.h"


double *bf_leslie_matrix(struct cats_configuration *conf, struct lambda_parameters *l_param,
                         bool silent, int32_t *N_out){
        bool print_rate = !silent;

        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        cats_dt_rates eggs_per_female = calculate_rate_for_matrix(&module_conf->eggs_per_female, l_param, print_rate);
        cats_dt_rates reproduction_rate = calculate_rate_for_matrix(&module_conf->reproduction_rate, l_param, print_rate);
        cats_dt_rates stationary = module_conf->probability_to_stay;
        cats_dt_rates mobile = 1.0 - stationary;
        cats_dt_rates egg_fraction_source = module_conf->egg_fraction_source;

        cats_dt_rates local_eggs =  (stationary + mobile * egg_fraction_source ) * eggs_per_female;

        // female -> female
        // to achieve the target reproduction rate, the number of eggs per female laid in the cell
        // that survive and become adult has to be the reproduction rate divided by the female fraction divided by the number of eggs

        cats_dt_rates eggs_to_adults_rate = bf_egg_to_adult_survival_rate(reproduction_rate, local_eggs) ;
        cats_dt_rates result =  local_eggs * eggs_to_adults_rate;

        printf("scale %Lf: eggs %Lf, eggs to adults %Lf\n", conf->param->scale_factor, eggs_per_female, eggs_to_adults_rate);
        double *matrix = calloc_or_die(1, sizeof(double));

        *matrix = (double) result;
        *N_out = 1;
        return matrix;

}