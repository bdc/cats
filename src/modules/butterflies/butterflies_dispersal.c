// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_dispersal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "inline_population.h"
#include "butterflies_inline.h"
#include "misc/cats_random.h"
#include "populations/carrying_capacity.h"
#include "inline_overlays.h"

#include <math.h>
#include <gsl/gsl_randist.h>
#include "butterflies_main.h"
#include "butterflies_dispersal.h"
#include "configuration/configuration.h"
#include "modules/module_header.h"

const int N_DIRECTIONS = 9;
/*
 *  directions
 *   4  3  2
 *   5  0  1
 *   6  7  8
 */

const cats_dt_coord DIRECTION_OFFSETS[9][2] = {
        {+0, +0},
        {+0, +1},
        {-1, +1},
        {-1, +0},
        {-1, -1},
        {+0, -1},
        {+1, -1},
        {+1, +0},
        {+1, +1}
};


static void inline single_random_walk(struct cats_thread_info *ts, struct cats_grid *grid, cats_dt_coord source_row,
                                      cats_dt_coord source_col, int32_t eggs, cats_dt_rates egg_fraction_step,
                                      __attribute__((unused)) int32_t rw_num)
{

        const int module_id = CATS_MODULE_ID;
        const struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        const struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        const bool debug_rw = module_conf->debug_rw;
        const int64_t stat_id_deposits = module_conf->stat_ids[BF_RANDOM_WALK_DEPOSIT_COUNT];
        const int64_t stat_id_rw_steps = module_conf->stat_ids[BF_RANDOM_WALK_STEP_COUNT];

        int32_t eggs_left = eggs;
        const cats_dt_coord max_steps = module_conf->animal_dispersal_max_radius;


        cats_dt_coord row = source_row;
        cats_dt_coord col = source_col;

        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;


        for (cats_dt_coord step = 0; step < max_steps; step++) {
                const unsigned long int direction = gsl_rng_uniform_int(ts->rng, N_DIRECTIONS);


                const cats_dt_coord *offsets = DIRECTION_OFFSETS[direction];
                const cats_dt_coord row_offset = offsets[0];
                const cats_dt_coord col_offset = offsets[1];

                assert(row_offset >= -1 && row_offset <= 1);
                assert(col_offset >= -1 && col_offset <= 1);

                row += row_offset;
                col += col_offset;

                if (row >= rows || row < 0 || col >= cols || col < 0) {
                        return; // we escaped the simulation extent and got lost
                }

                increase_custom_stat(ts->stats, stat_id_rw_steps, 1);


                // is the cell a valid dispersal target location?
                if (!(data->info_layer[row][col] & BF_CELL_VALID_DISPERSAL_TARGET)) {
                        if (debug_rw) {
                                fprintf(module_conf->debug_rw_file, "%d,%d,%d,%d,%d,%d,%d\n", rw_num, row, col,
                                        step + 1,
                                        module_conf->animal_dispersal_max_radius - step - 1, 0, eggs_left);
                        }

                        continue;
                }


                int32_t eggs_to_deposit = (int32_t) ceilf((float) (eggs_left * egg_fraction_step));
                assert(eggs_to_deposit >= 0);
                //printf("x %d %d -> %d %d: %d of %d / %Lf\n", source_row, source_col, row, col, eggs_to_deposit, eggs, egg_fraction_step);
                if (eggs_to_deposit > eggs_left) {
                        eggs_to_deposit = eggs_left;
                }

                eggs_left -= eggs_to_deposit;
                data->eggs[row][col] += (float) eggs_to_deposit;
                increase_custom_stat(ts->stats, stat_id_deposits, 1);
                if (debug_rw) {
                        fprintf(module_conf->debug_rw_file, "%d,%d,%d,%d,%d,%d,%d\n", rw_num, row, col, step + 1,
                                module_conf->animal_dispersal_max_radius - step - 1, eggs_to_deposit, eggs_left);
                }


                if (eggs_left == 0) break;

        }

        assert(eggs_left >= 0);
}






void
butterflies_cell_dispersal(struct cats_grid *grid, struct cats_thread_info *ts, cats_dt_coord row, cats_dt_coord col,
                           bool check_exclusion, bool local_only)
{
        const struct cats_configuration *conf = ts->conf;
        if (check_exclusion
            && (cell_excluded_by_overlay(conf, row, col) || bf_cell_excluded_by_generation(grid, row, col)))
                return;


        // total adults: the number of adults that became adult in this cell, possibly exceeding the carrying capacity (thanks to poisson processes)
        const cats_dt_population total_adults = get_adult_population(grid, row, col);

        if (total_adults == 0) return;
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        // total females: how many of the total adults are female, as drawn from a binomial distribution with p = 0.5
        // can be safely cast, because gsl_ran_binomial will return a number <= total_adults
        const cats_dt_population total_females = (cats_dt_population) gsl_ran_binomial(ts->rng,
                                                                                       (double) module_conf->female_fraction,
                                                                                       total_adults);
        assert(total_females >= 0 && total_females <= total_adults);

        // total males: the rest
        const cats_dt_population total_males = total_adults - total_females;

        // we need at least one female and one male
        if (total_females == 0 || total_males == 0) return;

        // here we calculate the number of eggs per female, so we can return early if the cell is unsuitable

        cats_dt_rates eggs_per_f = calculate_rate(&module_conf->eggs_per_female, total_adults, &grid->param, grid, row,
                                                  col, NULL);
        int32_t eggs_per_female = (int32_t) ceill(eggs_per_f);
        assert(eggs_per_female >= 0);

        if (eggs_per_female == 0) return;


        // how many females will leave the cell
        // wandering_females: the number of females that will leave the cell and do a random walk
        cats_dt_rates probability_to_leave = (1.0 -  module_conf->probability_to_stay);
        cats_dt_population wandering_females = poisson_population_capped(ts->rng,
                                                                         total_females * probability_to_leave,
                                                                         total_females);

        // stationary_females: the number of females that will not leave the cell, and leave all their eggs here
        cats_dt_population stationary_females = total_females - wandering_females;
        assert(stationary_females >= 0);
        assert(wandering_females >= 0);

        const int64_t stat_id_rw = module_conf->stat_ids[BF_RANDOM_WALK_COUNT];
        const int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        const bool debug_rw = module_conf->debug_rw;

        int32_t eggs_to_disperse_per_female = (int32_t) ceill(
                eggs_per_female * (1.0 - module_conf->egg_fraction_source));

        // all the eggs of all the stationary females + the eggs the wandering females leave in the source cell
        const cats_dt_rates source_cell_eggs = stationary_females * eggs_per_female +
                                               wandering_females * (eggs_per_female - eggs_to_disperse_per_female);

        data->eggs[row][col] += (float) ceill(source_cell_eggs);


        if (eggs_to_disperse_per_female == 0) {
                log_message(LOG_ERROR, "%s: random walk with no eggs to distribute in cell %d %d", __func__, row, col);
                return;
        }

        const cats_dt_rates egg_fraction_step = module_conf->egg_fraction_step;



        if (local_only) {
                return;
        }

        if (debug_rw) {
                fprintf(module_conf->debug_rw_file, "# total source cell eggs = %d\n", (int) ceill(source_cell_eggs));
                fprintf(module_conf->debug_rw_file, "# stationary females = %d\n", stationary_females);
                fprintf(module_conf->debug_rw_file, "# wandering females = %d\n", wandering_females);
                fprintf(module_conf->debug_rw_file, "# total females = %d\n", wandering_females + stationary_females);
                fprintf(module_conf->debug_rw_file, "# wandering females source cell eggs = %d\n",
                        (int) (wandering_females * (eggs_per_female - eggs_to_disperse_per_female)));
                fprintf(module_conf->debug_rw_file, "# stationary females source cell eggs = %d\n",
                        (int) (stationary_females * eggs_per_female));
                fprintf(module_conf->debug_rw_file, "id,row,col,step,steps left,eggs deposited,eggs left\n");

        }

        for (int32_t rw_number = 0; rw_number < wandering_females; rw_number++) {
                if (debug_rw) {

                        fprintf(module_conf->debug_rw_file, "%d,%d,%d,0,%d,%d,%d\n", rw_number, row, col,
                                module_conf->animal_dispersal_max_radius,
                                (int) ceill(eggs_per_female * module_conf->egg_fraction_source),
                                (int) ceill(eggs_to_disperse_per_female));

                }

                single_random_walk(ts, grid, row, col, eggs_to_disperse_per_female, egg_fraction_step, rw_number);
                increase_custom_stat(ts->stats, stat_id_rw, 1);

        }
        if (debug_rw) {
                fflush(module_conf->debug_rw_file);
                fclose(module_conf->debug_rw_file);
                log_message(LOG_IMPORTANT, "Ending simulation early - first cell with random walks complete");
                exit_cats(EXIT_SUCCESS);
        }
}