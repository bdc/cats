
// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_initial_population.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "lambda/leslie_matrix.h"
#include "inline.h"

#include "butterflies_dispersal.h"
#include "butterflies_generations.h"
#include "populations/population.h"
#include "paths/paths.h"

#include "butterflies_populations.h"

#include "butterflies_main.h"
#include "butterflies_actions.h"
#include "modules/module_header.h"
#include "actions/cats_actions.h"
#include "butterflies_initial_population.h"


void bf_initial_population_adjustment(struct cats_configuration *conf, struct cats_grid *grid)
{
        int64_t init_populated_cells = count_populated_cells(grid);
        int64_t invalid_resources = butterflies_prune_invalid_cells(grid);
        log_message(LOG_IMPORTANT, "Loaded initial populations: %ld cells occupied before adjustment",
                    init_populated_cells);
        if (grid->param.initial_population.set_to_cc == true) {

//#define DEBUG_INITIAL_POPULATIONS 1
#ifdef DEBUG_INITIAL_POPULATIONS
                const int32_t max_i = 10;
                struct cats_vital_rate *cc_rate = &grid->param.carrying_capacity;
                cats_dt_rates OT =  grid->param.OT;
                for (int32_t i = 0; i < max_i; i++) {

                        cats_dt_rates suit = OT + i * (1.0 - OT)/max_i;
                        cats_dt_rates cc = cc_rate->func->func(cc_rate, &grid->param, suit, 0, NAN);
                        log_message(LOG_INFO, "Carrying capacity for suitability %Lf: %Lf", suit, cc);

                }
                const cats_dt_coord rows = grid->dimension.rows;
                const cats_dt_coord cols = grid->dimension.cols;
                int64_t start = 0;
                int64_t multi_excluded = 0;

                for (cats_dt_coord row = 0; row < rows; row++) {
                        for (cats_dt_coord col = 0; col < cols; col++) {

                                if (get_adult_population(grid, row, col) == 0) continue;
                                start += 1;
                                cats_dt_rates multiplier = 1.0;
                                if (conf->overlays.overlay[OL_HABITAT_TYPE_CC].enabled) {
                                        multiplier *= conf->overlays.habitat_cc->data[row][col];
                                }
                                cats_dt_rates suit = get_suitability(grid, row, col);
                                cats_dt_rates cc_raw = cc_rate->func->func(cc_rate, &grid->param, suit, 0, NAN);
                                if (multiplier == 0) {
                                    multi_excluded += 1;
                                }
                                cats_dt_population cc = get_carrying_capacity(grid, row, col);

                                printf("DEBUG::row %d col %d, suit %Lf, cc multi %Lf, cc raw %Lf, cc %d\n", row, col, suit, multiplier, cc_raw, cc);
                        }
                }
#endif

                increase_initial_population_to_cc(grid, conf);
                int64_t populated_cells_after_cc = count_populated_cells(grid);
                log_message(LOG_IMPORTANT, "\t%ld cells occupied after setting population sizes to carrying capacity",
                            populated_cells_after_cc);
        }


        if (grid->param.initial_population.suitability_threshold > 0.0) {
                prune_initial_population_under_threshold(conf, grid);
        }

        int64_t after_populated_cells = count_populated_cells(grid);

        log_message(LOG_IMPORTANT, "Pruned initial populations from %ld cells to %ld, (%ld wrong habitat)",
                    init_populated_cells, after_populated_cells, invalid_resources);


        cats_dt_population rows = grid->dimension.rows;
        cats_dt_population cols = grid->dimension.cols;

        threaded_action(&bf_initial_population_to_eggs, grid, conf, TS_DISPERSAL);

#ifdef BF_DEBUG
        int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
#endif


        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
#ifdef BF_DEBUG
                        if (get_adult_population(grid, row, col) || data->eggs[row][col]) {
                                printf(""
                                       "BFDBG::%s::initial adults -> eggs::%d,%d,%d,%f\n",
                                       __func__, row, col, get_adult_population(grid, row, col),
                                       data->eggs[row][col]);
                        }
#endif
                        set_population_ignore_cc(grid, row, col, 0);
                }
        }


        grid->param.initial_population.adjusted = true;
}