// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_main.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include "modules/module_header.h"
#include "butterflies_main.h"
#include "configuration/load_configuration_species_params.h"
#include "module.h"
#include "actions/cats_actions.h"
#include "butterflies_actions.h"
#include "butterflies_vital_rates.h"
#include "cats_ini/cats_ini.h"
#include "butterflies_actions_setup.h"
#include "paths/output_paths.h"
#include "paths/directory_helper.h"
#include "butterflies_populations.h"
#include "butterflies_paths.h"
struct cats_global global;
struct cats_debug_options cats_debug;

#include "lambda/leslie_matrix.h"
#include "actions/setup_actions.h"
#include "butterflies_scale.h"


cats_dt_rates bf_expected_local_eggs_per_female(struct cats_configuration *conf, struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        cats_dt_rates stationary = module_conf->probability_to_stay;
        cats_dt_rates mobile = 1.0 - stationary;
        cats_dt_rates egg_fraction_source = module_conf->egg_fraction_source;
        cats_dt_rates eggs_per_female = calculate_rate(&module_conf->eggs_per_female, NAN, &grid->param, grid, row, col, NULL);
        cats_dt_rates local_eggs =  (stationary + mobile * egg_fraction_source ) * eggs_per_female;
        return local_eggs;
}



void *butterfly_grid_init(__attribute__((unused)) struct cats_configuration *conf, struct cats_grid *grid,
                          __attribute__((unused)) void *ignored)
{
        log_message(LOG_INFO, "%s: %s: grid init of grid %d with %d rows and %d cols",
                    module_name, __func__, grid->id, grid->dimension.cols, grid->dimension.rows);


        struct grid_data_butterflies *data = malloc_or_die(sizeof(struct grid_data_butterflies));
        log_message(LOG_INFO, "allocating data for generations");
        data->generations = new_raw_2d_array_from_dimension(grid->dimension, sizeof(float));
        data->info_layer = new_raw_2d_array_from_dimension(grid->dimension, sizeof(int32_t));
        log_message(LOG_INFO, "done allocating data for generations");
        data->generation_current = 0;

        //struct conf_data_butterflies *conf_data = CATS_MODULE_DATA;
        data->eggs = new_raw_2d_array_from_dimension(grid->dimension, sizeof(float));

        if (grid->param.parametrization != PARAM_HYBRID) {
                log_message(LOG_ERROR, "%s only works with hybrid parametrisation mode", module_name);
                exit_cats(EXIT_FAILURE);

        }

        return data;
}


void *
butterfly_grid_cleanup(__attribute__((unused)) struct cats_configuration *conf, struct cats_grid *grid, void *data)
{
        log_message(LOG_INFO, "%s: grid cleanup", module_name);
        assert(grid != NULL);
        struct grid_data_butterflies *grid_data = data;
        free_grid(&grid_data->generations, grid->dimension.rows);
        free(grid_data->generations);
        grid_data->generations = NULL;
        free_grid(&grid_data->info_layer, grid->dimension.rows);
        free(grid_data->info_layer);
        grid_data->info_layer = NULL;
        grid_data->generation_current = -1;
        return NULL;
}


void load_butterflies_species_params(struct cats_configuration *conf, struct cats_ini *ini, const char *section_name,
                                     struct cats_species_param *param)
{
        struct conf_data_butterflies *data = CATS_MODULE_DATA;

        load_conf_vital_rate(&data->eggs_per_female, conf, ini, section_name, param);
        load_conf_vital_rate(&data->butterfly_generations, conf, ini, section_name, param);
        load_conf_vital_rate(&data->reproduction_rate, conf, ini, section_name, param);


        //load_conf_vital_rate(&data->butterfly_egg_to_adult_survival, conf, ini, section_name, param);

        load_conf_value(true, ini, section_name, "butterflies random walk steps maximum",
                        &data->animal_dispersal_max_radius);
        load_conf_value(true, ini, section_name, "butterflies egg fraction source", &data->egg_fraction_source);
        bool l = load_conf_value(false, ini, section_name, "butterflies egg fraction step", &data->egg_fraction_step);

        if (!l) {
                data->egg_fraction_step = 0.5;
                log_message(LOG_INFO, "using default value for butterflies egg fraction step: %Lf\n",
                            data->egg_fraction_step);
        }

        l = load_conf_value(false, ini, section_name, "butterflies female fraction", &data->female_fraction);

        if (!l) {
                data->female_fraction = 0.5;
                log_message(LOG_INFO, "using default value for butterflies female fraction: %Lf\n",
                            data->female_fraction);
        }
        if (data->female_fraction <= 0.0 || data->female_fraction >= 1.0) {
                log_message(LOG_ERROR, "butterflies female fraction must be in (0, 1), is %Lf", data->female_fraction);
                exit_cats(EXIT_FAILURE);
        }


        if (data->egg_fraction_step <= 0.0 || data->egg_fraction_step > 1.0) {
                log_message(LOG_ERROR, "butterflies egg fraction step must be in (0, 1], is %Lf",
                            data->egg_fraction_step);
                exit_cats(EXIT_FAILURE);
        }

        if (data->animal_dispersal_max_radius <= 0) {
                log_message(LOG_ERROR, "butterflies random walk steps maximum must be > 0, is %d",
                            data->animal_dispersal_max_radius);
                exit_cats(EXIT_FAILURE);
        }

        data->debug_rw = false;
        data->debug_rw_file = NULL;
        data->debug_rw_filename = NULL;
        load_conf_value(true, ini, section_name, "butterflies probability to stay", &data->probability_to_stay);
        load_conf_value(false, ini, section_name, "butterflies debug random walks", &data->debug_rw);

        if (data->debug_rw) {
                struct string_array *path = get_output_directory(conf, "debug");
                check_and_create_directory_if_needed(path);
                struct string_array *name = new_string_array_init("rw-debug");
                string_array_add(name, conf->run_name);
                data->debug_rw_filename = assemble_filename(path, name, "_", "csv");
                data->debug_rw_file = fopen(data->debug_rw_filename, "w");
                ENSURE_FILE_OPENED(data->debug_rw_file, data->debug_rw_filename);
                free_string_array(&path);
                free_string_array(&name);

                log_message(LOG_IMPORTANT, "Random walk debugging is enabled, reducing number of threads to one");
                conf->param_max_threads = 1;
        }

        if (data->probability_to_stay < 0.0 || data->probability_to_stay > 1.0) {
                log_message(LOG_ERROR, "butterflies probability to stay has to be in range [0, 1]");
                exit_cats(EXIT_FAILURE);
        }

        data->generations_max = (int32_t) ceill(data->butterfly_generations.max_rate);
        data->generations_min = (int32_t) ceill(data->butterfly_generations.min_rate);
        param->plant_dispersal_max_radius = data->animal_dispersal_max_radius;
        param->max_adult_cc_fraction = 1.0;


        if (!data->actions_added) {
                bf_register_actions(conf);
                list_actions_full(conf);
                log_message(LOG_EMPTY, " ");
                bf_add_actions(conf);
                log_message(LOG_EMPTY, " ");

                data->actions_added = true;


        }

}


int cats_module_init(struct cats_configuration *conf)
{
        struct conf_data_butterflies *data = calloc_or_die(1, sizeof(struct conf_data_butterflies));
        enum cats_module_flags flags = MODULE_ALTERNATE_DEMOGRAPHIC | MODULE_OVERRIDE_ACTIONS;
        int32_t id = register_module(conf, module_name, data, flags);
        register_cats_grid_init_function(conf, butterfly_grid_init, butterfly_grid_cleanup);
        register_load_species_param_config_func(conf, load_butterflies_species_params);
        register_create_leslie_matrix_func(conf, bf_leslie_matrix);
        bf_add_vital_rates(conf, data);

        log_message(LOG_INFO, "Hello from '%s' (id: %d)", module_name, id);


        for (enum butterfly_stats which = BF_STAT_MIN; which < BF_STAT_MAX; which++) {
                data->stat_ids[which] = add_custom_stat(&conf->stats_registry, bf_get_stats_field_name(which));
        }

        bf_add_directories(conf);
        return id;
}
