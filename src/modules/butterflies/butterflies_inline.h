// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_inline.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_BUTTERFLIES_INLINE_H
#define CATS_BUTTERFLIES_INLINE_H

#include <math.h>
#include <stdbool.h>

#include "butterflies_main.h"
#include "modules/module_header.h"


static inline bool bf_cell_excluded_by_generation(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        const int module_id = CATS_MODULE_ID;
        const struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        if (data->generation_current == 0.0 || data->generation_current > (int32_t) ceilf(data->generations[row][col])) return true;
        return false;
}

#endif //CATS_BUTTERFLIES_INLINE_H
