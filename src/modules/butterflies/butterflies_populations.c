// SPDX-License-Identifier: GPL-3.0-or-later
//
// butterflies_populations.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <math.h>

#include "paths/paths.h"
#include "paths/output_paths.h"
#include "actions/cats_actions.h"
#include "misc/cats_random.h"
#include "data/cats_grid.h"
#include "threading/threading-helpers.h"
#include "modules/module_header.h"

#include "butterflies_populations.h"
#include "butterflies_main.h"
#include "butterflies_inline.h"
#include "inline.h"
#include "populations/population.h"

int64_t count_populated_cells(const struct cats_grid *grid)
{
        int64_t cells = 0;
        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        if (get_adult_population(grid, row, col)) {
                                cells += 1;
                        }
                }
        }

        return cells;
}


int64_t butterflies_prune_invalid_cells(struct cats_grid *grid)
{
        int64_t invalid_habitat = 0;
        const int module_id = CATS_MODULE_ID;
        const struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;
        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {

                        if (!(data->info_layer[row][col] & BF_CELL_VALID_DISPERSAL_TARGET)) {
                                if(get_adult_population(grid, row, col)) {
                                        invalid_habitat += 1;
                                }
                                set_population_ignore_cc(grid, row, col, 0);
                        }
                }
        }

        return invalid_habitat;
}


cats_dt_rates
bf_egg_to_adult_survival_rate(cats_dt_rates reproduction_rate, cats_dt_rates eggs)
{
        return reproduction_rate / eggs;
}

float get_fractional_generation(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;
        const int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;

        float this_generation_fraction = 1.0f;

        float local_generation = data->generations[row][col];
        int32_t global_max_generation = module_conf->generations_max;
        int32_t local_max_generation = (int32_t) ceilf(local_generation);
        int32_t global_current_generation = data->generation_current;
        float current_generation_float = (float) data->generation_current;

        if (global_current_generation == global_max_generation) {
                this_generation_fraction = 1.0f;
        } else if (current_generation_float == local_generation
                   || data->generation_current == module_conf->generations_min) {
                this_generation_fraction = 1.0f;
        } else if (data->generation_current == local_max_generation) {
                this_generation_fraction = (float) local_max_generation - local_generation;
        }

        assert(this_generation_fraction > 0);
        assert(this_generation_fraction <= 1.0);

        return this_generation_fraction;
}


void bf_cell_maturation(struct cats_grid *grid, struct cats_thread_info *ts, cats_dt_coord row, cats_dt_coord col,
                        bool check_exclusion)
{

        const struct cats_configuration *conf = ts->conf;
        assert (conf->grid_count == 1);
        if (check_exclusion) {
                if (bf_cell_excluded_by_generation(grid, row, col)) return;
        }


        const int module_id = CATS_MODULE_ID;
        struct grid_data_butterflies *data = grid->grid_modules[module_id].module_data;


        if (data->eggs[row][col] == 0) return;
        if (data->eggs[row][col] < 0) {
                log_message(LOG_ERROR, "Number of eggs < 0: row %d col %d: %f", row, col, data->eggs[row][col]);
                exit(EXIT_FAILURE);
        }


        // The number of generations per cell is usually not an integer value:
        // the non-integer part of the number of generations is used in the generation which is
        // one greater than the integer part of the number of generations.
        // i.e. if the number of generations is 2.4, 40% of all eggs in the cell
        // will be considered as if they belonged to generation 3
        // the minimum number of generations is 1 if the cell is climatically viable (and the host species is present)
        // if the number of generations is 0, the cell will be ignored
        //
        // note that the generations are processed backwards
        // i.e. if the maximum number of generations is 5, individual cells will have 1 ... 5 generations
        // first the all cells which have 5 (> 4) generations are modelled
        // then all the cells with have at least 4 (> 3) generations are modelled
        // ...
        // at last all cells with have at least 1 generation are modelled
        struct conf_data_butterflies *module_conf = CATS_MODULE_DATA;

        float this_generation_fraction = get_fractional_generation(grid, row, col);



        //cats_dt_population max_cc = (cats_dt_population) grid->param.carrying_capacity.max_rate;
        const float eggs = this_generation_fraction * data->eggs[row][col];
#ifdef BF_DEBUG
        printf("BFDBG::%s::row %d, col %d: local gen %f gen fraction %f, eggs %f, this gen eggs %f\n",
               __func__, row, col, local_generation, this_generation_fraction, data->eggs[row][col], eggs);
#endif
        if (eggs == 0) return;

        if (eggs > data->eggs[row][col]) {
                log_message(LOG_ERROR, "Removing more eggs than present: %d %d: %f/%f", row, col, data->eggs[row][col],
                            eggs);
                exit_cats(EXIT_FAILURE);
        }

        data->eggs[row][col] -= eggs;
        assert(data->eggs[row][col] >= 0);

        // not capped, we can have more adults than CC

        cats_dt_environment suit = get_suitability(grid, row, col);
        cats_dt_rates reproduction_rate = calculate_rate(&module_conf->reproduction_rate, NAN, &grid->param,
                                                         grid, row, col, NULL);
        cats_dt_rates suit_ts = module_conf->reproduction_rate.suitability_cutoff;
        if (suit_ts < suit_ts && reproduction_rate > 0) {
                log_message(LOG_ERROR, "Suitability %f under threshold %Lf, but adults per female = %Lf", suit, suit_ts,
                            reproduction_rate);
                exit_cats(EXIT_FAILURE);
        }

        //printf("row %d col %d: suitability: %f, eggs: %f, reproduction rate %Lf\n", row, col, suit, eggs, reproduction_rate);

        if (reproduction_rate == 0) {
                data->eggs[row][col] = 0;
                set_population_ignore_cc(grid, row, col, 0);
                return;
        }

        cats_dt_rates expected_eggs = bf_expected_local_eggs_per_female(ts->conf, grid, row, col);
        cats_dt_rates survival = bf_egg_to_adult_survival_rate(reproduction_rate, expected_eggs)/module_conf->female_fraction;
        cats_dt_population adults = poisson(ts->rng, eggs * survival);
        assert(adults >= 0);
        set_population(grid, row, col, adults);
#ifdef BF_DEBUG
        adults = get_adult_population(grid, row, col);
        printf("BFDBG::%s::row %d, col %d: year %d, suitability %f, OT %Lf\n", __func__, row, col, conf->time.year_current, suit, grid->param.OT);
        printf("BFDBG::%s::row %d, col %d: year %d, eggs %f, survival rate %Lf, reproduction rate %Lf, adults %d\n",
               __func__, row, col, conf->time.year_current, eggs, survival, reproduction_rate, adults);
        printf("XXXX,%d,%f,%d\n", conf->time.year_current, current_generation_float,adults);
#endif


}


void bf_area_kill_adults(struct cats_grid *grid, struct cats_thread_info *ts)
{
        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;
        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;

        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {
                        set_population_ignore_cc(grid, row, col, 0);
                }
        }
}