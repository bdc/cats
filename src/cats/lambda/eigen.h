
// SPDX-License-Identifier: GPL-3.0-or-later
//
// eigen.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_EIGEN_H
#define CATS_EIGEN_H

#include <gsl/gsl_eigen.h>
#include "data/species_parameters.h"

bool are_eigen_value_and_vector_real(gsl_complex eigen_value, gsl_vector_complex_view eigen_vector, int32_t N);

void print_eigen_system(gsl_complex eigen_value, gsl_vector_complex_view *eigen_vector, int32_t N,
                        struct cats_species_param *param);

gsl_complex get_largest_eigen_value(double *matrix, int32_t N, bool silent, struct cats_species_param *param,
                                    gsl_matrix_complex **e_vec, bool debug);

#endif //CATS_EIGEN_H
