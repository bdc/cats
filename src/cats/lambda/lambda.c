// SPDX-License-Identifier: GPL-3.0-or-later
//
// lambda.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include "lambda.h"
#include "matrix_helpers.h"
#include "eigen.h"
#include "dispersal/dispersal_helper.h"
#include "eigen_power.h"
#include "inline.h"
#include "dispersal/dispersal.h"

void debug_lambda_1_found(struct cats_configuration *conf, int32_t species_id, gsl_complex largest_eigen_value,
                          double *matrix,
                          int32_t N, struct lambda_parameters *l_param);

void debug_lambda_problem(struct cats_configuration *conf, gsl_complex largest_eigen_value,
                          struct lambda_parameters *l_param);


cats_dt_rates calculate_lambda(struct cats_configuration *conf, struct lambda_parameters *l_param, bool silent,
                               struct cats_module *module)
{


        double *matrix = NULL;
        int32_t N = 0;

        if (module == NULL) {
                matrix = create_leslie_matrix(conf, l_param, silent, &N);
        } else if (module->create_leslie_matrix != NULL)  {
                matrix = module->create_leslie_matrix(conf, l_param, silent, &N);

        } else {
                log_message(LOG_ERROR, "no function to create leslie matrix specified");
                exit_cats(EXIT_FAILURE);
        }


        bool debug = conf->command_line_options.debug_flags & DEBUG_LAMBDA;
        const int32_t species_id = l_param->species_id;



        //

        double *matrix_copy = NULL;


        if (silent == false || conf->command_line_options.debug_flags & DEBUG_LAMBDA) {
                matrix_copy = copy_matrix(matrix, N, N);
        }


        gsl_matrix_complex *eigen_vectors = NULL;// gsl_matrix_complex_alloc(N, N);
        gsl_complex largest_eigen_value = get_largest_eigen_value(matrix, N, silent, &conf->param[species_id],
                                                                  &eigen_vectors, debug);
        gsl_vector_complex_view eigen_vector = gsl_matrix_complex_column(eigen_vectors, 0);

        bool e_val_ok = are_eigen_value_and_vector_real(largest_eigen_value, eigen_vector, N) && GSL_REAL(largest_eigen_value) > 0;
        //printf("%d\n",  conf->command_line_options.debug_flags & DEBUG_LAMBDA);
        if (!e_val_ok && (silent == false || conf->command_line_options.debug_flags & DEBUG_LAMBDA)) {
                log_message(LOG_RAW, "============================\n");
                log_message(LOG_ERROR, "Eigenvalue or eigenvector not real and positive");
                print_eigen_system(largest_eigen_value, &eigen_vector, N, &conf->param[species_id]);
                log_message(LOG_RAW, "Original matrix\n");
                print_matrix(matrix_copy, N, N);
                log_message(LOG_RAW, "Schur form T:\n");
                print_matrix(matrix, N, N);

                if (module == NULL) {
                        debug_lambda_1_found(conf, species_id, largest_eigen_value, matrix_copy, N, l_param);
                        log_message(LOG_RAW, "============================\n");
                }

        }

        if (silent == false && l_param->calculate_scale == true) {

                if (fabs(GSL_REAL(largest_eigen_value) - 1.0) < LAMBDA_EPS) {
                        log_message(LOG_RAW, "Final matrix\n");
                        print_matrix(matrix_copy, N, N);
                        if (module == NULL) {
                                debug_lambda_1_found(conf, species_id, largest_eigen_value, matrix_copy, N, l_param);
                        }
                }
        }

        // FREE EVERYTHING
        gsl_matrix_complex_free(eigen_vectors);
        cats_dt_rates return_value = NAN;

        if (e_val_ok) {
                return_value = GSL_REAL(largest_eigen_value);
        } else {
                debug_lambda_problem(conf, largest_eigen_value, l_param);
                return_value = NAN;
                //return_value = eigen_value_power(matrix_copy, N);
        }

        free(matrix);
        free(matrix_copy);

        return return_value;
}


void debug_lambda_problem(struct cats_configuration *conf, gsl_complex largest_eigen_value,
                          struct lambda_parameters *l_param)
{
        if (conf->command_line_options.debug_flags & DEBUG_LAMBDA) {
                log_message(LOG_ERROR, "Eigenvalue not real and positive: %g + %gi: %0.12f\n",
                            GSL_REAL(largest_eigen_value),
                            GSL_IMAG(largest_eigen_value), (double) l_param->suitability);
        }
}


void debug_lambda_1_found(struct cats_configuration *conf, int32_t species_id, gsl_complex largest_eigen_value,
                          double *matrix, int32_t N, struct lambda_parameters *l_param)
{


        const int32_t seed_persistence = get_vital_age_from_param(&conf->param[species_id],
                                                                  VA_SEED_PERSISTENCE); // ;conf->param[species_id].seed_persistence;
        const int32_t mat_max = get_vital_age_from_param(&conf->param[species_id],
                                                         VA_AGE_OF_MATURITY_MAX); //conf->param[species_id].max_age_of_maturity;
        const int32_t mat_min = get_vital_age_from_param(&conf->param[species_id],
                                                         VA_AGE_OF_MATURITY_MIN);// ;conf->param[species_id].min_age_of_maturity;
        const int32_t stages_seeds = seed_persistence;

        const int32_t stages_juveniles = mat_max;
        log_message(LOG_RAW, "\n");
        log_message(LOG_IMPORTANT, "found lambda within %f of 1.0: %f",
                    LAMBDA_EPS,
                    GSL_REAL(largest_eigen_value));
        log_message(LOG_INFO,
                    "%dx%d matrix, seed persistence %d) "
                    "age of maturity min %d/max %d "
                    "(stages seeds %d, juveniles %d)",
                    N, N, seed_persistence, mat_min, mat_max, stages_seeds, stages_juveniles);

        get_rate_for_matrix(VR_ADULT_SURVIVAL, l_param, true);
        get_rate_for_matrix(VR_CLONAL_GROWTH, l_param, true);
        get_rate_for_matrix(VR_FLOWERING_FREQUENCY, l_param, true);
        get_rate_for_matrix(VR_SEED_YIELD, l_param, true);
        get_rate_for_matrix(VR_SEED_SURVIVAL, l_param, true);
        get_rate_for_matrix(VR_GERMINATION_RATE, l_param, true);
        get_rate_for_matrix(VR_GERMINATION_TO_ADULT_SURVIVAL, l_param, true);
        const struct cats_dispersal *dispersal = &conf->dispersal[species_id];
        cats_dt_rates local_dispersal = get_average_local_dispersal(dispersal, conf);
        log_message(LOG_INFO, "scale factor: %f", (double) conf->param[species_id].scale_factor);
        log_message(LOG_INFO, "local dispersal: %f", (double) local_dispersal);
        log_message(LOG_INFO, "resulting matrix\n");
        print_matrix(matrix, N, N);

}