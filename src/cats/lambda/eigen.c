// SPDX-License-Identifier: GPL-3.0-or-later
//
// eigen.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include <logging/logging.h>
#include "eigen.h"
#include "matrix_helpers.h"
#include "inline.h"


void print_eigen_system(gsl_complex eigen_value, gsl_vector_complex_view *eigen_vector, int32_t N,
                        struct cats_species_param *param)
{
        log_message(LOG_IMPORTANT, "Eigenvalue: %g + %gi", GSL_REAL(eigen_value), GSL_IMAG(eigen_value));
        log_message(LOG_IMPORTANT, "Eigenvector");
        log_message(LOG_RAW, "\n");
        const int32_t seed_persistence = get_vital_age_from_param(param, VA_SEED_PERSISTENCE);

        for (int32_t j = 0; j < N; ++j) {
                char *name = NULL;
                gsl_complex vector_entry = gsl_vector_complex_get(&eigen_vector->vector, j);
                if (j < seed_persistence) {
                        log_message(LOG_RAW, "EV::S%d::%g::%gi\n", j + 1, GSL_REAL(vector_entry), GSL_IMAG(vector_entry));
                } else if (j < N - 1) {
                        log_message(LOG_RAW, "EV::J%d::%g::%gi\n", j - seed_persistence + 1, GSL_REAL(vector_entry),
                               GSL_IMAG(vector_entry));
                } else {
                        log_message(LOG_RAW, "EV::A::%g::%gi\n", GSL_REAL(vector_entry), GSL_IMAG(vector_entry));
                }

                free(name);
        }
        log_message(LOG_RAW, "\n");

}


bool are_eigen_value_and_vector_real(gsl_complex eigen_value, gsl_vector_complex_view eigen_vector, int32_t N)
{

        double imag_eigen_value = GSL_IMAG(eigen_value);
        if (fabs(imag_eigen_value) > ZERO_EPS) return false;

        for (int32_t i = 0; i < N; i++) {
                gsl_complex vector_entry = gsl_vector_complex_get(&eigen_vector.vector, i);
                double imag = GSL_IMAG(vector_entry);

                if (fabs(imag) > ZERO_EPS) return false;

        }
        return true;

}


gsl_complex get_largest_eigen_value(double *matrix, int32_t N, bool silent, struct cats_species_param *param,
                                    gsl_matrix_complex **e_vec, bool debug)
{
        if (debug) {
                print_matrix(matrix, N, N);
        }

        gsl_matrix_view m = gsl_matrix_view_array(matrix, N, N);
        gsl_vector_complex *eigen_values = gsl_vector_complex_alloc(N);

        gsl_eigen_nonsymmv_workspace *w = gsl_eigen_nonsymmv_alloc(N);
        gsl_matrix_complex *eigen_vectors = gsl_matrix_complex_alloc(N, N);
        gsl_eigen_nonsymmv(&m.matrix, eigen_values, eigen_vectors, w);


        gsl_eigen_nonsymmv_sort(eigen_values, eigen_vectors, GSL_EIGEN_SORT_ABS_DESC);

        if (debug == true) {

                for (int32_t i = 0; i < N; i++) {
                        gsl_complex evec = gsl_vector_complex_get(eigen_values, i);
                        log_message(LOG_RAW, "EV %d: %+0.17e %+0.17e i\n", i, GSL_REAL(evec), GSL_IMAG(evec));
                }
        }

        gsl_complex eigen_val_0 = gsl_vector_complex_get(eigen_values, 0);

        // sometimes we get two eigenvalues with the same magnitude but different signs
        // the order of these is not fixed, so we check if the second-largest EV has the same magnitude and the correct sign

        if (GSL_REAL(eigen_val_0) < 0) {
                gsl_complex eigen_val_1 = gsl_vector_complex_get(eigen_values, 1);

                double diff_real = fabs(GSL_REAL(eigen_val_0)) - fabs(GSL_REAL(eigen_val_1));
                double diff_imag = fabs(GSL_IMAG(eigen_val_0)) - fabs(GSL_IMAG(eigen_val_1));

                if (fabs(diff_real) < 1e-10 && fabs(diff_imag) < 1e-10) {
                        log_message(LOG_INFO, "At least two eigenvalues with the same largest magnitude, but different signs. Using the positive eigenvalue.");
                        eigen_val_0 = eigen_val_1;
                }
        }

        gsl_eigen_nonsymmv_free(w);
        gsl_vector_complex_free(eigen_values);
        *e_vec = eigen_vectors;

        return eigen_val_0;
}