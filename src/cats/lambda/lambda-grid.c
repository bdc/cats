// SPDX-License-Identifier: GPL-3.0-or-later
//
// lambda-grid.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "leslie_matrix.h"
#include "paths/output_paths.h"
#include "inline.h"
#include "grids/gdal_save.h"
#include "environment/environment_set.h"
#include "memory/cats_memory.h"
#include "lambda.h"
#include "inline_vital_rates.h"
#include "inline_overlays.h"
#include "inline_carrying_capacity.h"

#define LAMBDA_SUITABILITY_GRANULARITY 1000


void prepare_lambda_cache(struct cats_species_param *param)
{
        if (param->parametrization != PARAM_HYBRID) return;
        if (param->lambda_cache != NULL) return;

        param->lambda_cache = calloc_or_die(LAMBDA_SUITABILITY_GRANULARITY + 1, sizeof(cats_dt_rates));

        for (int32_t i = 0; i < LAMBDA_SUITABILITY_GRANULARITY + 1; i++) {
                param->lambda_cache[i] = -1;
        }
}


cats_dt_rates
lookup_lambda(struct cats_configuration *conf, struct cats_grid *grid, cats_dt_population N, cats_dt_population K,
              cats_dt_coord row, cats_dt_coord col)
{
        struct lambda_parameters l_param = {0};
        assert(grid != NULL);
        l_param.species_id = grid->id;
        l_param.calculate_scale = false;
        l_param.N = N;
        l_param.K = K;
        l_param.param = &grid->param;

        switch (grid->param.parametrization) {

                case PARAM_UNDEFINED:
                        break;
                case PARAM_HYBRID:
                        l_param.grid = NULL;
                        l_param.row = 0;
                        l_param.col = 0,
                                l_param.suitability = get_suitability(grid, row, col);
                        break;
                case PARAM_DIRECT_VITAL_RATE:
                        l_param.grid = grid;
                        l_param.row = row;
                        l_param.col = col;
                        l_param.suitability = NAN;
                        break;
        }
        /*
        cats_leslie_matrix_func func = NULL;
        if (grid->param.demographic_module && grid->param.demographic_module->create_leslie_matrix) {
                func = grid->param.demographic_module->create_leslie_matrix;
        }
         */
        // we have density dependence
        if (N >= 0 || grid->param.parametrization != PARAM_HYBRID) {
                return calculate_lambda(conf, &l_param, true, NULL);

        }
        assert(grid->param.parametrization == PARAM_HYBRID);

        struct cats_species_param *param = &conf->param[grid->id];
        if (param->lambda_cache == NULL) {
                prepare_lambda_cache(param);
        }

        cats_dt_environment suit = l_param.suitability;
        int32_t index = suit * (LAMBDA_SUITABILITY_GRANULARITY);
        if (param->lambda_cache[index] >= 0) {
                return param->lambda_cache[index];
        } else {
                l_param.N = 0;
                cats_dt_rates lambda = calculate_lambda(conf, &l_param, true, NULL);
                param->lambda_cache[index] = lambda;
                return lambda;
        }

}


void save_lambda_grid(struct cats_configuration *conf, struct cats_grid *grid, bool density)
{
        if (grid->param.custom_vital_rates || grid->param.custom_vital_ages || grid->param.custom_dispersal) {
                log_message(LOG_ERROR, "unable to calculate lambda (non-default parametrisation)");
                return;
        }
        if (grid->param.parametrization == PARAM_HYBRID) {
                int32_t id = grid->id;
                char *suit_name = conf->param[id].suitability_name;
                struct cats_environment *env = get_environment(conf, suit_name);
                load_environment_if_needed(conf, env->environments[0], conf->time.year_start);
        }
        assert(grid != NULL);


        cats_dt_coord rows = conf->geometry.dimension.rows;
        cats_dt_coord cols = conf->geometry.dimension.cols;


        double **lambdas = new_raw_2d_array_from_dimension(grid->dimension,
                                                           sizeof(double));
        log_message(LOG_IMPORTANT, "Calculating lambda grid");
        cats_dt_rates max_cc = get_vital_rate_maximum(&grid->param.carrying_capacity);
        //cats_dt_rates max_cc = get_max_rate_from_grid(grid, VR_CARRYING_CAPACITY);

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        //cats_dt_rates suit = get_suitability_from_env(env, row, col);
                        cats_dt_rates lambda;
                        if (cell_excluded_by_overlay(conf, row, col)) {
                                lambda = NAN;
                        } else {
                                if (density) {

                                        lambda = lookup_lambda(conf, grid, get_adult_population(grid, row, col),
                                                               get_adult_carrying_capacity(grid, row, col), row, col);

                                } else {
                                        lambda = lookup_lambda(conf, grid, 0, (cats_dt_population) max_cc, row, col);
                                }
                        }

                        lambdas[row][col] = (double) lambda;
                }
        }

        struct grid_wrapper data = gridwrapper(lambdas, grid->dimension);
        char *filename = get_lambda_grid_name(grid, conf, density);
        save_grid_to_gdal(&data, GDT_Float64, conf, filename, conf->param[grid->id].species_name);
        free(filename);
        free_grid(&lambdas, grid->dimension.rows);
}