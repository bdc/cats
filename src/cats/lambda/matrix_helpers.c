
// SPDX-License-Identifier: GPL-3.0-or-later
//
// matrix_helpers.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <stdio.h>
#include <logging/logging.h>
#include <memory/cats_memory.h>
#include "cats_global.h"


void print_matrix(double *matrix, int32_t rows, int32_t cols)
{
        assert(matrix != NULL);
        assert(rows > 0);
        assert(cols > 0);
        log_message(LOG_RAW, "[\n");
        for (int32_t row = 0; row < rows; row++) {
                log_message(LOG_RAW, "  ");
                for (int32_t col = 0; col < cols; col++) {
                        log_message(LOG_RAW, "%+0.17e", matrix[row * cols + col]);
                        if (col < cols -1) {log_message(LOG_RAW, ", ");}
                        if (row < rows  -1 &&  col == cols -1) log_message(LOG_RAW, ";");
                }

                log_message(LOG_RAW, "\n");
        }

        log_message(LOG_RAW, "]\n");
}


double *empty_matrix(int32_t rows, int32_t columns)
{
        assert(rows > 0);
        assert(columns > 0);
        if (rows < 0 || columns < 0) {
                log_message(LOG_ERROR, "%s: rows (%d) and columns(%d) must both be > 0", __func__, rows, columns);
                exit_cats(EXIT_FAILURE);
        }

        double *matrix = calloc_or_die(rows * columns, sizeof(double));
        for (int32_t i = 0; i < rows * columns; i++) {
                matrix[i] = 0.0;
        }
        return matrix;
}


double *copy_matrix(const double *in, int32_t rows, int32_t columns)
{
        double *copy = empty_matrix(rows, columns);
        for (int32_t i = 0; i < rows * columns; i++) {
                copy[i] = in[i];
        }
        return copy;
}