// SPDX-License-Identifier: GPL-3.0-or-later
//
// leslie_matrix.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stdbool.h>
#include <stdio.h>
#include "dispersal/dispersal.h"
#include "leslie_matrix.h"
#include "memory/cats_memory.h"
#include "logging/logging.h"
#include "configuration/configuration.h"
#include "inline.h"
#include "matrix_helpers.h"
#include "cats_global.h"
#include "hybrid/scalefactor.h"
#include "dispersal/dispersal_helper.h"
#include "inline_vital_rates.h"


cats_dt_rates
calculate_rate_for_matrix(struct cats_vital_rate *rate,
                    const struct lambda_parameters *l_param,
                    bool print)
{
        assert(l_param != NULL);
        assert(l_param->param != NULL);

        const struct cats_species_param *param = l_param->param; //&conf->param[grid_id];

        const cats_dt_rates N = l_param->N;
        const cats_dt_rates K = l_param->K;
        struct link_override_parameters override = {-1};

        if (param->parametrization == PARAM_HYBRID) {
                override.override_suitability = l_param->suitability;
        }

        override.override_population = N;
        override.override_carrying_capacity = l_param->K;

        cats_dt_rates value = calculate_rate(rate, N, param, l_param->grid, l_param->row, l_param->col, &override);

        if (print) {
                if (param->parametrization == PARAM_HYBRID) {
                        log_message(LOG_INFO, "%s: %Lf (maximum %Lf) for population %Lf/%Lf at suitability %Lf",
                                    rate->name, value, rate->max_rate, N, K, l_param->suitability);
                } else {
                        log_message(LOG_INFO, "%s: %Lf (maximum %Lf) for population %Lf/%Lf",
                                    rate->name, value, rate->max_rate, N, K);
                }

        }

        return value;
}

cats_dt_rates
get_rate_for_matrix(enum cats_vital_rate_id rate_type,
                    const struct lambda_parameters *l_param,
                    bool print)
{
        assert(l_param != NULL);
        assert(l_param->param != NULL);

        const struct cats_species_param *param = l_param->param; //&conf->param[grid_id];
        const struct cats_vital_rate *info = &param->vital_rates[rate_type];
        const cats_dt_rates N = l_param->N;
        const cats_dt_rates K = l_param->K;
        struct link_override_parameters override = {-1};

        if (param->parametrization == PARAM_HYBRID) {
                override.override_suitability = l_param->suitability;
        }

        override.override_population = N;
        override.override_carrying_capacity = l_param->K;

        cats_dt_rates rate = calculate_rate(info, N, param, l_param->grid, l_param->row, l_param->col, &override);

        if (print) {
                log_message(LOG_INFO, "%s: %f (maximum %f) for adult population density %f/%f",
                            info->name, (double) rate, (double) info->max_rate, (double) N, (double) K);
        }

        return rate;
}


double *
create_leslie_matrix(struct cats_configuration *conf, struct lambda_parameters *l_param, bool silent, int32_t *N_out)
{


#define LM_START
#ifndef LM_START
        log_message(LOG_IMPORTANT, "using method LM_MID (old)");
#else
        //log_message(LOG_IMPORTANT, "using method LM_START (new)");

#endif

        assert (N_out != NULL);
        const int32_t species_id = l_param->species_id;
        const int32_t mat_max = get_vital_age_from_param(&conf->param[species_id], VA_AGE_OF_MATURITY_MAX);
        const int32_t mat_min = get_vital_age_from_param(&conf->param[species_id], VA_AGE_OF_MATURITY_MIN);
        const int32_t seed_persistence = get_vital_age_from_param(&conf->param[species_id], VA_SEED_PERSISTENCE);

        //const int32_t seed_persistence = conf->param[species_id].seed_persistence;
        //const int32_t mat_max = conf->param[species_id].max_age_of_maturity;
        //const int32_t mat_min = conf->param[species_id].min_age_of_maturity;

        const int32_t stages_seeds = seed_persistence;
        const int32_t stages_juveniles = mat_max;
        const int32_t stages_adults = 1;
        const int32_t N = stages_seeds + stages_juveniles + stages_adults;
        *N_out = N;
        double *matrix = empty_matrix(N, N);

        if (l_param->calculate_scale == true && conf->direct_scale_factor > 0) {
                conf->param[species_id].scale_factor = conf->direct_scale_factor;
        }

        const struct cats_dispersal *dispersal = &conf->dispersal[species_id];
        cats_dt_rates local_dispersal = get_average_local_dispersal(dispersal, conf);

        bool print_rate = !silent;

        // load rates for target suitability
        cats_dt_rates adult_survival = get_rate_for_matrix(VR_ADULT_SURVIVAL, l_param, print_rate);
        cats_dt_rates clonal_growth = get_rate_for_matrix(VR_CLONAL_GROWTH, l_param, print_rate);
        cats_dt_rates flowering_frequency = get_rate_for_matrix(VR_FLOWERING_FREQUENCY, l_param, print_rate);
        cats_dt_rates seed_yield = get_rate_for_matrix(VR_SEED_YIELD, l_param, print_rate);
        cats_dt_rates seed_survival = get_rate_for_matrix(VR_SEED_SURVIVAL, l_param, print_rate);
        cats_dt_rates germination_rate = get_rate_for_matrix(VR_GERMINATION_RATE, l_param, print_rate);
        cats_dt_rates germination_to_adult_survival = get_rate_for_matrix(VR_GERMINATION_TO_ADULT_SURVIVAL,
                                                                          l_param,
                                                                          print_rate);
        cats_dt_rates juvenile_transition_rate = get_juvenile_tr_from_germination_to_adult_survival(
                germination_to_adult_survival, &conf->param[species_id]);
        // printf("%Lf %Lf\n", juvenile_survival, germination_to_adult_survival);
        fflush(stdout);

        cats_dt_rates hapaxanthy_survival = 1.0;
        if (l_param->param->hapaxanthy > 0.0) {
                hapaxanthy_survival = (1.0 - l_param->param->hapaxanthy) * flowering_frequency;
                assert(hapaxanthy_survival >= 0);
                assert(hapaxanthy_survival <= 1.0);
                log_message(LOG_INFO, "hapaxanthy survival: %f, times flowering frequency %f",
                            (double) (1.0 - l_param->param->hapaxanthy), (double) hapaxanthy_survival);
        }

        if (print_rate) {
                log_message(LOG_INFO, "juvenile transition: %f", (double) juvenile_transition_rate);
                log_message(LOG_INFO, "local dispersal %f", (double) local_dispersal);
        }


        double m0 = (double) age_specific_maturation_rate(0, juvenile_transition_rate, mat_min, mat_max);
        double dl = (double) (flowering_frequency * seed_yield * local_dispersal);

        if (print_rate) {
                for (int32_t i = 0; i < mat_max + 1; i++) {
                        log_message(LOG_RAW, "m%d = %f\n", i,
                                    (double) age_specific_maturation_rate(i, juvenile_transition_rate, mat_min,
                                                                          mat_max));
                }
                log_message(LOG_RAW, "d = %f\n", dl);
        }

        // special case:
        // cell_maturation max = 0
        // seed persistence = 1

        if (mat_max == 0 && seed_persistence == 1) {

                if (l_param->calculate_scale) {
                        double x = (double) (adult_survival * clonal_growth +
                                             seed_yield * flowering_frequency * local_dispersal * seed_survival *
                                             germination_rate * m0 * hapaxanthy_survival);

                        log_message(LOG_INFO, "Sanity check for short lived plants: lambda should be %f", x);
                }

                //return x;
        }

        // FIRST ROW: transition to seeds0

        // seeds: Si -> S0
        for (int32_t s = 0; s < seed_persistence; s++) {
                assert(s < N);
                matrix[0 * N + s] = (double) (seed_survival * germination_rate * m0 * dl);
        }

        // juveniles: Ji -> S0
        for (int32_t j = 0; j < mat_max; j++) {

                int32_t col = seed_persistence + j;
                assert(col < N);
                cats_dt_rates m = age_specific_maturation_rate(j + 1, juvenile_transition_rate, mat_min, mat_max);
#ifndef LM_START
                matrix[0 * N + col] = (double) (m * dl * seed_survival);

#else
                cats_dt_rates juvenile_survival_rate = age_modified_juvenile_survival_rate(
                        juvenile_transition_rate, j, mat_min, mat_max);
                matrix[0 * N + col] = (double) (juvenile_survival_rate * m * dl);
#endif
        }

        // adults: A -> S0
        for (int32_t a = seed_persistence + mat_max; a < N; a++) {
                assert(a == N - 1);
#ifndef LM_START
                matrix[0 * N + a] = (double) (clonal_growth * dl * seed_survival);
#else
                matrix[0 * N + a] = (double) (adult_survival * clonal_growth * dl);
#endif
        }

        // SEED ROWS: S_i -> S_{i+1}
        for (int32_t s = 1; s < seed_persistence; s++) {
                matrix[s * N + (s - 1)] = (double) (seed_survival * (1.0 - germination_rate));

        }

        // JUVENILE ROW 1: Si -> J0
        for (int32_t s = 0; s < seed_persistence; s++) {
                int32_t row = seed_persistence;
                if (row * N + s < N * N) {

                } else {
                        log_message(LOG_RAW, "N = %d\n", N);
                        log_message(LOG_RAW, "s = %d\n", s);
                        log_message(LOG_RAW, "row = %d\n", row);
                        exit(EXIT_FAILURE);
                }
#ifndef LM_START
                cats_dt_rates juvenile_survival_rate = age_modified_juvenile_survival_rate(
                        juvenile_transition_rate, 0, mat_min, mat_max);
                matrix[row * N + s] = (double) (germination_rate * (1.0 - m0) * juvenile_survival_rate);
#else
                matrix[row * N + s] = (double) (seed_survival * germination_rate * (1.0 - m0));
#endif
        }

        // JUVENILE ROWS J1+ ...: J_i -> J_{i+1}

        for (int32_t j = 0; j < mat_max; j++) {
                cats_dt_rates juvenile_survival_rate = age_modified_juvenile_survival_rate(
                        juvenile_transition_rate, j, mat_min, mat_max);
                int32_t row = j + seed_persistence + 1;
                int32_t col = j + seed_persistence;
                assert(row < N);
                cats_dt_rates m = age_specific_maturation_rate(j + 1, juvenile_transition_rate, mat_min, mat_max);
#ifndef LM_START

                matrix[row * N + col] = (double) ((1.0 - m) * juvenile_survival_rate);
#else

                matrix[row * N + col] = (double) (juvenile_survival_rate * (1.0 - m));
#endif

        }

        // LAST ROW
        int32_t row = seed_persistence + mat_max;
        assert(row == N - 1);
        for (int32_t s = 0; s < seed_persistence; s++) {
#ifndef LM_START
                matrix[row * N + s] = (double) (germination_rate * m0 * adult_survival);
#else
                matrix[row * N + s] = (double) (seed_survival * germination_rate * m0 * hapaxanthy_survival);
#endif

        }
        for (int32_t j = 0; j < mat_max; j++) {

                int32_t col = seed_persistence + j;
                cats_dt_rates m = age_specific_maturation_rate(j + 1, juvenile_transition_rate, mat_min, mat_max);
#ifndef LM_START

                assert(col < N - 1);
                matrix[row * N + col] = (double) (m * adult_survival);
#else
                cats_dt_rates juvenile_survival_rate = age_modified_juvenile_survival_rate(
                        juvenile_transition_rate, j, mat_min, mat_max);
                assert(col < N - 1);
                matrix[row * N + col] = (double) (juvenile_survival_rate * m * hapaxanthy_survival);
#endif
        }

        // LAST ENTRY
#ifndef LM_START
        matrix[row * N + N - 1] = (double) (clonal_growth * adult_survival * hapaxanthy_survival);
#else
        matrix[row * N + N - 1] = (double) (adult_survival * clonal_growth * hapaxanthy_survival);
#endif


        return matrix;
}
