// SPDX-License-Identifier: GPL-3.0-or-later
//
// leslie_matrix.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_LESLIE_MATRIX_H
#define CATS_LESLIE_MATRIX_H

#include "data/cats_datatypes.h"
#include "configuration/configuration.h"

struct lambda_parameters {
        int32_t species_id;
        struct cats_species_param *param;
        struct cats_grid *grid;
        cats_dt_rates suitability;
        cats_dt_population N;
        cats_dt_population K;
        cats_dt_coord row;
        cats_dt_coord col;
        bool calculate_scale;
};

cats_dt_rates
get_rate_for_matrix(enum cats_vital_rate_id rate_type, const struct lambda_parameters *l_param,
                    bool print);

cats_dt_rates
calculate_rate_for_matrix(struct cats_vital_rate *rate,
                          const struct lambda_parameters *l_param,
                          bool print);
double *
create_leslie_matrix(struct cats_configuration *conf, struct lambda_parameters *l_param, bool silent, int32_t *N_out);

#endif //CATS_LESLIE_MATRIX_H
