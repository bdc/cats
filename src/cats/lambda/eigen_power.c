// SPDX-License-Identifier: GPL-3.0-or-later
//
// eigen_power.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include "eigen_power.h"
#include "logging/logging.h"
#include "memory/cats_memory.h"


static double *ep_empty_vec(int N)
{
        double *v = malloc_or_die(N * sizeof(double));
        for (int i = 0; i < N; i++) v[i] = 0.0;
        return v;
}


static double ep_vec_dot(const double *v1, const double *v2, int n)
{
        double r = 0;
        for (int i = 0; i < n; i++) r += v1[i] * v2[i];
        return r;
}


static void ep_matrix_times_vector(const double *matrix, int n, const double *vec, double *result)
{
        for (int row = 0; row < n; row++) {
                result[row] = 0.0;
                for (int col = 0; col < n; col++) {
                        int idx = row * n + col;
                        result[row] += matrix[idx] * vec[col];
                }
        }
}


static void ep_scale_vec(double *v, int n)
{
        double max = v[0];

        for (int i = 1; i < n; i++) {
                if (v[i] > max) max = v[i];
        }
        for (int i = 0; i < n; i++) v[i] = v[i] / max;
}


static bool ep_matrix_ok(const double *matrix, int N)
{
        for (int i = 0; i < N * N; i++) {
                double val = matrix[i];
                if (val < 0 || val > 1) {
                        return false;
                }
        }
        return true;
}


static double ep_max_diff(const double *v1, const double *v2, int N)
{
        double max = 0;
        for (int i = 0; i < N; i++) {
                double diff = fabs(v1[i] - v2[i]);
                if (diff > max) max = diff;
        }
        return max;
}


static void ep_copy_vec(const double *from, double *to, int N)
{
        for (int i = 0; i < N; i++) to[i] = from[i];
}


double eigen_value_power(double *matrix, int N)
{
        /*
        if (!ep_matrix_ok(matrix, N)) {
                log_message(LOG_WARNING, "Matrix element outside [0, 1], stopping");
                return NAN;
        }
        */
        double *v1 = ep_empty_vec(N);
        double *v2 = ep_empty_vec(N);
        //double *old = ep_empty_vec(N);

        // initialize v1
        for (int i = 0; i < N; i++) {
                v1[i] = 1.0 / sqrt(((double) N * N));
        }


        const int max_iterations = 1000;
        for (int i = 0; i < max_iterations; i += 2) {
                //ep_copy_vec(v1, old, N);
                // do 2 iterations per loop iteration, switch v1 and v2

                // input in v1 result in v2
                ep_matrix_times_vector(matrix, N, v1, v2);
                ep_scale_vec(v2, N);
                // input in v2 result in v1
                ep_matrix_times_vector(matrix, N, v2, v1);
                ep_scale_vec(v1, N);

                // double diff = ep_max_diff(v1, old, N);
        }

        // N * v1 -> v2
        ep_matrix_times_vector(matrix, N, v1, v2);
        double ev = ep_vec_dot(v2, v1, N) / ep_vec_dot(v1, v1, N);

        free(v1);
        free(v2);
        return ev;
}
