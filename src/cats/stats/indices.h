// SPDX-License-Identifier: GPL-3.0-or-later
//
// indices.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once


int64_t count_cells(const struct cats_grid *const grid, enum cats_stats what);

void print_populated_cell_stats(struct cats_grid *grid);

void print_seeded_cell_stats(struct cats_grid *grid);

void
get_pop_sum_and_populated_max_cc(cats_dt_population_sum *N_all, cats_dt_population *K_max, struct cats_grid **parent,
                                 struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col);