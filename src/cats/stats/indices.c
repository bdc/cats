// SPDX-License-Identifier: GPL-3.0-or-later
//
// indices.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#ifndef  _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <math.h>
#include <assert.h>

#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "populations/carrying_capacity.h"
#include "actions/cats_actions.h"
#include "logging.h"
#include "indices.h"
#include "inline.h"
#include "memory/cats_memory.h"
#include "inline_overlays.h"
#include "inline_carrying_capacity.h"
#include "inline_population.h"
#include "populations/population.h"


void
get_pop_sum_and_populated_max_cc(cats_dt_population_sum *N_all, cats_dt_population *K_max, struct cats_grid **parent,
                                 struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col)
{
        const int n_classes = conf->grid_count;
#ifdef __STDC_NO_VLA__
        cats_dt_population  *class_cc = malloc_or_die(sizeof(cats_dt_population) * n_classes);
#else
        cats_dt_population class_cc[n_classes];
#endif


        *N_all = get_population_all_classes_sum((const struct cats_grid **) parent, row, col);
        get_carrying_capacity_all_classes(parent, row, col, class_cc);
        for (int i = 0; i < n_classes; i++) {
                if (class_cc[i] > *K_max) {
                        *K_max = class_cc[i];
                }
        }
#ifdef __STDC_NO_VLA__
        free(class_cc);
#endif
}


double shannon_index_single_cell(const struct cats_grid **grids, struct cats_configuration *conf, cats_dt_coord row,
                                 cats_dt_coord col, int threshold, bool check_mask)
{
        if (check_mask && cell_excluded_by_overlay(conf, row, col)) {
                log_message(LOG_ERROR, "needs reimplementation for mask types");
                conf->global_stats.populated_by_classes[0]++;
                return -1.0;
        }

        const int grid_count = conf->grid_count;
#ifdef __STDC_NO_VLA__
        cats_dt_population  *pop_class = malloc_or_die(sizeof(cats_dt_population) * grid_count);
#else
        cats_dt_population pop_class[grid_count];
#endif


        int64_t pop_unfiltered = 0;
        int64_t pop_all = get_population_all_classes_ts(grids, row, col, &pop_class[0], threshold, &pop_unfiltered);

        int count = 0;
        double index = 0.0;

        for (int i = 0; i < grid_count; i++) {
                if (pop_class[i] > 0) {
                        double pop_fraction = (double) pop_class[i] / (double) pop_all;
                        index += (pop_fraction * log(pop_fraction));
                        count++;
                }
        }

        index = index * -1.0;

        conf->global_stats.populated_by_classes[count]++;

        if (pop_unfiltered >= threshold) conf->global_stats.stats[MCS_POPULATED_OVER_TS]++;
        if (pop_unfiltered > 0) conf->global_stats.stats[MCS_POPULATED_OVER_0]++;
#ifdef __STDC_NO_VLA__
        free(pop_class);
#endif
        return index;
}


void get_shannon_sum_grid(const struct cats_grid **grids, struct cats_configuration *conf, double *shannon_sum,
                          int64_t *count)
{
        assert(shannon_sum != NULL);
        assert(count != NULL);
        int threshold = conf->stats_populated_threshold;

        const struct cats_grid *grid = grids[0];

        double sum = 0.0;
        int64_t cnt = 0;

        bool check_mask = conf->overlays.have_overlays == true;

        for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                for (cats_dt_coord col = 0; col < grid->dimension.cols; col++) {
                        double shannon = shannon_index_single_cell(grids, conf, row, col, threshold, check_mask);

                        if (shannon >= 0.0) {
                                sum += shannon;
                                cnt++;
                        }
                }
        }

        *shannon_sum = sum;
        *count = cnt;
}


int64_t count_cells(const struct cats_grid *grid, enum cats_stats what)
{
        int64_t result = 0;
        for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                for (cats_dt_coord col = 0; col < grid->dimension.cols; col++) {
                        switch (what) {
                                case CS_POPULATED:
                                        if (get_adult_population(grid, row, col) > 0) result++;
                                        break;
                                default:
                                        return -1;
                        }
                }
        }

        return result;

}