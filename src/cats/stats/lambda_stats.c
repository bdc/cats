// SPDX-License-Identifier: GPL-3.0-or-later
//
// lambda_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "lambda_stats.h"
#include "paths/output_paths.h"
#include "inline.h"
#include "metadata.h"
#include "inline_vital_rates.h"


FILE *
create_and_initialize_lambda_stat_file(struct cats_configuration *conf, const char *identifier, struct cats_grid *grid)
{
        char *fn = get_lambda_stat_filename(conf, identifier);
        FILE *file = fopen(fn, "w");
        ENSURE_FILE_OPENED(file, fn)
        assert(file != NULL);
        free(fn);

        cats_dt_rates max_germ_to_adult = get_default_vital_rate_maximum_from_param(&conf->param[0],
                                                                                    VR_GERMINATION_TO_ADULT_SURVIVAL);
        // FIXME at OT not necessarily time scale factor
        cats_dt_rates juvenile_transition_rate = get_juvenile_tr_from_germination_to_adult_survival(
                max_germ_to_adult * conf->param[0].scale_factor, &conf->param[0]);
        cats_dt_rates juvenile_transition_rate_max = get_juvenile_tr_from_germination_to_adult_survival(
                max_germ_to_adult, &conf->param[0]);
        fprintf(file, "# species name: %s\n", conf->param[0].species_name_actual);
        fprintf(file, "# run name: %s\n", conf->run_name);
        fprintf(file, "# burn in time: %d\n", conf->lambda_burn_in_time);
        fprintf(file, "# scale factor: %Lf\n", conf->param[0].scale_factor);
        add_simulation_metadata(file, conf);
        add_parameter_metadata(file, &conf->param[0]);

        fprintf(file, "# OT: %Lf\n", conf->param[0].OT);
        fprintf(file, "# ZT: %Lf\n", conf->param[0].ZT);

        fprintf(file, "# lambda at OT: %Lf\n", conf->lambda_at_OT);
        const int32_t seed_persistence = get_vital_age_from_param(&conf->param[0], VA_SEED_PERSISTENCE);
        const int32_t max_age_of_maturity = get_vital_age_from_param(&conf->param[0], VA_AGE_OF_MATURITY_MAX);
        const int32_t min_age_of_maturity = get_vital_age_from_param(&conf->param[0], VA_AGE_OF_MATURITY_MIN);
        fprintf(file, "# minimum age of maturity: %d\n", min_age_of_maturity);
        fprintf(file, "# maximum age of maturity: %d\n", max_age_of_maturity);


        fprintf(file, "# seed persistence: %d\n", seed_persistence);

        fprintf(file, "# %s maximum: %Lf\n", conf->param[0].carrying_capacity.name,
                conf->param[0].carrying_capacity.max_rate);
        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {
                fprintf(file, "# %s maximum: %Lf\n", conf->param[0].vital_rates[vr_idx].name,
                        conf->param[0].vital_rates[vr_idx].max_rate);
        }

        const struct cats_vital_rate *link = &conf->param[0].carrying_capacity;
        cats_dt_rates rate = calculate_rate(link, NAN, &conf->param[0], grid, 0, 0, NULL);
        fprintf(file, "# %s at OT: %Lf\n", link->name, rate);

        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {
                const struct cats_vital_rate *link2 = get_default_vital_rate(grid, vr_idx);
                cats_dt_rates rate2 = calculate_rate(link2, NAN, &conf->param[0], grid, 0, 0, NULL);
                fprintf(file, "# %s at OT: %Lf\n", conf->param[0].vital_rates[vr_idx].name, rate2);
        }

        fprintf(file, "# juvenile transition at scale factor * germination to adult survival rate maximum: %Lf\n",
                juvenile_transition_rate);
        fprintf(file, "# juvenile transition at  germination to adult survival rate maximum: %Lf\n",
                juvenile_transition_rate_max);

        fprintf(file, "when,year,population,cc,suitability,scale,juveniles,juveniles_weighted,seeds");
        for (int32_t i = 0; i < seed_persistence + 1; i++) {
                fprintf(file, ",S%d", i);
        }

        for (int32_t i = 0; i < max_age_of_maturity + 1; i++) {
                fprintf(file, ",J%d", i);
        }
        fprintf(file, "\n");
        return file;
}
