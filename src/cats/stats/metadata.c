// SPDX-License-Identifier: GPL-3.0-or-later
//
// metadata.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "defaults.h"
#include "metadata.h"


void add_simulation_metadata(FILE *file, const struct cats_configuration *conf)
{
        fprintf(file, "# CATS simulation parameters\n");
        fprintf(file, "# populated threshold: %d\n", conf->stats_populated_threshold);
        fprintf(file, "# cats-version: %s/%s\n", VERSION, COMPILATION_DATE);
        fprintf(file, "# poisson dampening factor: %f\n", global.poisson_dampening_factor);
        fprintf(file, "# poisson dampening minimum: %f\n", global.poisson_dampening_minimum);
        fprintf(file, "# poisson maximum: %f\n", global.poisson_maximum);
        fprintf(file, "# poisson maximum draw difference: %d\n", global.poisson_maximum_draw_diff);
}


void add_parameter_metadata(FILE *file, const struct cats_species_param *param)
{
        fprintf(file, "# maximum adult fraction of carrying capacity: %Lf\n", param->max_adult_cc_fraction);
        if (param->parametrization == PARAM_HYBRID) {
                fprintf(file, "# scale factor: %Lf\n", param->scale_factor);

        }
}

void add_stats_header_explanation(FILE *file, const struct cats_species_param *param)
{
        fprintf(file, "# \n");
        fprintf(file, "# Field explanations\n");
        fprintf(file, "# phase -- simulation phase: B ... burn-in, W ... warm-up, S ... simulation\n");
        fprintf(file, "# year -- simulation year\n");
        fprintf(file, "# run -- run name\n");
        fprintf(file, "# species -- species name\n");
        fprintf(file, "# id -- species id\n");
        fprintf(file, "# populated -- cells with population > 0\n");
        fprintf(file, "# unpopulated -- cells with population == 0\n");
        fprintf(file, "# excluded cells -- cells excluded by exclusion overlay\n");
        fprintf(file, "# populated_fit -- cells with population > 0 and suitability >= OT\n");
        fprintf(file, "# populated_unfit -- cells with population > 0 and suitability < OT\n");
        fprintf(file, "# unpopulated fit -- cells with population == 0 and suitability >= OT\n");
        fprintf(file, "# unpopulated_unfit -- cells with population == 0 and suitability < OT\n");
        fprintf(file, "# populated_under_ZT -- cells with population > 0 and suitability < ZT\n");
        fprintf(file, "# unpopulated_under_ZT -- cells with population == 0 and suitability < ZT\n");
        fprintf(file, "# fields ending with _TS -- same as above, only that cells are populated/unpopulated depending on threshold (see above)\n");
        fprintf(file, "# \n");
}