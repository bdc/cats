// SPDX-License-Identifier: GPL-3.0-or-later
//
// grid_stats.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_STATS_H_
#define CATS_STATS_H_

#include <stdint.h>
#include <stdio.h>
#include "cats_global.h"
#include "configuration/configuration.h"
#include "data/cats_grid.h"
#include "grid_stats.h"
#include "threading/threading-helpers.h"


void cell_stats(const struct cats_grid **parent, struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
                struct cats_thread_info *ts);

void merge_thread_stats(struct statistics *result, int grid_count, struct cats_thread_info *threads, int num_threads);

struct string_array *
add_common_stats_header(struct string_array *x, struct cats_configuration *conf, int grid_id, bool header,
                        bool global_stats);

void write_grid_stats(struct cats_configuration *conf, struct cats_grid *grid, bool header);

struct string_array *
add_common_stats_header(struct string_array *x, struct cats_configuration *conf, int grid_id, bool header,
                        bool global_stats);

void initialize_grid_stats(struct cats_grid *grid, struct cats_configuration *conf);


void calculate_grid_statistics(struct cats_grid *grid, struct cats_configuration *conf);


void cleanup_grid_stats(struct cats_grid *grid, struct cats_configuration *conf);

void consolidate_thread_stats(struct cats_grid *grid, struct cats_configuration *conf, struct cats_thread_info *threads,
                              int num_threads);

void collect_adult_animals_stats(struct cats_grid *grid, struct cats_configuration *conf);

double shannon_index_single_cell(const struct cats_grid **grids, struct cats_configuration *conf, cats_dt_coord row,
                                 cats_dt_coord col, int threhshold, bool check_mask);

void get_shannon_sum_grid(const struct cats_grid **grids, struct cats_configuration *conf, double *shannon_sum,
                          int64_t *count);


#endif