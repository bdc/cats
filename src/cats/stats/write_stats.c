// SPDX-License-Identifier: GPL-3.0-or-later
//
// write_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stdlib.h>
#include <memory/raw_arrays.h>
#include <string.h>
#include <assert.h>
#include "configuration/configuration.h"
#include "logging.h"
#include "data/cats_grid.h"
#include "grid_stats.h"
#include "defaults.h"
#include "metadata.h"


const char *get_stat_name(enum cats_stats which)
{


        switch (which) {
                case CS_POPULATED:
                        return "populated";
                case CS_UNPOPULATED:
                        return "unpopulated";
                case STAT_MIN:
                        break;
                case CS_POPULATED_FIT:
                        return "populated_fit";
                case CS_POPULATED_UNFIT:
                        return "populated_unfit";
                case CS_UNPOPULATED_FIT:
                        return "unpopulated_fit";
                case CS_UNPOPULATED_UNFIT:
                        return "unpopulated_unfit";
                case CS_POPULATED_UNDER_ZT:
                        return "populated_under_ZT";
                case CS_UNPOPULATED_UNDER_ZT:
                        return "unpopulated_under_ZT";
                case CS_POPULATED_FIT_TS:
                        return "populated_fit_TS";
                case CS_POPULATED_UNFIT_TS:
                        return "populated_unfit_TS";
                case CS_UNPOPULATED_FIT_TS:
                        return "unpopulated_fit_TS";
                case CS_UNPOPULATED_UNFIT_TS:
                        return "unpopulated_unfit_TS";
                case CS_POPULATED_UNDER_ZT_TS:
                        return "populated_under_ZT_TS";
                case CS_UNPOPULATED_UNDER_ZT_TS:
                        return "unpopulated_under_ZT_TS";
                case CS_EXCLUDED:
                        return "excluded cells";
                case MCS_POPULATED_OVER_TS:
                case MCS_POPULATED_OVER_0:
                case GS_SEEDS_GERMINATED:
                case CS_POPULATED_BEFORE_CC:
                case CS_POPULATED_AFTER_CC:
                case CS_SEEDS_BEFORE_POISSON:
                case CS_SEEDS_AFTER_POISSON:
                case CS_SEEDS_PRODUCED:
                case STAT_MAX:
                        break;

        }

        log_message(LOG_ERROR, "unknown name for stat %d", which);
        exit_cats(EXIT_FAILURE);
}


void add_stat(struct string_array *result, struct statistics *stats, enum cats_stats which, bool header)
{
        if (header) {
                const char *name = get_stat_name(which);
                string_array_add(result, name);
        } else {
                string_array_add_int64(result, stats->stats[which], NULL);
        }

}


struct string_array *add_plant_stats(struct cats_configuration *conf, struct cats_grid *grid, bool header)
{
        struct string_array *output = new_string_array();

        output = add_common_stats_header(output, conf, grid->id, header, false);

        enum cats_stats last = GRID_STAT_LAST;
        if (grid->param.parametrization != PARAM_HYBRID) {
                last = GRID_STAT_LAST_DIRECT;
        }
        for (enum cats_stats i = GRID_STAT_FIRST; i <= last; i++) {
                add_stat(output, &grid->stats, i, header);
        }


        return output;

}


struct string_array *
add_module_stats(struct cats_configuration *conf, struct cats_grid *grid, bool header, int32_t module_id)
{
        if (grid->param.module_data[module_id].grid_stat_function == NULL) return NULL;
        struct string_array *module_stats = grid->param.module_data[module_id].grid_stat_function(conf, grid, header);
        return module_stats;

}


void write_to_file(FILE *file, const struct string_array *data, const char *prefix)
{
        char *result = string_array_paste(data, ",");
        if (prefix && strlen(prefix)) {
                fprintf(file, "%s%s", prefix, result);
        } else {
                fprintf(file, "%s", result);
        }
        free(result);

}


void gui_output_grid_stats(const struct string_array *header, const struct string_array *data, int32_t grid_id)
{
        if (header == NULL || data == NULL) return;


        char *result = json_dict(header, data);
        fprintf(stderr, "JSON::{\"msgtype\": \"grid-stats\", \"grid-id\": %d, \"content\": %s}\n", grid_id, result);
        free(result);
}


void write_grid_stats(struct cats_configuration *conf, struct cats_grid *grid, bool header)
{

        if (conf->output.performance_file && grid->id == 0) fflush(conf->output.performance_file);

        if (grid->stats.file == NULL) {
                log_message(LOG_ERROR,
                            "%s: grid statistics file for grid id %d not open, skipping writing stats",
                            __func__, grid->id);
                return;
        }


        FILE *file = grid->stats.file;

        if (header) {
                add_simulation_metadata(file, conf);
                add_parameter_metadata(file, &grid->param);
                add_stats_header_explanation(file, &grid->param);
        }


        struct string_array *plant_stats = add_plant_stats(conf, grid, header);
        write_to_file(file, plant_stats, NULL);

        if (grid->stats.stats_header == NULL && header == true) {
                grid->stats.stats_header = copy_string_array(plant_stats);
        }

        if (header == false && global.enable_json_output) {
                gui_output_grid_stats(grid->stats.stats_header, plant_stats, grid->id);
        }

        free_string_array(&plant_stats);

        for (int32_t module_id = 0; module_id < conf->modules.count; module_id++) {
                struct string_array *module_stats = add_module_stats(conf, grid, header, module_id);
                if (module_stats == NULL) continue;
                write_to_file(file, module_stats, ",");
                if (conf->modules.module[module_id].stats_header == NULL) {
                        conf->modules.module[module_id].stats_header = copy_string_array(module_stats);
                }

                if (header == false && global.enable_json_output) {
                        gui_output_grid_stats(conf->modules.module[module_id].stats_header, module_stats, grid->id);
                }
                free_string_array(&module_stats);

        }

        fprintf(file, "\n");
        fflush(file);

}
