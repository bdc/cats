// SPDX-License-Identifier: GPL-3.0-or-later
//
// statistics.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#include <assert.h>
#include <stdlib.h>
#include "statistics.h"
#include "cats_strings/cats_strings.h"
#include "memory/cats_memory.h"
#include "configuration/configuration.h"


void zero_statistics_stats(struct statistics *stats)
{
        assert(stats != NULL);
        // save some values
        FILE *file = stats->file;
        char **has_been_populated = stats->has_been_populated;
        char **has_been_populated_ts = stats->has_been_populated_ts;
        int32_t *N = stats->populated_by_classes;
        struct string_array *header = stats->stats_header;
        int64_t *custom_stats = stats->custom_stats;
        int64_t custom_stat_count = stats->custom_stat_count;
        // reset everything
        struct statistics empty = {0};
        *stats = empty;


        // restore some values
        stats->file = file;
        stats->has_been_populated = has_been_populated;
        stats->has_been_populated_ts = has_been_populated_ts;
        stats->populated_by_classes = N;
        stats->stats_header = header;
        stats->custom_stats = custom_stats;
        stats->custom_stat_count = custom_stat_count;

        for (int32_t i = 0; i < STAT_MAX; i++) {
                stats->stats[i] = 0;
        }

        if (stats->custom_stats != NULL) {
                for (int64_t i = 0; i < stats->custom_stat_count; i++) {
                        stats->custom_stats[i] = 0;
                }
        }

}


void init_statistics(struct statistics *stats, struct cats_stats_registry *registry)
{
        if (stats->initialized) {
                log_message(LOG_WARNING, "statistics are already initialised");
        }
        struct statistics empty = {0};
        *stats = empty;
        stats->custom_stat_count = registry->count;
        stats->custom_stats = calloc_or_die(stats->custom_stat_count, sizeof(int64_t));
        stats->initialized = true;
}


void cleanup_statistics(struct statistics *stats)
{
        if (stats->stats_header) free_string_array(&stats->stats_header);
        free(stats->custom_stats);
        stats->custom_stats = NULL;
}


int64_t add_custom_stat(struct cats_stats_registry *registry, const char *name)
{
        int64_t count = registry->count;
        string_array_add(registry->names, name);
        registry->count += 1;
        return count;
}


void init_stats_registry(struct cats_stats_registry *registry)
{
        registry->count = 0;
        registry->names = new_string_array();
}

void cleanup_stats_registry(struct cats_stats_registry *registry)
{
        registry->count = 0;
        free_string_array(&registry->names);
        registry->names = NULL;
}
