// SPDX-License-Identifier: GPL-3.0-or-later
//
// statistics.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_STATISTICS_H
#define CATS_STATISTICS_H
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

struct cats_stats_registry {
    int64_t count;
    struct string_array *names;
};


// STAT_MIN and STAT_MAX are used for enumeration purposes
// CS (CELL STATS) count the number of cells where the value of the specified stat is larger than zero
// GS (GRID SUM) is the sum of the specified stat over the whole simulation area
// MCS (MULTIGRID CELL STATS) as cell cats_stats, but the value does not depend on the value in the cell of a single grid
//                              but on the cells with the same coordinates in all grids
// e.g. CS_POPULATED is the number of cells that have a population larger than zero
// while STAT_GRIDSUM_POPULATED is the sum of all adults of all cells
enum cats_stats {
        STAT_MIN,
        CS_POPULATED,                // FIRST GRID STAT FOR OUTPUT, SEE BELOW - DO NOT CHANGE ORDER
        CS_UNPOPULATED,
        CS_EXCLUDED,
        CS_POPULATED_FIT,
        CS_POPULATED_UNFIT,
        CS_UNPOPULATED_FIT,
        CS_UNPOPULATED_UNFIT,
        CS_POPULATED_UNDER_ZT,
        CS_UNPOPULATED_UNDER_ZT,
        CS_POPULATED_FIT_TS,
        CS_POPULATED_UNFIT_TS,
        CS_UNPOPULATED_FIT_TS,
        CS_UNPOPULATED_UNFIT_TS,
        CS_POPULATED_UNDER_ZT_TS,
        CS_UNPOPULATED_UNDER_ZT_TS, // LAST GRID STAT FOR OUTPUT, SEE BELOW - DO NOT CHANGE ORDER

        MCS_POPULATED_OVER_TS,
        MCS_POPULATED_OVER_0,
        GS_SEEDS_GERMINATED,
        CS_POPULATED_BEFORE_CC,
        CS_POPULATED_AFTER_CC,
        CS_SEEDS_BEFORE_POISSON,
        CS_SEEDS_AFTER_POISSON,
        CS_SEEDS_PRODUCED,
        STAT_MAX
};

#define GRID_STAT_FIRST CS_POPULATED
#define GRID_STAT_LAST CS_UNPOPULATED_UNDER_ZT_TS
#define GRID_STAT_LAST_DIRECT CS_EXCLUDED

/// @brief data structure for collected statistics, both on a per-grid and global basis
struct statistics {
        bool initialized;
        int64_t stats[STAT_MAX];       ///< individual cell stats, on for each \sa enum cats_stats
        int64_t *custom_stats;
        int64_t custom_stat_count;
        FILE *file;                    ///< file handle for output file
        int32_t *populated_by_classes; ///< only used for global stats: how many cells are populated by 1, 2, 3, ... N classes
        char **has_been_populated;     ///< which cells have already been populated at least once?
        char **has_been_populated_ts;  ///< which cells have already been populated (population > population threshold) at least once?
        struct string_array *stats_header;
};

void zero_statistics_stats(struct statistics *stats);

void init_statistics(struct statistics *stats, struct cats_stats_registry *registry);

void cleanup_statistics(struct statistics *stats);
void init_stats_registry(struct cats_stats_registry *registry);
int64_t add_custom_stat(struct cats_stats_registry *registry, const char *name);
#endif //CATS_STATISTICS_H
