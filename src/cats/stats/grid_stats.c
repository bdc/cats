// SPDX-License-Identifier: GPL-3.0-or-later
//
// grid_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "misc/misc.h"
#include "grid_stats.h"
#include "logging.h"
#include "paths/paths.h"
#include "temporal/phase_names.h"
#include "temporal/years.h"

#ifdef USEMPI
#include "mpi/mpi_stats.h"
#endif

#include "inline.h"
#include "memory/arrays.h"
#include "paths/output_paths.h"
#include "memory/cats_memory.h"
#include "inline_overlays.h"
#include "inline_population.h"
#include "populations/population.h"


struct string_array *add_stats_header_overlay(struct string_array *x, struct cats_configuration *conf, bool header)
{
        if (!conf->overlays.have_overlays) return x;


        for (enum overlay_type type = OL_NONE; type < OL_MAX; type++) {
                if (conf->overlays.overlay[type].enabled) {
                        if (header) {
                                char *overlay_name = compound_string("overlay", conf->overlays.overlay[type].name, "::");
                                string_array_add(x, overlay_name);
                                free(overlay_name);
                        } else {
                                string_array_add(x, conf->overlays.overlay[type].filename_loaded);
                        }
                }
        }

        return x;
}


struct string_array *
add_common_stats_header(struct string_array *x, struct cats_configuration *conf, int grid_id, bool header,
                        bool global_stats)
{

        if (header) {
                string_array_add(x, "phase");
                string_array_add(x, "year");
                string_array_add(x, "run");
                string_array_add(x, "replicate");
        } else {
                string_array_add(x, get_phase_shortname(conf->time.phase));
                string_array_add_int64(x, get_phase_year_abs(conf), NULL);
                string_array_add(x, conf->run_name);
                string_array_add_int64(x, conf->simulation.replicate, NULL);
        }


        if (header) {
                string_array_add(x, "species");
                if (!global_stats) {
                        string_array_add(x, "id");
                }
        } else {
                if (global_stats) {
                        string_array_add(x, conf->param[0].species_name_actual);
                } else {
                        string_array_add(x, conf->param[grid_id].species_name);
                        string_array_add_int64(x, grid_id, NULL);
                }
        }

        add_stats_header_overlay(x, conf, header);

        return x;
}


void cleanup_grid_stats(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);
        zero_statistics_stats(&grid->stats);

        if (grid->stats.has_been_populated) {
                free_grid(&grid->stats.has_been_populated, grid->dimension.rows);
                log_message(LOG_IMPORTANT, "cleaning up stats");
                grid->stats.has_been_populated = NULL;
        }

        if (grid->stats.has_been_populated_ts) {
                free_grid(&grid->stats.has_been_populated_ts, grid->dimension.rows);
                log_message(LOG_IMPORTANT, "cleaning up stats ts");
                grid->stats.has_been_populated_ts = NULL;
        }

        if (grid->stats.file) {
                fclose(grid->stats.file);
                log_message(LOG_INFO, "closing stat file of grid %d", grid->id);
        }
}


void consolidate_thread_stats(struct cats_grid *grid, struct cats_configuration *conf, struct cats_thread_info *threads,
                              int num_threads)
{
        struct cats_grid **parent = grid->parent;

        for (int32_t class = 0; class < conf->grid_count; class++) {
                struct cats_grid *g = parent[class];

                for (int32_t i = 0; i < num_threads; i++) {
                        for (int32_t stat_idx = STAT_MIN; stat_idx < STAT_MAX; stat_idx++) {
                                g->stats.stats[stat_idx] += threads[i].stats[class].stats[stat_idx];
                        }

                        if (g->stats.custom_stats) {
                                for (int64_t j = 0; j < threads[i].stats->custom_stat_count; j++){
                                        g->stats.custom_stats[j] += threads[i].stats[class].custom_stats[j];
                                }
                        }
                }
        }
}
/*

#ifndef CATS_ON_WINDOWS
inline
#endif
void
cell_stats(const struct cats_grid **parent, struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
           struct cats_thread_info *ts)
{
        assert(conf->grid_count == 1);
        bool excluded = cell_excluded_by_overlay(conf, row, col);
        const struct cats_grid *grid = parent[0];

        cats_dt_population N = get_adult_population(grid, row, col);

        cats_dt_environment suit = NAN;
        if (grid->param.parametrization == PARAM_HYBRID) suit = get_suitability(grid, row, col);
        increase_stat(&ts->stats[0], N, suit, &grid->param, 0, );
        increase_stat(&ts->stats[0], N, suit, &grid->param, conf->stats_populated_threshold, excluded);


}
*/