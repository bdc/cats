// SPDX-License-Identifier: GPL-3.0-or-later
//
// global_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <stdio.h>
#include <math.h>
#include "configuration/configuration.h"
#include "misc/misc.h"

#include "logging.h"
#include "paths/paths.h"
#include "stats/global_stats.h"
#include "stats/grid_stats.h"
#include "paths/output_paths.h"
#include "memory/cats_memory.h"

#ifdef USEMPI
#include "mpi/mpi_stats.h"
#endif


void initialize_global_stats(struct cats_configuration *conf)
{
        assert(conf != NULL);
        if (conf->grid_count == 1) return;

        char *filename = get_global_stat_filename(conf);
        remove(filename);


        conf->output.statsfile_global = fopen(filename, "a+");
        ENSURE_FILE_OPENED(conf->output.statsfile_global, filename)
        if (conf->output.summary_file) fprintf(conf->output.summary_file, "global-stats,%s,\n",filename);


        write_global_stats(conf, NULL, true);

        fflush(conf->output.statsfile_global);
        free(filename);
}


void write_global_stats(struct cats_configuration *conf, struct cats_grid *grid, bool header)
{
        assert(conf != NULL);
        if (conf->grid_count == 1) return;

        if (header == false) {
                assert(grid != NULL);
        }

        struct shannon_stats shannon;
        double result = NAN;

        if (!header) {
                shannon = calculate_global_stats(conf, grid);
                result = shannon.shannon_sum / (double) shannon.shannon_count;
                if (isnan(result)) result = 0.0;
        }

        struct string_array *x = new_string_array();

        x = add_common_stats_header(x, conf, 0, header, true);

        if (header) {
                string_array_add(x, "mean_shannon");
        } else {
                string_array_add_double(x, result, NULL);
        }

        if (header) {
                string_array_add(x, "popsum_over_0");
                string_array_add(x, "popsum_over_ts");
        } else {
                string_array_add_int64(x, conf->global_stats.stats[MCS_POPULATED_OVER_0], NULL);
                string_array_add_int64(x, conf->global_stats.stats[MCS_POPULATED_OVER_TS], NULL);
        }

        for (int i = 0; i < conf->grid_count + 1; i++) {
                if (header) {
                        int count = snprintf(NULL, 0, ",pop_by_%d_TS", i) + 1;

                        char *tmp = malloc_or_die(count);
                        snprintf(tmp, count, ",pop_by_%d_TS", i);
                        string_array_add(x, tmp);
                        free(tmp);
                } else {
                        string_array_add_int64(x, conf->global_stats.populated_by_classes[i], NULL);
                }
        }

        char *string = string_array_paste(x, ",");
        fprintf(conf->output.statsfile_global, "%s", string);
        fflush(conf->output.statsfile_global);
}


struct shannon_stats calculate_global_stats(struct cats_configuration *conf, struct cats_grid *grid)
{
        assert(grid != NULL);
        struct shannon_stats shannon = {0.0, 0, -314159, -314159};
        if (grid->id != 0) return shannon;

        assert(grid->parent != NULL);
        struct cats_grid **grids = grid->parent;

        get_shannon_sum_grid((const struct cats_grid **) grids, conf, &shannon.shannon_sum, &shannon.shannon_count);

#ifdef USEMPI
#ifndef TESTMPI
        shannon = collect_global_stats_mpi(conf, shannon);
        if (grid->id == 0 && conf->mpi.world_rank == 0) {
                log_message(LOG_IMPORTANT, "SHANNON RESULT: sum %f count %ld = %f", shannon.shannon_sum, shannon.shannon_count, shannon.shannon_sum / (double) shannon.shannon_count);
        } else {
                return shannon;
        }
#else
        log_message(LOG_IMPORTANT, "SKIPPED");
#endif


#else
        log_message(LOG_IMPORTANT, "SHANNON: sum %f count %"PRId64" = %f", shannon.shannon_sum, shannon.shannon_count,
                    shannon.shannon_sum / (double) shannon.shannon_count);
#endif
        return shannon;
}


void reset_global_stats(struct cats_configuration *conf)
{
        assert(conf != NULL);
        assert(conf->global_stats.populated_by_classes != NULL);

        for (int i = 0; i < conf->grid_count + 1; i++) {
                conf->global_stats.populated_by_classes[i] = 0;
        }

        conf->global_stats.stats[MCS_POPULATED_OVER_TS] = 0;
        conf->global_stats.stats[MCS_POPULATED_OVER_0] = 0;

}


void reset_global_stats_counts(struct cats_configuration *conf)
{
        for (int32_t stat_idx = STAT_MIN; stat_idx < STAT_MAX; stat_idx++) {
                conf->global_stats.stats[stat_idx] = 0;
        }

        conf->global_stats.stats[CS_POPULATED_FIT] = 0;
        conf->global_stats.stats[CS_POPULATED_UNFIT] = 0;
        conf->global_stats.stats[CS_UNPOPULATED_FIT] = 0;
        conf->global_stats.stats[CS_UNPOPULATED_UNFIT] = 0;
}

