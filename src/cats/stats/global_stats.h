// SPDX-License-Identifier: GPL-3.0-or-later
//
// global_stats.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_GLOBAL_STATS_H
#define CATS_GLOBAL_STATS_H

#include "configuration/configuration.h"

struct shannon_stats {
        double shannon_sum;
        int64_t shannon_count;
        int64_t popsum_over_ts;
        int64_t popsum_over_0;
};


void initialize_global_stats(struct cats_configuration *conf);

void write_global_stats(struct cats_configuration *conf, struct cats_grid *grid, bool header);

struct shannon_stats calculate_global_stats(struct cats_configuration *conf, struct cats_grid *grid);

void reset_global_stats(struct cats_configuration *conf);

void reset_global_stats_counts(struct cats_configuration *conf);

#endif //CATS_GLOBAL_STATS_H
