// SPDX-License-Identifier: GPL-3.0-or-later
//
// inline_overlays.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_INLINE_OVERLAYS_H
#define CATS_INLINE_OVERLAYS_H

#include "cats_global.h"
#include "assert.h"
#include "overlays/overlays.h"
#include "configuration/configuration.h"


static inline bool
cell_excluded_by_habitat(const struct cats_configuration *config, cats_dt_coord row, cats_dt_coord col)
{
        if (!config->overlays.have_overlays) return false;
        if (!config->overlays.overlay[OL_HABITAT_TYPE_CC].enabled) return false;

        return (config->overlays.habitat_cc->data[row][col] == 0.0);
}


static inline bool
cell_excluded_by_overlay(const struct cats_configuration *config, cats_dt_coord row, cats_dt_coord col)
{
        assert(config != NULL);

        if (config->overlays.have_overlays == false || config->overlays.overlay[OL_EXCLUSION].enabled == false) return false;

        assert(config->overlays.exclusion->data != NULL);

        const char value = config->overlays.exclusion->data[row][col];

        switch (value) { // NOLINT(hicpp-multiway-paths-covered)
                case OL_EXCLUSION_NOT_EXCLUDED:
                        return false;
                case OL_EXCLUSION_EXCLUDED:
                case OL_EXCLUSION_NAN:
                        return true;
        }
        log_message(LOG_ERROR, "unknown value in exclusion mask at row/column::%d,%d::%d",
                    row, col, value);
        exit_cats(EXIT_FAILURE);
}


#endif //CATS_INLINE_OVERLAYS_H
