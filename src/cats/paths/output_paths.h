// SPDX-License-Identifier: GPL-3.0-or-later
//
// output_paths.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_OUTPUT_PATHS_H
#define CATS_OUTPUT_PATHS_H

#include "configuration/configuration.h"

struct string_array *get_output_directory(const struct cats_configuration *conf, const char *directory);

char *get_lambda_grid_name(struct cats_grid *grid, struct cats_configuration *conf, bool density);

struct string_array *
standard_output_file_name(const struct cats_configuration *conf, const char *prefix, const char *infix,
                          const char *suffix);

char *get_current_population_filename(struct cats_grid *grid, struct cats_configuration *conf);

char *get_environment_output_filename(const struct cats_configuration *conf, const struct cats_environment *env);

char *get_current_juvenile_filename(struct cats_grid *grid, struct cats_configuration *conf, int year);

char *get_current_seeds_filename(struct cats_grid *grid, struct cats_configuration *conf, int year);

char *get_current_dispersed_seeds_filename(struct cats_grid *grid, struct cats_configuration *conf);

char *get_global_stat_filename(const struct cats_configuration *conf);

char *get_lambda_stat_filename(const struct cats_configuration *conf, const char *identifier);

char *get_grid_stat_filename(const struct cats_grid *grid, const struct cats_configuration *conf);

#endif //CATS_OUTPUT_PATHS_H
