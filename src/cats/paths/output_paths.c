// SPDX-License-Identifier: GPL-3.0-or-later
//
// output_paths.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <string.h>
#include <assert.h>

#include <logging/logging.h>

#include "data/cats_grid.h"
#include "temporal/phase_names.h"
#include "temporal/years.h"

#include "output_paths.h"
#include "paths.h"


const char *get_base_output_directory(const struct cats_configuration *conf)
{
        if (conf->output.output_directory && strlen(conf->output.output_directory)) {
                return conf->output.output_directory;
        }

        return DEFAULT_OUTPUT_DIRECTORY;
}


struct string_array *get_output_directory(const struct cats_configuration *conf, const char *directory)
{
        struct string_array *dir_list = new_string_array_init(get_base_output_directory(conf));
        string_array_add(dir_list, directory);
        return dir_list;
}


struct string_array *
standard_output_file_name(const struct cats_configuration *conf, const char *prefix, const char *infix,
                          const char *suffix)
{
        struct string_array *filename = new_string_array();
        if (prefix) string_array_add(filename, prefix);

        string_array_add(filename, conf->run_name);
        if (infix) string_array_add(filename, infix);

        add_time_to_filename(filename, conf);
        add_replicate_to_filename(filename, conf);

        if (suffix) string_array_add(filename, suffix);

        return filename;
}


char *get_lambda_grid_name(struct cats_grid *grid, struct cats_configuration *conf, bool density)
{

        assert(grid != NULL);
        assert(conf != NULL);

        int id = grid->id;

        if (conf->grid_count > 1) {
                log_message(LOG_UNIMPLEMENTED, "%s: REGRESSION: output names for more than one class not enabled ",
                            __func__); // REGRESSION
                exit(EXIT_FAILURE);
        }
/*
        if (conf->param[id].generations_max > 1) {
                log_message(LOG_UNIMPLEMENTED, "%s: REGRESSION: output names for more than one generation not enabled",
                            __func__); // REGRESSION
                exit(EXIT_FAILURE);
        }
*/
        struct string_array *path = get_output_directory(conf, "lambda");

        struct string_array *name;
        if (density) {
                name = standard_output_file_name(conf, "lambda-density", conf->param[id].species_name, NULL);
        } else {
                name = standard_output_file_name(conf, "lambda-potential", conf->param[id].species_name, NULL);

        }
        char *extension = get_extension(conf, NULL);
        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);


        return filename;

}


char *get_current_population_filename(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        //int id = grid->id;


        if (conf->grid_count > 1) {
                log_message(LOG_UNIMPLEMENTED, "%s: REGRESSION: output names for more than one class not enabled ",
                            __func__); // REGRESSION
                exit(EXIT_FAILURE);
        }

        struct string_array *path = get_output_directory(conf, "adults");

        char *extension = get_extension(conf, "adults");
        struct string_array *name = standard_output_file_name(conf, NULL, NULL, NULL);
        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}


char *get_environment_output_filename(const struct cats_configuration *conf, const struct cats_environment *env)
{
        struct string_array *path = get_output_directory(conf, "environments");
        struct string_array *name = standard_output_file_name(conf, NULL, env->name, NULL);


        char *extension = get_extension(conf, "env");
        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);
        return filename;
}


char *get_juvenile_name(struct cats_configuration *conf, int grid_id, int class)
{
        struct string_array *name = new_string_array();
        string_array_add(name, conf->param[grid_id].species_name);
        string_array_add_int(name, class, "c%03d");
        char *result = string_array_paste(name, "_");
        free_string_array(&name);
        return result;
}


char *get_seed_name(struct cats_configuration *conf, int grid_id, int class)
{
        struct string_array *name = new_string_array();
        string_array_add(name, conf->param[grid_id].species_name);
        string_array_add_int(name, class, "c%03d");
        char *result = string_array_paste(name, "_");
        free_string_array(&name);
        return result;
}


char *get_current_juvenile_filename(struct cats_grid *grid, struct cats_configuration *conf, int class)
{
        assert(conf != NULL);
        assert(grid != NULL);
        struct string_array *path = get_output_directory(conf, "juveniles");

        char *juvenile_name = get_juvenile_name(conf, grid->id, class);
        struct string_array *name = standard_output_file_name(conf, NULL, juvenile_name, NULL);
        free(juvenile_name);

        char *extension = get_extension(conf, "juveniles");
        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}


char *get_current_seeds_filename(struct cats_grid *grid, struct cats_configuration *conf, int class)
{
        assert(conf != NULL);
        assert(grid != NULL);
        struct string_array *path = get_output_directory(conf, "seeds");

        char *seed_name = get_seed_name(conf, grid->id, class);
        struct string_array *name = standard_output_file_name(conf, NULL, seed_name, NULL);
        free(seed_name);


        char *extension = get_extension(conf, "seeds");
        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}


char *get_current_dispersed_seeds_filename(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(conf != NULL);
        assert(grid != NULL);
        struct string_array *path = get_output_directory(conf, "seeds");

        struct string_array *name = new_string_array();
        string_array_add(name, conf->run_name);
        int id = grid->id;

        if (conf->param_count > 1) {
                string_array_add(name, conf->param[id].species_name);
        }

        string_array_add(name, get_phase_shortname(conf->time.phase));
        string_array_add_int(name, get_current_year(&conf->time), conf->time.format_string);
        string_array_add_int(name, conf->simulation.replicate, "r%03d");

        char *extension = get_extension(conf, "dispersed");

        char *filename = assemble_filename(path, name, "_", extension);

        free_string_array(&path);
        free_string_array(&name);
        free(extension);

        return filename;
}


char *get_global_stat_filename(const struct cats_configuration *conf)
{
        assert(conf != NULL);

        if (conf->grid_count > 1) {
                log_message(LOG_UNIMPLEMENTED, "%s: output names for more than one class not enabled ",
                            __func__); // REGRESSION
                exit(EXIT_FAILURE);
        }

        assert(conf != NULL);
        struct string_array *path = get_output_directory(conf, "stats");
        struct string_array *name = new_string_array();
        string_array_add(name, conf->run_name);
        string_array_add(name, conf->param[0].species_name);
        string_array_add_int(name, conf->simulation.replicate, "r%03d");
        string_array_add(name, "global");
        //string_array_add(name, VERSION);
        char *filename = assemble_filename(path, name, "_", "csv");
        free_string_array(&path);
        free_string_array(&name);

        return filename;
}


char *get_lambda_stat_filename(const struct cats_configuration *conf, const char *identifier)
{
        assert(conf != NULL);


        assert(conf != NULL);
        struct string_array *path = get_output_directory(conf, "lambda-stats");
        struct string_array *name = new_string_array();
        string_array_add(name, conf->run_name);
        string_array_add(name, conf->param[0].species_name);
        if (conf->command_line_options.lambda_cc > 0) {
                string_array_add_int(name, conf->command_line_options.lambda_cc, "xcc-%d");
        }
        if (identifier != NULL) {
                string_array_add(name, identifier);
        }
        string_array_add_int(name, conf->simulation.replicate, "r%03d");
        char *filename = assemble_filename(path, name, "_", "csv");
        free_string_array(&path);
        free_string_array(&name);

        return filename;
}


char *get_grid_stat_filename(const struct cats_grid *grid, const struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        const int32_t id = grid->id;


        if (id > 0) {
                log_message(LOG_UNIMPLEMENTED, "output names for more than one class not enabled"); // REGRESSION
                exit(EXIT_FAILURE);
        }
/*
        if (conf->param[id].generations_max > 1) {
                log_message(LOG_UNIMPLEMENTED, "output names for more than one generation not enabled"); // REGRESSION
                exit(EXIT_FAILURE);
        }
        assert(conf != NULL);
*/
        struct string_array *path = get_output_directory(conf, "stats");

        struct string_array *name = new_string_array();
        string_array_add(name, conf->run_name);
        string_array_add_int(name, conf->simulation.replicate, "r%03d");
        char *filename = assemble_filename(path, name, "_", "csv");
        free_string_array(&path);
        free_string_array(&name);

        return filename;
}