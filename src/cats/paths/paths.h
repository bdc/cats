// SPDX-License-Identifier: GPL-3.0-or-later
//
// paths.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include <stdint.h>
#include <cats_strings/cats_strings.h>

struct cats_configuration;
struct cats_environment_variable;
struct cats_environment;
struct cats_grid;

void add_time_to_filename(struct string_array *filename, const struct cats_configuration *conf);

char *get_extension(const struct cats_configuration *conf, const char *suffix);

char *assemble_path(const struct string_array *path);

char *
get_environment_filename(const struct cats_configuration *conf, const struct cats_environment_variable *pred, int year);


char *get_initial_population_filename(const char *basename);

void add_replicate_to_filename(struct string_array *filename, const struct cats_configuration *conf);
