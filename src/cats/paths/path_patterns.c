// SPDX-License-Identifier: GPL-3.0-or-later
//
// path_patterns.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "path_patterns.h"
#include "temporal/timeformat.h"


char *filename_pattern_substitution(const char *pattern, const struct cats_configuration *conf, int32_t year)
{
        char *time_string = formatted_year(&conf->time, year);
        char *year_pattern = replace_substring(pattern, "%Y", time_string);
        char *replicate_string = NULL;
        int rc = asprintf(&replicate_string, "%d", conf->simulation.replicate);
        asprintf_check(rc);
        char *replicate_pattern = replace_substring(year_pattern, "%n", replicate_string);
        free(year_pattern);
        free(time_string);
        free(replicate_string);
        return replicate_pattern;
}