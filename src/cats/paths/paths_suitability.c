// SPDX-License-Identifier: GPL-3.0-or-later
//
// paths_suitability.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <stdlib.h>
#include "configuration/configuration.h"
#include "environment/environment.h"
#include "temporal/timeformat.h"
#include "data/species_parameters.h"
#include "path_patterns.h"


char *
get_environment_filename(const struct cats_configuration *conf, const struct cats_environment_variable *environment,
                         int32_t year)
{
        assert(conf != NULL);
        assert(environment != NULL);

        struct string_array *name = new_string_array();

        char *pattern = filename_pattern_substitution(environment->pattern, conf, year);

        string_array_add(name, pattern);
        free(pattern);

        char *filename = NULL;
        filename = assemble_filename(NULL, name, "", "");


        free_string_array(&name);
        return filename;
}

