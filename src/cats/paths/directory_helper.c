// SPDX-License-Identifier: GPL-3.0-or-later
//
// directory_helper.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <string.h>
#include <logging/logging.h>
#include "misc/filesystem.h"
#include "paths/paths.h"
#include "paths/output_paths.h"
#include "directory_helper.h"
#include "data/species_parameters.h"
#include "data/error.h"

bool check_and_create_directory_if_needed(const struct string_array *path)
{
        if (path == NULL || path->count == 0) {
                fprintf(stderr, "Unable to check directory - no path specified\n");
                log_message(LOG_ERROR, "\tunable to check directory - no path specified");
                exit_cats(E_SYSTEM_ERROR);
        }

        char *dir = assemble_path(path);
        bool exists = dir_exists(dir);
        if (!exists) {
                fprintf(stderr, "directory '%s' does not exist. Trying to create.\n", dir);
                log_message(LOG_WARNING, "\tdirectory '%s' does not exist. Trying to create.", dir);
                make_directory(dir);
        }

        bool writeable = dir_writeable(dir);
        free(dir);
        return writeable;
}


struct string_array *get_needed_directories(const struct cats_configuration *conf)
{
        struct string_array *array = new_string_array();
        string_array_add(array, "adults");
        string_array_add(array, "stats");


        if (conf->output.write_all && conf->output.have_glm) {
                string_array_add(array, "environments");
        }


        if (conf->output.write_all) {
                string_array_add(array, "seeds");
                string_array_add(array, "juveniles");
        }


        if (conf->command_line_options.output_directory) {
        }

        if (conf->timing_info) string_array_add(array, "performance");

        for (int32_t i = 0; i < conf->param_count; i++) {
                if (conf->param[i].save_lambda_grid || conf->param[i].save_lambda_grid_density) {
                        string_array_add(array, "lambda");
                        break;
                }
        }

        return array;
}


void ensure_needed_directories_exist(const struct cats_configuration *conf, const struct string_array *output_dirs)
{
        log_message(LOG_INFO, "%s: Checking output directories", __func__);


        struct string_array *output = new_string_array_init(conf->output.output_directory);
        check_and_create_directory_if_needed(output);
        free_string_array(&output);
        for (int32_t i = 0; i < output_dirs->count; i++) {
                struct string_array *path = get_output_directory(conf, output_dirs->string[i]);
                check_and_create_directory_if_needed(path);
                free_string_array(&path);


        }

        free_string_array(&output);
}

void setup_module_directories(struct cats_configuration *conf)
{
        struct string_array *dirs_to_check = conf->output.needed_module_output_directories;
        ensure_needed_directories_exist(conf, dirs_to_check);
}


void setup_directories(struct cats_configuration *conf)
{
        if (conf->command_line_options.output_directory) {
                log_message(LOG_IMPORTANT, "overriding output directory from '%s' to '%s'", conf->output.output_directory,
                            conf->command_line_options.output_directory);
                free(conf->output.output_directory);
                conf->output.output_directory = strdup(conf->command_line_options.output_directory);
        } else {
                log_message(LOG_IMPORTANT, "output directory is '%s'", conf->output.output_directory);
        }

        struct string_array *dirs_to_check = get_needed_directories(conf);
        ensure_needed_directories_exist(conf, dirs_to_check);
        free_string_array(&dirs_to_check);
}