// SPDX-License-Identifier: GPL-3.0-or-later
//
// paths_mpi.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_PATHS_MPI_H
#define CATS_PATHS_MPI_H


#ifdef USEMPI

#include "data/cats_grid.h"

char *get_seeds_received_filename(struct cats_grid *grid, struct cats_configuration *conf);

char *get_seeds_produced_filename(struct cats_grid *grid, struct cats_configuration *conf);

#endif

#endif //CATS_PATHS_MPI_H
