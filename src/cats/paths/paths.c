// SPDX-License-Identifier: GPL-3.0-or-later
//
// paths.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef  _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <assert.h>
#include <string.h>
#include "cats_global.h"
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "cats_strings/cats_strings.h"


#include "temporal/phase_names.h"
#include "temporal/years.h"


void add_replicate_to_filename(struct string_array *filename, const struct cats_configuration *conf)
{
        string_array_add_int(filename, conf->simulation.replicate, "r%03d");
}


void add_time_to_filename(struct string_array *filename, const struct cats_configuration *conf)
{
        string_array_add(filename, get_phase_shortname(conf->time.phase));
        string_array_add_int(filename, get_current_year(&conf->time), conf->time.format_string);
}


char *assemble_path(const struct string_array *path) // better name
{
        return string_array_paste(path, "/");

}


char *assemble_filename(struct string_array *path, struct string_array *filename, char *filesep, char *extension)
{
        char *name_string = string_array_paste(filename, filesep);
        char *result = NULL;

        if (path && path->count > 0) {
                char *path_string = string_array_paste(path, "/");
                if (extension && strlen(extension)) {
                        char *fn = compound_string(name_string, extension, ".");
                        result = compound_string(path_string, fn, DIRECTORY_SEPARATOR);
                        free(fn);
                } else {
                        result = compound_string(path_string, name_string, DIRECTORY_SEPARATOR);
                }
                free(path_string);
        } else {

                if (extension && strlen(extension)) {
                        result = compound_string(name_string, extension, ".");
                } else {
                        result = strdup(name_string);
                }
        }

        free(name_string);
        return result;
}


char *get_extension(const struct cats_configuration *conf, const char *suffix)
{
        assert(conf != NULL);
        assert(conf->geometry.file_format);
        char *extension = NULL;

        if (suffix && strlen(suffix)) {
                extension = compound_string(suffix, conf->geometry.file_format, ".");

        } else {
                extension = strdup(conf->geometry.file_format);
        }


        return extension;
}


char *get_initial_population_filename(const char *basename)
{


        struct string_array *name = new_string_array();
        string_array_add(name, basename);

        char *filename = assemble_filename(NULL, name, "_", "");

        free_string_array(&name);
        return filename;
}
