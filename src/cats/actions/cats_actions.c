// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_actions.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include "configuration/configuration.h"
#include "logging.h"
#include "cats_actions.h"
#include "performance/timer.h"

const char *status_strings[] = {"SKIPPED: ",
                                "DONE:    "};


enum cats_simulation_stages get_run_actions(const struct cats_configuration *conf)
{
        switch (conf->time.phase) {
                case PHASE_SIMULATION:
                        return RUN_SIM;
                case PHASE_BURN_IN:
                        return RUN_BURN_IN;
                case PHASE_WARM_UP:
                        return RUN_WARM_UP;
                case PHASE_END:
                        log_message(LOG_ERROR, "Invalid simulation phase");
                        exit(EXIT_FAILURE);

        }

        log_message(LOG_ERROR, "Invalid simulation phase");
        exit(EXIT_FAILURE);
}


void run_simulation_phase(struct cats_grid **grid, struct cats_configuration *conf, int32_t time_step)
{
        int32_t time = setup_current_time_step(conf, time_step);
        reset_global_stats_counts(conf);

        log_message(LOG_MARK, "\nSTARTING %s %d", conf->time.phase_name, time);
        log_message(LOG_INFO, "CYCLE::START::%s::%d", conf->time.phase_name, time);
        struct cats_timer year_timer = start_new_timer();
        enum cats_simulation_stages stage = get_run_actions(conf);


        for (int action = 0; action < conf->cycle.num_actions; action++) {
                if (!(conf->cycle.actions[action].stages & stage)){
                        continue;
                }
                conf->cycle.current_action = action;

                struct cats_timer timer = start_new_timer();
                log_message(LOG_IMPORTANT, "START:   %s : %"PRId64"",
                            conf->cycle.actions[action].message, conf->cycle.action_counter);
                bool did_run = false;

                for (int32_t class = 0; class < conf->grid_count; class++) {
                        int result = (conf->cycle.actions[action].func)(grid[class], conf);
                        if (!did_run) did_run = (result == ACTION_RUN);
                }

                // log results
                if (did_run) {
                        stop_and_log_timer(&timer, conf->cycle.actions[action].message, LOG_INFO, &conf->time, conf);
                } else {
                        stop_timer(&timer);
                }
                log_message(LOG_IMPORTANT, "%s %s : %"PRId64"\n", status_strings[did_run],
                            conf->cycle.actions[action].message,
                            conf->cycle.action_counter);

                conf->cycle.action_counter++;
        }


        stop_and_log_timer(&year_timer, "whole time step", LOG_INFO, &conf->time, conf);
        log_message(LOG_MARK, "\nENDING %s %d: time elapsed %10.3f/%10.3f  seconds", conf->time.phase_name, time,
                    year_timer.time_normal, year_timer.time_real);
        log_message(LOG_INFO, "CYCLE::END::%s::%d::%f::%f", conf->time.phase_name, time, year_timer.time_normal,
                    year_timer.time_real);

}
