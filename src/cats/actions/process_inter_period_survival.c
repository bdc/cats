// SPDX-License-Identifier: GPL-3.0-or-later
//
// process_inter_period_survival.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdlib.h>
#include <assert.h>
#include "threading/threading-helpers.h"
#include "plants/inter_period.h"
#include "process_inter_period_survival.h"
#include "inline.h"
#include "inline_overlays.h"
#include "inline_carrying_capacity.h"
#include "actions/cats_actions.h"
#include "populations/plant_juveniles.h"

void area_inter_period_survival(struct cats_grid *grid, struct cats_thread_info *ts);


void write_stats(struct cats_grid *grid, const struct cats_configuration *conf, FILE *f, const char *identifier)
{
        struct cats_grid *g = grid;
        const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);
        const int32_t max_age_of_maturity = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);
        cats_dt_population_sum j_sum = get_juvenile_sum(g, 0, 0);
        cats_dt_population_sum j_sum_weighted = 0;
        if (j_sum > 0) {
                j_sum_weighted = get_weighted_juvenile_sum(g, 0, 0);
        }
        fprintf(f, "%s,%d,%d,%d,%f,%Lf,%"PRId64",%"PRId64",%f",
                identifier,
                conf->time.year_current,
                get_adult_population(g, 0, 0),
                get_adult_carrying_capacity(g, 0, 0),
                get_suitability(g, 0, 0),
                g->param.scale_factor,
                j_sum,
                j_sum_weighted,
                get_seed_sum(g, 0, 0));

        fprintf(f, ",%f", g->dispersed_seeds[0][0]);
        for (int32_t i = 0; i < seed_persistence; i++) {
                if (g->seed_bank[0][0]) {
                        fprintf(f, ",%f", g->seed_bank[0][0][i]);
                } else {
                        fprintf(f, ",%f", 0.0);

                }
        }
        for (int32_t i = 0; i < max_age_of_maturity + 1; i++) {
                if (g->juveniles[0][0]) {
                        fprintf(f, ",%d", g->juveniles[0][0][i]);
                } else {

                        fprintf(f, ",%d", 0);
                }
        }
        fprintf(f, "\n");

        fflush(f);

}


enum action_status inter_period_survival_action(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);


        if (conf->command_line_options.lambda_test) {
                write_stats(grid, conf, conf->output.lambda_stat_file, "pre-trans");
        }


        threaded_action(&area_inter_period_survival, grid, conf, TS_DEFAULT);

        if (conf->command_line_options.lambda_test) {
                write_stats(grid, conf, conf->output.lambda_stat_file, "post-trans");
        }


        return ACTION_RUN;
}


void area_inter_period_survival(struct cats_grid *grid, struct cats_thread_info *ts)
{
        assert(grid != NULL);
        assert(ts != NULL);


        struct cats_configuration *conf = grid->conf;
        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;
        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;

        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        inter_period_survival(grid, conf, row, col, ts);
                }
        }

}
