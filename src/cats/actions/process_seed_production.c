// SPDX-License-Identifier: GPL-3.0-or-later
//
// process_seed_production.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "configuration/configuration.h"
#include "threading/threading-helpers.h"
#include "plants/seeds.h"

#ifdef CATS_DEBUG_THREADING
#include "debug/debug.h"
#endif

#include "inline_overlays.h"
#include "actions/cats_actions.h"


enum action_status process_produce_seeds(struct cats_grid *data, struct cats_configuration *config)
{
        assert(config != NULL);
        assert(data != NULL);

        if (config->command_line_options.lambda_test) {
                cell_seed_production(data, 0, 0, data->single_thread);
                return ACTION_RUN;
        }


        struct cats_grid **parent = data->parent;
        for (int32_t i = 0; i < config->grid_count; i++) {
                struct cats_grid *g = parent[i];
                g->stats.stats[CS_SEEDS_PRODUCED] = 0;
        }


        // seed production can be done independently for each grid
        threaded_action(&area_seed_production, data, config, TS_DEFAULT);

        return ACTION_RUN;
}


void area_seed_production(struct cats_grid *grid, struct cats_thread_info *ts)
{
        assert(ts != NULL);
        assert(grid != NULL);
        assert(grid->seeds_produced != NULL);

        struct cats_configuration *conf = ts->conf;
        assert(conf != NULL);


        for (cats_dt_coord row = ts->area.start_row; row < ts->area.end_row; row++) {
                for (cats_dt_coord col = ts->area.start_col; col < ts->area.end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        cell_seed_production(grid, row, col, ts);
                }
        }

}
