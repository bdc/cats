// SPDX-License-Identifier: GPL-3.0-or-later
//
// action_population_grid_output.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>

#include "inline.h"
#include "inline_carrying_capacity.h"

#include "actions/cats_actions.h"
#include "configuration/configuration.h"
#include "grids/gdal_save.h"
#include "lambda/lambda-grid.h"
#include "temporal/years.h"


enum action_status action_save_adults(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(conf != NULL);
        assert(grid != NULL);
        if (conf->command_line_options.lambda_test) {
                if (grid->param.parametrization == PARAM_HYBRID) {
                        log_message(LOG_RAW, "POP,%d,%d,%f,%f,%f,%d\n", conf->time.year_current,
                                    get_adult_population(grid, 0, 0),
                                    get_suitability(grid, 0, 0), (double) grid->param.OT,
                                    (double) grid->param.scale_factor, get_adult_carrying_capacity(grid, 0, 0));
                }
                return ACTION_NOT_RUN;
        }


        if (!is_output_year(&conf->time)) return ACTION_NOT_RUN;

        save_population_to_gdal(grid, conf);

        if (conf->output.write_all) {
                save_juveniles_to_gdal(grid, conf);
                save_seeds_to_gdal(grid, conf);
        }

        for (int32_t i = 0; i < conf->param_count; i++) {
                if (conf->param[i].save_lambda_grid) {
                        save_lambda_grid(conf, grid, false);
                        //conf->param[i].save_lambda_grid = false;
                }

                if (conf->param[i].save_lambda_grid_density) {
                        save_lambda_grid(conf, grid, true);
                        //conf->param[i].save_lambda_grid_density = false;
                }
        }

        return ACTION_RUN;
}

