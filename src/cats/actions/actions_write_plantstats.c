// SPDX-License-Identifier: GPL-3.0-or-later
//
// actions_write_plantstats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "threading/threading-helpers.h"
#include "configuration/configuration.h"
#include "performance/timer.h"
#include "stats/grid_stats.h"
#include "stats/global_stats.h"
#include "actions/cats_actions.h"
#include "inline_overlays.h"
#include "inline_population.h"
#include "inline.h"

#ifdef USEMPI
#include "mpi/mpi_stats.h"
#endif

void area_gather_stats(struct cats_grid *grid, struct cats_thread_info *ts);


static inline void increase_stat(struct statistics *stats, cats_dt_population_sum N, cats_dt_rates suitability,
                                 const struct cats_species_param *param, cats_dt_population threshold, cats_dt_rates ZT,
                                 cats_dt_rates OT)
{
        // only do this for threshold == 0, so it does not get updated on the second pass
        if (threshold == 0) {
                if (N > 0) {
                        stats->stats[CS_POPULATED]++;
                } else {
                        stats->stats[CS_UNPOPULATED]++;
                }
        }



        // anything below only relevant for hybrid mode
        if (param->parametrization != PARAM_HYBRID) return;


        int32_t pop_fit = CS_POPULATED_FIT;
        int32_t pop_unfit = CS_POPULATED_UNFIT;
        int32_t unpop_fit = CS_UNPOPULATED_FIT;
        int32_t unpop_unfit = CS_UNPOPULATED_UNFIT;
        int32_t pop_under_zt = CS_POPULATED_UNDER_ZT;
        int32_t unpop_under_zt = CS_UNPOPULATED_UNDER_ZT;

        if (threshold > 0) {
                pop_fit = CS_POPULATED_FIT_TS;
                pop_unfit = CS_POPULATED_UNFIT_TS;
                unpop_fit = CS_UNPOPULATED_FIT_TS;
                unpop_unfit = CS_UNPOPULATED_UNFIT_TS;
                pop_under_zt = CS_POPULATED_UNDER_ZT_TS;
                unpop_under_zt = CS_UNPOPULATED_UNDER_ZT_TS;
        }

        if (N > threshold) {
                if (suitability >= OT) {
                        stats->stats[pop_fit]++;
                } else {
                        stats->stats[pop_unfit]++;
                }

                if (suitability < ZT) {
                        stats->stats[pop_under_zt]++;
                }
        } else {
                if (suitability >= OT) {
                        stats->stats[unpop_fit]++;
                } else {
                        stats->stats[unpop_unfit]++;
                }

                if (suitability < ZT) {
                        stats->stats[unpop_under_zt]++;
                }
        }
}


enum action_status action_write_plant_stats(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);
        if (conf->command_line_options.lambda_test) return ACTION_NOT_RUN;

        // calculate grid statistics
        struct cats_timer timer = start_new_timer();
        if (grid->id == 0) {
                threaded_action(&area_gather_stats, grid, conf, TS_BLOCK);
        }
        stop_and_log_timer(&timer, "grid stats", LOG_INFO, &conf->time, conf);

        // calculate global statistics - only if we have more than one grid, and then only for the first
        if (grid->id == 0 && conf->grid_count > 1) {
                start_timer(&timer);
                reset_global_stats(conf);
                write_global_stats(conf, grid, false);
                stop_and_log_timer(&timer, "global stats", LOG_INFO, &conf->time, conf);
        }

#ifdef USEMPI
#ifdef TESTMPI
        log_message(LOG_ERROR, "MPI SKIPPED STATS");
        return ACTION_NOT_RUN;
#else
        if (grid->id == 0) {
                collect_and_write_plant_stats_mpi(conf, grid);
        }

#endif
#else
        write_grid_stats(conf, grid, false);
#endif
        return ACTION_RUN;
}


void area_gather_stats(struct cats_grid *grid, struct cats_thread_info *ts)
{
        assert(grid != NULL);

        // gathering grid stats is done for all grids at the same time
        if (grid->id > 0) return;
        struct cats_configuration *conf = ts->conf;
        assert(conf != NULL);


        cats_dt_rates ZT = grid->param.ZT;
        cats_dt_rates OT = grid->param.OT;
        cats_dt_population threshold = conf->stats_populated_threshold;

        for (cats_dt_coord row = ts->area.start_row; row < ts->area.end_row; row++) {
                for (cats_dt_coord col = ts->area.start_col; col < ts->area.end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        bool excluded = cell_excluded_by_overlay(conf, row, col);
                        if (excluded) {
                                ts->stats[0].stats[CS_EXCLUDED]++;
                                continue;
                        }
                        cats_dt_population N = get_adult_population(grid, row, col);
                        cats_dt_environment suit = NAN;
                        if (grid->param.parametrization == PARAM_HYBRID) suit = get_suitability(grid, row, col);
                        increase_stat(&ts->stats[0], N, suit, &grid->param, 0, ZT, OT);
                        increase_stat(&ts->stats[0], N, suit, &grid->param, threshold, ZT, OT);


                        //cell_stats(parent, conf, row, col, ts);
                }
        }

}
