// SPDX-License-Identifier: GPL-3.0-or-later
//
// process_clonal_growth.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "logging.h"
#include "configuration/configuration.h"
#include "populations/population.h"
#include "plants/clonal_growth.h"
#include "threading/threading-helpers.h"

#ifdef CATS_DEBUG_THREADING
#include "debug/debug.h"
#endif

#include "inline_vital_rates.h"
#include "inline_overlays.h"
#include "actions/cats_actions.h"

void area_process_clonal_growth(struct cats_grid *grid, struct cats_thread_info *ts);


enum action_status process_clonal_growth(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        // clonal growth is density dependent and only done once for all grids
        if (grid->id > 0) return ACTION_NOT_RUN;

        const int32_t year = conf->time.year_current;

        if (conf->command_line_options.lambda_test) {
                if (year == 0) return ACTION_NOT_RUN;
                cell_clonal_growth(grid, grid->single_thread, 0, 0);
                return ACTION_RUN;

        }

        const cats_dt_rates max_clonal_growth = get_default_vital_rate_maximum_from_grid(grid, VR_CLONAL_GROWTH);
        if (max_clonal_growth == 0.0) return ACTION_NOT_RUN;

        if (conf->time.initial_cg_skipped == false) {
                conf->time.initial_cg_skipped = true;
                log_message(LOG_INFO, "CG  : skipping clonal growth in starting year");
                return ACTION_NOT_RUN;
        }

        threaded_action(&area_process_clonal_growth, grid, conf, TS_DEFAULT);

        return ACTION_RUN;
}


void area_process_clonal_growth(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;
        if (conf->grid_count > 1 && grid->id > 0) return;

        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;
        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;


        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        cell_clonal_growth(grid, ts, row, col);
                }
        }
}
