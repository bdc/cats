// SPDX-License-Identifier: GPL-3.0-or-later
//
// setup_actions.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "configuration/configuration.h"

#include "actions/setup_actions.h"
#include "logging.h"
#include "actions/cats_actions.h"
#include "actions/process_inter_period_survival.h"
#include "cats_strings/cats_strings.h"
#include "dispersal/dispersal.h"
#include "memory/cats_memory.h"


void list_actions(const struct cats_configuration *conf)
{
        log_message(LOG_INFO, "CATS annual cycle consists of %d actions", conf->cycle.num_actions);
        for (int32_t i = 0; i < conf->cycle.num_actions; i++) {
                log_message(LOG_INFO, "\taction % 2d: %s", i, conf->cycle.actions[i].message);
        }
}


const char *ACTION_ALWAYS = "_always";
const char *ACTION_DEFAULT = "_default";


void add_default_actions(struct cats_configuration *conf)
{
        assert(conf != NULL);
        //register_default_action_functions(conf);
        log_message(LOG_INFO, "setting up default actions");

        append_action(conf, action_grid_stats_reset, ALL_STAGES, "statistics reset", ACTION_ALWAYS);
        append_action(conf, action_environment_update, ALL_STAGES, "environment update", ACTION_ALWAYS);
        append_action(conf, action_overlay_update, ALL_STAGES, "overlay update", ACTION_ALWAYS);
        append_action(conf, action_apply_carrying_cap, ALL_STAGES, "carrying capacity application", ACTION_ALWAYS);

        append_action(conf, process_clonal_growth, ALL_STAGES, "clonal growth", ACTION_DEFAULT);
        append_action(conf, action_apply_carrying_cap, ALL_STAGES, "carrying capacity application", ACTION_DEFAULT);
        append_action(conf, process_germination, ALL_STAGES, "germination", ACTION_DEFAULT);
        append_action(conf, process_maturation, ALL_STAGES, "maturation", ACTION_DEFAULT);
        append_action(conf, action_apply_carrying_cap, ALL_STAGES, "carrying capacity application", ACTION_DEFAULT);

        append_action(conf, process_produce_seeds, ALL_STAGES, "seed production", ACTION_DEFAULT);

        append_action(conf, process_cell_local_dispersal, ALL_STAGES, "cell-local dispersal", ACTION_DEFAULT);

        if (conf->dispersal->long_range_enabled) {
                append_action(conf, action_long_range_dispersal, ALL_STAGES,
                              "long range dispersal", ACTION_DEFAULT);
        }

        append_action(conf, process_disperse_seeds, ALL_STAGES, "kernel dispersal", ACTION_DEFAULT);

        append_action(conf, action_write_plant_stats, ALL_STAGES,
                      "statistics calculation and output", ACTION_DEFAULT);
        append_action(conf, action_save_adults, ALL_STAGES, "grid output", ACTION_ALWAYS);
        append_action(conf, inter_period_survival_action, ALL_STAGES,
                      "year-to-year survival and age transition", ACTION_DEFAULT);

}


/*! \brief Sets up and assembles the list of functions for the yearly cycle
 *
 *
 * @param conf the main configuration
 */
void setup_simulation_actions(struct cats_configuration *conf)
{

        if (conf->modules.override_actions_module) {

                // use the functions set up there
        } else {
                add_default_actions(conf);
        }


        list_actions(conf);
}


void
add_action_at(struct cats_configuration *conf, int32_t loc, action_function action, enum cats_simulation_stages when,
              const char *name, const char *module_name)
{
        if (!action_is_registered(conf, action)) {
                log_message(LOG_ERROR, "%s: function '%s' is not registered", __func__, name);
                exit(EXIT_FAILURE);
        }

        log_message(LOG_INFO, "\tadding action '%35s' at location %2d (before: %2d total)", name, loc,
                    conf->cycle.num_actions - 1);

        if (loc >= conf->cycle.num_actions || loc < 0) {
                log_message(LOG_ERROR, "%s: insert location %d for '%s' not in [0, array length %d)", __func__, loc,
                            name, conf->cycle.num_actions);
                exit(EXIT_FAILURE);
        }

        const char *default_string = "UNKNOWN";
        if (!name) name = default_string;
        char *name_real = compound_string(name, "real", " ");


        conf->cycle.actions[loc].func = action;
        conf->cycle.actions[loc].stages = when;
        snprintf(conf->cycle.actions[loc].message, ACTION_MESSAGE_LENGTH, "%-25s", name);
        snprintf(conf->cycle.actions[loc].message_real, ACTION_MESSAGE_LENGTH, "%-25s", name_real);
        free(name_real);
}


/*! \brief adds an action to the list of actions to be run during the yearly cycle
 *
 * @return the number of actions = length of the action list, after the current addition
 * @param conf
 * @param action action action function to be added to the end of the list
 * @param when phases: phases in which the function should be called, can be several enum cats_simulation_stages OR'd together
 * @param name short description string which is used for logging
 *
 */
int32_t
append_action(struct cats_configuration *conf, action_function action, enum cats_simulation_stages when,
              const char *name, const char *module_name)
{
        int32_t n = conf->cycle.num_actions;
        conf->cycle.actions = realloc_or_die(conf->cycle.actions, (n + 1) * sizeof(struct cats_action));
        conf->cycle.num_actions++;
        add_action_at(conf, n, action, when, name, module_name);

        return conf->cycle.num_actions;
}

void list_actions_full(struct cats_configuration *conf)
{

        log_message(LOG_INFO, "Number of registered action functions: %d", conf->action_functions.count);

        for (int32_t i = 0; i < conf->action_functions.count; i++) {
                log_message(LOG_INFO, "%d: %s %p ", i, conf->action_functions.names[i],
                            conf->action_functions.functions[i]);
        }
}


action_function get_action_by_name(struct cats_configuration *conf, const char *name)
{
        for (int32_t i = 0; i < conf->action_functions.count; i++) {
                if (!strcmp(conf->action_functions.names[i], name)) return conf->action_functions.functions[i];
        }
        return NULL;
}

int32_t
append_action_by_name(struct cats_configuration *conf, const char *action_name, enum cats_simulation_stages when,
                      const char *name, const char *module_name)
{
        int32_t n = conf->cycle.num_actions;
        conf->cycle.actions = realloc_or_die(conf->cycle.actions, (n + 1) * sizeof(struct cats_action));
        conf->cycle.num_actions++;
        action_function action = get_action_by_name(conf, action_name);
        if (action == NULL) {
                log_message(LOG_ERROR, "%s: could not find action '%s'", __func__, action_name);
                exit_cats(EXIT_FAILURE);
        }
        add_action_at(conf, n, action, when, name, module_name);

        return conf->cycle.num_actions;
}


void shift_action_functions_down_by_one(struct cats_configuration *conf, int32_t from)
{

        int32_t action_count = conf->cycle.num_actions;
        conf->cycle.actions = realloc_or_die(conf->cycle.actions, (action_count + 1) * sizeof(struct cats_action));
        conf->cycle.num_actions += 1;

        for (int32_t i = action_count; i > from; i--) {
                log_message(LOG_DEBUG, "%s: moving'%s' from %d to %d", __func__, conf->cycle.actions[i].message, i - 1,
                            i);
                conf->cycle.actions[i] = conf->cycle.actions[i - 1];
        }
}


int32_t
add_action_after_function(struct cats_configuration *conf, action_function function, enum cats_simulation_stages when,
                          const char *name, action_function after_function, const char *module_name)
{

        for (int32_t i = conf->cycle.num_actions; i > 0; i--) {
                if (conf->cycle.actions[i - 1].func == after_function) {
                        shift_action_functions_down_by_one(conf, i);
                        add_action_at(conf, i, function, when, name, module_name);
                }
        }
        return conf->cycle.num_actions;
}


void register_action_function(struct cats_configuration *conf, action_function function, const char *name,
                              const char *message)
{
        struct action_function_registry *registry = &conf->action_functions;

        if (registry->count >= MAX_ACTION_FUNCTIONS) {
                log_message(LOG_ERROR, "%s: too many action functions already registered,"
                                       "maximum is %d", __func__, MAX_ACTION_FUNCTIONS);
                exit(EXIT_FAILURE);
        }

        for (int32_t i = 0; i < registry->count; i++) {
                if (strcmp(name, registry->names[i]) == 0 || registry->functions[i] == function) {
                        log_message(LOG_WARNING,
                                    "%s: function '%s', '%p' is already registered: '%s', '%p'",
                                    __func__, name, function, registry->names[i], registry->functions[i]);
                        return;
                }
        }

        registry->functions[registry->count] = function;
        registry->names[registry->count] = strdup(name);
        registry->messages[registry->count] = strdup(message);
        registry->count++;
}


action_function get_action_function(const struct cats_configuration *conf, const char *name)
{
        const struct action_function_registry *registry = &conf->action_functions;

        for (int32_t i = 0; i < registry->count; i++) {
                if (strcmp(name, registry->names[i]) == 0) {
                        return registry->functions[i];
                }
        }

        log_message(LOG_ERROR, "%s: no action function with name '%s' found", __func__, name);
        exit(EXIT_FAILURE);
}


int32_t get_action_registry_id(const struct cats_configuration *conf, const action_function function)
{
        const struct action_function_registry *registry = &conf->action_functions;
        for (int32_t i = 0; i < registry->count; i++) {
                if (registry->functions[i] == function) return i;
        }
        return -1;
}


const char *get_action_registry_name_from_id(const struct cats_configuration *conf, int32_t id)
{
        const struct action_function_registry *registry = &conf->action_functions;
        if (id < 0 || id >= registry->count) {
                log_message(LOG_ERROR, "%s: invalid id %d", __func__, id);
                exit(EXIT_FAILURE);
        }

        return registry->names[id];
}


bool action_is_registered(const struct cats_configuration *conf, const action_function function)
{
        const struct action_function_registry *registry = &conf->action_functions;

        for (int32_t i = 0; i < registry->count; i++) {
                if (*(registry->functions[i]) == function) return true;
        }

        return false;

}


void register_default_action_functions(struct cats_configuration *conf)
{

        register_action_function(conf, action_grid_stats_reset, "action_reset_grid_stats", "resetting stats");
        register_action_function(conf, action_environment_update, "action_load_environments", "loading suitability");
        register_action_function(conf, action_overlay_update, "action_overlay_update", "loading overlays");
        register_action_function(conf, action_apply_carrying_cap, "action_apply_carrying_cap", "carrying capacity");
        register_action_function(conf, process_clonal_growth, "process_clonal_growth", "clonal growth");
        register_action_function(conf, process_germination, "action_age_seeds", "aging seeds");
        register_action_function(conf, process_maturation, "action_age_juveniles", "aging juveniles");
        register_action_function(conf, process_produce_seeds, "process_produce_seeds", "producing seeds");
        register_action_function(conf, process_cell_local_dispersal, "process_cell_local_dispersal", "self-dispersal");
        register_action_function(conf, action_long_range_dispersal, "action_long_range_dispersal",
                                 "long range dispersal");
        register_action_function(conf, process_disperse_seeds, "process_disperse_seeds", "dispersing seeds");

        register_action_function(conf, action_write_plant_stats, "action_write_plant_stats",
                                 "gathering and writings stats");
        register_action_function(conf, action_save_adults, "action_save_adults", "save adults");

        register_action_function(conf, inter_period_survival_action, "inter_period_survival_action",
                                 "inter period survival");

}


void quit_if_action_not_registered(struct cats_configuration *conf, action_function function, const char *name)
{
        if (!action_is_registered(conf, function)) {
                log_message(LOG_ERROR, "%s: function '%s' is not registered", __func__, name);
                exit(EXIT_FAILURE);
        }
}


const char *
get_action_registry_name_from_function(const struct cats_configuration *conf, const action_function function)
{
        const struct action_function_registry *registry = &conf->action_functions;

        for (int32_t i = 0; i < registry->count; i++) {
                if (registry->functions[i] == function) return registry->names[i];
        }


        log_message(LOG_ERROR, "%s: function is not registered", __func__);
        exit(EXIT_FAILURE);

}


void replace_function(struct cats_configuration *conf, action_function function, enum cats_simulation_stages when,
                      const char *name, action_function to_replace, const char *module_name)
{
        quit_if_action_not_registered(conf, function, name);
        quit_if_action_not_registered(conf, function, "function to replace");


        struct action_function_registry *registry = &conf->action_functions;

        int32_t replaced = 0;
        for (int32_t i = 0; i < registry->count; i++) {
                if (registry->functions[i] == to_replace) add_action_at(conf, i, function, when, name, module_name);
                replaced += 1;
        }

        log_message(LOG_INFO, "%s: action '%s' has replaced by '%s' %d times",
                    __func__, get_action_registry_name_from_function(conf, function), name, replaced);
}


void cleanup_action_functions(struct action_function_registry *registry)
{
        for (int32_t i = 0; i < registry->count; i++) {
                free(registry->names[i]);
                free(registry->messages[i]);
                free(registry->messages_real[i]);
                registry->functions[i] = NULL;

        }
        registry->count = 0;

}