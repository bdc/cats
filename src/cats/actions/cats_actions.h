// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_actions.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_ACTIONS_H
#define CATS_ACTIONS_H

#include "cats_global.h"
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "threading/threading-helpers.h"

enum action_status {
        ACTION_NOT_RUN = -1, ///< action was not run for this grid and this time step
        ///< (for example because the action is only run once for all grids),
        ///< no output and timing info will be produced
        ACTION_RUN = 0 ///< action was run
};


enum action_status action_overlay_update(struct cats_grid *grid, struct cats_configuration *conf);

void run_simulation_phase(struct cats_grid **grid, struct cats_configuration *conf, int32_t time_step);


enum action_status action_grid_stats_reset(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status action_write_plant_stats(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status action_save_adults(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status action_environment_update(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status action_apply_carrying_cap(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status process_clonal_growth(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status process_germination(struct cats_grid *grid, struct cats_configuration *conf);


enum action_status process_maturation(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status process_produce_seeds(struct cats_grid *data, struct cats_configuration *config);


enum action_status action_long_range_dispersal(struct cats_grid *grid, struct cats_configuration *conf);

enum action_status process_disperse_seeds(struct cats_grid *grid, struct cats_configuration *conf);


void reset_global_stats_counts(struct cats_configuration *conf);

enum action_status process_cell_local_dispersal(struct cats_grid *data, struct cats_configuration *config);

#endif // CATS_ACTIONS_H