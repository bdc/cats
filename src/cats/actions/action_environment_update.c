// SPDX-License-Identifier: GPL-3.0-or-later
//
// action_environment_update.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "configuration/configuration.h"
#include "logging.h"
#include "environment/load_environment.h"
#include "cats_actions.h"


enum action_status action_environment_update(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        if (conf->command_line_options.lambda_test || conf->command_line_options.lambda_gradient) return ACTION_NOT_RUN;

        if (grid->id != 0) {
                return ACTION_NOT_RUN;
        }

        const int32_t year = conf->time.year_current;
        int rc;

        switch (conf->time.phase) {
                case PHASE_BURN_IN:
                        rc = update_environment_burnin(conf, year);
                        break;
                case PHASE_WARM_UP:
                        rc = update_environment_warmup(conf, year);
                        break;
                case PHASE_SIMULATION:
                        rc = update_environment_simulation(conf, year);
                        break;
                default:
                        log_message(LOG_ERROR, "requested phase out ouf bounds: %d", conf->time.phase);
                        exit(EXIT_FAILURE);
        }

        return rc;
}
