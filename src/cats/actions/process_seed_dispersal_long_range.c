// SPDX-License-Identifier: GPL-3.0-or-later
//
// process_seed_dispersal_long_range.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <assert.h>

#include "configuration/configuration.h"
#include "grids/cats_grid.h"
#include "actions/cats_actions.h"
#include "plants/long_range_dispersal.h"
#include "dispersal/dispersal.h"


enum action_status action_long_range_dispersal(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);

        if (grid->dispersal->long_range_enabled == false) return ACTION_NOT_RUN;
        if (!grid->seeds_produced) return ACTION_NOT_RUN;

        disperse_long_range(grid);

        return ACTION_RUN;
}
