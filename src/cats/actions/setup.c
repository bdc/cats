// SPDX-License-Identifier: GPL-3.0-or-later
//
// setup.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <assert.h>

#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "setup.h"
#include "misc/misc.h"
#include "actions/process_inter_period_survival.h"
#include "overlays/overlays.h"
#include "populations/carrying_capacity.h"
#include "stats/lambda_stats.h"
#include "inline_carrying_capacity.h"
#include "temporal/simulation_time.h"
#include "populations/population.h"
#include "dispersal/dispersal.h"


void setup_lambda_test_simulation(struct cats_configuration *conf, struct cats_grid *grid) // FIXME MAYBE MOVE
{
#ifdef USEMPI
        if (conf->mpi.world_rank != 0) {
                return;
        }
#endif
        if (!conf->command_line_options.lambda_test) return;
        if (grid->population == NULL) {
                log_message(LOG_ERROR, "population grid not initialized");
                exit_cats(EXIT_FAILURE);
        }

        cats_dt_coord row = 0;
        cats_dt_coord col = 0;

        grid->suitability->environments[0]->current.values[row][col] = (cats_dt_environment) grid->param.OT; // FIXME -> set suitability

        cats_dt_population target_pop = get_adult_carrying_capacity(grid, row, col) / 5;
        set_population_ignore_cc(grid, row, col, target_pop);
        conf->lambda_pop = target_pop;
        cell_apply_carrying_capacity(grid, grid->single_thread, row, col, false);

        if (conf->time.simulation_length < 100) {
                conf->time.simulation_length = 100;
                log_message(LOG_WARNING, "increasing simulation length to 100 time steps");
        }

        conf->lambda_burn_in_time = conf->time.simulation_length / 5;
        conf->output.lambda_stat_file = create_and_initialize_lambda_stat_file(conf, NULL, grid);
        write_stats(grid, conf, conf->output.lambda_stat_file, "initial");

}


void setup_lambda_gradient_simulation(struct cats_configuration *conf, struct cats_grid *grid) // FIXME MAYBE MOVE
{
#ifdef USEMPI
        if (conf->mpi.world_rank != 0) {
                return;
        }
#endif
        if (!conf->command_line_options.lambda_gradient) return;
        if (grid->population == NULL) {
                log_message(LOG_ERROR, "population grid not initialized");
                exit_cats(EXIT_FAILURE);
        }

        cats_dt_coord rows = conf->geometry.dimension.rows;
        cats_dt_coord cols = conf->geometry.dimension.cols;
        assert(conf->geometry.dimension.cols > 1);

        cats_dt_environment diff = 1.0f / ((cats_dt_environment) conf->geometry.dimension.cols - 1.0f);
        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        grid->suitability->environments[0]->current.values[row][col] = (cats_dt_environment) col * diff;
                }
        }

        conf->geometry.cell_size_x = -1;
        conf->geometry.cell_size_y = 1;

        if (grid->dispersal->geometry->cell_size_x) {
                conf->geometry.cell_size_x = grid->dispersal->geometry->cell_size_x;
                conf->geometry.cell_size_y = grid->dispersal->geometry->cell_size_y;
        }


        cats_dt_population target_pop = (cats_dt_population) get_vital_rate_maximum(&grid->param.carrying_capacity);
        for (cats_dt_coord col = 0; col < cols; col++) {
                set_population_ignore_cc(grid, rows - 1, col, target_pop);
                cell_apply_carrying_capacity(grid, grid->single_thread, rows - 1, col, false);
        }

        if (conf->command_line_options.lambda_years > 0) {
                conf->time.simulation_length = conf->command_line_options.lambda_years;
        }

        if (conf->time.simulation_length < 100) {
                conf->time.simulation_length = 100;
                log_message(LOG_WARNING, "increasing simulation length to 100 time steps");
        }

        conf->lambda_burn_in_time = conf->time.simulation_length / 5;
        conf->output.lambda_stat_file = create_and_initialize_lambda_stat_file(conf, "gradient", grid);
        write_stats(grid, conf, conf->output.lambda_stat_file, "initial");

}


void setup_dispersal_structures(struct cats_dispersal *dispersal, int count) // FIXME MAYBE MOVE
{
        assert(count >= 0);
        if (count == 0) return;

        dispersal->local_dispersal = 0.0;
        dispersal->vector_count = count;
        dispersal->name = NULL;
        dispersal->types = new_raw_1d_array(count, sizeof(enum dispersal_type));
        dispersal->prob_min = new_raw_1d_array(count, sizeof(double));
        dispersal->prob_max = new_raw_1d_array(count, sizeof(double));
        dispersal->radius = new_raw_1d_array(count, sizeof(int32_t));
        dispersal->rows = new_raw_1d_array(count, sizeof(int32_t));
        dispersal->cols = new_raw_1d_array(count, sizeof(int32_t));
        dispersal->p = new_raw_1d_array(count, sizeof(cats_dt_dispersal **));
        dispersal->plin = new_raw_1d_array(count, sizeof(cats_dt_dispersal *));
        dispersal->filenames = new_raw_1d_array(count, sizeof(char *));
        dispersal->geometry = new_raw_1d_array(count, sizeof(struct simulation_geometry));

        dispersal->long_range_radius = 0;
        dispersal->long_range_prob = 0.0;
        dispersal->long_range_target_count = 0;
        dispersal->loaded = false;
        dispersal->max_vector_diameter = 0;
        dispersal->long_range_enabled = false;
        struct simulation_geometry empty_geom = {0};
        for (int i = 0; i < count; i++) {
                dispersal->types[i] = 0;
                dispersal->prob_min[i] = 0.0;
                dispersal->prob_max[i] = 0.0;
                dispersal->radius[i] = 0;
                dispersal->rows[i] = 0;
                dispersal->cols[i] = 0;
                dispersal->p[i] = NULL;
                dispersal->geometry[i] = empty_geom;
        }
}
