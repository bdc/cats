// SPDX-License-Identifier: GPL-3.0-or-later
//
// process_seed_dispersal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "grids/cats_grid.h"
#include "threading/threading-helpers.h"
#include "configuration/configuration.h"
#include "logging.h"
#include "plants/seeds.h"
#include "performance/timer.h"
#include "grids/gdal_save.h"
#include "dispersal/dispersal.h"
#include "threading/threading.h"
#include "dispersal/local_dispersal.h"

#ifdef USEMPI
#include "mpi/mpi_cats.h"
#include "mpi/mpi_seeds.h"
#include "mpi/mpi_debug.h"
#endif

#include "inline_overlays.h"
#include "actions/cats_actions.h"
#include "temporal/years.h"

void area_post_process_seeds(struct cats_grid *grid, struct cats_thread_info *ts);

struct dispersal_stats {
        int64_t produced_cells;
        int64_t seeded_cells_initial;
        int64_t seeded_cell_final;
};


void print_dispersal_stats(struct cats_configuration *conf, struct cats_grid *grid, struct dispersal_stats *stats)
{
        const double area_p = 100.0 / (double) raster_cell_count(grid->dimension);
        log_message(LOG_INFO, "%s %4d: grid %d ('%s') seeds were produced in %"PRIi64" (%0.3f%%) cells",
                    conf->time.phase_name, conf->time.year_current, grid->id, conf->param[grid->id].species_name,
                    stats->produced_cells, area_p * (double) stats->produced_cells);
        log_message(LOG_INFO, "%s %4d: grid %d ('%s') ... and dispersed into %"PRIi64"(%0.3f%%) cells",
                    conf->time.phase_name, conf->time.year_current, grid->id, conf->param[grid->id].species_name,
                    stats->seeded_cells_initial, area_p * (double) stats->seeded_cells_initial);
        log_message(LOG_INFO, "%s %4d: grid %d ('%s') ... after poisson in   %"PRIi64" (%0.3f%%) cells",
                    conf->time.phase_name, conf->time.year_current, grid->id, conf->param[grid->id].species_name,
                    stats->seeded_cell_final, area_p * (double) stats->seeded_cell_final);
}


void area_cell_local_dispersal(struct cats_grid *grid, struct cats_thread_info *ts);


enum action_status process_cell_local_dispersal(struct cats_grid *data, struct cats_configuration *config)
{
        struct cats_grid *grid = (struct cats_grid *) data;
        if (grid->dispersal->local_dispersal <= 0.0) return ACTION_NOT_RUN;


        threaded_action(&area_cell_local_dispersal, grid, config, TS_DISPERSAL);
        return ACTION_RUN;
}


void area_cell_local_dispersal(struct cats_grid *grid, struct cats_thread_info *ts)
{
        assert(grid != NULL);
        assert(ts != NULL);

        if (grid->dispersal->local_dispersal <= 0.0) return;
        struct cats_configuration *conf = ts->conf;

        for (cats_dt_coord row = ts->area.start_row; row < ts->area.end_row; row++) {
                for (cats_dt_coord col = ts->area.start_col; col < ts->area.end_col; col++) {
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        cell_local_dispersal(conf, grid, row, col, ts);
                }
        }
}


enum action_status process_disperse_seeds(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(conf != NULL);
        assert(grid != NULL);

        if (conf->command_line_options.lambda_test) {
                disperse_seeds(grid, 0, 0, grid->single_thread);
                grid->seeds_produced[0][0] = 0.0f;
                cell_post_process_seeds(grid, conf, 0, 0, grid->single_thread);
                return ACTION_RUN;
        }

        // pre dispersal stats
        grid->stats.stats[CS_SEEDS_AFTER_POISSON] = 0;
        grid->stats.stats[CS_SEEDS_BEFORE_POISSON] = 0;

        struct cats_timer timer = start_new_timer();
        threaded_action(&dispersal_wrapper, grid, conf, TS_DISPERSAL);

        stop_log_and_restart_timer(&timer, "actual dispersal", LOG_INFO, &conf->time, conf);

        // cleanup
        stop_log_and_restart_timer(&timer, "cleanup dispersal threads", LOG_INFO, &conf->time, conf);

#ifdef USEMPI
        send_and_receive_dispersed_seeds_mpi(grid, conf);

        if (conf->debug & DEBUG_MPI) {
                mpi_save_locally_dispersed_seeds(conf, grid);
        }
#endif

        if (conf->output.write_all && is_output_year(&conf->time)) {
                save_dispersed_seeds_to_gdal(grid, conf);
        }

        threaded_action(&area_post_process_seeds, grid, conf, TS_DEFAULT);

#ifdef USEMPI
        clean_seeds_0_mpi_area(grid, conf);
#endif

        stop_and_log_timer(&timer, "post dispersal", LOG_INFO, &conf->time, conf);
        // and print them

        struct dispersal_stats stats = {
                .produced_cells = grid->stats.stats[CS_SEEDS_PRODUCED],
                .seeded_cell_final = grid->stats.stats[CS_SEEDS_AFTER_POISSON],
                .seeded_cells_initial = grid->stats.stats[CS_SEEDS_BEFORE_POISSON]
        };
        print_dispersal_stats(conf, grid, &stats);

        return ACTION_RUN;
}


void area_post_process_seeds(struct cats_grid *grid, struct cats_thread_info *ts)
{

        assert(ts != NULL);
        assert(grid != NULL);
        assert(grid->dispersed_seeds != NULL);

        struct cats_configuration *conf = ts->conf;
        assert(conf != NULL);

        for (cats_dt_coord row = ts->area.start_row; row < ts->area.end_row; row++) {
                for (cats_dt_coord col = ts->area.start_col; col < ts->area.end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) {
                                grid->dispersed_seeds[row][col] = 0;
                        } else {
                                cell_post_process_seeds(grid, conf, row, col, ts);
                        }
                }
        }
}


void dispersal_wrapper(struct cats_grid *grid, struct cats_thread_info *ts)
{
        const cats_dt_coord end_row = ts->area.end_row;
        const cats_dt_coord end_col = ts->area.end_col;
        struct cats_configuration *conf = grid->conf;

        for (cats_dt_coord row = ts->area.start_row; row < end_row; row++) {
                for (cats_dt_coord col = ts->area.start_col; col < end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        if (grid->seeds_produced[row][col] > 0.0) disperse_seeds(grid, row, col, ts);
                        grid->seeds_produced[row][col] = 0.0f;
                }
        }
}
