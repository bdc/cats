// SPDX-License-Identifier: GPL-3.0-or-later
//
// process_germination.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "configuration/configuration.h"
#include "plants/seeds.h"
#include "threading/threading-helpers.h"
#include "inline_overlays.h"
#include "actions/cats_actions.h"


enum action_status process_germination(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        if (conf->command_line_options.lambda_test) {
                cell_germination(grid, conf, 0, 0, grid->single_thread);

                return ACTION_RUN;
        }


        if (grid->id > 0) return ACTION_NOT_RUN;

        grid->stats.stats[GS_SEEDS_GERMINATED] = 0;
        threaded_action(&area_germination, grid, conf, TS_BLOCK);
        return ACTION_RUN;
}


void area_germination(struct cats_grid *grid, struct cats_thread_info *ts)
{
        assert(grid != NULL);
        assert(ts != NULL);

        // aging seeds is done for all grids at the same time
        if (grid->id > 0) return;

        //struct cats_grid **parent = grid->parent;
        struct cats_configuration *conf = grid->conf;

        for (cats_dt_coord row = ts->area.start_row; row < ts->area.end_row; row++) {
                for (cats_dt_coord col = ts->area.start_col; col < ts->area.end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        cell_germination(grid, conf, row, col, ts);
                }
        }
}
