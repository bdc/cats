// SPDX-License-Identifier: GPL-3.0-or-later
//
// action_apply_carrying_cap.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include "configuration/configuration.h"
#include "threading/threading-helpers.h"
#include "populations/population.h"
#include "populations/carrying_capacity.h"
#include "actions/cats_actions.h"


void reset_grid_stats_cc(struct statistics *stats)
{
        assert(stats != NULL);
        stats->stats[CS_POPULATED_BEFORE_CC] = 0;
        stats->stats[CS_POPULATED_AFTER_CC] = 0;
}


enum action_status action_apply_carrying_cap(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        if (conf->command_line_options.lambda_test) {
                if (conf->time.year_current <= conf->lambda_burn_in_time) {
                        set_population(grid, 0, 0, conf->lambda_pop);
                }

                cell_apply_carrying_capacity(grid, grid->single_thread, 0, 0, false);
                return ACTION_RUN;
        }


        if (grid->param.initial_population.adjusted == false) {
                adjust_initial_population(conf, grid);
                grid->param.initial_population.adjusted = true;
        }


        // this function is only called once and handles all grids,
        // because the carrying capacity might be density dependent.
        if (grid->id > 0) return ACTION_NOT_RUN;

        struct cats_grid **parent = grid->parent;
        for (int32_t class = 0; class < conf->grid_count; class++) {
                struct cats_grid *g = parent[class];
                reset_grid_stats_cc(&g->stats);
        }

        threaded_action(&apply_carrying_capacity, grid, conf, TS_DEFAULT);
        print_populated_cell_change_carrying_capacity(conf, parent);
        return ACTION_RUN;
}