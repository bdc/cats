// SPDX-License-Identifier: GPL-3.0-or-later
//
// setup_actions.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_SETUP_ACTIONS_H
#define CATS_SETUP_ACTIONS_H

#include "configuration/configuration.h"

#ifndef CATS_STAGES_DEFINITIONS
#define CATS_STAGES_DEFINITIONS
#define ALL_STAGES RUN_SCALE | RUN_BURN_IN | RUN_WARM_UP | RUN_SIM
//#define ALL_EXCEPT_SCALE RUN_BURNIN | RUN_WARMUP | RUN_SIM
//#define SCALE_ONLY RUN_SCALE
#endif

void setup_simulation_actions(struct cats_configuration *conf);

int32_t append_action(struct cats_configuration *conf, action_function action, enum cats_simulation_stages when,
                      const char *name, const char *module_name);

action_function get_action_function(const struct cats_configuration *conf, const char *name);


void register_default_action_functions(struct cats_configuration *conf);

int32_t
add_action_after_function(struct cats_configuration *conf, action_function function, enum cats_simulation_stages when,
                          const char *name, action_function after_function, const char *module_name);

bool action_is_registered(const struct cats_configuration *conf, action_function function);

void replace_function(struct cats_configuration *conf, action_function function, enum cats_simulation_stages when,
                      const char *name, action_function to_replace, const char *module_name);

void cleanup_action_functions(struct action_function_registry *registry);

void list_actions(const struct cats_configuration *conf);

void list_actions_full(struct cats_configuration *conf);

void register_action_function(struct cats_configuration *conf, action_function function, const char *name,
                              const char *message);

int32_t append_action_by_name(struct cats_configuration *conf, const char *action_name, enum cats_simulation_stages when,
                      const char *name, const char *module_name);

#endif //CATS_SETUP_ACTIONS_H
