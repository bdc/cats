// SPDX-License-Identifier: GPL-3.0-or-later
//
// action_overlay_update.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include <string.h>
#include "configuration/configuration.h"
#include "temporal/timeformat.h"
#include "logging.h"
#include "grids/cats_grid.h"
#include "overlays/overlay_exclusion.h"
#include "actions/cats_actions.h"
#include "paths/path_patterns.h"

bool overlay_update_needed(struct cats_configuration *conf, int32_t time, enum overlay_type type);


enum action_status action_overlay_update(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        // overlays are global, we only load them once, for grid 0
        if (grid->id > 0 || conf->overlays.have_overlays == false) return ACTION_NOT_RUN;

        const int32_t year = conf->time.year_current;



        for (enum overlay_type type = OL_NONE; type < OL_MAX; type++) {
                if (type == OL_NONE) continue;

                if (conf->overlays.overlay[type].enabled) {
                        bool updated = update_overlay_if_needed(conf, year, type);

                        if (type == OL_EXCLUSION && updated == true) {
                                destroy_excluded_cells_all_grids(conf, grid);
                        }
                }
        }

        return ACTION_RUN;
}


bool overlay_update_needed(struct cats_configuration *conf, int32_t time, enum overlay_type type)
{
        if (!conf->overlays.overlay[type].enabled) return false;
        int reload_interval = conf->overlays.overlay[type].reload_interval;

        switch (type) {
                case OL_EXCLUSION:
                        if (conf->time.phase != PHASE_SIMULATION && conf->overlays.exclusion == NULL) return true;
                        break;
                case OL_HABITAT_TYPE_CC:
                        if (conf->time.phase != PHASE_SIMULATION && conf->overlays.habitat_cc == NULL) return true;
                        break;
                case OL_RESOURCE:
                        if (conf->time.phase != PHASE_SIMULATION && conf->overlays.resources == NULL) return true;
                        break;
                case OL_MAX:
                case OL_NONE:
                        return false;

                default:
                        log_message(LOG_ERROR, "%s: undefined switch path", __func__);
                        exit_cats(EXIT_FAILURE);
        }

        // once the masks is loaded, it will not reload_interval in burn-in
        if (conf->time.phase == PHASE_BURN_IN) { return false; }

        if (conf->time.phase == PHASE_WARM_UP) {
                log_message(LOG_UNIMPLEMENTED, "warmup mask not yet handled");
                exit_cats(EXIT_FAILURE);
        }

        if (conf->time.phase == PHASE_SIMULATION) {
                int elapsed_time = (time - conf->time.year_start);

                if ((reload_interval && elapsed_time % reload_interval == 0)) return true;
                if (time == conf->time.year_start) return true;
                return false;
        }
        log_message(LOG_ERROR, "unhandled simulation phase %d", conf->time.phase);
        exit_cats(EXIT_FAILURE);
}


char *get_overlay_filename(struct cats_configuration *conf, enum overlay_type type, int32_t time)
{
        assert(conf != NULL);
        char *pattern = conf->overlays.overlay[type].filename;
        if (conf->overlays.overlay[type].is_static) {
                return strdup(pattern);
        }

        char *result = filename_pattern_substitution(pattern, conf, time);

        return result;

}


bool update_overlay_if_needed(struct cats_configuration *conf, int32_t time, enum overlay_type type)
{
        if (overlay_update_needed(conf, time, type) == false) {
                log_message(LOG_INFO, "OVRL: reusing old overlay for overlay '%s'", conf->overlays.overlay[type].name);
                return false;
        }


        char *overlay_filename = get_overlay_filename(conf, type, time);
        log_message(LOG_INFO, "OVRL: overlay '%s': loading from file '%s'", conf->overlays.overlay[type].name,
                    overlay_filename);
        load_overlay_from_file(conf, type, overlay_filename);

        log_message(LOG_INFO, "OVRL: overlay '%s': loaded from file '%s'", conf->overlays.overlay[type].name, overlay_filename);
        free(overlay_filename);


        return true;
}
