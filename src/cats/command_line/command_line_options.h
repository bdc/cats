// SPDX-License-Identifier: GPL-3.0-or-later
//
// command_line_options.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_COMMAND_LINE_OPTIONS_H
#define CATS_COMMAND_LINE_OPTIONS_H

#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>
#include "logging/logging.h"
#include "cats_global.h"


#define OPTION_LOG_FILE 1111
#define OPTION_LOG_FILE_NAME "log-file"

#define OPTION_SUMMARY_FILE 1112
#define OPTION_SUMMARY_FILE_NAME "summary-file"

#define OPTION_DEBUG_MPI 4001
#define OPTION_DEBUG_MPI_NAME "debug-mpi"

#define OPTION_DEBUG_CONFIG_PARSING 5001
#define OPTION_DEBUG_CONFIG_PARSING_NAME "debug-config-parsing"

#define OPTION_DEBUG_CONFIG_UNUSED 5002
#define OPTION_DEBUG_CONFIG_UNUSED_NAME "debug-config-unread"

#define OPTION_DEBUG_CONFIG_UNSPECIFIED 5003
#define OPTION_DEBUG_CONFIG_UNSPECIFIED_NAME "debug-config-unspecified"

#define OPTION_DEBUG_LAMBDA 5100
#define OPTION_DEBUG_LAMBDA_NAME "debug-lambda"

#define OPTION_DEBUG_VR 5040
#define OPTION_DEBUG_VR_NAME "debug-vital-rates"

#define OPTION_DEBUG_VR_SUBDIVISIONS 5041
#define OPTION_DEBUG_VR_SUBDIVISIONS_NAME "debug-vital-rates-subdivisions"

#define OPTION_DEBUG_VR_SCALE 5042
#define OPTION_DEBUG_VR_SCALE_NAME "debug-vital-rates-scale"

#define OPTION_DEBUG_VR_OT 5043
#define OPTION_DEBUG_VR_OT_NAME "debug-vital-rates-ot"

#define OPTION_DEBUG_VR_ZT 5044
#define OPTION_DEBUG_VR_ZT_NAME "debug-vital-rates-zt"

#define OPTION_POISSON_DAMPENING 6001
#define OPTION_POISSON_DAMPENING_NAME "poisson-dampening"

#define OPTION_POISSON_DAMPENING_MIN 6002
#define OPTION_POISSON_DAMPENING_MIN_NAME "poisson-dampening-min"

#define OPTION_POISSON_MAXIMUM 6003
#define OPTION_POISSON_MAXIMUM_NAME "poisson-maximum"

#define OPTION_POISSON_MAX_DRAW_DIFF 6004
#define OPTION_POISSON_MAX_DRAW_DIFF_NAME "poisson-maximum-draw-difference"

#define OPTION_SCALE_GRADIENT 7777
#define OPTION_SCALE_GRADIENT_NAME "scale-gradient"

#define OPTION_SCALE_LAMBDA_ONLY 7776
#define OPTION_SCALE_LAMBDA_ONLY_NAME "scale-only"

#define OPTION_SCALE_LAMBDA_TEST 7775
#define OPTION_SCALE_LAMBDA_TEST_NAME "scale-test"

#define OPTION_SCALE_LAMBDA_CC 7774
#define OPTION_SCALE_LAMBDA_CC_NAME "scale-test-cc"

#define OPTION_SCALE_LAMBDA_YEARS 7773
#define OPTION_SCALE_LAMBDA_YEARS_NAME "scale-test-years"

#define OPTION_JSON 8000
#define OPTION_JSON_NAME "json"


struct program_options {
        char *summary_file;
        char *log_file;
        bool quiet;
        char *configuration_file;
        int32_t replicate;
        bool no_input_rasters_required;
        double direct_scale_factor;
        char *output_directory;

        bool debug_config;
        bool calculate_lambda_quit;
        bool lambda_gradient;
        int32_t lambda_cc;
        int32_t lambda_years;
        bool lambda_test;
        enum cats_debug_flags debug_flags;

        enum cats_log_level default_log_level;
        bool show_unused_config_values;
        bool show_unspecified_config_values;
        bool have_poisson_dampening_factor;
        bool have_poisson_maximum;
        bool debug_vr;
        bool need_conf;
        bool have_poisson_dampening_minimum;
        bool have_poisson_maximum_draw_diff;
        cats_dt_rates debug_vr_scale;
        cats_dt_rates debug_vr_zt;
        cats_dt_rates debug_vr_ot;
        int32_t debug_vr_subdivisions;
};


struct program_options check_cats_main_arguments(int argc, char **argv);

#endif //CATS_COMMAND_LINE_OPTIONS_H
