// SPDX-License-Identifier: GPL-3.0-or-later
//
// command_line_options.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <string.h>
#include <assert.h>
#include "command_line_options.h"
#include "command_line_info.h"


static struct option longopts[] = {
        {"scale-factor",                       required_argument, NULL, 's',},
        {"replicate",                          required_argument, NULL, 'r',},
        {"output-directory",                   required_argument, NULL, 'o'},
        {"log-level",                          required_argument, NULL, 'l',},
        {"version",                            no_argument,       NULL, 'v',},
        {"help",                               no_argument,       NULL, 'h',},
        {"quiet",                              no_argument,       NULL, 'q'},
        {OPTION_SCALE_LAMBDA_ONLY_NAME,        no_argument,       NULL, OPTION_SCALE_LAMBDA_ONLY},
        {OPTION_SCALE_LAMBDA_TEST_NAME,        no_argument,       NULL, OPTION_SCALE_LAMBDA_TEST},
        {OPTION_SCALE_GRADIENT_NAME,           no_argument,       NULL, OPTION_SCALE_GRADIENT},

        {OPTION_LOG_FILE_NAME,                 required_argument, NULL, OPTION_LOG_FILE},
        {OPTION_SUMMARY_FILE_NAME,             required_argument, NULL, OPTION_SUMMARY_FILE},


        {OPTION_JSON_NAME,                     no_argument,       NULL, OPTION_JSON},
        {OPTION_DEBUG_MPI_NAME,                no_argument,       NULL, OPTION_DEBUG_MPI},
        {OPTION_SCALE_LAMBDA_CC_NAME,          required_argument, NULL, OPTION_SCALE_LAMBDA_CC},
        {OPTION_SCALE_LAMBDA_YEARS_NAME,       required_argument, NULL, OPTION_SCALE_LAMBDA_YEARS},
        {OPTION_DEBUG_CONFIG_PARSING_NAME,     no_argument,       NULL, OPTION_DEBUG_CONFIG_PARSING},
        {OPTION_DEBUG_CONFIG_UNUSED_NAME,      no_argument,       NULL, OPTION_DEBUG_CONFIG_UNUSED},
        {OPTION_DEBUG_CONFIG_UNSPECIFIED_NAME, no_argument,       NULL, OPTION_DEBUG_CONFIG_UNSPECIFIED},
        {OPTION_DEBUG_LAMBDA_NAME,             no_argument,       NULL, OPTION_DEBUG_LAMBDA},
        {OPTION_DEBUG_VR_NAME,                 no_argument,       NULL, OPTION_DEBUG_VR},

        {OPTION_DEBUG_VR_SCALE_NAME,           required_argument, NULL, OPTION_DEBUG_VR_SCALE},
        {OPTION_DEBUG_VR_SUBDIVISIONS_NAME,    required_argument, NULL, OPTION_DEBUG_VR_SUBDIVISIONS},
        {OPTION_DEBUG_VR_OT_NAME,              required_argument, NULL, OPTION_DEBUG_VR_OT},

        {OPTION_POISSON_DAMPENING_NAME,        required_argument, NULL, OPTION_POISSON_DAMPENING},
        {OPTION_POISSON_DAMPENING_MIN_NAME,    required_argument, NULL, OPTION_POISSON_DAMPENING_MIN},
        {OPTION_POISSON_MAXIMUM_NAME,          required_argument, NULL, OPTION_POISSON_MAXIMUM},
        {OPTION_POISSON_MAX_DRAW_DIFF_NAME,    required_argument, NULL, OPTION_POISSON_MAX_DRAW_DIFF},
        {NULL, 0,                                                 NULL, 0}
};


int32_t convert_long_double(char *string, long double *value, const char *name)
{
        bool conversion_success = string_to_long_double(string, value);
        if (!conversion_success) {
                fprintf(stderr,
                        "Option --%s: invalid argument '%s'. Must be a floating point number\n",
                        name, string);
                return 1;
        }
        return 0;
}


int32_t convert_integer(char *string, int32_t *value, const char *name)
{
        bool conversion_success = string_to_integer(string, value);
        if (!conversion_success) {
                fprintf(stderr,
                        "Option --%s: invalid argument '%s'. Must be an integer\n",
                        name, string);
                return 1;
        }
        return 0;
}


int32_t check_integer_greater_than(int32_t value, int32_t valid_min_excl, const char *name, const char *msg)
{
        if (value > valid_min_excl) {
                return 0;
        }

        if (msg != NULL) {
                fprintf(stderr, "Option --%s: %s must be > %d, is %d\n", msg, name, valid_min_excl, value);
        } else {
                fprintf(stderr, "Option --%s: must be > %d, is %d\n", name, valid_min_excl, value);
        }
        return 1;

}


int32_t expect_integer_greater_0(char *string, const char *name, int32_t *value)
{
        int32_t error = convert_integer(string, value, name);
        if (error == 0) {
                error += check_integer_greater_than(*value, 0, name, NULL);
        }
        return error;
}


int32_t expect_long_double_0_1_excl(char *string, const char *name, long double *value)
{
        int32_t error = convert_long_double(string, value, name);

        if (error == 0
            && (*value <= 0 || *value >= 1)) {
                fprintf(stderr, "Option --%s: must be in range (0, 1), is %Lf\n", name, *value);
                error += 1;
        }

        return error;
}


/// @brief assures that CATS was called with at least one command line argument (configuration file) and prints usage otherwise
struct program_options check_cats_main_arguments(int argc, char **argv)
{
        struct program_options options = {
                .replicate = 0,
                .summary_file = NULL,
                .log_file = NULL,
                .quiet = false,
                .direct_scale_factor = -1,
                .default_log_level = LOG_INFO,
                .lambda_test = false,
                .lambda_gradient = false,
                .lambda_cc = -1,
                .lambda_years = -1,
                .calculate_lambda_quit = false,
                .debug_flags = DEBUG_NONE,
                .debug_config = false,
                .output_directory = NULL,
                .show_unspecified_config_values=false,
                .show_unused_config_values=false,
                .have_poisson_dampening_factor=false,
                .have_poisson_maximum=false,
                .have_poisson_dampening_minimum=false,
                .have_poisson_maximum_draw_diff=false,
                .no_input_rasters_required = false,
                .debug_vr = false,
                .need_conf = true,
                .debug_vr_subdivisions = 200,
                .debug_vr_scale = -1.0,
                .debug_vr_ot = -1.0,
                .debug_vr_zt = -1.0};

        int opt;

        int error = 0;
        int option_index = 0;
        // FIXME unify error messages, start with Error:
        while ((opt = getopt_long(argc, argv, "qs:r:l:v:ho:",
                                  longopts, &option_index)) != -1) {

                bool success;
                bool conversion_success;
                float value;
                bool bool_value;
                switch (opt) {
                         case 'o':
                                options.output_directory = strdup(optarg);
                                break;
                        case 'r':
                                success = string_to_integer(optarg, &options.replicate);
                                if (!success) { log_message(LOG_WARNING, "unable to convert %s to number", optarg); }
                                break;
                        case 's':
                                success = string_to_double(optarg, &options.direct_scale_factor);
                                if (!success) { log_message(LOG_WARNING, "unable to convert %s to number", optarg); }
                                break;
                        case 'q':
                                options.quiet = true;
                                break;

                        case 'l':

                                if (strcmp(optarg, "info") == 0) options.default_log_level = LOG_INFO;
                                else if (strcmp(optarg, "important") == 0) options.default_log_level = LOG_IMPORTANT;
                                else if (strcmp(optarg, "warning") == 0) options.default_log_level = LOG_WARNING;
                                else if (strcmp(optarg, "debug") == 0) options.default_log_level = LOG_DEBUG;
                                else if (strcmp(optarg, "error") == 0) options.default_log_level = LOG_ERROR;
                                else {
                                        fprintf(stderr,
                                                "Option --log-level: unknown log level '%s'. Valid values: debug, info, important, warning, error\n",
                                                optarg);
                                        error += 1;
                                }

                                break;
                        case 'h' :
                                print_usage(argv[0], NULL);
                                exit_cats(EXIT_SUCCESS);
                        case 'v' :
                                print_version_info();
                                exit_cats(EXIT_SUCCESS);

                        case OPTION_SCALE_LAMBDA_ONLY:
                                options.calculate_lambda_quit = true;
                                break;
                        case OPTION_JSON:
                                global.enable_json_output = true;
                                break;
                        case OPTION_DEBUG_MPI:
                                options.debug_flags = options.debug_flags | DEBUG_MPI;
                                break;
                        case OPTION_SCALE_LAMBDA_TEST:
                                options.lambda_test = true;
                                break;
                        case OPTION_SUMMARY_FILE:
                                options.summary_file = strdup(optarg);
                                break;
                        case OPTION_SCALE_LAMBDA_CC:
                                conversion_success = string_to_integer(optarg, &options.lambda_cc);
                                if (!conversion_success) {
                                        fprintf(stderr,
                                                "Option --%s: invalid argument '%s'. Valid: integer >= 0.\n",
                                                OPTION_SCALE_LAMBDA_CC_NAME, optarg);
                                        error += 1;
                                } else if (options.lambda_cc < 0) {
                                        fprintf(stderr,
                                                "Option --%s: carrying capacity for scale factor must be > 0 (or 0 to use the original carrying capacity\n",
                                                OPTION_SCALE_LAMBDA_CC_NAME);
                                        error += 1;
                                }

                                break;
                        case OPTION_SCALE_LAMBDA_YEARS:
                                conversion_success = string_to_integer(optarg, &options.lambda_years);
                                if (!conversion_success) {
                                        fprintf(stderr,
                                                "Option --%s: invalid argument '%s'. Valid: integer > 0\n",
                                                OPTION_SCALE_LAMBDA_YEARS_NAME, optarg);
                                        error += 1;
                                }
                                if (options.lambda_years <= 0) {
                                        fprintf(stderr,
                                                "Option --%s: number of simulation years for scale factor must be > 0\n",
                                                OPTION_SCALE_LAMBDA_YEARS_NAME);
                                        error += 1;
                                }

                                break;
                        case OPTION_DEBUG_CONFIG_UNUSED:
                                options.show_unused_config_values = true;
                                break;
                        case OPTION_DEBUG_CONFIG_UNSPECIFIED:
                                options.show_unspecified_config_values = true;
                                break;
                        case OPTION_DEBUG_CONFIG_PARSING:
                                options.show_unspecified_config_values = true;
                                options.show_unused_config_values = true;
                                break;
                        case OPTION_DEBUG_LAMBDA:
                                options.debug_flags = options.debug_flags | DEBUG_LAMBDA;
                                break;
                        case OPTION_DEBUG_VR:
                                options.debug_vr = true;
                                options.need_conf = false;
                                break;
                        case OPTION_DEBUG_VR_SUBDIVISIONS:
                                error += expect_integer_greater_0(optarg, OPTION_DEBUG_VR_SUBDIVISIONS_NAME,
                                                                  &options.debug_vr_subdivisions);


                                break;
                        case OPTION_DEBUG_VR_SCALE:
                                error += expect_long_double_0_1_excl(optarg, OPTION_DEBUG_VR_SCALE_NAME,
                                                                     &options.debug_vr_scale);

                                break;
                        case OPTION_DEBUG_VR_ZT:
                                error += expect_long_double_0_1_excl(optarg, OPTION_DEBUG_VR_ZT_NAME,
                                                                     &options.debug_vr_zt);
                                break;
                        case OPTION_DEBUG_VR_OT:
                                error += expect_long_double_0_1_excl(optarg, OPTION_DEBUG_VR_OT_NAME,
                                                                     &options.debug_vr_ot);
                                break;


                        case OPTION_POISSON_MAXIMUM:
                                conversion_success = string_to_float(optarg, &value);
                                if (conversion_success) {
                                        if (value >= 0.0) {
                                                global.poisson_maximum = value;
                                                options.have_poisson_maximum = true;
                                        } else {
                                                fprintf(stderr, "Option --%s must be >= 0\n",
                                                        OPTION_POISSON_MAXIMUM_NAME);
                                                error += 1;
                                        }
                                } else {
                                        fprintf(stderr, "Option --%s must be a number >= 0\n",
                                                OPTION_POISSON_MAXIMUM_NAME);
                                        error += 1;
                                }
                                break;
                        case OPTION_LOG_FILE:
                                options.log_file = strdup(optarg);
                                break;

                        case OPTION_POISSON_DAMPENING_MIN:
                                conversion_success = string_to_float(optarg, &value);
                                if (conversion_success) {
                                        if (value >= 1.0) {
                                                global.poisson_dampening_minimum = value;
                                                options.have_poisson_dampening_minimum = true;
                                        } else {
                                                fprintf(stderr, "Option --%s must be > 0\n",
                                                        OPTION_POISSON_DAMPENING_MIN_NAME);
                                                error += 1;
                                        }
                                } else {
                                        fprintf(stderr, "Option --%s must be a number > 0\n",
                                                OPTION_POISSON_DAMPENING_MIN_NAME);
                                        error += 1;
                                }
                                break;
                        case OPTION_POISSON_DAMPENING:
                                conversion_success = string_to_float(optarg, &value);
                                if (conversion_success) {
                                        if (value >= 0.0 && value <= 1.0) {
                                                global.poisson_dampening_factor = value;
                                                options.have_poisson_dampening_factor = true;
                                        } else {
                                                log_message(LOG_ERROR,
                                                            "--%s must be in range [0, 1]\n",
                                                            OPTION_POISSON_DAMPENING_NAME);
                                                exit_cats(EXIT_FAILURE);
                                        }
                                } else {
                                        fprintf(stderr,
                                                "Option --%s must be a number in range [0, 1]\n",
                                                OPTION_POISSON_DAMPENING_NAME);
                                        error += 1;
                                }
                                break;
                        case OPTION_SCALE_GRADIENT:
                                options.lambda_gradient = true;
                                break;
                        case OPTION_POISSON_MAX_DRAW_DIFF:
                                conversion_success = string_to_bool(optarg, &bool_value);
                                if (conversion_success) {
                                        global.poisson_maximum_draw_diff = bool_value;
                                        options.have_poisson_maximum_draw_diff = true;
                                } else {
                                        fprintf(stderr,
                                                "Option --%s must be boolean value (0/1/yes/no/true/false)\n",
                                                OPTION_POISSON_MAX_DRAW_DIFF_NAME);
                                        error += 1;
                                }
                                break;
                        default:
                                //print_usage(argv[0], "");
                                exit_cats(EXIT_FAILURE);
                }
        }


        if (options.lambda_gradient || options.lambda_test || options.calculate_lambda_quit) {
                options.no_input_rasters_required = true;
        }

        if (error) {
                fflush(stdout);
                exit_cats(EXIT_FAILURE);
        }

        assert(argc < 2 || optind < argc);
        if (argc > 1 && options.configuration_file == NULL && options.need_conf == true) {
                options.configuration_file = strdup(argv[optind]);
        }

        if (options.configuration_file == NULL && options.need_conf == true) {
                print_usage(argv[0], "no configuration file specified");
        }
        return options;
}
