// SPDX-License-Identifier: GPL-3.0-or-later
//
// command_line_info.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "grids/cats_grid.h"
#include "debug/debug.h"
#include "vital_rates/vital_rates_helper.h"
#include "command_line_options.h"
#include "command_line_info.h"
#include "logging/logging.h"
#include "cats_ini/cats_ini.h"
#include "defaults.h"
#include "cats_global.h"

#include <gsl/gsl_version.h>
#include <gdal_version.h>
#include <string.h>

#include "configuration/configuration.h"


#ifdef CATS_ON_WINDOWS
#include <windows.h>
#else

#include <sys/utsname.h>
#include <unistd.h>

#endif


void print_usage(const char *name, const char *error_msg)
{
        fprintf(stderr, "Usage: %s [OPTIONS] configuration-file  \n", name);
        fprintf(stderr, "\n");
        fprintf(stderr, "Options:\n");
        fprintf(stderr,
                "  -r, --replicate=<replicate>          Replicate number (integer >= 0). Default: 0\n");
        fprintf(stderr,
                "  -o, --output-directory=<path>        Output directory (overriding configuration)\n");
        fprintf(stderr,
                "  -h, --help                           Show this message\n");
        fprintf(stderr,
                "  -v, --version                        Show version information\n");
        fprintf(stderr,
                "  -q, --quiet                          Don't output logging output to console (log file is not affected)\n");
        fprintf(stderr,
                "  -l, --log-level=<level>              Default log level (default info). Allowed values: debug, info, important, warning, error\n");

        fprintf(stderr,
                "  --%s                               Output additional information in JSON format to stderr, prefixed with 'JSON::' ( e.g. for use with a GUI)\n",
                OPTION_JSON_NAME);

        fprintf(stderr,
                "  --%s <logfile>                 Save program output to log file <log-file>\n",
                OPTION_LOG_FILE_NAME);

        fprintf(stderr,
                "  --%s <summary-file>        Save a list of output filenames to the csv file <summary-file>\n",
                OPTION_SUMMARY_FILE_NAME);
        fprintf(stderr, "\n");

        fprintf(stderr, "[Hybrid mode only] Scale factor options:\n");
        fprintf(stderr,
                "  -s, --scale-factor=<scale>           Override scale factor with a floating point value "
                "in range (0, 1).\n");
        fprintf(stderr,
                "  --%s                         Calculate scale factor, display information and quit without running the simulation\n",
                OPTION_SCALE_LAMBDA_ONLY_NAME);
        fprintf(stderr,
                "  --%s                         Verify correctness of scale factor by running a "
                "simulation on 1x1 grid and writing information to a file\n",
                OPTION_SCALE_LAMBDA_TEST_NAME);
        fprintf(stderr,
                "  --%s <cc>                 Override scale factor test carrying capacity "
                "with <cc>\n",
                OPTION_SCALE_LAMBDA_CC_NAME);
        fprintf(stderr,
                "  --%s <years>           Override simulation length of scale factor test "
                "(default: 10000) \n",
                OPTION_SCALE_LAMBDA_YEARS_NAME);
        fprintf(stderr, "  --%s                     Run a simulation on a virtual suitability grid\n", OPTION_SCALE_GRADIENT_NAME);
        fprintf(stderr, "\nRandom number options:\n");
        fprintf(stderr,
                "  --%s                  Which fraction [0.0, 1.0] of random quantities is rounded instead of drawn from a poisson distribution. Default %f\n",
                OPTION_POISSON_DAMPENING_NAME, DEFAULT_POISSON_DAMPENING_FACTOR);
        fprintf(stderr,
                "  --%s              When using dampening > 0, only round if the rounded quantity is at least this value. Default %f\n",
                OPTION_POISSON_DAMPENING_MIN_NAME, DEFAULT_POISSON_DAMPENING_MIN_ROUNDED_TS);
        fprintf(stderr,
                "  --%s                    Maximum parameter when drawing from poisson distribution instead of rounding for performance reasons. Default %f\n",
                OPTION_POISSON_MAXIMUM_NAME, DEFAULT_POISSON_MAXIMUM);
        fprintf(stderr,
                "  --%s    Draw the difference to the maximum from a poisson distribution and add to the rounded remainder, instead of rounding the entire quantity. Default: %s\n",
                OPTION_POISSON_MAX_DRAW_DIFF_NAME, bool_to_string(DEFAULT_POISSON_MAXIMUM_DRAW_DIFF));
        fprintf(stderr, "\n");
        fprintf(stderr, "\nDebugging options:\n");
        fprintf(stderr,
                "  --%s                       Show additional output for problems with lambda\n",
                OPTION_DEBUG_LAMBDA_NAME);
        fprintf(stderr,
                "  --%s                Show entries in the configuration files that were not read and quit\n",
                OPTION_DEBUG_CONFIG_UNUSED_NAME);
        fprintf(stderr,
                "  --%s           Show optional configuration parameters that were not specified and quit\n",
                OPTION_DEBUG_CONFIG_UNSPECIFIED_NAME);
        fprintf(stderr,
                "  --%s               Same as --debug-config-unread --debug-config-unspecified\n",
                OPTION_DEBUG_CONFIG_PARSING_NAME);


#ifdef USEMPI
        fprintf(stderr,
                "  --debug-mpi                   Turn on additional MPI specific debugging output\n");
#endif
        if (error_msg != NULL) {


                fprintf(stderr, "\n");
                fprintf(stderr, "Error: %s\n", error_msg);
        }
        exit(EXIT_FAILURE);
}


/// @brief prints CATS version info (git version, compiler, build and library information)
void print_version_info(void)
{
        log_message(LOG_RAW, "CATS version %s\n\n", VERSION);
        log_message(LOG_RAW, "using libraries:\n");
        log_message(LOG_RAW, "\tGSL  version: %s\n", GSL_VERSION);
        log_message(LOG_RAW, "\tGDAL version: %d.%d.%d\n", GDAL_VERSION_MAJOR, GDAL_VERSION_MINOR, GDAL_VERSION_REV);
        log_message(LOG_RAW, "\n");
        log_message(LOG_RAW, "compilation info:\n");
        log_message(LOG_RAW, "\tdate:     %s\n",__DATE__);
        log_message(LOG_RAW, "\ttime:     %s\n",__TIME__);
        log_message(LOG_RAW, "\tcompiler: %s\n", COMPILER);
        log_message(LOG_RAW, "\n");
}


/// @brief formats and returns the current time
void time_string(char *time_string, size_t length)
{
        time_t zeit = time(NULL);
        struct tm *local_time = localtime(&zeit);
        strftime(time_string, length, "%Y-%m-%d %H:%M:%S", local_time);
}


/// @brief prints CATS run time information (PWD, date, parameters, ...)
void print_runtime_information(int argc, char **argv)
{
        char buffer[100];
        time_string(buffer, 100);

        log_message(LOG_RAW, "run time information:\n");

        char *node_name;
#ifdef CATS_ON_WINDOWS
        node_name = "UNKNOWN";
#else
        struct utsname uts;
        uname(&uts);
        node_name = uts.nodename;
#endif

#ifdef CATS_ON_WINDOWS
        char cwd_buffer[MAX_PATH];
        GetCurrentDirectory(MAX_PATH, cwd_buffer);
        log_message(LOG_RAW, "\tcwd: %s\n", cwd_buffer);
#else
        char *cwd = get_current_dir_name();
        log_message(LOG_RAW, "  cwd:            %s\n", cwd);
        free(cwd);
#endif
        log_message(LOG_RAW, "  date:           %s\n", buffer);
        log_message(LOG_RAW, "  command:        %s\n", argv[0]);
        log_message(LOG_RAW, "  arguments (%d):  ", argc - 1);

        for (int i = 1; i < argc; i++) {
                log_message(LOG_RAW, "%s ", argv[i]);
        }

        log_message(LOG_RAW, "\n");
        // HOSTNAME
        log_message(LOG_RAW, "  host:           %s\n", node_name);
        log_message(LOG_RAW, "\n");
}


void show_run_info(struct cats_configuration *conf, struct program_options *options)
{

        print_config_summary(conf);
        print_vital_rates(conf);


        bool quit = false;
        if (conf->command_line_options.show_unused_config_values) {
                print_unused_values(conf->ini, options->configuration_file);
                quit = true;
        }

        if (conf->command_line_options.show_unspecified_config_values) {
                print_complement_values(conf->ini, options->configuration_file);
                quit = true;
        }
        if (quit) {
                exit_cats(EXIT_SUCCESS);
        }

}