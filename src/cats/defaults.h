// SPDX-License-Identifier: GPL-3.0-or-later
//
// defaults.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_DEFAULTS_H
#define CATS_DEFAULTS_H

#define DEFAULT_POPULATED_THRESHOLD 2
#define SUIT_EPS 1e-6
#define DEFAULT_DEMOGRAPHIC_SLOPE 15.0
#define DEFAULT_POISSON_DAMPENING_MIN_ROUNDED_TS 1.0f
#define DEFAULT_POISSON_DAMPENING_FACTOR 0.0f
#define DEFAULT_POISSON_MAXIMUM 5000.0f
#define DEFAULT_POISSON_MAXIMUM_DRAW_DIFF true

#define MAX_MODULES 16
#define ZERO_EPS 1e-10
#define LAMBDA_EPS  1e-6
#define MAX_ENVIRONMENTS 128
#define MAX_VITAL_RATES 100
#define MAX_VITAL_NAME_LEN 64
#define MAX_VITAL_AGES 10
#define SCALE_LAMBDA_DEFAULT_YEARS 10000
#define DEFAULT_OUTPUT_DIRECTORY "cats-output"
#define MAX_CLASSES 1024

#endif //CATS_DEFAULTS_H
