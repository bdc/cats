// SPDX-License-Identifier: GPL-3.0-or-later
//
// vital_rates_helper.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "configuration/configuration.h"
#include "vital_rates_helper.h"
#include "data/species_parameters.h"


void print_vital_rates(struct cats_configuration *conf)
{
        for (int sp_idx = 0; sp_idx < conf->param_count; sp_idx++) {
                const struct cats_species_param *param = &conf->param[sp_idx];
                log_message(LOG_RAW, "Vital rates for %s\n", conf->param->species_name);
                print_vital_rate(&param->carrying_capacity, param);

                if (param->custom_vital_rates == false) {


                        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {
                                struct cats_vital_rate *rl = &conf->param[sp_idx].vital_rates[vr_idx];
                                print_vital_rate(rl, param);
                        }
                }


                for (int32_t i = 0; i < conf->modules.count; i++) {
                        for (int32_t j = 0; j < param->module_data[i].vital_rate_count; j++) {
                                print_vital_rate(param->module_data[i].vr[j], param);
                        }

                }

        }
}