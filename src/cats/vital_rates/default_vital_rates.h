// SPDX-License-Identifier: GPL-3.0-or-later
//
// default_vital_rates.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_DEFAULT_VITAL_RATES_H
#define CATS_DEFAULT_VITAL_RATES_H

#include "vital_rates/hybrid_functions.h"
#include "configuration/configuration.h"

void postprocess_vital_rates(struct cats_configuration *conf);

void setup_carrying_capacity(struct cats_configuration *conf);

void post_process_vital_rate(struct cats_configuration *conf, struct cats_vital_rate *vr, struct cats_environment *set,
                             struct cats_species_param *param);

const char *get_default_vital_rate_name(struct cats_vital_rate *vr);

enum vital_rate_hybrid_suitability_threshold get_default_vital_rate_hybrid_cutoff(struct cats_vital_rate *vr);


#endif //CATS_DEFAULT_VITAL_RATES_H
