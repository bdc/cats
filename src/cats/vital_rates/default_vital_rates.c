// SPDX-License-Identifier: GPL-3.0-or-later
//
// default_vital_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "vital_rates/default_vital_rates.h"
#include "vital_rates/setup_rates.h"
#include "environment/environment_set.h"
#include "inline.h"


enum vital_rate_hybrid_suitability_threshold get_default_vital_rate_hybrid_cutoff(struct cats_vital_rate *vr)
{
        if (vr->is_carrying_capacity) {
                return HYBRID_SUIT_TS_ZERO;
        }

        enum cats_vital_rate_id vr_idx = vr->default_rate_id;
        switch (vr_idx) {
                case VR_MIN:
                case VR_MAX:
                        break;

                case VR_ADULT_SURVIVAL:
                case VR_SEED_SURVIVAL:
                        //case VR_CARRYING_CAPACITY:
                        return HYBRID_SUIT_TS_ZERO;

                case VR_CLONAL_GROWTH:
                case VR_GERMINATION_RATE:
                case VR_GERMINATION_TO_ADULT_SURVIVAL:
                        return HYBRID_SUIT_TS_ZT;
                case VR_FLOWERING_FREQUENCY:
                case VR_SEED_YIELD:
                        return HYBRID_SUIT_TS_OT;
        }
        log_message(LOG_ERROR, "%s: Unhandled vital rate id %d", __func__, vr_idx);
        exit_cats(EXIT_FAILURE);

}


enum cats_density_dependence get_default_vital_rate_default_density_dependence(struct cats_vital_rate *vr)
{
        if (vr->is_carrying_capacity) return NO_DENSITY_DEP;

        enum cats_vital_rate_id vr_id = vr->default_rate_id;

        switch (vr_id) {
                case VR_MIN:
                case VR_MAX:
                        break;

                case VR_CLONAL_GROWTH:
                case VR_GERMINATION_RATE:
                case VR_GERMINATION_TO_ADULT_SURVIVAL:
                        return DENSITY_DEP_NEGATIVE;


                case VR_FLOWERING_FREQUENCY:
                case VR_ADULT_SURVIVAL:
                case VR_SEED_YIELD:
                case VR_SEED_SURVIVAL:
                        return NO_DENSITY_DEP;
        }

        log_message(LOG_ERROR, "%s: Unhandled vital rate id %d", __func__, vr_id);
        exit_cats(EXIT_FAILURE);
}


void set_vital_rate_default_limits(struct cats_vital_rate *vr)
{
        // first set all to NAN
        vr->lower_limit_excl = NAN;
        vr->upper_limit_excl = NAN;
        vr->lower_limit_incl = NAN;
        vr->upper_limit_incl = NAN;


        switch (vr->default_rate_id) {
                case VR_MIN:
                case VR_MAX:
                        break;

                        // clonal growth: [0, ...]
                case VR_CLONAL_GROWTH:
                        vr->lower_limit_incl = 0.0;
                        break;

                        // integer quantities: [1, ...]
                        //case VR_CARRYING_CAPACITY:
                case VR_SEED_YIELD:
                        vr->lower_limit_incl = 1.0;
                        break;

                        // reproductive rates: (0, 1]
                case VR_GERMINATION_RATE:
                case VR_FLOWERING_FREQUENCY:
                        vr->lower_limit_excl = 0.0;
                        vr->upper_limit_incl = 1.0;
                        break;

                        // permissive survival rates: [0, 1]
                case VR_ADULT_SURVIVAL:
                        vr->lower_limit_incl = 0.0;
                        vr->upper_limit_incl = 1.0;
                        break;

                        // strict survival rates: (0, 1]
                case VR_GERMINATION_TO_ADULT_SURVIVAL:
                case VR_SEED_SURVIVAL:
                        vr->lower_limit_excl = 0.0;
                        vr->upper_limit_incl = 1.0;
                        break;
        }
}


enum cats_rate_function_type get_default_vital_rate_hybrid_function(struct cats_vital_rate *vr)
{
        enum cats_vital_rate_id idx = vr->default_rate_id;
        switch (idx) {
                case VR_MIN:
                case VR_MAX:
                        return LINK_UNASSIGNED;
                case VR_CLONAL_GROWTH:
                        //case VR_CARRYING_CAPACITY:
                case VR_GERMINATION_RATE:
                case VR_FLOWERING_FREQUENCY:
                case VR_ADULT_SURVIVAL:
                case VR_GERMINATION_TO_ADULT_SURVIVAL:
                case VR_SEED_YIELD:
                        return LINK_SUITABILITY_SIGMOID;
                case VR_SEED_SURVIVAL:
                        return LINK_CONSTANT;
        }
        log_message(LOG_ERROR, "%s: unknown vital rate with index %d", __func__, idx);
        exit_cats(EXIT_FAILURE);
}


const char *get_default_vital_rate_name(struct cats_vital_rate *vr)
{
        if (vr->is_carrying_capacity) return "carrying capacity";

        enum cats_vital_rate_id idx = vr->default_rate_id;
        switch (idx) {
                case VR_CLONAL_GROWTH:
                        return "clonal growth rate";
                        //case VR_CARRYING_CAPACITY:
                        //        return "carrying capacity";
                case VR_GERMINATION_RATE:
                        return "germination rate";
                case VR_FLOWERING_FREQUENCY:
                        return "flowering frequency";
                case VR_ADULT_SURVIVAL:
                        return "adult survival rate";
                case VR_GERMINATION_TO_ADULT_SURVIVAL:
                        return "germination to adult survival rate";
                case VR_SEED_SURVIVAL:
                        return "seed survival rate";
                case VR_SEED_YIELD:
                        return "seed yield";
                case VR_MAX:
                case VR_MIN:
                        break;

        }
        log_message(LOG_ERROR, "%s: unknown vital rate with index %d", __func__, idx);
        exit_cats(EXIT_FAILURE);
}


void setup_carrying_capacity(struct cats_configuration *conf)
{
        for (int32_t sp_idx = 0; sp_idx < conf->param_count; sp_idx++) {
                struct cats_vital_rate *vr = &conf->param[sp_idx].carrying_capacity;
                init_cats_vital_rate(vr);
                vr->suitability_cutoff_hint = HYBRID_SUIT_TS_ZERO;
                vr->suitability_cutoff = 0.0;
                vr->is_carrying_capacity = true;
                vr->default_rate_id = VR_UNSPECIFIED;
                vr->is_integer_quantity = true;
                vr->lower_limit_excl = NAN;
                vr->upper_limit_excl = NAN;
                vr->upper_limit_incl = NAN;
                vr->lower_limit_incl = 1.0;
                vr->max_rate = NAN;
                vr->density = NO_DENSITY_DEP;
                set_vital_rate_name(vr, "carrying capacity");
                set_vital_rate_link_hybrid_function(vr, conf, LINK_SUITABILITY_SIGMOID);
        }
}


void setup_default_vital_rates(struct cats_configuration *conf)
{
        for (int32_t sp_idx = 0; sp_idx < conf->param_count; sp_idx++) {
                struct cats_species_param *param = &conf->param[sp_idx];


                for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {

                        struct cats_vital_rate *vr = &param->vital_rates[vr_idx];
                        vr->default_rate_id = vr_idx;
                        set_vital_rate_default_limits(vr);
                        set_vital_rate_name(vr, get_default_vital_rate_name(vr));
                        set_vital_density(vr, get_default_vital_rate_default_density_dependence(vr));
                        set_vital_rate_link_hybrid_function(vr, conf, get_default_vital_rate_hybrid_function(vr));
                        set_vital_rate_suitability_cutoff_hint(vr, get_default_vital_rate_hybrid_cutoff(vr));
                        if (vr_idx == VR_SEED_YIELD) {
                                vr->is_integer_quantity = true;
                        } else {
                                vr->is_integer_quantity = false;
                        }
                }
        }
}


void post_process_vital_rate(struct cats_configuration *conf, struct cats_vital_rate *vr, struct cats_environment *set,
                             struct cats_species_param *param)
{
        if (param->parametrization == PARAM_HYBRID && isnan(vr->suitability_cutoff)) {
                switch (vr->suitability_cutoff_hint) {
                        case HYBRID_SUIT_TS_UNSPECIFIED:
                                log_message(LOG_ERROR,
                                            "%s: vital rate '%s' does not provide information about suitability cutoff",
                                            __func__, vr->name);
                                exit_cats(EXIT_FAILURE);
                                // break;
                        case HYBRID_SUIT_TS_ZT:
                                set_vital_cutoff(vr, param->ZT, param);
                                break;
                        case HYBRID_SUIT_TS_OT:
                                set_vital_cutoff(vr, param->OT, param);
                                break;
                        case HYBRID_SUIT_TS_ZERO:
                                set_vital_cutoff(vr, 0.0, param);
                                break;
                }
        }

        if (vr->environment_set != NULL) set = vr->environment_set;
        if (set != NULL) set_vital_environment_set(vr, set);
}


void postprocess_vital_rates(struct cats_configuration *conf)
{

        for (int32_t sp_idx = 0; sp_idx < conf->param_count; sp_idx++) {

                struct cats_species_param *param = &conf->param[sp_idx];

                log_message(LOG_INFO, "setting up vital rates for species '%s'", param->species_name);


                struct cats_environment *set = NULL;

                if (param->parametrization == PARAM_HYBRID) {
                        set = get_environment(conf, param->suitability_name);
                }

                post_process_vital_rate(conf, &param->carrying_capacity, set, param);


                if (param->demographic_module == NULL) {
                        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {
                                struct cats_vital_rate *vr = &param->vital_rates[vr_idx];
                                vr->default_rate_id = vr_idx;
                                post_process_vital_rate(conf, vr, set, param);
                        }
                }


                for (int32_t i = 0; i < conf->modules.count; i++) {
                        for (int32_t j = 0; j < param->module_data[i].vital_rate_count; j++) {
                                post_process_vital_rate(conf, param->module_data[i].vr[j], set, param);
                        }
                }
        }
}
