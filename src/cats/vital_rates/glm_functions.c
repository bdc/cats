// SPDX-License-Identifier: GPL-3.0-or-later
//
// glm_functions.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "configuration/configuration.h"
#include "logging.h"
#include "inline.h"
#include "glm_functions.h"


cats_dt_rates get_glm(const struct cats_vital_rate *rate_info,
                      cats_dt_rates N,
                      cats_dt_rates K,
                      cats_dt_coord row,
                      cats_dt_coord col)
{
        const struct cats_environment *set = rate_info->environment_set;
        const enum cats_density_dependence density_type = rate_info->density;

        if (set->type != ENVIRONMENT_TYPE_GLM) {
                log_message(LOG_ERROR, "%s: wrong environment set type for set '%s'", __func__, set->name);
                exit(EXIT_FAILURE);
        }

        cats_dt_rates result = set->glm.intercept;

        if (set->glm.type == GLM_QUADRATIC) {
                for (int32_t i = 0; i < set->count; i++) {
                        const cats_dt_rates predictor = set->environments[i]->current.values[row][col];
                        result += predictor * set->glm.linear[i] + predictor * predictor * set->glm.quadratic[i];
                }

        } else if (set->glm.type == GLM_LINEAR) {
                for (int32_t i = 0; i < set->count; i++) {
                        const cats_dt_rates predictor = set->environments[i]->current.values[row][col];
                        result += predictor * set->glm.linear[i];
                }
        } else {
                log_message(LOG_ERROR, "%s: environment set '%s' has unsupported GLM type '%s'", __func__, set->name,
                            glm_type_name(set->glm.type));
                exit(EXIT_FAILURE);
        }


        if (isnan(result)) return 0.0f;

        switch (set->glm.family) {

                case GLM_FAMILY_BERNOULLI:
                case GLM_FAMILY_BINOMIAL:
                        result = 1.0 / (1.0 + expl(-result));
                        break;

                case GLM_FAMILY_UNKNOWN:
                        log_message(LOG_ERROR, "%s: GLM link function not set for environment set '%s'", __func__,
                                    set->name);
                        exit(EXIT_FAILURE);
                case GLM_FAMILY_GAUSSIAN:
                        // link function is identity, we are done
                        break;
                case GLM_FAMILY_POISSON:
                        result = expl(result);
                        break;
        }

        const cats_dt_rates density = density_multiplier(density_type, N, K, rate_info->density_ts);
        return result * density;
}