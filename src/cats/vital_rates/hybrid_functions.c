// SPDX-License-Identifier: GPL-3.0-or-later
//
// hybrid_functions.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <math.h>
#include <assert.h>

#include "configuration/configuration.h"
#include "hybrid_functions.h"
#include "logging.h"
#include "inline.h"


static inline cats_dt_rates hybrid_density_multiplier(const struct cats_vital_rate *rate_info,
                                                      const struct cats_species_param *species_parameter,
                                                      cats_dt_rates suitability,
                                                      cats_dt_rates N,
                                                      cats_dt_rates K)
{

        return density_multiplier(rate_info->density, N, K, rate_info->density_ts);

}


static inline cats_dt_rates
get_rate_scale_factor(const struct cats_vital_rate *rate_info, const struct cats_species_param *species_parameter)
{
        cats_dt_rates scale = species_parameter->scale_factor;
        if (rate_info->have_rate_non_default_scale_factor) {
                if (rate_info->rate_scale_factor != -1.0) {
                        scale = rate_info->rate_scale_factor;
                } else if (rate_info->rate_minimum_scale_factor != -1.0) {
                        scale = max_rates(rate_info->rate_minimum_scale_factor, scale);
                } else {
                        log_message(LOG_ERROR, "%s: invalid state for vital rate '%s' scale factor", __func__,
                                    rate_info->name);
                        exit_cats(EXIT_FAILURE);
                }
        }

        assert(scale >= 0);
        assert(scale <= 1);
        return scale;

}


cats_dt_rates
sigmoid(const struct cats_vital_rate *rate_info,
        const struct cats_species_param *species_parameter,
        cats_dt_rates suitability,
        cats_dt_rates N,
        cats_dt_rates K)
{
        assert(species_parameter != NULL);
        assert(rate_info != NULL);


        if (rate_info->environment_set == NULL) {
                log_message(LOG_ERROR, "%s: environment is NULL", __func__);
                exit_cats(EXIT_FAILURE);
        }

        if (rate_info->environment_set->type != ENVIRONMENT_TYPE_SUITABILITY) {
                log_message(LOG_ERROR, "%s: environment not of type suitability", __func__);
                exit_cats(EXIT_FAILURE);
        }

        const cats_dt_rates max_rate = rate_info->max_rate;


        if (max_rate == 0.0) {
                return max_rate;
        }

        cats_dt_rates scale = get_rate_scale_factor(rate_info, species_parameter);


        const cats_dt_rates rate_at_OT = max_rate * scale;
        assert(max_rate >= 0);


        assert(isnan(max_rate) == false);
        assert(isnan(suitability) == false);
        assert(suitability >= 0);
        assert(suitability <= 1);

        const cats_dt_rates ds = species_parameter->demographic_slope;

        const cats_dt_rates OT = species_parameter->OT;
        assert(max_rate >= 0);

        assert(!isnan(OT));
        assert(!isnan(rate_at_OT));
        assert(!isnan(ds));
        assert(!isnan(scale));
        cats_dt_rates rate;

        rate = max_rate /
               (1.0
                + expl(ds * (OT - suitability))
                  * (max_rate / rate_at_OT - 1.0)
               );

        const cats_dt_rates density = hybrid_density_multiplier(rate_info, species_parameter, suitability, N, K);

        if (density < 1.0) {

                N = N - K * rate_info->density_ts;
                K = K - K * rate_info->density_ts;
                assert(N >= 0);
                assert(K >= 0);

                switch (rate_info->density) {
                        case DENSITY_DEP_NEGATIVE:
                                assert(K >= 0);
                                assert(N >= 0);
                                if (K == 0) return 0.0;
                                rate = min_rates(rate, rate * density + ((N / K) * rate_at_OT));
                                break;
                        case DENSITY_DEP_POSITIVE:
                                if (K == 0) return 0.0;
                                rate = max_rates(rate, rate * density + (1.0 - (N / K)) * rate_at_OT);
                                break;
                        case NO_DENSITY_DEP:
                                break;
                }
        }


        assert(rate >= 0.0);


        return (rate);
}


cats_dt_rates constant_value_rate(const struct cats_vital_rate *rate_info,
                                  const struct cats_species_param *species_parameter,
                                  cats_dt_rates suitability,
                                  cats_dt_rates N,
                                  cats_dt_rates K)
{
        assert(rate_info->max_rate >= 0.0);
        if (rate_info->suitability_cutoff > 0.0 && suitability + SUIT_EPS < rate_info->suitability_cutoff) return 0.0;


        assert(!isnan(rate_info->max_rate));
        cats_dt_rates rate = rate_info->max_rate;
        return rate;
}


cats_dt_rates linear_rate(const struct cats_vital_rate *rate_info,
                          const struct cats_species_param *species_parameter,
                          cats_dt_rates suitability,
                          cats_dt_rates N,
                          cats_dt_rates K)
{
        assert(rate_info->max_rate > 0.0);
        assert(!isnan(rate_info->max_rate));
        assert(suitability >= 0);
        assert(suitability <= 1);
        if (suitability + SUIT_EPS < rate_info->suitability_cutoff) return 0.0;

        cats_dt_rates max_rate = rate_info->max_rate;

        cats_dt_rates scale = get_rate_scale_factor(rate_info, species_parameter);

        cats_dt_rates slope = (1.0 - scale) / (1.0 - species_parameter->OT);
        cats_dt_rates intercept = scale - slope * species_parameter->OT; // FIXME
        cats_dt_rates y = slope * suitability + intercept;

        const cats_dt_rates density = hybrid_density_multiplier(rate_info, species_parameter, suitability, N, K);
        cats_dt_rates rate = y * max_rate;
        if (density == 1.0) return rate;


        cats_dt_rates rate_at_OT = max_rate * scale;
        rate = min_rates(rate, rate * density + ((N / K) * rate_at_OT)); // was: max_rate * scale;
        return rate;

}

