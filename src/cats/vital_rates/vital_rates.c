// SPDX-License-Identifier: GPL-3.0-or-later
//
// vital_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include "cats_global.h"
#include "configuration/configuration.h"
#include "direct_functions.h"
#include "inline.h"


void init_cats_vital_rate(struct cats_vital_rate *vital_rate)
{
        vital_rate->max_rate = NAN;
        vital_rate->is_integer_quantity = false;
        vital_rate->density = NO_DENSITY_DEP;
        vital_rate->name[0] = '\0';
        vital_rate->suitability_cutoff = NAN;
        vital_rate->suitability_cutoff_hint = HYBRID_SUIT_TS_UNSPECIFIED;
        vital_rate->func = NULL;
        vital_rate->environment_set = NULL;
        vital_rate->density_ts = 0.0f;
        vital_rate->environment_multiplier = 1.0;
        vital_rate->min_rate = 0.0;
        vital_rate->rate_scale_factor = -1.0;
        vital_rate->rate_minimum_scale_factor = -1.0;
        vital_rate->have_rate_non_default_scale_factor = false;
        vital_rate->preset.have_minimum_scale_factor = false;
        vital_rate->preset.value_maximum = NAN;
        vital_rate->preset.have_maximum = false;
        vital_rate->preset.value_minimum_scale_factor = NAN;
        vital_rate->default_rate_id = VR_UNSPECIFIED;
}


void init_cats_vital_rates(struct cats_vital_rate *vital_rates)
{
        for (int32_t i = 0; i < MAX_VITAL_RATES; i++) {
                init_cats_vital_rate(&vital_rates[i]);
        }
}


cats_dt_rates calculate_rate(const struct cats_vital_rate *rate_info,
                             cats_dt_rates N,
                             const struct cats_species_param *param,
                             const struct cats_grid *grid,
                             cats_dt_coord row,
                             cats_dt_coord col,
                             struct link_override_parameters *override)
{

        const struct cats_vital_rate_hybrid_function *func = rate_info->func;

        cats_dt_rates K = NAN;

        cats_dt_rates suit = NAN;
        cats_dt_rates rate = NAN;

        load_information_for_density_maybe(&K, &N, rate_info, grid, row, col, override);

        assert(rate_info->environment_set != NULL);



        switch (rate_info->environment_set->type) {

                case ENVIRONMENT_TYPE_UNKNOWN:
                        log_message(LOG_ERROR, "%s: rate '%s' has environment not set", __func__, rate_info->name);
                        exit_cats(EXIT_FAILURE);

                case ENVIRONMENT_TYPE_CONSTANT:
                        rate = get_constant_rate(rate_info, N, K, row, col);
                        rate = clamp(rate, rate_info->min_rate, rate_info->max_rate);
                        break;

                case ENVIRONMENT_TYPE_SUITABILITY:
                        suit = get_suitability_if_needed(rate_info, grid, override, row, col);
                        //printf("%s: K %Lf N %Lf suit %Lf\n", rate_info->func->short_name, K, N, suit);
                        if (rate_info->suitability_cutoff > 0.0 && suit + SUIT_EPS < rate_info->suitability_cutoff) {
                                rate = 0.0;
                        } else {
                                rate = func->func(rate_info, param, suit, N, K);
                                rate = clamp(rate, rate_info->min_rate, rate_info->max_rate);
                        }
                        /*
                        if (rate_info->is_carrying_capacity) {

                                printf("K %Lf scale %Lf, suit %Lf, N %Lf, K %Lf\n", rate, grid->param.scale_factor, suit, N, K);
                        }*/
                        break;
                case ENVIRONMENT_TYPE_GLM:
                        rate = get_glm(rate_info, N, K, row, col);
                        rate = clamp(rate, rate_info->min_rate, rate_info->max_rate);
                        break;
                case ENVIRONMENT_TYPE_DIRECT:
                        rate = get_direct_rate(rate_info, N, K, row, col);
                        rate = clamp(rate, rate_info->min_rate, rate_info->max_rate);
                        break;
        }


        switch (rate_info->default_rate_id) {
                case VR_UNSPECIFIED:
                        break;
                case VR_CLONAL_GROWTH:
                        rate = 1.0 + rate;
                        break;
                case VR_GERMINATION_RATE:
                case VR_FLOWERING_FREQUENCY:
                case VR_ADULT_SURVIVAL:
                case VR_GERMINATION_TO_ADULT_SURVIVAL:
                case VR_SEED_SURVIVAL:
                case VR_SEED_YIELD:
                case VR_MAX:
                        break;
        }


        return rate;
}