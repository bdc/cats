// SPDX-License-Identifier: GPL-3.0-or-later
//
// vital_rate_ranges.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include "vital_rate_ranges.h"
#include "logging/logging.h"


int check_vital_rate_hybrid(struct cats_vital_rate *vr)
{
        int error = 0;

        if (vr->func->type == LINK_CONSTANT && vr->have_rate_non_default_scale_factor) {
                log_message(LOG_ERROR,
                            "vital rate '%s' is of type '%s: rate scale factor may not be specified",
                            vr->name, vr->func->short_name);
                error += 1;

        }
        return error;
}


bool is_set(cats_dt_rates limit)
{
        return !isnan(limit);
}


int check_vital_rate_all(struct cats_vital_rate *vr)
{
        int error = 0;

        cats_dt_rates lower = NAN;
        cats_dt_rates upper = INFINITY;
        char *lower_symbol = "(";
        char *upper_symbol = ")";

        if (is_set(vr->upper_limit_incl)) {
                upper = vr->upper_limit_incl;
                upper_symbol = "]";
        } else if (is_set(vr->upper_limit_excl)) {
                upper = vr->upper_limit_excl;
                upper_symbol = ")";
        }

        if (is_set(vr->lower_limit_incl)) {
                lower = vr->lower_limit_incl;
                lower_symbol = "[";
        } else if (is_set(vr->lower_limit_excl)) {
                lower = vr->lower_limit_excl;
                lower_symbol = "(";
        }

        if (is_set(vr->upper_limit_incl) && vr->max_rate > vr->upper_limit_incl) error++;
        if (is_set(vr->upper_limit_excl) && vr->max_rate >= vr->upper_limit_excl) error++;


        if (is_set(vr->lower_limit_incl) && vr->max_rate < vr->lower_limit_incl) error++;
        if (is_set(vr->lower_limit_excl) && vr->max_rate <= vr->lower_limit_excl) error++;

        if (error) {
                log_message(LOG_ERROR, "%s: vital rate '%s' maximum value %f outside of allowed range %s%f, %f%s ",
                            __func__, vr->name, (double) vr->max_rate, lower_symbol, (double) lower, (double) upper,
                            upper_symbol);
        }

        return error > 0;
}