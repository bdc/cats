// SPDX-License-Identifier: GPL-3.0-or-later
//
// setup_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include <assert.h>
#include <cats_strings/cats_strings.h>
#include "logging.h"
#include "setup_rates.h"
#include "string.h"
#include "environment/environment_registry.h"
#include "environment/environment_set.h"
#include "configuration/load_configuration.h"
#include "data/species_parameters.h"


void set_link_function(struct cats_vital_rate_hybrid_function *link, cats_rate_function func)
{
        assert(link != NULL);
        assert(func != NULL || link->type == LINK_UNASSIGNED);

        link->func = func;
}


void set_vital_rate_name(struct cats_vital_rate *vital, const char *name)
{
        assert(vital != NULL);
        assert(name != NULL);
        assert(strlen(name) > 0);

        strncpy(vital->name, name, MAX_VITAL_NAME_LEN);
        vital->name[MAX_VITAL_NAME_LEN] = '\0'; // size of vital->name is MAX_VITAL_NAME_LEN + 1
}


void
set_vital_rate_suitability_cutoff_hint(struct cats_vital_rate *vital, enum vital_rate_hybrid_suitability_threshold hint)
{
        vital->suitability_cutoff_hint = hint;
}


void
set_vital_cutoff(struct cats_vital_rate *vital, const cats_dt_rates value, const struct cats_species_param *param)
{
        assert(vital != NULL);
        if (!isnan(vital->suitability_cutoff)) {
                log_message(LOG_INFO, "vital rate suitability cut-off for '%s' manually set, not overwriting",
                            vital->name);
                return;
        }
        if (value >= 0.0 && value <= param->OT) {
                vital->suitability_cutoff = value;
        } else {
                log_message(LOG_ERROR,
                            "%s: vital rate '%s' for species '%s': vital rate cutoff '%f' out of range [0, OT = %f].",
                            __func__, vital->name, param->species_name,
                            (double) value, (double) param->OT);
                if (value > param->OT) {
                        log_message(LOG_ERROR,
                                    "%s: The cut off value must be lower than the occurrence threshold OT = %f",
                                    __func__, (double) param->OT);
                }

                exit_cats(EXIT_FAILURE);
        }
}


void set_vital_cutoff_from_string(struct cats_vital_rate *vital, const struct cats_species_param *param,
                                  const char *string)
{
        assert(vital != NULL);
        if (string == NULL || strlen(string) == 0) {
                return;
        }
        cats_dt_rates value = NAN;
        set_suitability_from_string(&value, param, string);
        set_vital_cutoff(vital, value, param);

}


void set_vital_rate_maximum(struct cats_vital_rate *vital, cats_dt_rates max_rate)
{
        assert(vital != NULL);
        assert(max_rate >= 0.0);
        vital->max_rate = max_rate;
}


void set_vital_rate_minimum(struct cats_vital_rate *vital, cats_dt_rates minimum)
{
        assert(vital != NULL);
        if (minimum < 0) {
                log_message(LOG_ERROR, "%s (%s): vital rate minimum %Lf must not be negative ", __func__,
                            vital->name, minimum);
                exit_cats(EXIT_FAILURE);
        }

        if (minimum > vital->max_rate) {
                log_message(LOG_ERROR, "%s (%s): vital rate minimum %Lf must be less or equal to maximum %Lf "
                            , __func__, vital->name, minimum, vital->max_rate);
                exit_cats(EXIT_FAILURE);
        }

        vital->min_rate = minimum;

}

void set_vital_density(struct cats_vital_rate *vital, enum cats_density_dependence density)
{
        assert(vital != NULL);
        if (vital->is_carrying_capacity && density != NO_DENSITY_DEP) {

                log_message(LOG_ERROR, "%s (%s): carrying capacity may never depend on population density", __func__, vital->name);

                exit_cats(EXIT_FAILURE);
        }
        vital->density = density;
}


void set_vital_density_ts(struct cats_vital_rate *vital, cats_dt_rates ts)
{
        assert(vital != NULL);
        if (isnan(ts) || ts < 0 || ts > 1) {
                log_message(LOG_ERROR, "%s: vital rate '%s' density threshold not in range [0, 1]: %f", __func__,
                            vital->name, (double) ts);
        }
        vital->density_ts = ts;
}


void set_vital_density_from_string(struct cats_vital_rate *vital, char *string)
{
        assert(vital != NULL);
        if (string == NULL || strlen(string) == 0) return;

        char *copy = strdup(string);
        copy = left_trim(right_trim(copy));
        if (copy == NULL || strlen(copy) == 0) {
                log_message(LOG_ERROR, "%s: vital rate '%s': empty string supplied", __func__, vital->name);
                exit_cats(EXIT_FAILURE);
        }

        bool result;
        bool valid = string_to_bool(copy, &result);
        if (!valid) {

                log_message(LOG_ERROR, "%s: vital rate '%s': could not parse string '%s'", __func__, vital->name, copy);
                exit_cats(EXIT_FAILURE);
        }

        if (result) {
                set_vital_density(vital, DENSITY_DEP_NEGATIVE);
        } else {
                set_vital_density(vital, NO_DENSITY_DEP);
        }

        free(copy);

}


void set_vital_environment_set(struct cats_vital_rate *vital, struct cats_environment *set)
{
        assert(vital != NULL);
        assert(set != NULL);
        if (vital->environment_set != NULL) {
                log_message(LOG_ERROR, "\t%s: environmental set for rate link '%s' already set: %s", __func__,
                            vital->name, set->name);
        }
        log_message(LOG_INFO, "\t%s: setting environment set '%s' for rate link '%s'", __func__, set->name,
                    vital->name);
        vital->environment_set = set;

}


void set_vital_rate_link_hybrid_function(struct cats_vital_rate *vr, struct cats_configuration *conf,
                                         enum cats_rate_function_type link_type)
{
        assert(vr != NULL);
        vr->func = &conf->vital_dependency_registry[link_type];
}


void set_link_name(struct cats_vital_rate_hybrid_function *link, const char *name)
{
        assert(link != NULL);
        assert(name != NULL);
        strncpy(link->description, name, MAX_VITAL_NAME_LEN);
        link->description[MAX_VITAL_NAME_LEN] = '\0'; // size of vital->name is MAX_VITAL_NAME_LEN + 1
}


void set_link_shortname(struct cats_vital_rate_hybrid_function *link, const char *name)
{
        assert(link != NULL);
        assert(name != NULL);
        strncpy(link->short_name, name, MAX_VITAL_NAME_LEN);
        link->short_name[MAX_VITAL_NAME_LEN] = '\0'; // size of vital->name is MAX_VITAL_NAME_LEN + 1
}


void print_link(const struct cats_vital_rate_hybrid_function *link, int32_t count)
{
        assert(link != NULL);
        if (link->type == LINK_MIN || link->type == LINK_MAX) return;
        log_message(LOG_INFO, "\tlink function (id % 2d): '%s' ('%s') (type id: % 2d)", count,
                    link->description, link->short_name, link->type);

}


void
print_vital_rate(const struct cats_vital_rate *vital_rate, const struct cats_species_param *param)
{

        if (vital_rate == NULL && param->parametrization == PARAM_HYBRID) {
                log_message(LOG_ERROR, "vital function has no assigned link function");
                exit(EXIT_FAILURE);
        }

        if (vital_rate->func == NULL) {
                log_message(LOG_ERROR, "vital function '%s' has no assigned link function", vital_rate->name);
                exit_cats(EXIT_FAILURE);
        }
        const char *link_name = vital_rate->func->description;

        int32_t link_id = 0;
        if (param->parametrization == PARAM_HYBRID) {
                link_id = vital_rate->func->type;
        }

        print_string(2, vital_rate->name, "");
        print_integer(4, "vital rate id", vital_rate->default_rate_id);
        if (vital_rate->environment_set == NULL) {
                print_string(4, "linked environment name", "None");

        } else {
                print_string(4, "linked environment name", vital_rate->environment_set->name);
                print_string(4, "linked environment type",
                             get_environment_set_type_name(vital_rate->environment_set->type));

        }
        print_rate(4, "maximum rate value", vital_rate->max_rate);
        print_rate(4, "minimum rate value", vital_rate->min_rate);


        if (param->parametrization == PARAM_HYBRID) {
                cats_dt_rates OT = param->OT;
                cats_dt_rates ZT = param->ZT;
                print_string(4, "link to suitability function name", link_name);
                print_integer(4, "link to suitability function id", link_id);


                if (vital_rate->suitability_cutoff == ZT) {
                        print_string(4, "minimum suitability for rate > 0", "ZT");

                } else if (vital_rate->suitability_cutoff == OT) {
                        print_string(4, "minimum suitability for rate > 0", "OT");

                }
                if (vital_rate->rate_scale_factor != -1.0) {
                        print_rate(4, "scale factor (rate specific)", vital_rate->rate_scale_factor);
                }

                if (vital_rate->rate_minimum_scale_factor != -1.0) {
                        print_rate(4, "scale factor (rate specific, at least)", vital_rate->rate_minimum_scale_factor);
                }

                if (vital_rate->rate_minimum_scale_factor == -1 && vital_rate->rate_scale_factor == -1) {
                        print_string(4, "scale factor", "using default scale factor");

                }


                print_rate(4, "minimum suitability for rate > 0", vital_rate->suitability_cutoff);

                print_rate(4, "[not applicable] environment multiplier", vital_rate->environment_multiplier);
                if (vital_rate->have_rate_non_default_scale_factor) {
                        if (vital_rate->rate_scale_factor != -1.0) {
                                print_rate(4, "rate scale factor", vital_rate->rate_scale_factor);
                        } else {
                                print_rate(4, "minimum rate scale factor", vital_rate->rate_minimum_scale_factor);
                        }

                } else {
                        print_string(4, "rate value OT", "default for link function");
                }

        }
        if (param->parametrization == PARAM_DIRECT_VITAL_RATE) {
                print_rate(4, "environment multiplier", vital_rate->environment_multiplier);
        }


        switch (vital_rate->density) {

                case NO_DENSITY_DEP:
                        print_string(4, "adult population density dependence", "no dependency");
                        break;
                case DENSITY_DEP_NEGATIVE:
                        print_string(4, "adult population density dependence", "negative");
                        break;
                case DENSITY_DEP_POSITIVE:
                        print_string(4, "adult population density dependence", "positive");
                        break;
        }
        print_rate(4, "adult population density suitability threshold", vital_rate->density_ts);
        log_message(LOG_RAW, "\n");

}


int32_t setup_default_links(struct cats_vital_rate_hybrid_function *vital_dependency_registry)
{
        int32_t link_count = 0;
        log_message(LOG_INFO, "setting up default link functions");
        for (enum cats_rate_function_type link_idx = LINK_MIN; link_idx < LINK_MAX; link_idx++) {
                link_count++;
                struct cats_vital_rate_hybrid_function *link = &vital_dependency_registry[link_idx];
                link->type = link_idx;

                // we use a switch here so that we make sure we don't forget any type
                switch (link_idx) {

                        case LINK_UNASSIGNED:
                                set_link_name(link, "invalid (LINK_UNASSIGNED == LINK_MIN)");
                                set_link_shortname(link, "invalid");
                                set_link_function(link, NULL);
                                break;
                        case LINK_SUITABILITY_SIGMOID:
                                set_link_name(link, "sigmoid (suitability-dependent)");
                                set_link_shortname(link, "sigmoid");
                                set_link_function(link, sigmoid);
                                break;

                        case LINK_CONSTANT:
                                set_link_name(link, "constant value");
                                set_link_function(link, constant_value_rate);
                                set_link_shortname(link, "constant");
                                break;
                        case LINK_LINEAR:
                                set_link_name(link, "linear with suitability");
                                set_link_function(link, linear_rate);
                                set_link_shortname(link, "linear");
                                break;
                        case LINK_CUSTOM:
                                set_link_name(link, "invalid (custom, not yet named)");
                                set_link_shortname(link, "invalid");
                                break;
                        case LINK_MAX:
                                set_link_name(link, "invalid (LINK_MAX out of range)");
                                set_link_shortname(link, "invalid");
                                break;
                }

                print_link(link, link_idx);
        }
        return link_count;
}


int32_t
register_function_type(struct cats_configuration *conf, cats_rate_function function, const char *name,
                       const char *short_name)
{
        int32_t id = conf->link_count;
        if (id > MAX_VITAL_RATES) {
                log_message(LOG_ERROR, "too many link functions");
                return -1;
        }
        struct cats_vital_rate_hybrid_function *link = &conf->vital_dependency_registry[id];
        set_link_shortname(link, short_name);
        set_link_function(link, function);
        set_link_name(link, name);
        link->type = LINK_CUSTOM;
        conf->link_count += 1;
        return id;
}


enum cats_rate_function_type get_function_type(const struct cats_configuration *conf, const char *short_name)
{
        for (int32_t i = LINK_MIN + 1; i < conf->link_count; i++) {
                if (strcmp(conf->vital_dependency_registry[i].short_name, short_name) == 0) {
                        return i;
                }
        }

        log_message(LOG_ERROR, "%s: could not find vital rate link with short name '%s'", __func__, short_name);
        exit(EXIT_FAILURE);
}