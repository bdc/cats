// SPDX-License-Identifier: GPL-3.0-or-later
//
// hybrid_functions.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_HYBRID_FUNCTIONS_H
#define CATS_HYBRID_FUNCTIONS_H

#include "data/cats_datatypes.h"
#include "configuration/configuration.h"

#define RATES_EPS 0.00001

cats_dt_rates
sigmoid(const struct cats_vital_rate *rate_info,
        const struct cats_species_param *species_parameter,
        cats_dt_rates suitability,
        cats_dt_rates N,
        cats_dt_rates K);


cats_dt_rates constant_value_rate(const struct cats_vital_rate *rate_info,
                                  const struct cats_species_param *species_parameter,
                                  cats_dt_rates suitability,
                                  cats_dt_rates N,
                                  cats_dt_rates K);

cats_dt_rates linear_rate(const struct cats_vital_rate *rate_info,
                          const struct cats_species_param *species_parameter,
                          cats_dt_rates suitability,
                          cats_dt_rates N,
                          cats_dt_rates K);

#endif