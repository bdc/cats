// SPDX-License-Identifier: GPL-3.0-or-later
//
// setup_rates.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_SETUP_RATES_H
#define CATS_SETUP_RATES_H

#include <stdbool.h>
#include "vital_rates/hybrid_functions.h"
#include "configuration/configuration.h"

void set_vital_rate_suitability_cutoff_hint(struct cats_vital_rate *vital,
                                            enum vital_rate_hybrid_suitability_threshold hint);

void set_vital_density_ts(struct cats_vital_rate *vital, cats_dt_rates ts);

void set_vital_rate_name(struct cats_vital_rate *vital, const char *name);

int32_t setup_default_links(struct cats_vital_rate_hybrid_function *vital_dependency_registry);

void set_vital_environment_set(struct cats_vital_rate *vital, struct cats_environment *set);

void setup_default_vital_rates(struct cats_configuration *conf);

void set_vital_density(struct cats_vital_rate *vital, enum cats_density_dependence density);

void load_vital_rates(struct cats_configuration *conf);

void set_vital_cutoff(struct cats_vital_rate *vital, cats_dt_rates value,
                      const struct cats_species_param *param);

enum cats_rate_function_type get_function_type(const struct cats_configuration *conf, const char *short_name);

void set_vital_cutoff_from_string(struct cats_vital_rate *vital, const struct cats_species_param *param,
                                  const char *string);

void set_vital_density_from_string(struct cats_vital_rate *vital, char *string);

void set_vital_rate_link_hybrid_function(struct cats_vital_rate *vr, struct cats_configuration *conf,
                                         enum cats_rate_function_type link_type);

void set_vital_density(struct cats_vital_rate *vital, enum cats_density_dependence density);

void set_vital_rate_minimum(struct cats_vital_rate *vital, cats_dt_rates minimum);

#endif //CATS_SETUP_RATES_H
