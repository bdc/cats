// SPDX-License-Identifier: GPL-3.0-or-later
//
// direct_functions.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include <math.h>

#include "configuration/configuration.h"
#include "logging.h"
#include "inline.h"

#include "direct_functions.h"


cats_dt_rates get_direct_rate(const struct cats_vital_rate *rate_info,
                              cats_dt_rates N,
                              cats_dt_rates K,
                              cats_dt_coord row,
                              cats_dt_coord col)
{
        const struct cats_environment *set = rate_info->environment_set;
        enum cats_density_dependence density_type = rate_info->density;

        if (set->type != ENVIRONMENT_TYPE_DIRECT) {
                log_message(LOG_ERROR, "%s: wrong environment set type for set '%s'", __func__, set->name);
                exit(EXIT_FAILURE);
        }
        cats_dt_rates rate = set->environments[0]->current.values[row][col] * rate_info->environment_multiplier;
        if (isnan(rate)) return 0.0f;

        cats_dt_rates dens_multiplier = density_multiplier(density_type, N, K, rate_info->density_ts);

        return rate * dens_multiplier;
}


// no density!
cats_dt_rates get_constant_rate(const struct cats_vital_rate *rate_info,
                                cats_dt_rates N,
                                cats_dt_rates K,
                                cats_dt_coord row,
                                cats_dt_coord col)
{
        const struct cats_environment *set = rate_info->environment_set;

        if (set->type != ENVIRONMENT_TYPE_CONSTANT) {
                log_message(LOG_ERROR, "%s: wrong environment set type for set '%s'", __func__, set->name);
                exit(EXIT_FAILURE);
        }

        cats_dt_rates rate = set->environments[0]->constant_value;
        if (isnan(rate)) return 0.0f;
        return rate;
}


