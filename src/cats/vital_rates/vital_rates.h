// SPDX-License-Identifier: GPL-3.0-or-later
//
// vital_rates.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_VITAL_RATES_H
#define CATS_VITAL_RATES_H

#include "data/cats_datatypes.h"
#include "defaults.h"

struct cats_species_param;
struct cats_grid;
struct link_override_parameters;

enum cats_density_dependence {
        NO_DENSITY_DEP,
        DENSITY_DEP_NEGATIVE,
        DENSITY_DEP_POSITIVE
};

enum cats_vital_rate_id {
        VR_MIN = 0,
        VR_UNSPECIFIED = 0,
        VR_CLONAL_GROWTH = 1,
        VR_GERMINATION_RATE,
        VR_FLOWERING_FREQUENCY,
        VR_ADULT_SURVIVAL,
        VR_GERMINATION_TO_ADULT_SURVIVAL,
        VR_SEED_SURVIVAL,
        VR_SEED_YIELD,
        VR_MAX
};

enum cats_rate_function_type {
        LINK_MIN = 0,
        LINK_UNASSIGNED = 0,
        LINK_SUITABILITY_SIGMOID,
        LINK_CONSTANT,
        LINK_LINEAR,
        LINK_CUSTOM,
        LINK_MAX
};


struct cats_vital_rate_preset {
        bool have_minimum_scale_factor;
        cats_dt_rates value_minimum_scale_factor;
        bool have_maximum;
        cats_dt_rates value_maximum;
};

enum vital_rate_hybrid_suitability_threshold {
        HYBRID_SUIT_TS_UNSPECIFIED = 0,
        HYBRID_SUIT_TS_ZT = 1,
        HYBRID_SUIT_TS_OT = 2,
        HYBRID_SUIT_TS_ZERO = 3
};

struct cats_vital_rate {

        cats_dt_rates max_rate;                         ///< The maximum value this rate can assume.
        cats_dt_rates min_rate;                         ///< The minimum value this rate can assume.

        bool is_integer_quantity;                       ///< should this value be rounded?
        enum cats_vital_rate_id default_rate_id;        ///< Specifies the rate type, e.g. flowering
        char name[MAX_VITAL_NAME_LEN + 1];              ///< human readable name of the rate type


        cats_dt_rates upper_limit_incl;
        cats_dt_rates lower_limit_incl;

        cats_dt_rates upper_limit_excl;
        cats_dt_rates lower_limit_excl;

        struct cats_vital_rate_preset preset;           ///< preset information

        struct cats_environment *environment_set;       ///< associated environment which is used to quantify the rate's value
        enum cats_density_dependence density;           ///< density type (e.g. no density dependence or negative)
        cats_dt_rates density_ts;                       ///< the threshold of abundance to carrying capacity,
        ///< above which population density effects will occur
        ///< Note: the fraction is abundance relative to the maximum
        ///< adult carrying capacity


        // HYBRID MODE ONLY
        enum vital_rate_hybrid_suitability_threshold suitability_cutoff_hint;
        cats_dt_rates suitability_cutoff;               ///< Hybrid mode only; only used in conjunction with suitabilities.
        ///< The cut-off in suitability, under which the rate is always zero
        ///< ignoring minimum rate value
        ///< Only used in conjunction with suitabilities.
        cats_dt_rates rate_scale_factor;                ///< Hybrid mode only; use this value as scale factor
        cats_dt_rates rate_minimum_scale_factor;        ///< Hybrid mode only; use this value as scale factor, is less than scale factor
        bool have_rate_non_default_scale_factor;        ///< Hybrid mode only; is either rate_scale_factor or rate_minimum_scale_factor set=

        bool is_carrying_capacity;                      ///< special case flag: does this vital rate describe the carrying capacity


        struct cats_vital_rate_hybrid_function *func;   ///< Link function and dependencies

        // DIRECT MODE ONLY
        cats_dt_rates environment_multiplier;       ///< Direct mode only. The environment value will be multiplied
        ///< with this value. This allows the same environment to be used
        ///< with multiple vital rates, scaled linearly.

};

cats_dt_rates calculate_rate(const struct cats_vital_rate *rate_info,
                             cats_dt_rates N,
                             const struct cats_species_param *param,
                             const struct cats_grid *grid,
                             cats_dt_coord row,
                             cats_dt_coord col,
                             struct link_override_parameters *override);

typedef cats_dt_rates (*cats_rate_function)(const struct cats_vital_rate *rate_info,
                                            const struct cats_species_param *species_parameter,
                                            cats_dt_rates suitability,
                                            cats_dt_rates N,
                                            cats_dt_rates K);

/// @brief describes the dependency on the environment
struct cats_vital_rate_hybrid_function {
        cats_rate_function func;                  ///< pointer to the actual function
        enum cats_rate_function_type type;        ///< function type
        char description[MAX_VITAL_NAME_LEN + 1]; ///< function name (long)
        char short_name[MAX_VITAL_NAME_LEN + 1];  ///< function name (short), used in configuration files

};

struct link_override_parameters {
        cats_dt_rates override_suitability;
        cats_dt_rates override_population;
        cats_dt_rates override_carrying_capacity;
};

void init_cats_vital_rate(struct cats_vital_rate *vital_rate);

void init_cats_vital_rates(struct cats_vital_rate *vital_rates);

#endif //CATS_VITAL_RATES_H
