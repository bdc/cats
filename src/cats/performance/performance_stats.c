// SPDX-License-Identifier: GPL-3.0-or-later
//
// performance_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "performance_stats.h"


void add_performance_info(const struct cats_configuration *conf, int year, const char *name, double time_cpu,
                          double time_wall)
{
        if (!conf->output.performance_file) return;
        fprintf(conf->output.performance_file, "%d,%s,%f,%f\n", year, name, time_cpu, time_wall);
}