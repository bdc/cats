// SPDX-License-Identifier: GPL-3.0-or-later
//
// timer.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include "configuration/configuration.h"

struct cats_timer {
        clock_t start_clock;
        struct timeval start_timeval;
        double time_normal;
        double time_real;
};

struct simulation_time;

struct cats_timer start_new_timer(void);

void log_timer(const struct cats_timer *t, const char *string, int loglevel, const struct simulation_time *time,
               const struct cats_configuration *conf);

void start_timer(struct cats_timer *t);

void stop_timer(struct cats_timer *t);

void stop_and_log_timer(struct cats_timer *t, const char *string, int loglevel, const struct simulation_time *time,
                        const struct cats_configuration *conf);

void
stop_log_and_restart_timer(struct cats_timer *t, const char *string, int loglevel, const struct simulation_time *time,
                           const struct cats_configuration *conf);