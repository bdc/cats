// SPDX-License-Identifier: GPL-3.0-or-later
//
// timer.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <time.h>
#include <assert.h>
#include <stdint.h>

#include "timer.h"
#include "logging.h"
#include "misc/misc.h"
#include "temporal/phase_names.h"
#include "temporal/years.h"
#include "performance_stats.h"
#include <memory/cats_memory.h>


struct timeval *time_difference(struct timeval *start, struct timeval *end)
{
        struct timeval *diff = malloc_or_die(sizeof(struct timeval));

        if (start->tv_sec == end->tv_sec) {
                diff->tv_sec = 0;
                diff->tv_usec = end->tv_usec - start->tv_usec;
        } else {
                diff->tv_usec = 1000000 - start->tv_usec;
                diff->tv_sec = end->tv_sec - (start->tv_sec + 1);
                diff->tv_usec += end->tv_usec;

                if (diff->tv_usec >= 1000000) {
                        diff->tv_usec -= 1000000;
                        diff->tv_sec += 1;
                }
        }

        return diff;
}


struct cats_timer start_new_timer(void)
{
        struct cats_timer t = {0};
        t.start_clock = clock();
        //gettimeofday(&t.start_timeval, NULL);
        get_time(&t.start_timeval);
        return t;
}


void start_timer(struct cats_timer *t)
{
        assert(t != NULL);
        t->start_clock = clock();
        get_time(&t->start_timeval);
        //gettimeofday(&t->start_timeval, NULL);
}


void stop_timer(struct cats_timer *t)
{
        assert(t != NULL);
        clock_t now = clock();

        struct timeval time_now;
        double elapsed_time;
        get_time(&time_now);
        //gettimeofday(&time_now, NULL);
        elapsed_time = ((double) (now - t->start_clock) / CLOCKS_PER_SEC);

        t->time_normal = elapsed_time;

        double difference_real;
        struct timeval *difference;

        difference = time_difference(&t->start_timeval, &time_now);
        difference_real = (double) difference->tv_sec + (double) difference->tv_usec / (1000.0 * 1000.0);
        t->time_real = difference_real;

        free(difference);
}


void log_timer(const struct cats_timer *t, const char *string, int loglevel, const struct simulation_time *time,
               const struct cats_configuration *conf)
{
        const char *phase_default = "?";
        const char *phase = phase_default;
        int year = -1;

        if (time) {
                phase = get_phase_name(time->phase);
                year = get_current_year(time);
        }


        log_message(loglevel, "timer::%s::%4d::%-30s      ::%10.3f s::%10.3f s", phase, year, string, t->time_normal,
                    t->time_real);

        add_performance_info(conf, year, string, t->time_normal, t->time_real);
}


void stop_and_log_timer(struct cats_timer *t, const char *string, int loglevel, const struct simulation_time *time,
                        const struct cats_configuration *conf)
{
        stop_timer(t);
        log_timer(t, string, loglevel, time, conf);

}


void
stop_log_and_restart_timer(struct cats_timer *t, const char *string, int loglevel, const struct simulation_time *time,
                           const struct cats_configuration *conf)
{
        stop_timer(t);
        log_timer(t, string, loglevel, time, conf);
        start_timer(t);
}