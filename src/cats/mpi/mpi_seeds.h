// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_seeds.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_MPI_SEEDS_H
#define CATS_MPI_SEEDS_H

#include "configuration/configuration.h"

void send_and_receive_dispersed_seeds_mpi(struct cats_grid *grid, struct cats_configuration *conf);

void add_seeds0_from_above(const struct cats_configuration *conf, struct cats_grid *grid, const double *from_above,
                           int radius);

void add_seeds0_from_below(const struct cats_configuration *conf, struct cats_grid *grid, const double *from_below,
                           int radius);

#endif //CATS_MPI_SEEDS_H
