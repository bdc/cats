// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_cats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef  _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifdef USEMPI

#include <assert.h>
#include "cats_global.h"
#include "logging.h"
#include "configuration/configuration.h"
#include "misc/misc.h"
#include "misc/output.h"
#include "memory/raw_arrays.h"
#include "paths/paths.h"
#include "grids/gdal_load.h"
#include "mpi_cats.h"
#include "memory/cats_memory.h"
#include "mpi/mpi_grid_helpers.h"


float **get_grid_from_mpi(struct cats_grid *grid, struct cats_configuration *conf, float **in)
{
        assert(grid != NULL);
        assert(conf != NULL);
        assert(grid->id >= 0);

        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier: waiting for all processes"); }


        float *receive_buffer = NULL;

        if (conf->mpi.world_rank == 0) {
                int64_t receive_count_all = raster_cell_count(conf->geometry.dimension_orig);
                receive_buffer = new_raw_1d_array(receive_count_all, sizeof(float)); //new_float_array(receive_count);
        }

        int64_t send_count = grid->dimension.rows * grid->dimension.cols;
        float *send = new_raw_1d_array(send_count, sizeof(float));
        //new_float_array(send_count);
        //MALLOC(send, sizeof(float) * send_count);


        for (cats_dt_coord r = 0; r < grid->dimension.rows; r++) {
                for (cats_dt_coord c = 0; c < grid->dimension.cols; c++) {
                        int64_t i = r * grid->dimension.cols + c;
                        send[i] = in[r][c];
                }
        }

        int receive_count = get_cell_count_single_grid(conf);

        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier: waiting for all processes"); }


        fflush(stdout);
        MPI_Request request;
        MPI_Status status;
        MPI_Igatherv(send, receive_count, MPI_FLOAT, receive_buffer, conf->geometry.cells_per_chunk,
                     conf->geometry.displacements, MPI_FLOAT, 0, MPI_COMM_WORLD, &request);
        MPI_Wait(&request, &status);


        free(send);

        if (conf->mpi.world_rank == 0) {
                float **tmp_grid = new_raw_2d_array(conf->geometry.dimension_orig.rows,
                                                    conf->geometry.dimension_orig.cols,
                                                    sizeof(float));

                int32_t cols = conf->geometry.dimension_orig.cols;

                for (cats_dt_coord r = 0; r < conf->geometry.dimension_orig.rows; r++) {
                        for (cats_dt_coord c = 0; c < conf->geometry.dimension_orig.cols; c++) {
                                tmp_grid[r][c] = receive_buffer[c + r * cols];
                        }
                }

                free(receive_buffer);
                return tmp_grid;
        }

        return NULL;
}


void check_mpi_rc(int rc, int expected, char *error)
{
        if (rc != expected) {
                log_message(LOG_ERROR, "%s", error);
                exit(EXIT_FAILURE);
        }
}


int get_mpi_rank(void)
{
        int rank = -1;
        int rc = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        check_mpi_rc(rc, MPI_SUCCESS, "error during MPI_Comm_rank.");
        return rank;
}


void mpi_init(int argc, char **argv)
{
        log_message(LOG_MPI, "MPI initialized");
        int rc = MPI_Init(&argc, &argv);
        check_mpi_rc(rc, MPI_SUCCESS, "error during MPI_Init.");

}


void mpi_update(void)
{
        int32_t mpi_rank = get_mpi_rank();
        logging_set_mpi_rank(mpi_rank);
        global.mpi_world_rank = mpi_rank;
}


void update_mpi_geometry(struct cats_configuration *conf)
{

        // save original grid dimensions...
        conf->geometry.dimension_orig.rows = conf->geometry.dimension.rows;
        conf->geometry.dimension_orig.cols = conf->geometry.dimension.cols;

        // ... and set up new grid dimensions and offsets
        int rank = conf->mpi.world_rank;
        int size = conf->mpi.world_size;
        int row_chunk = conf->geometry.dimension.rows / size;
        conf->geometry.row_offset = rank * row_chunk;
        conf->geometry.col_offset = 0;
        conf->geometry.dimension.rows = row_chunk;
        conf->geometry.use_sub_grid = true;

        if (rank == size - 1) {
                conf->geometry.dimension.rows = conf->geometry.dimension_orig.rows - (size - 1) * row_chunk;
        }

        log_message(LOG_MPI, "rank %d of %d - %d rows starting at %d", rank, size, conf->geometry.dimension.rows,
                    conf->geometry.row_offset);

        conf->geometry.displacements = calloc_or_die(conf->mpi.world_size, sizeof(int));
        conf->geometry.cells_per_chunk = calloc_or_die(conf->mpi.world_size, sizeof(int));
        conf->geometry.rows_per_chunk = calloc_or_die(conf->mpi.world_size, sizeof(int));
        //MALLOC(conf->geometry.displacements,   sizeof(int) * conf->mpi.world_size);
        //MALLOC(conf->geometry.cells_per_chunk, sizeof(int) * conf->mpi.world_size);
        //MALLOC(conf->geometry.rows_per_chunk,  sizeof(int) * conf->mpi.world_size);


        for (int32_t i = 0; i < conf->mpi.world_size - 1; i++) {
                conf->geometry.cells_per_chunk[i] = row_chunk * conf->geometry.dimension.cols;
                conf->geometry.displacements[i] = row_chunk * conf->geometry.dimension_orig.cols * i;
                conf->geometry.rows_per_chunk[i] = row_chunk;
        }

        conf->geometry.cells_per_chunk[conf->mpi.world_size - 1] =
                (conf->geometry.dimension_orig.rows - (conf->mpi.world_size - 1) * row_chunk) *
                conf->geometry.dimension.cols;
        conf->geometry.displacements[conf->mpi.world_size - 1] =
                row_chunk * conf->geometry.dimension_orig.cols * (conf->mpi.world_size - 1);
        conf->geometry.rows_per_chunk[conf->mpi.world_size - 1] = conf->geometry.dimension_orig.rows - (size - 1);
}


int mpi_setup(struct cats_configuration *conf)
{


        int rc = MPI_Comm_rank(MPI_COMM_WORLD, &conf->mpi.world_rank);
        check_mpi_rc(rc, MPI_SUCCESS, "error during MPI_Comm_rank.");


        rc = MPI_Comm_size(MPI_COMM_WORLD, &conf->mpi.world_size);
        check_mpi_rc(rc, MPI_SUCCESS, "error during MPI_Comm_size.");

        // for logging purposes
        global.mpi_world_rank = conf->mpi.world_rank;

        // log rank and processor
        int name_len;

        rc = MPI_Get_processor_name(conf->mpi.processor_name, &name_len);
        check_mpi_rc(rc, MPI_SUCCESS, "error during MPI_Get_processor_name.");
        log_message(LOG_MPI, "Hello world from processor %s, rank %d  out of %d processors", conf->mpi.processor_name,
                    conf->mpi.world_rank, conf->mpi.world_size);


        return conf->mpi.world_rank;
}


void mpi_end(struct cats_configuration *conf)
{
        MPI_Finalize();
}


#endif
