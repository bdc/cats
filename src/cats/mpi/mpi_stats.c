// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#ifdef USEMPI

#include <string.h>
#include <assert.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "misc/misc.h"
#include "stats/grid_stats.h"
#include "logging.h"
#include "memory/raw_arrays.h"
#include "mpi/mpi_stats.h"
#include "memory/cats_memory.h"


void collect_and_write_plant_stats_mpi(struct cats_configuration *conf, struct cats_grid *grid)
{

        struct cats_grid **parent = grid->parent;
        int root_node_id = 0;
        MPI_Request request;
        MPI_Status status;
        int recv_count = conf->mpi.world_size * STAT_MAX * conf->grid_count;
        int64_t *recv_buf = NULL;

        if (conf->mpi.world_rank == 0) {
                recv_buf = calloc_or_die(recv_count, sizeof(int64_t));
        }

        for (int32_t grid_id = 0; grid_id < conf->grid_count; grid_id++) {

                MPI_Igather(&grid->stats.stats, STAT_MAX, MPI_INT64_T, recv_buf, STAT_MAX, MPI_INT64_T, root_node_id,
                            MPI_COMM_WORLD, &request);
                MPI_Wait(&request, &status);
                MPI_Barrier(MPI_COMM_WORLD);
                struct cats_grid *g = parent[grid_id];

                if (conf->mpi.world_rank == 0) {
                        for (int node_id = 1; node_id < conf->mpi.world_size; node_id++) {
                                for (int32_t stat_idx = STAT_MIN; stat_idx < STAT_MAX; stat_idx++) {
                                        g->stats.stats[stat_idx] += recv_buf[node_id * STAT_MAX + stat_idx];
                                }
                        }


                        write_grid_stats(conf, g, false);
                }

                MPI_Barrier(MPI_COMM_WORLD);
        }

        if (conf->mpi.world_rank == 0) {
                free(recv_buf);
        }
}


#endif


#ifdef USEMPI


struct shannon_stats collect_global_stats_mpi(struct cats_configuration *conf, struct shannon_stats shannon)
{


        MPI_Datatype types[4] = {MPI_DOUBLE, MPI_INT64_T, MPI_INT64_T, MPI_INT64_T};
        MPI_Datatype stats_type;
        MPI_Aint offsets[4];
        int nitems = 4;
        int blocklens[4] = {1, 1, 1, 1};

        offsets[0] = offsetof(struct shannon_stats, shannon_sum);
        offsets[1] = offsetof(struct shannon_stats, shannon_count);
        offsets[2] = offsetof(struct shannon_stats, popsum_over_ts);
        offsets[3] = offsetof(struct shannon_stats, popsum_over_0);

        MPI_Type_create_struct(nitems, blocklens, offsets, types, &stats_type);
        MPI_Type_commit(&stats_type);

        int sendcount = 1;
        int root = 0;
        MPI_Request request;
        MPI_Status status;
        int recvcount = 1;
        struct shannon_stats *recvbuf = NULL;

        if (conf->mpi.world_rank == 0) {
                struct shannon_stats empty_stats = {0};
                recvbuf = malloc_or_die(conf->mpi.world_size * sizeof(struct shannon_stats));
                //MALLOC(recvbuf, conf->mpi.world_size * sizeof(struct shannon_stats));

                for (int32_t i = 0; i < conf->mpi.world_size; i++) {
                        recvbuf[i] = empty_stats;
                }

        }

        MPI_Igather(&shannon, sendcount, stats_type, recvbuf, recvcount, stats_type, root, MPI_COMM_WORLD, &request);
        MPI_Wait(&request, &status);
        MPI_Barrier(MPI_COMM_WORLD);

        if (conf->mpi.world_rank == 0) {
                log_message(LOG_MPI, "SHANNON  %d: sum %f - count %ld\n", 0, shannon.shannon_sum,
                            shannon.shannon_count);

                for (int i = 1; i < conf->mpi.world_size; i++) {
                        log_message(LOG_MPI, "SHANNON %d: sum %f - count %ld\n", i, recvbuf[i].shannon_sum,
                                    recvbuf[i].shannon_count);
                        shannon.shannon_sum += recvbuf[i].shannon_sum;
                        shannon.shannon_count += recvbuf[i].shannon_count;
                        shannon.popsum_over_ts += recvbuf[i].popsum_over_ts;
                        shannon.popsum_over_0 += recvbuf[i].popsum_over_0;
                }

                free(recvbuf);

                conf->global_stats.stats[MCS_POPULATED_OVER_TS] = shannon.popsum_over_ts;
                conf->global_stats.stats[MCS_POPULATED_OVER_0] = shannon.popsum_over_0;
        }

        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Type_free(&stats_type);


        int32_t *recvbuf2 = NULL;
        int count = conf->grid_count + 1;
        if (conf->mpi.world_rank == 0) {
                recvbuf2 = malloc_or_die(sizeof(int32_t) * count);
                //MALLOC(recvbuf2, sizeof(int32_t) * count);
        }

        MPI_Reduce(conf->global_stats.populated_by_classes, recvbuf2, count, MPI_INT32_T, MPI_SUM, 0, MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) {
                free(conf->global_stats.populated_by_classes);
                conf->global_stats.populated_by_classes = recvbuf2;
        }

        return shannon;
}


#endif
