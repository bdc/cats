// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_stats.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include "configuration/configuration.h"
#include "stats/global_stats.h"

struct shannon_stats collect_global_stats_mpi(struct cats_configuration *conf, struct shannon_stats shannon);

void collect_and_write_plant_stats_mpi(struct cats_configuration *conf, struct cats_grid *grid);