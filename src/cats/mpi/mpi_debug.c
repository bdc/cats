// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_debug.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "misc/output.h"
#include "paths/paths.h"
#include "mpi_debug.h"
#include "mpi_grid_helpers.h"
#include "paths/output_paths.h"


char *get_node_name(const struct cats_configuration *conf)
{


        struct string_array *name = new_string_array();
        string_array_add(name, "node");
        string_array_add_int(name, conf->mpi.world_rank, "%d");
        char *result = string_array_paste(name, "_");
        free_string_array(&name);
        return result;
}


char *dispersed_seeds_mpi_local_filename(const struct cats_configuration *conf)
{
        struct string_array *dirs = new_string_array_init("populations");
        string_array_add(dirs, "seeds");
        char *node_name = get_node_name(conf);
        struct string_array *name = standard_output_file_name(conf, NULL, NULL, node_name);
        free(node_name);
        char *extension = get_extension(conf, NULL);

        char *filename = assemble_filename(dirs, name, "_", extension);
        free_string_array(&dirs);
        free_string_array(&name);
        free(extension);
        return filename;
}


void mpi_save_locally_dispersed_seeds(struct cats_configuration *conf, struct cats_grid *grid)
{
        char *filename = dispersed_seeds_mpi_local_filename(conf);
        struct cats_dimension offset = {0};
        offset.cols = 0;
        int radius = 0;

        int rows_before = 0;
        int rows_after = 0;
        get_rows_around(conf, &rows_before, &rows_after, &radius, grid->id);
        offset.rows = conf->geometry.row_offset - rows_before;
        log_message(LOG_RAW, "rank %d, rows %d cols %d, rows before %d, rows after %d, row offset %d\n\n", conf->mpi.world_rank,
               grid->dimension.rows,
               grid->dimension.cols, rows_before, rows_after, conf->geometry.row_offset);
        struct cats_dimension dimension;
        dimension.cols = grid->dimension.cols;
        dimension.rows = grid->dimension.rows + rows_before + rows_after;
        save_2d_grid(grid->seeds_0_all, dimension, filename, conf, &offset);
        free(filename);
}