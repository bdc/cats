// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_scalefactor.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "mpi_scalefactor.h"
#include <memory/raw_arrays.h>
#include "logging.h"
#include "vital_rates/hybrid_functions.h"
#include "data/cats_grid.h"
#include <memory/cats_memory.h>


#ifdef USEMPI


// calculates rates at ot for MPI enables simulations
// only the process with rank 0 calculates the scale factor
// and broadcasts it to all other processes
void broadcast_scale_factor(struct cats_configuration *conf)
{
        long double *rates = NULL; // use long double instead of RATEPRE to make sending easier

        rates = malloc_or_die(conf->grid_count * sizeof(long double));
        zero_array(rates, conf->grid_count);

        if (conf->mpi.world_rank == 0) {
                log_message(LOG_MPI, "Rank %d will broadcast rates", conf->mpi.world_rank);

                // copy values to transfer structure
                for (int i = 0; i < conf->grid_count; i++) {
                        rates[i] = conf->param[i].scale_factor;
                }
        } else {
                log_message(LOG_MPI, "Rank %d will wait to receive rates", conf->mpi.world_rank);
        }

        MPI_Bcast(rates, conf->grid_count, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);

        // copy values from transfer structure
        if (conf->mpi.world_rank > 0) {
                for (int i = 0; i < conf->grid_count; i++) {
                        conf->param[i].scale_factor = rates[i];
                }
        }

        free(rates);

        for (int i = 0; i < conf->grid_count; i++) {
                log_message(LOG_MPI, "Rank %d - rates for grid % 3d: %Lf", conf->mpi.world_rank, i,
                            conf->param[i].scale_factor);
        }
}


#endif