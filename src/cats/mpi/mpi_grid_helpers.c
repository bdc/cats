// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_grid_helpers.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <string.h>
#include <limits.h>
#include "logging.h"
#include "configuration/configuration.h"
#include "mpi_grid_helpers.h"
#include "memory/raw_arrays.h"
#include "memory/cats_memory.h"
#include "grids/cats_grid.h"

#ifdef USEMPI


int get_cell_count_single_grid(const struct cats_configuration *conf)
{
        int64_t send_count_needed = raster_cell_count(conf->geometry.dimension);
        if (send_count_needed > INT_MAX) {
                log_message(LOG_ERROR, "%s: too many elements to send via MPI", __func__);
                exit(EXIT_FAILURE);
        }
        int send_count = (int) send_count_needed;
        return send_count;
}


void get_rows_around(const struct cats_configuration *conf, int *rows_before, int *rows_after, int *radius, int id)
{
        int rank = conf->mpi.world_rank;
        *radius = conf->param[id].plant_dispersal_max_radius - 1;

        int last_rank = conf->mpi.world_size - 1;

        if (rank > 0) {
                *rows_before = *radius;
        } else {
                *rows_before = 0;
        }

        if (rank < last_rank) {
                *rows_after = *radius;
        } else {
                *rows_after = 0;
        }
}


#endif


void copy_subgrid(float **dst, float **src, int dst_rows, int src_dst_cols, int skip_src_rows)
{
        assert(src != NULL);
        assert(dst != NULL);
        assert(dst_rows >= 0);
        assert(src_dst_cols >= 0);
        assert(skip_src_rows >= 0);

        for (int r = 0; r < dst_rows; r++) {
                for (int c = 0; c < src_dst_cols; c++) {
                        dst[r][c] = src[r + skip_src_rows][c];
                }
        }
}


double *subgrid_to_1d(float **src, int dest_rows, int src_dest_cols, int skip_src_rows)
{

        assert(skip_src_rows >= 0);
        int size = dest_rows * src_dest_cols;
        double *dest = calloc_or_die(size, sizeof(double));
        //memset(dest, 0, sizeof(double) * size);

        for (int r = 0; r < dest_rows; r++) {
                for (int c = 0; c < src_dest_cols; c++) {
                        dest[r * src_dest_cols + c] = src[r + skip_src_rows][c];

                }
        }

        return dest;
}