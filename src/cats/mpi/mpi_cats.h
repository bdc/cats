// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_cats.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_MPI_CATS_H_
#define CATS_MPI_CATS_H_

#include "cats_global.h"

struct cats_configuration;
struct cats_grid;

void mpi_update(void);

int get_mpi_rank(void);

void mpi_init(int argc, char **argv);

int mpi_setup(struct cats_configuration *conf);

void mpi_end(struct cats_configuration *conf);

void update_mpi_geometry(struct cats_configuration *conf);

void get_rows_around(const struct cats_configuration *conf, int *rows_before, int *rows_after, int *radius, int id);

float **get_grid_from_mpi(struct cats_grid *grid, struct cats_configuration *conf, float **in);

#endif