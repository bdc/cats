// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_seeds.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "logging.h"
#include "grids/cats_grid.h"
#include "mpi_grid_helpers.h"
#include "memory/raw_arrays.h"
#include "mpi_seeds.h"


void send_and_receive_dispersed_seeds_mpi(struct cats_grid *grid, struct cats_configuration *conf)
{
        int rank = conf->mpi.world_rank;
        int last_rank = conf->mpi.world_size - 1;

        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier: send and receive dispersed seeds: start"); }

        double *to_topbuffer = NULL;
        double *to_bottombuffer = NULL;

        int rows_before = 0, rows_after = 0, radius = 0;


        get_rows_around(conf, &rows_before, &rows_after, &radius, grid->id);

        int count = radius * grid->dimension.cols;

        double *from_topbuffer = new_raw_1d_array(count, sizeof(double));
        double *from_bottombuffer = new_raw_1d_array(count, sizeof(double));
        //from_topbuffer = new_double_array(count);
        //from_bottombuffer = new_double_array(count);


        // all but the first: send data up
        if (rank != 0) {
                to_topbuffer = subgrid_to_1d(grid->seeds_0_all, radius, grid->dimension.cols, 0);
        }

        // all but the last: send data down
        if (rank != last_rank) {
                to_bottombuffer = subgrid_to_1d(grid->seeds_0_all, radius, grid->dimension.cols,
                                                grid->dimension.rows + rows_before);
        }

        copy_subgrid(grid->dispersed_seeds, grid->seeds_0_all, grid->dimension.rows, grid->dimension.cols, rows_before);

        MPI_Request request_totop, request_tobottom, request_fromtop, request_frombottom;
        MPI_Status status_totop, status_tobottom, status_fromtop, status_frombottom;

        if (rank > 0) {
                MPI_Isend(to_topbuffer, count, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &request_totop);
        }

        if (rank < last_rank) {
                MPI_Isend(to_bottombuffer, count, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &request_tobottom);
        }

        if (rank < last_rank) {
                MPI_Irecv(from_bottombuffer, count, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &request_frombottom);
        }

        if (rank > 0) {
                MPI_Irecv(from_topbuffer, count, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &request_fromtop);
        }

        if (rank > 0) {
                MPI_Wait(&request_fromtop, &status_fromtop);
                MPI_Wait(&request_totop, &status_totop);
        }

        if (rank < last_rank) {
                MPI_Wait(&request_tobottom, &status_tobottom);
                MPI_Wait(&request_frombottom, &status_frombottom);
        }

        MPI_Barrier(MPI_COMM_WORLD);

        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "send and receive dispersed seeds: done"); }

        if (rank > 0) {
                add_seeds0_from_above(conf, grid, from_topbuffer, radius);
                free(to_topbuffer);
                to_topbuffer = NULL;
        }

        if (rank < last_rank) {
                add_seeds0_from_below(conf, grid, from_bottombuffer, radius);
                free(to_bottombuffer);
                to_bottombuffer = NULL;
        }

        // save_seeds_info(conf, grid);

        free(from_bottombuffer);
        free(from_topbuffer);
        from_bottombuffer = NULL;
        from_topbuffer = NULL;


/*
        char *filename;
        int rc = asprintf(&filename, "disptest/seeeds-dispersed-%d-%d-%d.tiff",  conf->time.year_current, grid->id, conf->param_replicate);
        asprintf_check(rc);
        save_2d_grid(grid->dispersed_seeds, grid->rows, grid->cols, filename, conf);
        free(filename);
*/

}


void add_seeds0_from_below(const struct cats_configuration *conf, struct cats_grid *grid, const double *from_below,
                           int radius)
{
        log_message(LOG_INFO, "dispersal from below");
        int32_t cols = conf->geometry.dimension_orig.cols;
        int start_row = grid->dimension.rows - radius;
        //printf("%d %d %d\n", radius, start_row, grid->dimension.rows);
        assert(radius + start_row <= grid->dimension.rows);

        for (int r = 0; r < radius; r++) {
                for (int c = 0; c < cols; c++) {
                        grid->dispersed_seeds[start_row + r][c] += from_below[r * cols + c];
                }
        }
}


void add_seeds0_from_above(const struct cats_configuration *conf, struct cats_grid *grid, const double *from_above,
                           int radius)
{
        log_message(LOG_INFO, "dispersal from above");
        int32_t cols = conf->geometry.dimension_orig.cols;
        assert(radius < grid->dimension.rows);


        for (int r = 0; r < radius; r++) {
                for (int c = 0; c < cols; c++) {
                        grid->dispersed_seeds[r][c] += from_above[r * cols + c];
                }
        }
}