// SPDX-License-Identifier: GPL-3.0-or-later
//
// mpi_save.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "paths/paths.h"
#include "cats_global.h"
#include "memory/raw_arrays.h"
#include "grids/cats_grid.h"
#include "data/cats_grid.h"
#include "mpi_save.h"
#include "mpi_seeds.h"
#include "logging.h"
#include "grids/gdal_load.h"
#include "grids/gdal_save.h"
#include "memory/cats_memory.h"
#include "mpi/mpi_grid_helpers.h"
#include "paths/output_paths.h"


/*
int32_t *mpi_combine_int32_grid(struct cats_configuration *conf, int32_t **input)
{
        int32_t *receive_buffer = NULL;
        int64_t receive_count = raster_cell_count(conf->geometry.dimension_orig);
        int send_count = get_cell_count_single_grid(conf);

        if (conf->mpi.world_rank == 0) {
                receive_buffer = calloc_or_die(receive_count, sizeof(int32_t));
        }

        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }
        MPI_Request request;
        MPI_Status status;
        MPI_Igatherv(input, send_count, MPI_FLOAT, receive_buffer, conf->geometry.cells_per_chunk,
                     conf->geometry.displacements, MPI_FLOAT, 0, MPI_COMM_WORLD, &request);
        MPI_Wait(&request, &status);
        int32_t *result = NULL;
        if (conf->mpi.world_rank == 0) {

                cats_dt_coord cols = conf->geometry.dimension_orig.cols;
                cats_dt_coord rows = conf->geometry.dimension_orig.rows;

                result = calloc_or_die(rows * cols, sizeof(int32_t));


                for (int i = 0; i < receive_count; i++) {
                        result[i] = receive_buffer[i * cols];
                }

        }
        MPI_Barrier(MPI_COMM_WORLD);
        return result;
}
*/

void *save_seeds0_to_gdal_mpi(struct cats_grid *grid, struct cats_configuration *conf)
{
        float *seeds0 = malloc_or_die(sizeof(float) * grid->dimension.rows * grid->dimension.cols);

        for (int r = 0; r < grid->dimension.rows; r++) {
                for (int c = 0; c < grid->dimension.cols; c++) {
                        seeds0[r * grid->dimension.cols + c] = grid->dispersed_seeds[r][c];
                }
        }

        int64_t receive_count = raster_cell_count(
                conf->geometry.dimension_orig); //conf->geometry.dimension_orig.cols * conf->geometry.dimension_orig.cols;
        float *receive_buffer = NULL;

        if (conf->mpi.world_rank == 0) {
                receive_buffer = malloc_or_die(sizeof(float) * receive_count);
        }

        float *send = seeds0;

        int send_count = get_cell_count_single_grid(conf);

        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }
        MPI_Request request;
        MPI_Status status;
        MPI_Igatherv(send, send_count, MPI_FLOAT, receive_buffer, conf->geometry.cells_per_chunk,
                     conf->geometry.displacements, MPI_FLOAT, 0, MPI_COMM_WORLD, &request);
        MPI_Wait(&request, &status);

        if (conf->mpi.world_rank == 0) {
                char *filename;
                int id = grid->id;
                int rc = asprintf(&filename, "mpitest/seeds0/seeds0_%s_%s_%d_%d.tiff",
                                  conf->param[id].species_name, conf->run_name, conf->time.year_current,
                                  conf->simulation.replicate);
                asprintf_check(rc);

                float **tmpgrid = malloc_or_die(sizeof(float *) * conf->geometry.dimension_orig.rows);
                //MALLOC(tmpgrid, sizeof(float *) * conf->geometry.dimension_orig.rows);
                int32_t cols = conf->geometry.dimension_orig.cols;
                for (int i = 0; i < conf->geometry.dimension_orig.rows; i++) {
                        tmpgrid[i] = &receive_buffer[i * cols];
                }

                struct grid_wrapper xdata = gridwrapper(tmpgrid, conf->geometry.dimension_orig);
                //xdata.data_float = tmpgrid;
                save_grid_to_gdal(&xdata, GDT_Float32, conf, filename, conf->param[id].species_name);

                free(filename);
                free(receive_buffer);
                free(tmpgrid);
        }
        free(seeds0);
        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }

        return 0;
}


void *save_population_to_gdal_mpi(struct cats_grid *grid, struct cats_configuration *conf)
{

        assert(grid != NULL);
        assert(conf != NULL);

        int id = grid->id;
        assert(id >= 0);

        log_message(LOG_INFO, "cycle %3d: saving adult population", conf->time.year_current);
        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }

        int64_t receive_count = raster_cell_count(
                conf->geometry.dimension_orig); //conf->geometry.dimension_orig.cols * conf->geometry.dimension_orig.cols;
        int32_t *receive_buffer = NULL;

        if (conf->mpi.world_rank == 0) {
                receive_buffer = malloc_or_die(sizeof(int32_t) * receive_count);
        }

        int32_t *send = grid->_population;
        int send_count = get_cell_count_single_grid(conf);
        //int32_t send_count = conf->geometry.dimension.rows * conf->geometry.dimension.cols;

        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }

        //int MPI_Gatherv(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, const int *recvcounts, const int *displs,  MPI_Datatype recvtype, int root, MPI_Comm comm)

        fflush(stdout);
        MPI_Request request;
        MPI_Status status;
        MPI_Igatherv(send, send_count, MPI_INT, receive_buffer, conf->geometry.cells_per_chunk,
                     conf->geometry.displacements, MPI_INT, 0, MPI_COMM_WORLD, &request);
        MPI_Wait(&request, &status);

        if (conf->mpi.world_rank == 0) {


                int32_t **tmp_grid = malloc_or_die(sizeof(int32_t *) * conf->geometry.dimension_orig.rows);
                //MALLOC(tmpgrid, sizeof(int32_t *) * conf->geometry.dimension_orig.rows);
                int32_t cols = conf->geometry.dimension_orig.cols;
                for (int i = 0; i < conf->geometry.dimension_orig.rows; i++) {
                        tmp_grid[i] = &receive_buffer[i * cols];
                }

                struct grid_wrapper data = gridwrapper(tmp_grid, conf->geometry.dimension_orig);

                char *filename = get_current_population_filename(grid, conf);

                save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
                free(receive_buffer);
                free(filename);
                free(tmp_grid);
        }

        return 0;
}


void save_predictors_gdal_mpi(struct cats_grid *grid, struct cats_configuration *conf)
{
        log_message(LOG_UNIMPLEMENTED, "not implemented");
        double *suit = malloc_or_die(sizeof(double) * grid->dimension.rows * grid->dimension.cols);
        //MALLOC(suit, sizeof(double) * grid->rows * grid->cols);

        for (int r = 0; r < grid->dimension.rows; r++) {
                for (int c = 0; c < grid->dimension.cols; c++) {
                        //suit[r * grid->dimension.cols + c] = get_suitability_glm(conf, grid, r, c);
                }
        }

        int64_t receive_count = raster_cell_count(conf->geometry.dimension_orig);
        double *receive_buffer = NULL;

        if (conf->mpi.world_rank == 0) {
                receive_buffer = malloc_or_die(sizeof(double) * receive_count);
                //MALLOC(receive_buffer, sizeof(double) * receive_count);
        }

        double *send = suit;
        int send_count = get_cell_count_single_grid(conf);
        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }
        MPI_Request request;
        MPI_Status status;
        MPI_Igatherv(send, send_count, MPI_DOUBLE, receive_buffer, conf->geometry.cells_per_chunk,
                     conf->geometry.displacements, MPI_DOUBLE, 0, MPI_COMM_WORLD, &request);
        MPI_Wait(&request, &status);

        if (conf->mpi.world_rank == 0) {
                char *filename;
                int id = grid->id;
                int rc = asprintf(&filename, "predictors/generated/%s_%s_%d_%d.tiff",
                                  conf->param[id].species_name, conf->run_name, conf->time.year_current,
                                  conf->simulation.replicate);
                asprintf_check(rc);

                double **tmp_grid = malloc_or_die(sizeof(double *) * conf->geometry.dimension_orig.rows);
                //MALLOC(tmpgrid, sizeof(double *) * conf->geometry.dimension_orig.rows);
                int32_t cols = conf->geometry.dimension_orig.cols;
                for (int i = 0; i < conf->geometry.dimension_orig.rows; i++) {
                        tmp_grid[i] = &receive_buffer[i * cols];
                }

                struct grid_wrapper xdata = gridwrapper(tmp_grid, conf->geometry.dimension_orig);

                save_grid_to_gdal(&xdata, GDT_Float64, conf, filename, conf->param[id].species_name);

                free(filename);
                free(receive_buffer);
                free(tmp_grid);
        }
        free(suit);
        MPI_Barrier(MPI_COMM_WORLD);
        if (conf->mpi.world_rank == 0) { log_message(LOG_MPI, "Barrier"); }
}
