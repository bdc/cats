// SPDX-License-Identifier: GPL-3.0-or-later
//
// inline_carrying_capacity.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//



#ifndef CATS_INLINE_CARRYING_CAPACITY_H
#define CATS_INLINE_CARRYING_CAPACITY_H

#include <math.h>
#include <assert.h>
#include "defaults.h"
#include "inline_overlays.h"
#include "inline_population.h"
#include "inline_vital_rates.h"

#include "configuration/configuration.h"
#include "plants/juveniles.h"
#include "inline_vital_ages.h"


static inline cats_dt_rates juvenile_cc_multiplier(const struct cats_species_param *param, int32_t stage)
{
        const int32_t max_age_maturity = get_vital_age_from_param(param, VA_AGE_OF_MATURITY_MAX);
        cats_dt_rates weight = (cats_dt_rates) (stage + 1) / (cats_dt_rates) (max_age_maturity + 1);
        assert(weight <= 1.0);
        assert(weight > 0.0);
        return weight * weight;

}


static inline cats_dt_population
get_adult_carrying_capacity(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        return multiply_population(get_carrying_capacity(grid, row, col), grid->param.max_adult_cc_fraction);
}


static inline cats_dt_population
get_adult_carrying_capacity_from_cc(const struct cats_grid *grid, cats_dt_population K)
{
        cats_dt_rates multiplier = grid->param.max_adult_cc_fraction;
        return multiply_population(K, multiplier);
}


#endif //CATS_INLINE_CARRYING_CAPACITY_H
