// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

/// @file cats_main.c
/// @brief starting point for CATS


/// \mainpage CATS
///
/// \section intro_sec Introduction
///

#include "cats_global.h"

#include <cats_ini/cats_ini.h>
#include <logging/logging.h>

#include "cats/command_line/command_line_info.h"
#include "cats/command_line/command_line_options.h"
#include "configuration/load_configuration.h"
#include "hybrid/scalefactor.h"

#include "temporal/years.h"
#include "paths/directory_helper.h"
#include "debug/debug.h"
#include "grids/cats_grid.h"
#include "modules/load_module.h"
#include "actions/setup_actions.h"
#include "configuration/load_configuration_species_params.h"
#include "environment/environment_set.h"
#include "vital_rates/default_vital_rates.h"


#ifdef USEMPI
#include "mpi/mpi_cats.h"
#endif

struct cats_global global;
struct cats_debug_options cats_debug;

struct cats_configuration *load_main_configuration(const struct program_options *options);

void run_simulation(struct cats_configuration *conf);

void start_single_cell_debugging(char **argv);


/// @brief main())
///
/// required command line parameters
/// * configuration file 
///
/// optional command line parameters
/// * run number 
/// * pre-defined scale factor

int main(int argc, char **argv)
{
        init_global_info("cats_main");
#ifdef USEMPI
        mpi_init(argc, argv);
#endif

        struct program_options options = check_cats_main_arguments(argc, argv);
        logging_initialize(options.default_log_level, &global.time_info, options.log_file, options.quiet);
        print_version_info();
        print_runtime_information(argc, argv);
#ifdef USEMPI
        mpi_update();
#endif



        log_message(LOG_IMPORTANT, "juvenile weights are squared");

        struct cats_configuration *conf = load_main_configuration(&options);


        //exit(1);

        DBG_SINGLE(start_single_cell_debugging(argv);)

        show_run_info(conf, &options);



        // HYBRID MODE ONLY
        setup_scale_factor(conf, &options);

        // HERE IT STARTS
        run_simulation(conf);

#ifdef USEMPI
        mpi_end(conf);
#endif

        cleanup_debugging(&cats_debug, conf);
        cleanup_configuration(&conf);
        // FIXME cleanup_logging
        free(global.program_name);

        fflush(stdout);
        fflush(stderr);
        exit_cats(EXIT_SUCCESS);
}

/// @brief loads the configuration information (first argument) and sets the simulation mode


void load_modules(struct cats_configuration *conf)
{
        for (int32_t i = 0; i < conf->modules.found_count; i++) {
                int32_t id = load_module(conf, &conf->modules.module[i]);

                log_message(LOG_IMPORTANT, "Loaded module %d of %d: '%s' with id %d and flags %d",
                            i + 1, conf->modules.count, conf->modules.module[i].name, id, conf->modules.module[i].flags);

        }

        for (int32_t i = 0; i < conf->param_count; i++) {
                struct cats_species_param *p = &conf->param[i];
                struct cats_module *demo_module = p->demographic_module;

                if (demo_module != NULL && !(demo_module->flags & MODULE_ALTERNATE_DEMOGRAPHIC)) {
                        log_message(LOG_ERROR, "%s: module id %d: '%s' does not provide demographics (flags: %d)",
                                    __func__, i, demo_module->name, demo_module->flags);
                        exit_cats(EXIT_FAILURE);
                }


                for (int32_t j = 0; j < MAX_MODULES; j++) {
                        struct module_species_data *module = &p->module_data[j];
                        if (module->vital_rate_count == 0) continue;
                        for (int32_t k = 0; k < module->vital_rate_count; k++) {
                                struct cats_vital_rate *vr = module->vr[k];
                                load_conf_vital_rate(vr, conf, conf->ini, p->species_config_section, p);


                                struct cats_environment *set = NULL;

                                if (p->parametrization == PARAM_HYBRID) {
                                        set = get_environment(conf, p->suitability_name);
                                        post_process_vital_rate(conf, vr, set, p);
                                }

                        }

                        log_message(LOG_INFO, "%s: Loading vital rate functions for module id %d", __func__, j);

                                if (p->module_data[j].load_species_param_function) {
                                        p->module_data[j].load_species_param_function(conf, conf->ini, p->species_config_section, p);
                                }


                }

        }

}


struct cats_configuration *load_main_configuration(const struct program_options *options)
{
        struct cats_configuration *conf = load_configuration_from_file(options->configuration_file, options);
        // should already have passed through check_cats_main_arguments at this point
        setup_directories(conf);
        post_process_configuration(conf);
        //print_config_summary(conf);
        register_default_action_functions(conf);


        load_modules(conf);
        setup_module_directories(conf);


        setup_simulation_actions(conf);
        // which values of the configuration file did we ignore?
        return conf;
}


void run_simulation(struct cats_configuration *conf)
{
        log_message(LOG_INFO, "Simulation will use %d grids (species)", conf->grid_count);

        struct cats_grid **grids = create_and_initialize_grids(conf, conf->grid_count);

        run_phase(grids, conf, PHASE_BURN_IN);
        run_phase(grids, conf, PHASE_WARM_UP);
        run_phase(grids, conf, PHASE_SIMULATION);

        cleanup_array_of_grids(&grids, conf->grid_count);
}
