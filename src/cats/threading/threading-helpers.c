// SPDX-License-Identifier: GPL-3.0-or-later
//
// threading-helpers.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "data/cats_datatypes.h"
#include "threading-helpers.h"
#include "logging.h"
#include "threading.h"
#include "misc/cats_maths_inline.h"
#include "misc/cats_random.h"
#include "stats/grid_stats.h"
#include "data/error.h"
#include "memory/cats_memory.h"

void create_custom_stats_for_thread(struct statistics *thread_stats, struct statistics *grid_stats)
{
        thread_stats->custom_stat_count = grid_stats->custom_stat_count;
        thread_stats->custom_stats = calloc_or_die(grid_stats->custom_stat_count, sizeof(int64_t));
}

void
initialize_thread(struct cats_thread_info *thread, struct cats_grid *grid, struct cats_configuration *conf, int32_t id)
{
        thread->rng = allocate_rng();
        thread->rng_seed = conf->rng_seed;

        for (int32_t i = 0; i < conf->grid_count; i++) {

                create_custom_stats_for_thread(&thread->stats[i], &grid->stats);
                zero_statistics_stats(&thread->stats[i]);
        }

        thread->seed = get_random_seed(false);

        for (int i = 0; i < 4; i++) {
                thread->seed4[i] = get_random_seed64(false);
        }
        thread->id = id;
        thread->grid = grid;
        thread->conf = conf;
        thread->rng_buf_size = 32;
        thread->rng_state_buffer = calloc_or_die(1, thread->rng_buf_size);
        thread->rng_state = calloc_or_die(1, sizeof(struct random_data));
        initstate_r(get_random_seed(false), thread->rng_state_buffer, thread->rng_buf_size, thread->rng_state);
}


int32_t get_dispersal_radius(const struct cats_configuration *conf, int32_t id)
{
        return max_int32t(50, conf->param[id].plant_dispersal_max_radius + 1);
}


void cleanup_threads(struct cats_thread_info *threads, int32_t num_threads)
{

        for (int32_t i = 0; i < num_threads; i++) {

                gsl_rng_free(threads[i].rng);
                free(threads[i].rng_state);
                free(threads[i].rng_state_buffer);
        }

        for (int32_t i = 0; i < num_threads; i++) {
                for (int32_t j = 0; j < threads[i].conf->grid_count; j++) {
                        free(threads[i].stats->custom_stats);
                }
        }
        free(threads);


}


void set_area(struct cats_area *area, cats_dt_coord start_row, cats_dt_coord end_row, cats_dt_coord start_col,
              cats_dt_coord end_col)
{
        assert(area != NULL);

        area->start_row = start_row;
        area->end_row = end_row;
        area->start_col = start_col;
        area->end_col = end_col;
}


void lock_mutex_or_die(pthread_mutex_t *mutex)
{
        int rc = pthread_mutex_lock(mutex);
        if (rc != 0) {
                log_message(LOG_ERROR, "error locking mutex");
                exit_cats(E_MUTEX_ERROR);
        }
}


void unlock_mutex_or_die(pthread_mutex_t *mutex)
{
        int rc = pthread_mutex_unlock(mutex);
        if (rc != 0) {
                log_message(LOG_ERROR, "error unlocking mutex");
                exit(E_MUTEX_ERROR);
        }
}


void threaded_action(void (*action)(struct cats_grid *, struct cats_thread_info *), void *data, void *config,
                     enum thread_strategy strategy)
{

        log_message(LOG_INFO, "running threaded code with strategy: %d (%s)", strategy,
                    get_threading_stragegy_name(strategy));

        run_action_threaded(action, data, config, strategy);
}


struct cats_thread_info full_area_thread(struct cats_configuration *conf, struct cats_grid *grid)
{
        struct cats_thread_info ti = {0};
        initialize_thread(&ti, grid, conf, 0);
        set_area(&ti.area, 0, grid->dimension.rows, 0, grid->dimension.cols);
        return ti;
}


const char *get_threading_stragegy_name(enum thread_strategy s)
{
        switch (s) {

                case TS_UNKNOWN:
                        return "unknown";
                case TS_DEFAULT:
                        return "default";
                case TS_ROW:
                        return "row-wise";
                case TS_DISPERSAL:
                        return "dispersal";
        }
        return "other";
}