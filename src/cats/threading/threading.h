// SPDX-License-Identifier: GPL-3.0-or-later
//
// threading.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_THREADING_H
#define CATS_THREADING_H

#include <pthread.h>
#include "data/cats_grid.h"

typedef void (*cats_task_function)(struct cats_grid *, struct cats_thread_info *);


/** @brief defines an rectangular sub-area of a grid
  The intervals are usually half-open: [start_row, end_row) and [start_col, start_col)
*/
struct cats_area {
        cats_dt_coord start_row;  ///< index of the first row
        cats_dt_coord end_row;    ///< index of the last row (exclusive)
        cats_dt_coord start_col;  ///< index of the first column
        cats_dt_coord end_col;    ///< index of the last column  (exclusive)
};


struct cats_thread_info {
        struct cats_area area;                  // part of the grid that the action acts on
        int id;
        struct cats_thread_controller *thread_controller;
        struct statistics stats[MAX_CLASSES];
        struct cats_grid *grid;
        struct cats_configuration *conf;
        gsl_rng *rng;
        unsigned int rng_seed;
        char *rng_state_buffer;
        int32_t rng_buf_size;
        struct random_data *rng_state;
        unsigned int seed;
        uint64_t seed4[4];

};

enum thread_strategy {
        TS_UNKNOWN = 0,
        TS_DEFAULT = 1,
        TS_BLOCK = 1,
        TS_ROW = 2,
        TS_DISPERSAL = 3,
};


enum THREAD_STATUS {
        THREAD_STATUS_WAIT = -1,      ///< no tasks available, wait and retry
        THREAD_STATUS_FINISHED = 0,   ///< all tasks processed, we can stop now
        THREAD_STATUS_OK = 1          ///< task is waiting
};


enum TASK_AREA_STATUS {
        TASK_AREA_TODO = 0,   ///< this area has not yet been processed
        TASK_AREA_INWORK = 1, ///< this area is currently being processed
        TASK_AREA_DONE = 2    ///< this area has already been processed
};


struct thread_task {
        struct cats_coordinates task_coords;
        struct cats_area area;
        enum THREAD_STATUS status;
};

typedef struct thread_task (*cats_task_getter)(struct cats_thread_controller *t, int32_t thread_id);

struct cats_thread_controller {
        int32_t num_threads;

        // thread strategy provides information about how the individual task areas are shaped and what
        // constraints have to be met, e.g. for dispersal
        enum thread_strategy strategy;

        // per-thread struct
        struct cats_thread_info *threads;


        cats_task_function task_func;           //< function which will be applied to each task area
        cats_task_getter task_getter;           //< function that will provide the next task area
        struct cats_grid *grid;                 //< pointer to the simulation grid
        struct cats_configuration *conf;        //< pointer to the simulation configuration
        pthread_mutex_t task_mutex;             //< mutex, protects all subsequent variables

        // the area of the simulation grid is subdivided into (non-overlapping) task areas
        // threads will request a task area using the task_getter function and apply the function
        // task_func on that area
        //
        // For most tasks (i.e. the cell-local ones) each cell (and so also each area) can be processed
        // in parallel. It is important that task functions do not modify any values outside their designated area.
        // This includes grid statistics. For this reason, all task function should only modify
        // the per-thread grid statistic, which gets aggregated after all threads have finished.
        //
        // There is on exception: dispersal tasks are per definition non-local. The task getter function for
        // dispersal tasks has to ensure, that no two threads can write to data of the same cell at the same time.
        // This is done by setting the task area larger than a single dispersal can be, and then forcing another constraint:
        // There has to be a buffer of at least 2 task areas (in each dimension) between any active dispersal areas.
        // The dispersal of cells within the active area can only reach neighbouring areas, but never further than that.

        // A grid of tasks. Each entry corresponds to one task. Each entry is in one of the TASK_AREA_STATUS:
        // waiting (called TO DO), currently active (IN WORK) or already finished (DONE).
        // Initially all tasks will be set to waiting.
        struct cats_2d_array_char *task_grid;

        // tasks_size_rows and task_size_cols give the size (in simulation grid cells) of single task.
        // The actual task can be smaller, if the tasks area exceeds the simulation grid area at its borders, i.e.
        // the simulation grid does not have to be an integer multiple of the individual task dimensions
        cats_dt_coord task_size_rows;
        cats_dt_coord task_size_cols;

        // bookkeeping variable, initially set to the number of tasks (rows * columns of the task grid).
        // Once we reach zero we know we are done. But we should still check whether all tasks have marked as done.
        int32_t tasks_remaining;

};


void run_action_threaded(void (*action)(struct cats_grid *, struct cats_thread_info *), void *data, void *config,
                         enum thread_strategy strategy);

#endif //CATS_THREADING_H
