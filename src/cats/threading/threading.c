// SPDX-License-Identifier: GPL-3.0-or-later
//
// threading.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stdint.h>
#include "cats_global.h"
#include <memory/cats_memory.h>

#include "block_tasks.h"
#include "misc/misc.h"
#include "stats/grid_stats.h"
#include "threading.h"
#include "threading/threading-helpers.h"
#include "threading/dispersal_tasks.h"


#ifdef CATS_DEBUG_THREADING
#include "debug/debug.h"
#endif
#if defined(CATS_ON_WINDOWS) && ! defined(__MINGW32__)
#include <Windows.h>
#else

#include <unistd.h>

#endif


void mark_task_done(struct cats_thread_controller *tc, struct thread_task task, int32_t thread_id)
{
        int32_t row = task.task_coords.row;
        int32_t col = task.task_coords.col;

        lock_mutex_or_die(&tc->task_mutex);


        if (tc->task_grid->data[row][col] != TASK_AREA_INWORK) {
                log_message(LOG_ERROR, "%s: threading error: task row %d col %d has status %d (thread %d)", __func__,
                            row, col,
                            tc->task_grid->data[row][col], thread_id);
                exit(EXIT_FAILURE);
        }

#ifdef CATS_DEBUG_THREADING
        debug_bare(thread_id, "{ \"ts\": %f, \"action_id\": %Ld, \"msg\": \"task finished\", \"thread id\": %d, "
                       "\"task\": {\"row\": %d, \"col\": %d }}\n", timestamp(), tc->conf->cycle.action_counter, thread_id, row, col);
#endif
        tc->task_grid->data[row][col] = TASK_AREA_DONE;
        unlock_mutex_or_die(&tc->task_mutex);
}


struct cats_thread_controller *
setup_threads(struct cats_configuration *conf, struct cats_grid *grid, int32_t max_threads,
              enum thread_strategy strategy, cats_task_function task_func)
{

        struct cats_thread_controller *t = calloc_or_die(1, sizeof(struct cats_thread_controller));
        t->num_threads = max_threads;
        t->strategy = strategy;
        t->conf = conf;
        t->grid = grid;
        t->task_func = task_func;
        t->threads = calloc_or_die(t->num_threads, sizeof(struct cats_thread_info));

        int rc = pthread_mutex_init(&t->task_mutex, NULL);
        if (rc != 0) {
                log_message(LOG_ERROR, "error initializing mutex");
                exit(EXIT_FAILURE);
        }

        for (int32_t i = 0; i < max_threads; i++) {
                initialize_thread(&t->threads[i], grid, conf, i);
                t->threads[i].thread_controller = t;
        }




        switch (t->strategy) {
                case TS_UNKNOWN:
                        log_message(LOG_ERROR, "undefined threading strategy");
                        exit(EXIT_FAILURE);
                case TS_ROW:
                        setup_block_tasks(t, 1, grid->dimension.cols);
                        break;
                case TS_BLOCK:
                        setup_block_tasks(t, 225, 225);
                        break;
                case TS_DISPERSAL:
                        setup_dispersal_tasks(t);
                        break;

        }

        return t;

}


void *thread_task_loop(void *data)
{
        struct cats_thread_info *thread = (struct cats_thread_info *) data;

        struct cats_thread_controller *tc = thread->thread_controller;

        while (1) {
                struct thread_task task = tc->task_getter(tc, thread->id);

                switch (task.status) {
                        case THREAD_STATUS_WAIT:
#ifdef CATS_ON_WINDOWS
                                Sleep(1);
#else
                                usleep(25);
#endif
                                continue;

                        case THREAD_STATUS_FINISHED:
                                pthread_exit(0);

                        case THREAD_STATUS_OK:

                                if (task.area.end_col > tc->grid->dimension.cols) {
                                        task.area.end_col = tc->grid->dimension.cols;
                                }
                                if (task.area.end_row > tc->grid->dimension.rows) {
                                        task.area.end_row = tc->grid->dimension.rows;
                                }
#ifdef CATS_DEBUG_THREADING
                                debug_bare(thread->id,
                                           "{ \"ts\": %f, \"action_id\": %Ld, \"msg\": \"task assigned\", \"thread id\": %d, "
                                           "\"task\": {\"row\": %d, \"col\": %d }, "
                                           "\"area\": {\"start row\": %d, \"start col\": %d, \"end row\": %d, \"end col\": %d}"
                                           "}\n", timestamp(),
                                           tc->conf->cycle.action_counter, thread->id, task.task_coords.row, task.task_coords.col,
                                           task.area.start_row, task.area.start_col, task.area.end_row, task.area.end_col);


#endif
                                thread->area = task.area;

                                tc->task_func(thread->grid, thread);
                                mark_task_done(tc, task, thread->id);
                                break;
                }
        }
}


bool all_tasks_done_check(const struct cats_thread_controller *t)
{
        const struct cats_2d_array_char *tg = t->task_grid;

        for (int32_t row = 0; row < tg->dimension.rows; row++) {
                for (int32_t col = 0; col < tg->dimension.cols; col++) {
                        if (tg->data[row][col] != TASK_AREA_DONE) return false;
                }
        }

        return true;
}


void verify_tasks(const struct cats_thread_controller *tc)
{
        int64_t errors = 0;
        for (int32_t r = 0; r < tc->task_grid->dimension.rows; r++) {
                for (int32_t c = 0; c < tc->task_grid->dimension.cols; c++) {
                        if (tc->task_grid->data[r][c] != TASK_AREA_DONE) {
                                log_message(LOG_ERROR, "task %d %d did not complete", r, c);
                                errors += 1;
                        }
                }
        }

        if (errors) {
                exit(EXIT_FAILURE);
        }
}


void run_threads(struct cats_thread_controller **th)
{
        struct cats_thread_controller *t = *th;

#ifdef __STDC_NO_VLA__
        pthread_t *threads = malloc_or_die(sizeof(pthread_t) * t->num_threads);
#else
        pthread_t threads[t->num_threads];
#endif
        log_message(LOG_DEBUG, "%s: starting %d threads", __func__, t->num_threads);
        for (int32_t i = 0; i < t->num_threads; i++) {
                int rc = pthread_create(&threads[i], NULL, thread_task_loop, &t->threads[i]);
                if (rc != 0) {
                        log_message(LOG_ERROR, "error creating thread");
                        exit(E_THREADING_ERROR);
                }
        }

        for (int32_t i = 0; i < t->num_threads; i++) {
                pthread_join(threads[i], NULL);
        }

        bool done = all_tasks_done_check(t);
        if (done == false) {
                log_message(LOG_ERROR, "%s: threaded tasks were not all finished", __func__);
                exit_cats(EXIT_FAILURE);
        }

        consolidate_thread_stats(t->grid, t->conf, t->threads, t->num_threads);
        verify_tasks(t);
        cleanup_threads(t->threads, t->num_threads);
        cleanup_2d_array_char(&t->task_grid);
        t->threads = NULL;
        free(t);
#ifdef __STDC_NO_VLA__
        free(threads);
#endif
        th = NULL;
}


void run_action_threaded(void (*action)(struct cats_grid *, struct cats_thread_info *), void *data, void *config,
                         enum thread_strategy strategy)
{

#ifdef CATS_DEBUG_THREADING
        reset_threading_grid(&debug);
#endif
        struct cats_configuration *conf = (struct cats_configuration *) config;
        struct cats_grid *grid = (struct cats_grid *) data;
        const int32_t num_threads = conf->param_max_threads;

        struct cats_thread_controller *t = setup_threads(conf, grid, num_threads, strategy, action);
#ifdef CATS_DEBUG_THREADING
        debug_bare(-1, "\n");
        debug_bare(-1, "{ \"ts\": %f, \"action_id\": %Ld,  \"msg\": \"thread start\", \"action\": \"%s\", \"year\": %d, \"threads\": %d, "
                                   "\"task grid\": {\"rows\": %d, \"cols\": %d }, \"task\": {\"rows\": %d, \"cols\": %d}}\n",
                   timestamp(), conf->cycle.action_counter, conf->cycle.actions[conf->cycle.current_action].message, conf->time.year_current, t->num_threads,
                   t->task_grid->dimension.rows, t->task_grid->dimension.cols, t->task_size_rows, t->task_size_cols);
#endif
        log_message(LOG_DEBUG, "%s: starting threads", __func__);
        run_threads(&t);
        log_message(LOG_DEBUG, "%s: done threads", __func__);
#ifdef CATS_DEBUG_THREADING
        debug_bare(-1, "{ \"ts\": %f, \"action_id\": %Ld,  \"msg\": \"thread end\", \"action\": \"%s\", \"year\": %d }\n",
                   timestamp(), conf->cycle.action_counter, conf->cycle.actions[conf->cycle.current_action].message, conf->time.year_current);
        debug_bare(-1, "\n");
        verify_threading_grid(&debug);
#endif
}