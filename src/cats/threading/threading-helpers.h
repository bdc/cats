// SPDX-License-Identifier: GPL-3.0-or-later
//
// threading-helpers.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_THREADING_HELPERS_H
#define CATS_THREADING_HELPERS_H

#include "data/cats_grid.h"
#include "threading/threading.h"

struct cats_thread_info full_area_thread(struct cats_configuration *conf, struct cats_grid *grid);


/**
 * @brief control structure for a thread
 *
 * Parallelisation is done by splitting up a grid into smaller, rectangular areas
 * which are processed in parallel using a task queue. Most actions consist of
 * independent tasks, in which the order of processing does not matter (as each
 * action is cell-local.
 * This is not true for seed dispersal, where the task dispatcher ensures
 * that different threads wont access the same cells (outsider their area) at
 * the same time.
 */
struct cats_thread_controller;
struct cats_configuration;

int32_t get_dispersal_radius(const struct cats_configuration *conf, int32_t id);

void threaded_action(void (*action)(struct cats_grid *grid, struct cats_thread_info *ts), void *data, void *config,
                     enum thread_strategy strategy);

const char *get_threading_stragegy_name(enum thread_strategy s);

void set_area(struct cats_area *area, cats_dt_coord start_row, cats_dt_coord end_row, cats_dt_coord start_col,
              cats_dt_coord end_col);

void lock_mutex_or_die(pthread_mutex_t *mutex);

void cleanup_threads(struct cats_thread_info *threads, int32_t num_threads);

void unlock_mutex_or_die(pthread_mutex_t *mutex);

void
initialize_thread(struct cats_thread_info *thread, struct cats_grid *grid, struct cats_configuration *conf, int32_t id);

#endif //CATS_THREADING_HELPERS_H
