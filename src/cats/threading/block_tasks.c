// SPDX-License-Identifier: GPL-3.0-or-later
//
// block_tasks.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <stdint.h>
#include <memory/arrays.h>
#include <memory/cats_memory.h>

#include "threading/threading-helpers.h"
#include "threading.h"
#include "misc/misc.h"
#include "threading/block_tasks.h"


struct thread_task block_task_getter(struct cats_thread_controller *tc, int32_t thread_id)
{
        struct thread_task result = {
                .task_coords.row = -1,
                .task_coords.col = -1,
                .status = THREAD_STATUS_WAIT,
                .area = {.start_row = -1, .start_col = -1}
        };

        lock_mutex_or_die(&tc->task_mutex);
        //printf("%d tasks remaining\n", t->tasks.tasks_remaining);
        if (tc->tasks_remaining == 0) {

                result.status = THREAD_STATUS_FINISHED;
                unlock_mutex_or_die(&tc->task_mutex);
                return result;
        }

        bool done = false;
        for (int32_t task_row = 0; task_row < tc->task_grid->dimension.rows; task_row++) {
                for (int32_t task_col = 0; task_col < tc->task_grid->dimension.cols; task_col++) {
                        if (tc->task_grid->data[task_row][task_col] == TASK_AREA_TODO) {
                                result.task_coords.row = task_row;
                                result.task_coords.col = task_col;
                                result.status = THREAD_STATUS_OK;
                                tc->task_grid->data[task_row][task_col] = TASK_AREA_INWORK;

                                tc->tasks_remaining -= 1;
                                done = true;

                                break;
                        }
                }
                if (done) { break; }
        }
        unlock_mutex_or_die(&tc->task_mutex);

        // the task area is a whole row, from the first to the last column
        // there is a 1:1 correspondence between task rows and grid rows
        if (result.status == THREAD_STATUS_OK) {
                result.area.start_row = result.task_coords.row * tc->task_size_rows;
                result.area.end_row = (result.task_coords.row + 1) * tc->task_size_rows;
                result.area.start_col = result.task_coords.col * tc->task_size_cols;
                result.area.end_col = (result.task_coords.col + 1) * tc->task_size_cols;

        }

        return result;
}


void setup_block_tasks(struct cats_thread_controller *t, int32_t rows, int32_t cols)
{
        // each task area has the size [rows] x [cols]
        // if rows  == 1 and cols == grid->dimension.cols threads are done row-wise
        // if rows  == grid->dimension.cols and cols == 1 threads are done col-wise

        //
        // number of task rows is the number of simulation grid rows / rows (rounded up)
        // number of task cols is the number of simulation grid cols / cols (rounded up)
        t->task_size_cols = cols;
        t->task_size_rows = rows;

        int32_t grid_rows = t->grid->dimension.rows / t->task_size_rows +
                            (t->grid->dimension.rows % t->task_size_rows != 0);
        int32_t grid_cols = t->grid->dimension.cols / t->task_size_cols +
                            (t->grid->dimension.cols % t->task_size_cols != 0);


        struct cats_dimension dim = {.rows = grid_rows, .cols = grid_cols};

        t->task_grid = new_2d_array_char(dim);
        t->tasks_remaining = t->task_grid->dimension.rows * t->task_grid->dimension.cols;

        t->task_getter = block_task_getter;
        //t->task_grid = new_char_grid(dim);
        //t->task_grid = new_2d_array(t->task_grid_rows, t->task_grid_cols, sizeof(char));
        for (int32_t r = 0; r < t->task_grid->dimension.rows; r++) {
                for (int32_t c = 0; c < t->task_grid->dimension.cols; c++) {
                        t->task_grid->data[r][c] = TASK_AREA_TODO;
                }
        }
}