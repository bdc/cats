// SPDX-License-Identifier: GPL-3.0-or-later
//
// dispersal_tasks.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_DISPERSAL_TASKS_H
#define CATS_DISPERSAL_TASKS_H

#include "threading/threading.h"


struct thread_task dispersal_task_getter(struct cats_thread_controller *tc, int32_t thread_id);

void setup_dispersal_tasks(struct cats_thread_controller *t);

#endif //CATS_DISPERSAL_TASKS_H
