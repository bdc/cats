// SPDX-License-Identifier: GPL-3.0-or-later
//
// dispersal_tasks.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <memory/arrays.h>
#include "threading/threading-helpers.h"
#include "threading/threading.h"
#include "misc/cats_maths_inline.h"

#include <memory/cats_memory.h>
#include "cats_global.h"
#include <stdint.h>
#include "dispersal_tasks.h"


bool dispersal_neighbourhood_is_clear(const struct cats_thread_controller *tc, int32_t task_row, int32_t task_col)
{
        const cats_dt_coord task_rows = tc->task_grid->dimension.rows;
        const cats_dt_coord task_cols = tc->task_grid->dimension.cols;

        const int64_t min_row = max_int64t(0, task_row - 2);
        const int64_t max_row = min_int64t(task_row + 3, task_rows);

        const int64_t min_col = max_int64t(0, task_col - 2);
        const int64_t max_col = min_int64t(task_col + 3, task_cols);

        for (int64_t row = min_row; row < max_row; row++) {
                for (int64_t col = min_col; col < max_col; col++) {
                        if (tc->task_grid->data[row][col] == TASK_AREA_INWORK) return false;
                }
        }

        return true;
}


struct thread_task dispersal_task_getter(struct cats_thread_controller *tc, int32_t thread_id)
{
        struct thread_task result = {
                .task_coords.row = -1,
                .task_coords.col = -1,
                .status = THREAD_STATUS_WAIT,
                .area = {.start_row = -1, .start_col = -1}
        };

        lock_mutex_or_die(&tc->task_mutex);

        if (tc->tasks_remaining == 0) {
                result.status = THREAD_STATUS_FINISHED;
                unlock_mutex_or_die(&tc->task_mutex);
                return result;
        }

        bool done = false;
        for (int32_t task_row = 0; task_row < tc->task_grid->dimension.rows; task_row++) {
                for (int32_t task_col = 0; task_col < tc->task_grid->dimension.cols; task_col++) {
                        if (tc->task_grid->data[task_row][task_col] == TASK_AREA_TODO
                            && dispersal_neighbourhood_is_clear(tc, task_row, task_col)) {

                                result.task_coords.row = task_row;
                                result.task_coords.col = task_col;
                                result.status = THREAD_STATUS_OK;
                                tc->task_grid->data[task_row][task_col] = TASK_AREA_INWORK;
                                tc->tasks_remaining -= 1;
                                done = true;
                                break;
                        }

                }
                if (done) break;
        }

        unlock_mutex_or_die(&tc->task_mutex);

        // the task area is a whole row, from the first to the last column
        // there is a 1:1 correspondence between task rows and grid rows
        if (result.status == THREAD_STATUS_OK) {
                result.area.start_row = result.task_coords.row * tc->task_size_rows;
                result.area.end_row = (result.task_coords.row + 1) * tc->task_size_rows;
                result.area.start_col = result.task_coords.col * tc->task_size_cols;
                result.area.end_col = (result.task_coords.col + 1) * tc->task_size_cols;

        }
        return result;
}


struct cats_dimension get_dispersal_area_dimensions(const struct cats_configuration *conf, const struct cats_grid *grid)
{
        int32_t id = grid->id;
        int32_t max_radius = get_dispersal_radius(conf, id);
        int32_t min_dimension = min_int32t(grid->dimension.cols, grid->dimension.rows);
        max_radius = max_int32t(max_radius, min_dimension / 32);
        struct cats_dimension result = {.rows = max_radius, .cols = max_radius};
        return result;
}


void setup_dispersal_tasks(struct cats_thread_controller *t)
{
        struct cats_dimension dispersal_area_dimensions = get_dispersal_area_dimensions(t->conf, t->grid);

        t->task_size_rows = dispersal_area_dimensions.rows;
        t->task_size_cols = dispersal_area_dimensions.cols;

        // all quantities are positive
        int32_t grid_rows = t->grid->dimension.rows / t->task_size_rows +
                            (t->grid->dimension.rows % t->task_size_rows != 0);
        int32_t grid_cols = t->grid->dimension.cols / t->task_size_cols +
                            (t->grid->dimension.cols % t->task_size_cols != 0);

        t->tasks_remaining = grid_rows * grid_cols;
        t->task_getter = dispersal_task_getter;

        struct cats_dimension dim = {.rows = grid_rows, .cols = grid_cols};
        t->task_grid = new_2d_array_char(dim);
        //t->task_grid = new_2d_array(t->task_grid_rows, t->task_grid_cols, sizeof(char));
        for (int32_t r = 0; r < t->task_grid->dimension.rows; r++) {
                for (int32_t c = 0; c < t->task_grid->dimension.cols; c++) {
                        t->task_grid->data[r][c] = TASK_AREA_TODO;
                }
        }
}
