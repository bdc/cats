// SPDX-License-Identifier: GPL-3.0-or-later
//
// inline_population.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_INLINE_POPULATION_H
#define CATS_INLINE_POPULATION_H

#include "cats_global.h"
#include <math.h>
#include "data/cats_datatypes.h"
#include "logging/logging.h"
#include "assert.h"
#include "configuration/configuration.h"
#include "inline_overlays.h"
#include "populations/carrying_capacity.h"
#include "populations/plant_juveniles.h"


static inline cats_dt_population
get_adult_population(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid != NULL && grid->population != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        assert(grid->population[row] != NULL);
        assert(grid->population[row][col] >= 0);
        return grid->population[row][col]; // getter
}


static inline cats_dt_population
get_adult_carrying_capacity(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);

static inline cats_dt_population
get_adult_carrying_capacity_from_cc(const struct cats_grid *grid, cats_dt_population K);

cats_dt_population
get_carrying_capacity(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);


static inline cats_dt_population round_population_safe(cats_dt_rates population)
{
        assert(population >= 0.0);

        if (population > CATS_MAX_POPULATION || population < 0.0) {
                log_message(LOG_ERROR, "%s: population %f out of allowed population range [0, %d].",
                            __func__, (double) population, CATS_MAX_POPULATION);
                exit(EXIT_FAILURE);
        }

        cats_dt_population result = (cats_dt_population) roundl(population);
        assert(result >= 0);
        assert(result < CATS_MAX_POPULATION);

        return result;
}


// returns number of actually removed individuals
static inline cats_dt_population
reduce_population_by(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population to_reduce)
{
        assert(to_reduce >= 0);
        assert(grid != NULL);
        assert(grid->population != NULL);
        assert(row < grid->dimension.rows);
        assert(col < grid->dimension.cols);
        assert(row >= 0);
        assert(col >= 0);
        assert(grid->population[row] != NULL);

        //cats_dt_population cc = get_carrying_capacity(grid, row, col);
        cats_dt_population pop = get_adult_population(grid, row, col);
        if (to_reduce > pop) { to_reduce = pop; }


        grid->population[row][col] = pop - to_reduce;// setter;
        return to_reduce;

}


static inline void
set_population_ignore_cc(const struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col,
                         const cats_dt_population pop)
{
        assert(grid != NULL && grid->population != NULL);
        assert(row >= 0 && row < grid->dimension.rows && col >= 0 && col < grid->dimension.cols);
        assert(grid->population[row] != NULL);

        if (pop > CATS_MAX_POPULATION || pop < 0.0) {
                log_message(LOG_ERROR, "%s: population %d out of allowed population range [0, %d].",
                            __func__, pop, CATS_MAX_POPULATION);
                exit(EXIT_FAILURE);
        }

        if (pop >= 0) grid->population[row][col] = pop; // setter ignore cc;
}


static inline cats_dt_population round_population_safe_down(cats_dt_rates population)
{
        // a) we don't want negative populations, ever
        // b) we truncate instead of rounding down, which is the same only for positive values

        if (population > CATS_MAX_POPULATION || population < 0.0) {
                log_message(LOG_ERROR, "%s: population %f out of allowed population range [0, %d].",
                            __func__, (double) population, CATS_MAX_POPULATION);
                exit(EXIT_FAILURE);
        }

        cats_dt_population result = (cats_dt_population) population;
        // technically unneeded
        // assert(result >= 0);
        // assert(result < CATS_MAX_POPULATION);

        return result;
}


static inline cats_dt_population multiply_population(cats_dt_population population, cats_dt_rates factor)
{
        return round_population_safe((cats_dt_rates) population * factor);
}


static inline cats_dt_population multiply_population_round_down(cats_dt_population population, cats_dt_rates factor)
{
        return round_population_safe_down((cats_dt_rates) population * factor);
}


static inline cats_dt_population_sum round_population_sum_safe(cats_dt_rates population)
{

        if (population > CATS_MAX_POPULATION_SUM || population < 0.0) {
                log_message(LOG_ERROR, "%s: population %f out of allowed population range [0, %" PRIi64"].",
                            __func__, (double) population, CATS_MAX_POPULATION_SUM);
                exit(EXIT_FAILURE);
        }
        cats_dt_population_sum result = (cats_dt_population_sum) llroundl(population);
        assert(result >= 0);
        assert(result < CATS_MAX_POPULATION_SUM);
        return result;
}


static inline cats_dt_population
get_population_ts(const struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col, const int threshold,
                  int64_t *unfiltered)
{
        assert(grid != NULL);
        assert(grid->population != NULL);
        assert(row < grid->dimension.rows);
        assert(col < grid->dimension.cols);
        assert(row >= 0);
        assert(col >= 0);
        assert(grid->population[row] != NULL);

        cats_dt_population tmp = grid->population[row][col]; // getter

        if (unfiltered) *unfiltered = tmp;

        if (grid->population[row][col] >= threshold) {
                return tmp;
        } else {
                return 0;
        }

}


static inline int64_t get_population_all_classes_x(const struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col,
                                                   cats_dt_population *class_pop)
{
        assert(grids != NULL);
        assert(grids[0]->conf != NULL);
        assert(class_pop != NULL);

        const struct cats_configuration *config = grids[0]->conf;
        const int32_t grid_count = config->grid_count;
        const bool excluded = cell_excluded_by_overlay(config, row, col);

        int64_t sum = 0;

        if (excluded) {
                for (int32_t class = 0; class < grid_count; class++) {
                        class_pop[class] = 0;
                }
                return 0;
        }

        for (int32_t class = 0; class < grid_count; class++) {
                cats_dt_population tmp = get_adult_population(grids[class], row, col);
                class_pop[class] = tmp;
                sum += tmp;

        }

        return sum;
}


static inline int64_t
get_population_all_classes_ts(const struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col,
                              cats_dt_population *class_pop, int32_t threshold, int64_t *unfiltered_sum)
{
        assert(grids != NULL);

        struct cats_configuration *config = grids[0]->conf;
        assert(config != NULL);
        int64_t sum = 0;
        int64_t unfiltered = 0;

        if (class_pop == NULL) {
                for (int32_t class = 0; class < config->grid_count; class++) {
                        sum += get_population_ts(grids[class], row, col, threshold, &unfiltered);
                        if (unfiltered_sum != NULL) {
                                *unfiltered_sum += unfiltered;
                        }
                }
                return sum;
        } else {
                for (int32_t class = 0; class < config->grid_count; class++) {
                        cats_dt_population tmp = get_population_ts(grids[class], row, col, threshold, &unfiltered);
                        class_pop[class] = tmp;
                        sum += tmp;

                        if (unfiltered_sum != NULL) {
                                *unfiltered_sum += unfiltered;
                        }
                }

                return sum;
        }
}


#include <math.h>
#include <assert.h>
#include "defaults.h"
#include "environment/glm.h"
#include "cats_global.h"
#include "../memory/raw_arrays.h"
#include "plants/plant_rates.h"
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "environment/load_environment.h"
#include "misc/misc.h"
#include "logging.h"
#include "overlays/overlays.h"
#include "misc/cats_maths_inline.h"
#include "memory/arrays.h"
#include "inline_vital_rates.h"
#include "inline_overlays.h"
#include "inline_carrying_capacity.h"

#endif //CATS_INLINE_POPULATION_H
