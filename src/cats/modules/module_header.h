// SPDX-License-Identifier: GPL-3.0-or-later
//
// module_header.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_MODULE_HEADER_H
#define CATS_MODULE_HEADER_H

#include "cats_global.h"
#include "modules/load_module.h"
#include <stdint.h>
#include <assert.h>
#include "logging.h"
#include "data/cats_grid.h"
#include "memory/cats_memory.h"

extern int CATS_MODULE_ID_INTERNAL;
extern void *CATS_MODULE_DATA_INTERNAL;


static int GET_CATS_MODULE_ID_INTERNAL()
{
        return CATS_MODULE_ID_INTERNAL;
}


static void *GET_CATS_MODULE_DATA_INTERNAL()
{
        return CATS_MODULE_DATA_INTERNAL;
}


#define CATS_MODULE_ID GET_CATS_MODULE_ID_INTERNAL()
#define CATS_MODULE_DATA GET_CATS_MODULE_DATA_INTERNAL()


#endif //CATS_MODULE_HEADER_H
