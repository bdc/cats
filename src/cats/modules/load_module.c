// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_module.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <dlfcn.h>
#include <logging/logging.h>
#include <string.h>
#include "configuration/configuration.h"
#include "load_module.h"
#include "module_header.h"


int CATS_MODULE_ID_INTERNAL = -1;
void *CATS_MODULE_DATA_INTERNAL = NULL;


void print_module(struct cats_module *module, int32_t id)
{
        print_category(0, "Module", module->name);
        print_string(1, "name", module->name);
        print_string(1, "filename", module->filename);
        print_integer(1, "id", id);
        print_string(1, "has config data", bool_to_string(module->data != NULL));

}


void print_module_species_data(struct module_species_data *module)
{
        print_string(1, "simulation grid initialization", bool_to_string(module->grid_init_function != NULL));
        print_string(1, "simulation grid clean-up", bool_to_string(module->grid_cleanup_function != NULL));
        print_string(1, "load species parameters", bool_to_string(module->load_species_param_function != NULL));
        print_integer(1, "vital rates", module->vital_rate_count);
        for (int32_t i = 0; i < module->vital_rate_count; i++) {
                print_string(2, "vital rate name", module->vital_rate_name[i]);

        }
}


void print_modules(struct cats_configuration *conf)
{

        for (int i = 0; i < conf->modules.count; i++) {
                print_module(&conf->modules.module[i], i);
                print_module_species_data(&conf->param->module_data[i]);
                log_message(LOG_RAW, "\n\n");
        }
}


int32_t load_module(struct cats_configuration *conf, struct cats_module *module)
{


        void *handle = dlopen(module->filename, RTLD_LAZY);
        if (!handle) {
                log_message(LOG_ERROR, "could not dlopen '%s': %s", module->filename, dlerror());
                exit(EXIT_FAILURE);
        }

        dlerror();
        int32_t (*register_function)(struct cats_configuration *conf);

        *(void **) (&register_function) = dlsym(handle, "cats_module_init");

        if (dlerror() != NULL) {
                log_message(LOG_ERROR, "could not load function 'cats_module_init' from '%s'", module->filename);
                exit(EXIT_FAILURE);
        }

        int32_t module_id = register_function(conf);
        //conf->modules.module[module_id].filename = strdup(filename);

        return module_id;
}


int32_t register_module(struct cats_configuration *conf, const char *module_name, void *module_data,
                        enum cats_module_flags module_flags)
{
        if (conf->modules.count == MAX_MODULES) {
                log_message(LOG_ERROR, "The maximum number of modules %d is already loaded", MAX_MODULES);
                exit_cats(EXIT_FAILURE);
        }


        int32_t module_id = conf->modules.count;
        CATS_MODULE_ID_INTERNAL = module_id;
        CATS_MODULE_DATA_INTERNAL = module_data;
        logging_initialize(conf->command_line_options.default_log_level, &conf->global->time_info, conf->output.log_file_name, conf->quiet);
        logging_set_module_name(module_name);

        conf->modules.module[module_id].flags = module_flags;

        conf->modules.module[module_id].canonical_name = strdup(module_name);
        conf->modules.module[module_id].data = module_data;
        if (module_flags & MODULE_OVERRIDE_ACTIONS) {
                if (conf->modules.override_actions_module == NULL) {
                        conf->modules.override_actions_module = &conf->modules.module[module_id];
                        log_message(LOG_IMPORTANT, "module %s replaces default actions", module_name);
                } else {
                        log_message(LOG_ERROR, "%s: module '%s' already overrides actions", __func__,
                                    conf->modules.override_actions_module->name);
                        exit_cats(EXIT_FAILURE);
                }
        }

        log_message(LOG_IMPORTANT, "registered module '%s' with id %d and flags %d", module_name, module_id,
                    module_flags);
        conf->modules.count++;
        return module_id;
}


void
register_cats_grid_init_function(struct cats_configuration *conf, cats_grid_function init, cats_grid_function cleanup)
{
        conf->param[0].module_data[CATS_MODULE_ID].grid_init_function = init;
        conf->param[0].module_data[CATS_MODULE_ID].grid_cleanup_function = cleanup;
}


void register_load_species_param_config_func(struct cats_configuration *conf, cats_load_species_param_function func)
{
        conf->param[0].module_data[CATS_MODULE_ID].load_species_param_function = func;
}


void register_create_leslie_matrix_func(struct cats_configuration *conf, cats_leslie_matrix_func func)
{
        conf->modules.module[CATS_MODULE_ID].create_leslie_matrix = func;
        log_message(LOG_INFO, "Using leslie matrix function at address %p", conf->modules.module[CATS_MODULE_ID].create_leslie_matrix);
}


void register_module_vital_rate(struct cats_configuration *conf, struct cats_vital_rate *vr, const char *name)
{

        struct module_species_data *m = &conf->param[0].module_data[CATS_MODULE_ID];
        int vr_count = m->vital_rate_count;
        m->vr = realloc_or_die(m->vr, sizeof(struct module_grid_data *) * (vr_count + 1));
        init_cats_vital_rate(vr);
        m->vital_rate_name = realloc_or_die(m->vital_rate_name, sizeof(char *) * (vr_count + 1));
        m->vital_rate_required = realloc_or_die(m->vital_rate_required, sizeof(bool) * (vr_count + 1));
        m->vr[vr_count] = vr;
        m->vital_rate_name[vr_count] = strdup(name);
        vr->is_carrying_capacity = false;

        strncpy(vr->name, name, MAX_VITAL_NAME_LEN + 1);
        vr->default_rate_id = m->vital_rate_count;

        m->vital_rate_count++;
}

void add_module_output_directory(struct cats_configuration *conf, const char *name)
{
        string_array_add(conf->output.needed_module_output_directories, name);
}