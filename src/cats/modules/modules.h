// SPDX-License-Identifier: GPL-3.0-or-later
//
// modules.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_MODULES_H
#define CATS_MODULES_H
#include <stdint.h>
#include <stdbool.h>
#include "defaults.h"
#include "data/cats_datatypes.h"
#include <gsl/gsl_eigen.h>
struct cats_ini;
struct cats_configuration;
struct lambda_parameters;
struct cats_grid;
struct cats_species_param;


typedef double *(*cats_leslie_matrix_func)(struct cats_configuration *conf, struct lambda_parameters *l_param,
                                           bool silent, int32_t *N_out);

typedef void *(*cats_grid_function)(struct cats_configuration *conf, struct cats_grid *grid, void *data);

typedef void (*cats_load_configuration_function)(struct cats_configuration *conf, struct cats_ini *ini,
                                                 const char *section_name, void *data);

typedef void (*cats_load_species_param_function)(struct cats_configuration *conf, struct cats_ini *ini,
                                                 const char *section_name, struct cats_species_param *param);

typedef struct string_array *(*cats_grid_stat_function)(struct cats_configuration *conf, struct cats_grid *grid,
                                                        bool header);

typedef void (*cats_cell_function)(struct cats_configuration *conf, struct cats_grid *grid, cats_dt_coord row,
                                   cats_dt_coord col, cats_dt_population K);

typedef void (*eigen_system_print_func)(gsl_complex eigen_value, gsl_vector_complex_view *eigen_vector, int32_t N, struct cats_species_param *param);

struct module_species_data {
        struct cats_module *module;
        cats_grid_function grid_init_function;
        cats_grid_function grid_cleanup_function;

        cats_load_species_param_function load_species_param_function;
        cats_grid_stat_function grid_stat_function;
        cats_cell_function cell_destroyed_action;
        cats_cell_function cell_carrying_capacity_action;

        struct cats_vital_rate **vr;
        char **vital_rate_name;
        bool *vital_rate_required;
        int32_t vital_rate_count;
};


enum cats_module_flags {
        MODULE_NO_FLAGS = 0,
        MODULE_ALTERNATE_DEMOGRAPHIC = 1,
        MODULE_OVERRIDE_ACTIONS = 1 << 1,

};



struct cats_module {
        char *name;
        char *canonical_name;
        char *filename;
        void *data;
        enum cats_module_flags flags;
        cats_leslie_matrix_func create_leslie_matrix;

        int32_t id;
        struct string_array *stats_header;
};

struct module_registry {
        int32_t count;       // how many are loaded
        int32_t found_count; // how many were found in the configuration
        struct cats_module module[MAX_MODULES];
        struct cats_module *override_actions_module;

};


void init_cats_module(struct cats_module *module, int32_t id);

void init_module_registry(struct module_registry *registry);

void init_module_species_data(struct module_species_data *m);

void cleanup_module_species_data(struct module_species_data *m);

void cleanup_module_registry(struct module_registry *registry);

#endif //CATS_MODULES_H
