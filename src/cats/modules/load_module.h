// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_module.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_LOAD_MODULE_H
#define CATS_LOAD_MODULE_H

#include "configuration/configuration.h"
#include "data/species_parameters.h"


typedef struct cats_string_array *(*grid_stat_function)(struct cats_configuration *conf, struct cats_grid *grid,
                                                        bool header);

void register_grid_stat_function(struct cats_configuration *conf, grid_stat_function func);

void register_create_leslie_matrix_func(struct cats_configuration *conf, cats_leslie_matrix_func func);

void
register_cats_grid_init_function(struct cats_configuration *conf, cats_grid_function init, cats_grid_function cleanup);

int32_t load_module(struct cats_configuration *conf, struct cats_module *module);

void register_module_vital_rate(struct cats_configuration *conf, struct cats_vital_rate *vr, const char *name);


int32_t register_module(struct cats_configuration *conf, const char *module_name, void *module_data,
                        enum cats_module_flags module_flags);

void register_load_species_param_config_func(struct cats_configuration *conf, cats_load_species_param_function func);

void print_modules(struct cats_configuration *conf);

void add_module_output_directory(struct cats_configuration *conf, const char *name);

#endif //CATS_LOAD_MODULE_H
