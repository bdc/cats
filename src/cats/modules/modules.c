// SPDX-License-Identifier: GPL-3.0-or-later
//
// modules.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stddef.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "modules.h"
#include "cats_strings/cats_strings.h"

void init_cats_module(struct cats_module *module, int32_t id)
{
        module->name = NULL;
        module->data = NULL;
        module->filename = NULL;
        module->canonical_name = NULL;
        module->id = id;
        module->create_leslie_matrix = NULL;
        module->stats_header = NULL;


}


void init_module_registry(struct module_registry *registry)
{
        assert(registry != NULL);
        registry->count = 0;
        registry->found_count = 0;
        registry->override_actions_module = NULL;
        memset(registry, 0, sizeof(struct module_registry));
        for (int32_t i = 0; i < MAX_MODULES; i++) {
                init_cats_module(&registry->module[i], i);
        }
}


void init_module_species_data(struct module_species_data *m)
{
        m->grid_init_function = NULL;
        m->grid_cleanup_function = NULL;
        m->load_species_param_function = NULL;
        m->grid_stat_function = NULL;
        m->module = NULL;
        m->vital_rate_count = 0;
        m->vr = NULL;
        m->vital_rate_name = NULL;
        m->vital_rate_required = NULL;
        m->cell_carrying_capacity_action = NULL;
        m->cell_destroyed_action = NULL;
}


void cleanup_module_registry(struct module_registry *registry)
{
        assert(registry != NULL);
        for (int32_t i = 0; i < registry->count; i++) {
                free(registry->module[i].name);
                free(registry->module[i].canonical_name);
                free(registry->module[i].filename);
                registry->module[i].create_leslie_matrix = NULL;
                free_string_array(&registry->module[i].stats_header);

        }
}


void cleanup_module_species_data(struct module_species_data *m)
{

        for (int32_t j = 0; j < m->vital_rate_count; j++) {
                free(m->vital_rate_name[j]);
                m->vital_rate_name[j] = NULL;
        }
        m->module = NULL;
        free(m->vital_rate_name);
        free(m->vr);
        free(m->vital_rate_required);
        m->vr = NULL;
        m->vital_rate_name = NULL;
        m->vital_rate_count = 0;
}
