// SPDX-License-Identifier: GPL-3.0-or-later
//
// population.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>

#include "data/cats_grid.h"
#include "configuration/configuration.h"

#include "logging.h"
#include "population.h"
#include "inline.h"
#include "plants/plant_structures.h"

#include "inline_carrying_capacity.h"
#include "inline_population.h"


void adjust_juvenile_populations(struct cats_configuration *conf, struct cats_grid *grid)
{
        if (conf->param_count > 1) {
                log_message(LOG_ERROR, "not implemented for multiple grids");
                exit_cats(EXIT_FAILURE);
        }
        log_message(LOG_IMPORTANT, "adding juveniles to initially populated cells");
        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;
        cats_dt_rates multi = grid->param.max_adult_cc_fraction / (1.0 - grid->param.max_adult_cc_fraction);
        const int32_t max_age_of_maturity = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);
        for (cats_dt_coord r = 0; r < rows; r++) {
                for (cats_dt_coord c = 0; c < cols; c++) {
                        cats_dt_population N = get_adult_population(grid, r, c);
                        if (N == 0) continue;
                        create_juvenile_structure_if_needed(grid, r, c);
                        for (int32_t i = 0; i < max_age_of_maturity + 1; i++) {
                                //cats_dt_rates w = juvenile_cc_multiplier(&grid->param, i);
                                cats_dt_rates w = grid->param.juvenile_cc_weights[i];
                                cats_dt_rates juv_pop = (cats_dt_rates) N * multi / w;
                                if (juv_pop > CATS_MAX_POPULATION) {
                                        juv_pop = CATS_MAX_POPULATION / 2.0;
                                }
                                grid->juveniles[r][c][i] = round_population_safe(juv_pop);
                        }
                }
        }

}


void adjust_initial_population(struct cats_configuration *conf, struct cats_grid *grid)
{
        // adjustment of initial population only happens once
        if (grid->param.initial_population.adjusted == true) return;

        if (grid->param.initial_population.set_to_cc == true) increase_initial_population_to_cc(grid, conf);
        if (grid->param.initial_population.suitability_threshold > 0.0) {
                prune_initial_population_under_threshold(conf, grid);
        }
        adjust_juvenile_populations(conf, grid);

        grid->param.initial_population.adjusted = true;

}


void increase_initial_population_to_cc(struct cats_grid *grid, const struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        const int32_t id = grid->id;
        assert(id == 0);
        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;

        // int32_t count = conf->param_count;

        int64_t populated_cells = 0;
        log_message(LOG_IMPORTANT, "Setting initial population of '%s' to carrying capacity",
                    grid->param.species_name);
        for (cats_dt_coord r = 0; r < rows; r++) {
                for (cats_dt_coord c = 0; c < cols; c++) {
                        cats_dt_population p = get_adult_population(grid, r, c);
                        if (p == 0) continue;
                        cats_dt_population K = get_adult_carrying_capacity(grid, r, c);

                        if (p > 0) {
                                populated_cells++;
                                set_population_ignore_cc(grid, r, c, K);
                        }
                }
        }

        log_message(LOG_INFO, "\tset grid %d (%s) %"PRId64" populated cells to local carrying capacity",
                    id, conf->param[id].species_name, populated_cells);
        //(cats_dt_population) (get_max_rate_from_param(&conf->param[id], VR_CARRYING_CAPACITY) / count));
}


void prune_initial_population_under_threshold(struct cats_configuration *conf, struct cats_grid *grid)
{
        assert(grid != NULL);
        assert(conf != NULL);

        if (grid->param.parametrization != PARAM_HYBRID) {
                log_message(LOG_INFO, "skipping pruning of initial populations -- only available in suitability mode");
                return;
        }
        if (grid->param.initial_population.adjusted == true) return;
        log_message(LOG_IMPORTANT, "Pruning initial population of '%s' under threshold %f", grid->param.species_name,
                    (double) grid->param.initial_population.suitability_threshold);

        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;

        int64_t populated = 0;
        int64_t destroyed = 0;

        const cats_dt_rates TS = grid->param.initial_population.suitability_threshold;
        for (cats_dt_coord r = 0; r < rows; r++) {
                for (cats_dt_coord c = 0; c < cols; c++) {

                        if (get_adult_population(grid, r, c) > 0) {
                                populated += 1;
                        } else {
                                continue;
                        }

                        if (get_suitability(grid, r, c) < TS) {
                                set_population_ignore_cc(grid, r, c, 0);  // prune initial population
                                destroyed += 1;
                        }
                }
        }

        log_message(LOG_INFO, "Species '%s': pruned %"PRId64" of %"PRId64" initial populations (suitability < threshold), %"PRId64" remaining",
                    grid->param.species_name, destroyed, populated, populated - destroyed);
}


cats_dt_population
set_population(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population pop)
{
        if (pop < 0) { return 0; }

        assert(grid != NULL);
        assert(grid->population != NULL);
        assert(row < grid->dimension.rows);
        assert(col < grid->dimension.cols);
        assert(row >= 0);
        assert(col >= 0);
        assert(grid->population[row] != NULL);

        cats_dt_population cc = get_adult_carrying_capacity(grid, row, col);
        cats_dt_population result = min_population_t(pop, cc);
        grid->population[row][col] = result; // setter;

        return result;
}


cats_dt_population
increase_population_by(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, int32_t to_add)
{
        assert(to_add >= 0);
        assert(grid != NULL);
        assert(grid->population != NULL);
        assert(row < grid->dimension.rows);
        assert(col < grid->dimension.cols);
        assert(row >= 0);
        assert(col >= 0);
        assert(grid->population[row] != NULL);
        cats_dt_population N = get_adult_population(grid, row, col);
        cats_dt_population K_tot = get_carrying_capacity(grid, row, col);
        cats_dt_population K_A = get_adult_carrying_capacity_from_cc(grid, K_tot);
        cats_dt_population max_increase = K_A - N;
        assert(max_increase >= 0);

        cats_dt_population actually_added = min_population_t(to_add, max_increase);

        // set population
        grid->population[row][col] = N + actually_added; // setter;
        cell_apply_juvenile_cc(grid, row, col, K_tot, N);


#ifdef OLD_JUVENILE_CC

        // do we need to prune juveniles?
        cats_dt_population_sum juvenile_sum = get_juvenile_sum(grid, row, col);

        // no juveniles? no problems.
        if (juvenile_sum == 0) return actually_added;

        // existing juveniles still fit into the cell? also no problem.
        cats_dt_population_sum max_allowed_juveniles = round_population_sum_safe((cats_dt_rates) K * JUVENILE_CC_MULTI) - N;
        if (max_allowed_juveniles <0 ) {
                log_message(LOG_RAW, "%d %Lf %d %ld %ld\n", K, (cats_dt_rates) K * JUVENILE_CC_MULTI, N,  round_population_sum_safe((cats_dt_rates) K * JUVENILE_CC_MULTI), max_allowed_juveniles);
        }
        assert(max_allowed_juveniles >= 0);


        if (juvenile_sum <= max_allowed_juveniles) return actually_added;
        // uh-oh! we the number of juveniles is greater than the available space!
        scale_down_juveniles(grid, row, col, max_allowed_juveniles, juvenile_sum);
        return actually_added;
#endif
        return actually_added;
}


cats_dt_population
change_population_by(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population diff)
{
        if (diff > 0) {
                return increase_population_by(grid, row, col, diff);
        } else {
                return reduce_population_by(grid, row, col, diff);
        }
}


/*
int64_t
get_populated_max_cc(const cats_dt_population *populations, const cats_dt_population *carrying_capacities,
                     const int32_t count)
{
        int64_t CC_max = 0;

        for (int32_t i = 0; i < count; i++) {
                if (populations[i] > 0 && carrying_capacities[i] > CC_max) {
                        CC_max = carrying_capacities[i];
                }
        }

        return CC_max;
}
*/
int64_t
get_population_all_classes_sum(const struct cats_grid **grids, const cats_dt_coord row, const cats_dt_coord col)
{
        assert(grids != NULL);
        assert(grids[0]->conf != NULL);

        struct cats_configuration *config = grids[0]->conf;
        int64_t N = 0;

        if (cell_excluded_by_overlay(config, row, col)) return 0;

        for (int32_t class = 0; class < config->grid_count; class++) {
                N += get_adult_population(grids[class], row, col);
        }

        return N;
}