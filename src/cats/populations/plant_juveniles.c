// SPDX-License-Identifier: GPL-3.0-or-later
//
// plant_juveniles.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>

#include "plant_juveniles.h"
#include "data/cats_datatypes.h"
#include "data/cats_grid.h"
#include "inline_vital_rates.h"
#include "memory/cats_memory.h"


void setup_plant_juvenile_weights(struct cats_species_param *param)
{
        assert(param->juvenile_cc_weights == NULL);
        const int32_t max_age_of_mat = get_vital_age_from_param(param, VA_AGE_OF_MATURITY_MAX);
        param->juvenile_cc_weights = calloc_or_die(max_age_of_mat + 1, sizeof(cats_dt_rates));

        for (int32_t j = 0; j < max_age_of_mat + 1; j++) {
                param->juvenile_cc_weights[j] = juvenile_cc_multiplier(param, j);
        }

}


cats_dt_population_sum
get_weighted_juvenile_sum(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid != NULL && grid->juveniles != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        if (grid->juveniles == NULL) return 0;

        const cats_dt_population *juveniles = grid->juveniles[row][col];
        if (juveniles == NULL) return 0;

        cats_dt_rates sum = 0;


        const int32_t max_stage = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX) + 1;
        for (int32_t i = 0; i < max_stage; i++) {
                //cats_dt_rates multi = juvenile_cc_multiplier(&grid->param, i);
                cats_dt_rates multi = grid->param.juvenile_cc_weights[i];
                cats_dt_rates entry = (cats_dt_rates) juveniles[i] * multi;
                if (entry < 0) {

                        log_message(LOG_ERROR, "%s: weighted juveniles for stage %d < 0: multiplier %f, juveniles %d",
                                    __func__, i, (double) multi, juveniles[i]);
                        fflush(stdout);
                        assert(entry >= 0);
                        exit_cats(EXIT_FAILURE);
                }
                sum += entry;
        }

        return round_population_sum_safe(sum);
}


void
scale_down_juveniles2(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population_sum juvenile_cc,
                      cats_dt_population_sum weighted_juvenile_sum)
{
        assert(grid != NULL && grid->juveniles != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        assert(grid->juveniles[row] != NULL);
        assert(juvenile_cc > 0);
        if (grid->juveniles[row][col] == NULL || weighted_juvenile_sum == 0) return;
        if (weighted_juvenile_sum < juvenile_cc) return;

        const int32_t mat_max = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX); // grid->param.max_age_of_maturity;

        cats_dt_rates factor = 1.0;
        for (int32_t i = 0; i < mat_max + 1; i++) {
                if (i > 0) {
                        weighted_juvenile_sum = get_weighted_juvenile_sum(grid, row, col);
                }
                assert(weighted_juvenile_sum > 0);
                factor = (cats_dt_rates) juvenile_cc / (cats_dt_rates) weighted_juvenile_sum;

                assert(factor > 0);
                if (factor > 1.0) continue;
                grid->juveniles[row][col][i] = multiply_population_round_down(grid->juveniles[row][col][i], factor);

        }

        // apply the last factor to all juvenile classes (except the last one, where it already was applied)
        if (factor < 1.0) {
                for (int32_t i = 0; i < mat_max + 1 - 1; i++) {
                        grid->juveniles[row][col][i] = multiply_population_round_down(grid->juveniles[row][col][i],
                                                                                      factor);
                }
        }

        weighted_juvenile_sum = get_weighted_juvenile_sum(grid, row, col);
        if (weighted_juvenile_sum > juvenile_cc) {
                log_message(LOG_ERROR, "%s: weighted juvenile sum > juvenile CC", __func__);
                log_message(LOG_RAW, "sum: %"PRId64", cc %"PRId64"\n", weighted_juvenile_sum, juvenile_cc);
                log_message(LOG_RAW, "factor %f\n", (double) factor);
                for (int32_t i = 0; i < mat_max + 1 - 1; i++) {
                        log_message(LOG_RAW, "juvenile stage %d: %d - multiplier %f\n",  i,
                                    grid->juveniles[row][col][i],
                                    (double) juvenile_cc_multiplier(&grid->param, i));
                }
                fflush(stdout);
                exit_cats(EXIT_FAILURE);
        }
        assert(weighted_juvenile_sum <= juvenile_cc);
}


void cell_apply_juvenile_cc(struct cats_grid *g, cats_dt_coord row,
                            cats_dt_coord col, cats_dt_population K_tot, cats_dt_population N)
{
        if (g->juveniles == NULL) return;
        if (g->juveniles[row][col] == NULL) return;

        cats_dt_population K_J = K_tot - N;

        if (K_J < 1) {
                destroy_juveniles(g, row, col);
                return;
        }


        int64_t juveniles_sum = get_weighted_juvenile_sum(g, row, col);

        if (juveniles_sum <= K_J) return;
        scale_down_juveniles2(g, row, col, K_J, juveniles_sum);

}
