// SPDX-License-Identifier: GPL-3.0-or-later
//
// carrying_capacity.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "carrying_capacity.h"
#include "threading/threading-helpers.h"
#include "population.h"
#include "plants/juveniles.h"
#include "inline_overlays.h"
#include "inline_population.h"


void
get_carrying_capacity_all_classes(struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col,
                                  cats_dt_population *class_cc)
{
        assert(grids != NULL);
        const struct cats_configuration *config = grids[0]->conf;
        assert(config != NULL);
        assert(class_cc != NULL);
        int32_t grid_count = config->grid_count;

        for (int32_t class = 0; class < grid_count; class++) {
                cats_dt_population tmp = get_adult_carrying_capacity(grids[class], row, col);
                class_cc[class] = tmp;
        }
}


cats_dt_population
get_carrying_capacity(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid->conf != NULL);

        const struct cats_configuration *conf = grid->conf;

        // if we have an exclusion mask AND it is set the CC is 0
        if (cell_excluded_by_overlay(conf, row, col)) { return 0; }

        double multiplier = 1.0;

        if (conf->overlays.overlay[OL_HABITAT_TYPE_CC].enabled) {
                multiplier *= conf->overlays.habitat_cc->data[row][col];
                if (multiplier <= 0.0) { return 0; }
        }

        cats_dt_rates cc;

        const struct cats_vital_rate *link = &grid->param.carrying_capacity;
        cc = calculate_rate(link, NAN, &grid->param, grid, row, col, NULL);

        // no overlay - just return the unmodified carrying capacity
        if (conf->overlays.have_overlays == false || multiplier == 1.0) return round_population_safe(cc);
        return round_population_safe(cc * multiplier);
}


void
cell_apply_carrying_capacity(struct cats_grid *grid, struct cats_thread_info *ts, cats_dt_coord row, cats_dt_coord col,
                             bool check_exclusion)
{
        const struct cats_configuration *conf = ts->conf;
        assert (conf->grid_count == 1);
        const int grid_id = grid->id;

        if (check_exclusion && cell_excluded_by_overlay(conf, row, col)) return;

        cats_dt_population K_tot = get_carrying_capacity(grid, row, col);


        if (grid->param.default_demographics) {
                cats_dt_population N = get_adult_population(grid, row, col);

                if (N == 0 && grid->juveniles[row][col] == NULL) return;
                if (N > 0) ts->stats[grid_id].stats[CS_POPULATED_BEFORE_CC] += 1;

                cats_dt_population K_A = get_adult_carrying_capacity_from_cc(grid, K_tot);

                if (N > K_A) set_population_ignore_cc(grid, row, col, K_A);
                if (K_A >= 1 && N > 0) ts->stats[grid_id].stats[CS_POPULATED_AFTER_CC] += 1;
                // N = K_A; // FIXME - what was this about?
                if (K_tot < 1) {
                        destroy_juveniles(grid, row, col);
                } else {
                        cell_apply_juvenile_cc(grid, row, col, K_tot, N);
                }


#ifdef OLD_JUVENILE_CC
                cats_dt_population_sum max_allowed_juveniles = round_population_sum_safe(K * JUVENILE_CC_MULTI) - N;
                int64_t juveniles_sum = get_juvenile_sum(grid, row, col);
                if (juveniles_sum <= max_allowed_juveniles) return;
                scale_down_juveniles(grid, row, col, max_allowed_juveniles, juveniles_sum);
#endif

        }

        for (int32_t i = 0; i < conf->modules.count; i++) {
                cats_cell_function apply_cc = grid->param.module_data[i].cell_carrying_capacity_action;
                if (apply_cc != NULL) {
                        apply_cc(grid->conf, grid, row, col, K_tot);
                }
        }

}


void grid_apply_carrying_capacity(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;

        const cats_dt_coord start_row = ts->area.start_row;
        const cats_dt_coord end_row = ts->area.end_row;

        const cats_dt_coord start_col = ts->area.start_col;
        const cats_dt_coord end_col = ts->area.end_col;

        for (cats_dt_coord row = start_row; row < end_row; row++) {
                for (cats_dt_coord col = start_col; col < end_col; col++) {
#ifdef CATS_DEBUG_THREADING
                        debug_threading_cell(conf, ts, row, col);
                        mark_cell_done(&debug, row, col);
#endif
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }
                        cell_apply_carrying_capacity(grid, ts, row, col, false);
                }
        }
}


void apply_carrying_capacity(struct cats_grid *grid, struct cats_thread_info *ts)
{
        struct cats_configuration *conf = ts->conf;

        if (conf->grid_count == 1) {
                grid_apply_carrying_capacity(grid, ts);
                return;
        }

        log_message(LOG_ERROR, "%s:: reached point we should have not reached", __func__);
        exit(EXIT_FAILURE);
}

