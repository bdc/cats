// SPDX-License-Identifier: GPL-3.0-or-later
//
// population.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_POPULATION_H_
#define CATS_POPULATION_H_

#include "data/cats_grid.h"
#include "plants/population_stats.h"

static inline cats_dt_population
reduce_population_by(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population to_reduce);

static inline void
set_population_ignore_cc(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population pop);

static inline int64_t get_population_all_classes_x(const struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col,
                                                   cats_dt_population *class_pop);

static inline int64_t
get_population_all_classes_ts(const struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col,
                              cats_dt_population *class_pop, int32_t threshold, int64_t *unfiltered_sum);

cats_dt_population
set_population(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population pop);

void adjust_initial_population(struct cats_configuration *conf, struct cats_grid *grid);

void prune_initial_population_under_threshold(struct cats_configuration *conf, struct cats_grid *grid);

void increase_initial_population_to_cc(struct cats_grid *grid, const struct cats_configuration *conf);

cats_dt_population
increase_population_by(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, int32_t to_add);

int64_t
get_population_all_classes_sum(const struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col);

#endif