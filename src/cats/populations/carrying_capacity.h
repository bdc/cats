// SPDX-License-Identifier: GPL-3.0-or-later
//
// carrying_capacity.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_CARRYING_CAPACITY_H
#define CATS_CARRYING_CAPACITY_H

#include "data/cats_grid.h"

cats_dt_population
get_carrying_capacity(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);

void apply_carrying_capacity(struct cats_grid *grid, struct cats_thread_info *thread);

void get_carrying_capacity_all_classes(struct cats_grid **grids, cats_dt_coord row, cats_dt_coord col,
                                       cats_dt_population *class_cc);

void
cell_apply_carrying_capacity(struct cats_grid *grid, struct cats_thread_info *ts, cats_dt_coord row, cats_dt_coord col,
                             bool check_exclusion);

#endif //CATS_CARRYING_CAPACITY_H
