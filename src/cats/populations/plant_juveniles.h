// SPDX-License-Identifier: GPL-3.0-or-later
//
// plant_juveniles.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_PLANT_JUVENILES_H
#define CATS_PLANT_JUVENILES_H

#include "data/cats_datatypes.h"

struct cats_grid;
struct cats_species_param;

void setup_plant_juvenile_weights(struct cats_species_param *param);

cats_dt_population_sum get_weighted_juvenile_sum(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);

void
scale_down_juveniles2(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, cats_dt_population_sum juvenile_cc,
                      cats_dt_population_sum weighted_juvenile_sum);

void cell_apply_juvenile_cc(struct cats_grid *g, cats_dt_coord row,
                            cats_dt_coord col, cats_dt_population K_tot, cats_dt_population N);

#endif //CATS_PLANT_JUVENILES_H
