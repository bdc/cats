// SPDX-License-Identifier: GPL-3.0-or-later
//
// juvenile_survival_calculation.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "configuration/configuration.h"
#include "inline.h"
#include "juvenile_survival_calculation.h"


void juvenile_start_status_old(int32_t age, cats_dt_rates start, cats_dt_rates new_adults, cats_dt_rates m,
                               cats_dt_rates juveniles, cats_dt_rates start_pop)
{

        log_message(LOG_RAW,
                    "Start of year %02d: %08.4f: %08.4f (%06.4f) new adults, %08.4f remaining discarded \n",
                    age, (double) start, (double) new_adults, (double) m, (double) juveniles);

}


void juvenile_dead_old(int32_t age, cats_dt_rates juveniles, cats_dt_rates start_pop)
{
        log_message(LOG_RAW, "ABC:nodes.append(['J%d', 'D', %f])\n", age - 1, (double) juveniles / (double) start_pop);
}


void juvenile_start_status(int32_t age, cats_dt_rates start, cats_dt_rates new_adults, cats_dt_rates m,
                           cats_dt_rates surviving, cats_dt_rates survival_rate)
{
        log_message(LOG_RAW, "Start of year %02d: %08.4f: %08.4f (%06.4f) new adults, %08.4f (%06.4f) surviving \n",
                    age, (double) start, (double) new_adults, (double) m, (double) surviving, (double) survival_rate);
}


void print_juvenile_dead(cats_dt_rates dead, cats_dt_rates start_pop, int32_t age)
{
        if (age == 0) {
                log_message(LOG_RAW, "ABC:nodes.append(['G', 'D', %f])\n", (double) dead / (double) start_pop);
        } else {
                log_message(LOG_RAW, "ABC:nodes.append(['J%d', 'D', %f])\n", age, (double) dead / (double) start_pop);
        }
}


void print_juvenile_adults(cats_dt_rates new_adults, cats_dt_rates start_pop, int32_t age)
{
        if (age == 0) {
                log_message(LOG_RAW, "ABC:nodes.append(['G', 'A', %f])\n", (double) new_adults / (double) start_pop);
        } else {

                log_message(LOG_RAW, "ABC:nodes.append(['J%d', 'A', %f])\n", age,
                            (double) new_adults / (double) start_pop);
        }
}


void print_juvenile_trans(cats_dt_rates juveniles, cats_dt_rates start_pop, int32_t age)
{
        if (age == 0) {
                log_message(LOG_RAW, "ABC:nodes.append(['G', 'J1', %f])\n", (double) juveniles / (double) start_pop);
        } else {
                log_message(LOG_RAW, "ABC:nodes.append(['J%d', 'J%d', %f])\n", age, age + 1,
                            (double) juveniles / (double) start_pop);
        }
}


cats_dt_rates
calculate_total_survival(cats_dt_rates survival_rate, int32_t age_min, int32_t age_max, bool print)
{
        cats_dt_rates start_pop = 100.0;
        cats_dt_rates juveniles = start_pop;
        cats_dt_rates adults = 0.0;
        if (print) {

                log_message(LOG_RAW, "Testing juvenile survival rate of %f\n", (double) survival_rate);
        }
        for (int32_t age = 0; age < age_max + 1; age++) {

                cats_dt_rates start = juveniles;
                cats_dt_rates m = age_specific_maturation_rate(age, survival_rate, age_min, age_max);

                cats_dt_rates new_adults = m * juveniles;
                adults += new_adults;
                juveniles -= new_adults;
                cats_dt_rates modified_survival_rate = age_modified_juvenile_survival_rate(survival_rate, age, age_min,
                                                                                           age_max);


                cats_dt_rates dead = juveniles * (1.0 - modified_survival_rate);

                if (print) print_juvenile_adults(new_adults, start_pop, age);

                if (age < age_max) {
                        cats_dt_rates surviving = juveniles * survival_rate;

                        if (print) juvenile_start_status(age, start, new_adults, m, surviving, survival_rate);
                        if (print) print_juvenile_dead(dead, start_pop, age);

                        juveniles -= dead;
                        if (print) print_juvenile_trans(juveniles, start_pop, age);

                } else {
                        if (print) juvenile_start_status_old(age, start, new_adults, m, juveniles, start_pop);
                        if (print) juvenile_dead_old(age, juveniles, start_pop);

                }

        }
        return adults / start_pop;
}


#define JUV_SURV_DELTA 1e-5


cats_dt_rates calculate_survival_rate(cats_dt_rates total_survival, int32_t age_min, int32_t age_max, bool verbose)
{

        // annuals: juveniles mature in the same vegetation period they germinate,
        // so total survival from germination to adult == cell_maturation rate == survival_rate_max
        if (age_max == 0) {
                return total_survival;
        }

        cats_dt_rates low = 0.0;
        cats_dt_rates high = 1.0;
        cats_dt_rates s = total_survival / 2;
        if (verbose) {
                log_message(LOG_RAW,
                            "finding year-to-year survival for total survival rate %f from germination to adult with age min and %d age max %d\n",
                            (double) total_survival, age_min, age_max);
        }
        int32_t count = 0;
        while (true) {
                count++;
                cats_dt_rates ts = calculate_total_survival(s, age_min, age_max, false);
                if (verbose) {
                        log_message(LOG_RAW, "year survival %f -> total survival %f\n", (double) s, (double) ts);
                }
                //printf("ts %Lf\n", fabsl(ts - total_survival)/1e-5);

                if (fabsl(ts - total_survival) < JUV_SURV_DELTA) {
                        //printf("year survival %0.5Lf -> total survival %0.5Lf\n", s, ts);
                        if (verbose) { log_message(LOG_RAW, "done\n"); }
                        break;
                }

                if (count > 1000) {
                        log_message(LOG_ERROR,
                                    "could not calculate juvenile transition rate for germination to adult survival rate %f",
                                    (double) total_survival);
                        log_message(LOG_ERROR, "target %f, reached %f, abs difference %g >  %g by %g",
                                    (double) total_survival, (double) ts, (double) fabsl(ts - total_survival),
                                    JUV_SURV_DELTA,
                                    (double) fabsl(ts - total_survival) - JUV_SURV_DELTA);
                        log_message(LOG_ERROR, "minimum and maximum age of maturity are %d and %d ", age_min, age_max);
                        log_message(LOG_ERROR, "transition rate %f", (double) s);
                        exit_cats(EXIT_FAILURE);
                }

                if (ts > total_survival) { // too many survive, reduce
                        high = s;
                        s = (low + s) / 2.0;
                } else {
                        low = s;
                        s = (high + s) / 2.0;
                }

        }
        if (verbose) {
                log_message(LOG_RAW, "%f\n", (double) s);
        }
        //printf("done\n");

        return s;

}
