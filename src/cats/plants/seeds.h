// SPDX-License-Identifier: GPL-3.0-or-later
//
// seeds.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_SEEDS_H
#define CATS_SEEDS_H

#include "threading/threading-helpers.h"


void cell_seed_production(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, struct cats_thread_info *ts);

void area_germination(struct cats_grid *grid, struct cats_thread_info *ts);

void create_seed_structure(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);

void destroy_seed_structure(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col);

void area_seed_production(struct cats_grid *grid, struct cats_thread_info *ts);


cats_dt_rates get_pollination_probability(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col,
                                          struct cats_thread_info *ts);


void cell_germination(struct cats_grid *grid, struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
                      struct cats_thread_info *ts);

void
cell_post_process_seeds(struct cats_grid *grid, struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
                        struct cats_thread_info *ts);

#ifdef USEMPI

void clean_seeds_0_mpi_area(struct cats_grid *grid, struct cats_configuration *conf);

#endif

#endif