// SPDX-License-Identifier: GPL-3.0-or-later
//
// juveniles.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <string.h>
#include <gsl/gsl_rng.h>
#include <stdint.h>
#include <assert.h>

#include "data/cats_grid.h"
#include "threading/threading.h"
#include "inline_overlays.h"
#include "inline_vital_ages.h"
#include "inline_vital_rates.h"
#include "inline.h"
#include "misc/cats_random.h"
#include "populations/population.h"


cats_dt_population
juvenile_adult_transition(struct cats_grid *g, cats_dt_coord row, cats_dt_coord col, int mat_min, int mat_max,
                          cats_dt_rates juvenile_survival_rate, struct cats_thread_info *ts,
                          cats_dt_population max_new_adults)
{
        cats_dt_population newly_matured_sum = 0;
        cats_dt_population space_left = max_new_adults;
        if (max_new_adults == 0) return 0;

        assert(space_left >= 0);

        // cell_maturation starts with the oldest juveniles
        // and only happens if there is still free space in the cell
        // once the cell is full, we stop and juveniles have to wait at least until
        // the next year to have a chance to mature

        for (int32_t age = mat_max; age >= mat_min; age--) {
                cats_dt_population juveniles = g->juveniles[row][col][age];

                if (juveniles == 0) continue;
                // the cell_maturation rate depends on the age -- the older the juvenile, the higher the probability to mature
                cats_dt_rates maturation_rate = age_specific_maturation_rate(age, juvenile_survival_rate, mat_min,
                                                                             mat_max);
                cats_dt_population newly_matured = poisson_population_capped(ts->rng, (cats_dt_rates) juveniles *
                                                                                      maturation_rate,
                                                                             juveniles);

                newly_matured = min_population_t(newly_matured, space_left);

                g->juveniles[row][col][age] = juveniles - newly_matured;
                assert(g->juveniles[row][col][age] >= 0);
                newly_matured_sum += newly_matured;
                space_left -= newly_matured;

                if (space_left == 0) {
                        // log_message(LOG_INFO, "%s: stopping cell_maturation, out of space", __func__);
                        break;
                }

                if (space_left < 0) {
                        log_message(LOG_ERROR, "%s: too many adults generated", __func__);
                        exit_cats(EXIT_FAILURE);
                }
        }
        increase_population_by(g, row, col, newly_matured_sum);
        return newly_matured_sum;
}


cats_dt_population
cell_maturation(struct cats_grid *grid, struct cats_thread_info *ts, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid != NULL);
        assert(grid->id == 0);

        struct cats_configuration *conf = ts->conf;
        // should have been checked in the calling function
#ifdef CATS_ON_WINDOWS
        if (cell_excluded_by_overlay(conf, row, col)) { return 0; }
#else
        if (__builtin_expect(cell_excluded_by_overlay(conf, row, col), false)) { return 0; }
#endif

        if (grid->juveniles[row][col] == NULL) return 0;

        const int32_t mat_max = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);
        const int32_t mat_min = get_vital_age(grid, VA_AGE_OF_MATURITY_MIN);

        cats_dt_rates germination_to_adult_survival;


        // we don't know yet which of these variables we actually need


        const struct cats_vital_rate *rate_link = get_default_vital_rate(grid, VR_GERMINATION_TO_ADULT_SURVIVAL);
        germination_to_adult_survival = calculate_rate(rate_link, NAN, &grid->param, grid, row, col, NULL);

        cats_dt_rates juvenile_transition_rate = get_juvenile_tr_from_germination_to_adult_survival(
                germination_to_adult_survival, &grid->param);

        cats_dt_rates K = get_adult_carrying_capacity(grid, row, col);
        cats_dt_rates N = get_adult_population(grid, row, col);

        cats_dt_population max_new_adults = (cats_dt_population) (K - N);
        assert(max_new_adults >= 0);
        // allow juveniles (including freshly germinated) to become adults, if they are old enough

        cats_dt_population new_adults = juvenile_adult_transition(grid, row, col, mat_min, mat_max,
                                                                  juvenile_transition_rate, ts,
                                                                  max_new_adults);

        return new_adults;

}
