// SPDX-License-Identifier: GPL-3.0-or-later
//
// seeds.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stdint.h>
#include <assert.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "juveniles.h"
#include "seeds.h"
#include "plant_structures.h"
#include "misc/cats_random.h"
#include "inline_vital_rates.h"
#include "inline_overlays.h"
#include "inline_population.h"
#include "populations/population.h"

#ifdef USEMPI
#include "mpi/mpi_cats.h"
#endif

inline static bool
have_seeds_in_cell(const struct cats_grid *grid, const struct cats_configuration *conf, cats_dt_coord row,
                   cats_dt_coord col, const struct cats_thread_info *ts);


static inline void
germinate_seeds_in_ground(struct cats_thread_info *ts, struct cats_grid *grid, cats_dt_coord row,
                          cats_dt_coord col, cats_dt_rates germination_rate);


void cell_germination(struct cats_grid *grid, struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
                      struct cats_thread_info *ts)
{
        assert(grid != NULL);
        assert(conf != NULL);

        // skip if we are excluded or don't have seeds
        if (cell_excluded_by_overlay(conf, row, col)) { return; }
        if (!have_seeds_in_cell(grid, conf, row, col, ts)) return;

        const struct cats_vital_rate *vital_rate = get_default_vital_rate(grid, VR_GERMINATION_RATE);
        cats_dt_rates germination_rate = calculate_rate(vital_rate, NAN, &grid->param, grid, row, col, NULL);

        germinate_seeds_in_ground(ts, grid, row, col, germination_rate);
#ifdef CATS_DEBUG_SINGLE_CELL
        debug_seeds(grid, conf, row, col, "seeds nach keimung");
#endif

}


void cell_seed_production(struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col, struct cats_thread_info *ts)
{
        assert(grid != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);

        const struct cats_configuration *conf = grid->conf;
#ifdef CATS_ON_WINDOWS
        if (cell_excluded_by_overlay(conf, row, col)) { return; }
#else
        if (__builtin_expect(cell_excluded_by_overlay(conf, row, col), false)) { return; }
#endif


        assert(grid->seeds_produced != NULL);
        // ensure that no seeds are left-over in the cell
        assert(grid->seeds_produced[row][col] == 0.0);

        grid->seeds_produced[row][col] = 0.0f;

        // no population -> no seeds
        cats_dt_rates N = get_adult_population(grid, row, col);
        if (N <= 0) return;


        // the number of seeds produced is the product of several factors
        // * the number of adults plants N
        // * the pollination rate (only for sexual reproduction, otherwise is rate is 1.0)
        // * the flowering frequency
        // * the seed yield of a single adult plant

        // this number is fed into the poisson distribution

        // for hapaxanth plants some adults will die: N * flowering frequency * hapaxanthy factor

        const cats_dt_rates pollination_rate = get_pollination_probability(grid, row, col, ts);
        assert(pollination_rate >= 0.0);
        assert(pollination_rate <= 1.0);


        // N already loaded

        const struct cats_vital_rate *rate_link = get_default_vital_rate(grid, VR_SEED_YIELD);
        //load_data_for_link(rate_link, grid, row, col, &suit, NULL, &K); // N already loaded
        const cats_dt_rates seed_yield = calculate_rate(rate_link, N, &grid->param, grid, row, col, NULL);

        rate_link = get_default_vital_rate(grid, VR_FLOWERING_FREQUENCY);
        //load_data_for_link(rate_link, grid, row, col, &suit, NULL, &K); // N already loaded
        const cats_dt_rates flowering_frequency = calculate_rate(rate_link, N, &grid->param, grid, row, col, NULL);


        const cats_dt_rates total_seeds = N * flowering_frequency * seed_yield * pollination_rate;
        assert(total_seeds >= 0.0);

        grid->seeds_produced[row][col] = poisson_seeds(ts->rng, total_seeds); // we don't cap

        if (grid->seeds_produced[row][col] > 0) ts->stats[grid->id].stats[CS_SEEDS_PRODUCED] += 1;

        cats_dt_rates hapaxanth = grid->param.hapaxanthy;

        if (hapaxanth > 0.0) {
                int32_t dying = round_population_safe(-1.0 * N * flowering_frequency * hapaxanth);
                increase_population_by(grid, row, col, dying);
                log_message(LOG_UNIMPLEMENTED, "hapaxanthy not enabled"); // FIXME CHECK
                abort();
        }
}


static inline void
add_germinated(struct cats_thread_info *ts, struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col,
               cats_dt_population germinated, const int32_t K_class)
{
        if (germinated <= 0) return;
        create_juvenile_structure_if_needed(grid, row, col);
        grid->juveniles[row][col][0] = (cats_dt_population) min_population_t(grid->juveniles[row][col][0] + germinated,
                                                                             K_class);
        assert(grid->juveniles[row][col][0] >= 0);

        int32_t id = grid->id;
        ts->stats[id].stats[GS_SEEDS_GERMINATED] += (int64_t) germinated;
}


static inline void
germinate_seeds_in_ground(struct cats_thread_info *ts, struct cats_grid *grid, cats_dt_coord row,
                          cats_dt_coord col, cats_dt_rates germination_rate)
{
        const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);

        const int max_k = (seed_persistence - 0);// -1);
        const cats_dt_population K_class = (cats_dt_population) get_vital_rate_maximum(&grid->param.carrying_capacity);
        //const cats_dt_population K_class = (cats_dt_population) get_max_rate_from_grid(grid, VR_CARRYING_CAPACITY);//grid->param.cc_max;

        for (int32_t k = 0; k < max_k; k++) {

                cats_dt_population germinated = poisson(ts->rng, grid->seed_bank[row][col][k] * germination_rate);

                if (germinated <= 0) continue;

                add_germinated(ts, grid, row, col, germinated, K_class);
                grid->seed_bank[row][col][k] = max_float(grid->seed_bank[row][col][k] - (float) germinated, 0.0f);

        }
}


static inline void
germinate_seeds0(struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col, struct cats_thread_info *ts,
                 const cats_dt_rates germination_rate)
{
        // how many seeds of the first year germinate?
        if (grid->dispersed_seeds[row][col] > 0) {
                log_message(LOG_RAW, "%f\n", grid->dispersed_seeds[row][col]);
                abort();
        }
        cats_dt_population germinated = poisson(ts->rng, grid->dispersed_seeds[row][col] * germination_rate);
        if (germinated <= 0) return;

        add_germinated(ts, grid, row, col, germinated,
                       (cats_dt_population) get_vital_rate_maximum(&grid->param.carrying_capacity));
        //(cats_dt_population) get_max_rate_from_param(&grid->param, VR_CARRYING_CAPACITY));
        // deduct germinated seeds from seed pool
        grid->dispersed_seeds[row][col] = max_float(grid->dispersed_seeds[row][col] - (float) germinated,
                                                    0.0f); // fixme max_seeds
}


inline static bool
have_seeds_in_cell(const struct cats_grid *grid, const struct cats_configuration *conf, cats_dt_coord row,
                   cats_dt_coord col, const struct cats_thread_info *ts)
{

        assert(grid->dispersed_seeds != NULL);
        assert(grid->dispersed_seeds[row][col] >= 0);
        return (grid->seed_bank[row][col] != NULL || grid->dispersed_seeds[row][col] > 0.5);
}


#ifdef USEMPI
void clean_seeds_0_mpi_area(struct cats_grid *grid, struct cats_configuration *conf)
{
#ifdef TESTMPI
        log_message(LOG_ERROR, "MPI SKIPPED SEEDS0 SETUP");
        return;
#else
#endif


        // kill seeds in surrounding dispersal area
        int rows_before = 0;
        int rows_after = 0;
        int radius;
        get_rows_around(conf, &rows_before, &rows_after, &radius, grid->id);
        log_message(LOG_RAW, "%d %d %d\n", rows_before, rows_after, radius);
        for (cats_dt_coord row = 0; row < grid->dimension.rows + rows_before + rows_after; row++) {
                for (cats_dt_coord col = 0; col < grid->dimension.cols; col++) {
                        grid->seeds_0_all[row][col] = 0.0f;
                }
        }
}


#endif


void
cell_post_process_seeds(struct cats_grid *grid, struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
                        struct cats_thread_info *ts)
{
        assert(grid != NULL);
        assert(grid->dispersed_seeds != NULL);
        assert(grid->dispersed_seeds[row][col] >= 0);

        if (cell_excluded_by_overlay(conf, row, col)) {
                grid->dispersed_seeds[row][col] = 0;
                return;
        }

        if (grid->dispersed_seeds[row][col] == 0.0) return;

        const int32_t id = grid->id;
        ts->stats[id].stats[CS_SEEDS_BEFORE_POISSON] += 1;

        cats_dt_seeds seeds_after_poisson = poisson_seeds(ts->rng,
                                                          grid->dispersed_seeds[row][col]); /* IMPORTANT stochasticity required */
        grid->dispersed_seeds[row][col] = seeds_after_poisson;
        if (seeds_after_poisson > 0) ts->stats[id].stats[CS_SEEDS_AFTER_POISSON] += 1;
}
