// SPDX-License-Identifier: GPL-3.0-or-later
//
// clonal_growth.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "clonal_growth.h"
#include "vital_rates/hybrid_functions.h"
#include "inline.h"
#include "misc/cats_random.h"
#include "inline_vital_rates.h"
#include "inline_population.h"
#include "populations/population.h"


void cell_clonal_growth(struct cats_grid *grid, const struct cats_thread_info *ts, cats_dt_coord row,
                        cats_dt_coord col)
{
        assert(grid->conf->grid_count == 1);

        cats_dt_rates N = get_adult_population(grid, row, col);
        if (N < 1) return;

        struct cats_vital_rate *link = &grid->param.vital_rates[VR_CLONAL_GROWTH];
        cats_dt_rates max_rate = get_default_vital_rate_maximum_from_param(&grid->param, VR_CLONAL_GROWTH);
        cats_dt_rates cg = calculate_rate(link, N, &grid->param, grid, row, col, NULL); // results in 1 + f(cg)
        cg = clamp(cg, 1.0, max_rate + 1.0);
        if (cg <= 1.0) { return; }

        cats_dt_population population_diff = poisson_population_capped(ts->rng, N * (cg - 1.0), (cats_dt_population) N);
        increase_population_by(grid, row, col, population_diff);
}
