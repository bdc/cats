// SPDX-License-Identifier: GPL-3.0-or-later
//
// plant_structures.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <string.h>

#include "plant_structures.h"
#include "juveniles.h"
#include "seeds.h"
#include "inline_population.h"
#include "populations/population.h"
#include <memory/cats_memory.h>


void destroy_plant_cell(struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col)
{
        assert(grid != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        destroy_juveniles(grid, row, col);
        destroy_seed_structure(grid, row, col);
        grid->dispersed_seeds[row][col] = 0.0f;
        grid->seeds_produced[row][col] = 0.0f;
        set_population_ignore_cc(grid, row, col, 0);
}


void create_seed_structure(const struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col)
{
        assert(grid != NULL);
        if (grid->seed_bank[row][col] != NULL) return;
        const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);
        const int32_t size = seed_persistence - 0; //  - 1);
        if (size <= 0) return; // no seed persistence

        assert(grid->seed_bank != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        assert(grid->seed_bank[row][col] == NULL);

        grid->seed_bank[row][col] = calloc_or_die(size, sizeof(cats_dt_seeds));


}


void destroy_seed_structure(const struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col)
{
        assert(grid != NULL);
        assert(grid->seed_bank != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);

        const int size = (seed_persistence - 0); // -1

        // do we actually have seeds in the ground?
        if (size <= 0 || grid->seed_bank[row][col] == NULL) return;

        for (uint32_t i = 0; i < size; i++) {
                grid->seed_bank[row][col][i] = 0.0f;
        }

        free(grid->seed_bank[row][col]);
        grid->seed_bank[row][col] = NULL;
}


#ifndef CATS_ON_WINDOWS
inline
#endif
void
create_juvenile_structure_if_needed(struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col)
{
        if (grid->juveniles[row][col]) return;
        const int32_t max_age_of_maturity = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);

        grid->juveniles[row][col] = create_juvenile_structure(max_age_of_maturity);

}


void destroy_juveniles(const struct cats_grid *grid, const cats_dt_coord row, const cats_dt_coord col)
{
        assert(grid != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        assert(grid->juveniles != NULL);


        const int32_t max_age_of_maturity = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);

        const int32_t stages = max_age_of_maturity + 1;
        assert(stages >= 0);

        if (grid->juveniles[row] && grid->juveniles[row][col]) {
                memset(grid->juveniles[row][col], 0, stages * sizeof(cats_dt_population));
                free(grid->juveniles[row][col]);
                grid->juveniles[row][col] = NULL;
        }
}


// creates the juvenile structure, an array from age 0 (germinated this year) to maximum age of maturity
// resulting in (maximum age of maturity) elements per array.
// NOTE: this could be reduced to (maximum age of maturity + 0), by reusing the field with index 0, as this field will
//  be empty at the end of vegetation period anyway.
//
// stage structure
// 0: germinated in the current vegetation period (emptied at the end of the veg. period, as the age will be advanced then)
// 1: germinated in the previous vegetation period
// ...
// minimum age of maturity: first chance to mature
// ...
// maximum age of maturity: last chance to mature
inline cats_dt_population *create_juvenile_structure(const int32_t maturity_max)
{
        assert(maturity_max >= 0);
        cats_dt_population *result = calloc_or_die((maturity_max + 1), sizeof(cats_dt_population));
        return result;
}
