// SPDX-License-Identifier: GPL-3.0-or-later
//
// long_range_dispersal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "long_range_dispersal.h"
#include "actions/cats_actions.h"
#include "vital_rates/hybrid_functions.h"
#include "inline_vital_rates.h"
#include "inline_overlays.h"
#include "dispersal/dispersal.h"


void disperse_long_range(struct cats_grid *grid)
{
        //WARNING: IF REWRITTEN TO USE THREADING, REPLACE grid->rng to ts->rng
#ifdef USEMPI
        log_message(LOG_UNIMPLEMENTED, "MPI SKIPPED LONG RANGE DISPERSAL");
                return;
#endif
        struct cats_configuration *conf = grid->conf;
        const cats_dt_coord max_rows = grid->dimension.rows;
        const cats_dt_coord max_cols = grid->dimension.cols;

        const cats_dt_rates event_prob = grid->dispersal->long_range_prob;
        const int32_t radius = grid->dispersal->long_range_radius;
        const int32_t events = grid->dispersal->long_range_target_count;

        // LOOP OVER GRID AND PICK A RANDOM NUMBER [0, 1)
        // ONLY DISPERSE IF <= LONG RANGE DISPERSAL PROBABILITY

        for (cats_dt_coord row = 0; row < max_rows; row++) {
                for (cats_dt_coord col = 0; col < max_cols; col++) {

                        // REQUIREMENTS
                        // 0) cell is not excluded
                        if (cell_excluded_by_overlay(conf, row, col)) { continue; }

                        // 1) seeds are present
                        if (grid->seeds_produced[row][col] == 0) continue;

                        // 2) only a certain probability of cells disperses long range
                        if (gsl_rng_uniform(grid->rng) > event_prob) continue; // FIXME IF THREADED

                        // CALCULATE SIZE OF A SINGLE DISPERSAL 'PACKAGE'

                        const struct cats_vital_rate *rate_link = get_default_vital_rate(grid, VR_SEED_YIELD);
                        cats_dt_rates package = calculate_rate(rate_link, NAN, &grid->param, grid, row, col, NULL);

                        // FOR EACH DISPERSAL EVENT CHOOSE A RANDOM TARGET CELL IN RANGE
                        for (int32_t i = 0; i < events; i++) {
                                // if a dispersal package falls outside the grid it is gone - no new attempt!
                                // fixme function random_coord

                                cats_dt_coord trow =
                                        (cats_dt_coord) gsl_rng_uniform_int(grid->rng, 2 * radius - 1) - radius +
                                        row; // FIXME IF THREADED
                                if (trow < 0 || trow >= max_rows) continue;

                                cats_dt_coord tcol =
                                        (cats_dt_coord) gsl_rng_uniform_int(grid->rng, 2 * radius - 1) - radius +
                                        col; // FIXME IF THREADED
                                if (tcol < 0 || tcol >= max_cols) continue;

                                // don't disperse more seeds than are left in the cell
                                if (package > grid->seeds_produced[row][col]) {
                                        package = grid->seeds_produced[row][col];
                                }

                                // disperse seeds and reduce number of local seeds
                                grid->dispersed_seeds[trow][tcol] += (cats_dt_seeds) package;
                                grid->seeds_produced[row][col] -= (cats_dt_seeds) package;

                                // stop if no more seeds are left to distribute
                                if (grid->seeds_produced[row][col] <= 0.0f) {
                                        grid->seeds_produced[row][col] = 0.0f;
                                        break;
                                }
                        }
                }
        }
}
