// SPDX-License-Identifier: GPL-3.0-or-later
//
// inter_period.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "vital_rates/hybrid_functions.h"
#include "misc/cats_random.h"
#include "inline.h"
#include "inter_period.h"
#include "plants/plant_structures.h"
#include "inline_vital_rates.h"
#include "inline_population.h"
#include "populations/population.h"


void inter_period_survival_seeds(struct cats_grid *grid, struct cats_configuration *conf, cats_dt_coord row,
                                 cats_dt_coord col, struct cats_thread_info *ts)
{
        // if we have no seeds, we are done
        if (grid->dispersed_seeds[row][col] == 0.0 && grid->seed_bank[row][col] == NULL) return;

        cats_dt_rates seed_survival_rate;


        const struct cats_vital_rate *rate_link_surv = get_default_vital_rate(grid, VR_SEED_SURVIVAL);
        seed_survival_rate = calculate_rate(rate_link_surv, NAN, &grid->param, grid, row, col, NULL);

        if (grid->dispersed_seeds[row][col] > 0) {
                cats_dt_seeds dying_0 = poisson_seeds_capped(ts->rng, grid->dispersed_seeds[row][col] *
                                                                      (1.0 - seed_survival_rate),
                                                             grid->dispersed_seeds[row][col]);

                grid->dispersed_seeds[row][col] = grid->dispersed_seeds[row][col] - dying_0;

                if (grid->dispersed_seeds[row][col] > 0.0 && grid->seed_bank[row][col] == NULL) {
                        create_seed_structure(grid, row, col);
                }
        }

        if (grid->dispersed_seeds[row][col] == 0.0 && grid->seed_bank[row][col] == NULL) return;

        const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);
        for (int32_t stage = 0; stage < seed_persistence - 0; stage++) { // -1
                cats_dt_seeds dying = poisson_seeds_capped(ts->rng,
                                                           grid->seed_bank[row][col][stage] *
                                                           (1.0 - seed_survival_rate),
                                                           grid->seed_bank[row][col][stage]);
                grid->seed_bank[row][col][stage] = grid->seed_bank[row][col][stage] - dying;

        }

        for (int32_t k = seed_persistence - 1; k >= 1; k--) { // -2
                grid->seed_bank[row][col][k] = grid->seed_bank[row][col][k - 1];
        }

        grid->seed_bank[row][col][0] = grid->dispersed_seeds[row][col];
        assert(grid->seed_bank[row][col][0] >= 0.0);
        grid->dispersed_seeds[row][col] = 0.0f;

}


void inter_period_survival_juveniles(struct cats_grid *grid, struct cats_configuration *conf, cats_dt_coord row,
                                     cats_dt_coord col, struct cats_thread_info *ts)
{
        if (grid->juveniles[row][col] == NULL) return;
        const int32_t mat_max = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);
        const int32_t mat_min = get_vital_age(grid, VA_AGE_OF_MATURITY_MIN);


        const struct cats_vital_rate *rate_link = get_default_vital_rate(grid, VR_GERMINATION_TO_ADULT_SURVIVAL);
        cats_dt_rates germination_to_adult_survival = calculate_rate(rate_link, NAN, &grid->param, grid, row, col,
                                                                     NULL);

        cats_dt_rates juvenile_transition_rate = get_juvenile_tr_from_germination_to_adult_survival(
                germination_to_adult_survival, &grid->param);

        for (int32_t i = 0; i <= mat_max; i++) {
                cats_dt_rates modified_juvenile_transition_rate = age_modified_juvenile_survival_rate(
                        juvenile_transition_rate, i, mat_min, mat_max);
                cats_dt_rates juvenile_mortality = (1.0 - modified_juvenile_transition_rate);

                assert(juvenile_mortality >= 0 && juvenile_mortality <= 1.0);
                assert(grid->juveniles[row][col][i] >= 0);

                cats_dt_population dying = poisson_population_capped(ts->rng,
                                                                     (cats_dt_rates) grid->juveniles[row][col][i] *
                                                                     juvenile_mortality,
                                                                     grid->juveniles[row][col][i]);

                grid->juveniles[row][col][i] = grid->juveniles[row][col][i] - dying;
                assert(grid->juveniles[row][col][i] >= 0);

        }
        // advance juvenile stage (after transition rates have been applied)
        for (int32_t stage = mat_max; stage > 0; stage--) {
                grid->juveniles[row][col][stage] = grid->juveniles[row][col][stage - 1];
        }

        // youngest stage now empty
        grid->juveniles[row][col][0] = 0;

}


void inter_period_survival_adults(struct cats_grid *grid, struct cats_configuration *conf,
                                  cats_dt_coord row,
                                  cats_dt_coord col, struct cats_thread_info *ts)
{

        cats_dt_rates N = get_adult_population(grid, row, col);
        if (N < 1) return;

        if (get_default_vital_rate_maximum_from_grid(grid, VR_ADULT_SURVIVAL) == 0.0) { // e.g. annuals, biennials
                set_population(grid, row, col, 0);
                return;
        }
        const struct cats_vital_rate *rate_link = get_default_vital_rate(grid, VR_ADULT_SURVIVAL);

        const cats_dt_rates survival = calculate_rate(rate_link, N, &grid->param, grid, row, col, NULL);
        const cats_dt_population dying = poisson_population_capped(ts->rng, N * (1.0 - survival),
                                                                   (cats_dt_population) N);
        reduce_population_by(grid, row, col, dying);
}


void inter_period_survival(struct cats_grid *grid, struct cats_configuration *conf,
                           cats_dt_coord row, cats_dt_coord col,
                           struct cats_thread_info *ts)
{

        inter_period_survival_seeds(grid, conf, row, col, ts);
        // survival happens after the carrying capacity has been applied
        // the number of adults and juveniles can only be reduced, so we shouldn't have to check
        // for juvenile carrying capacity violations.
        inter_period_survival_juveniles(grid, conf, row, col, ts);
        inter_period_survival_adults(grid, conf, row, col, ts);
}
