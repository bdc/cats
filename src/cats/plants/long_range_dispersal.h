// SPDX-License-Identifier: GPL-3.0-or-later
//
// long_range_dispersal.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_LONG_RANGE_DISPERSAL_H_
#define CATS_LONG_RANGE_DISPERSAL_H_

#include "data/cats_grid.h"
#include "configuration/configuration.h"

void disperse_long_range(struct cats_grid *grid);

#endif

