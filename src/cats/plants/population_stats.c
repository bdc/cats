// SPDX-License-Identifier: GPL-3.0-or-later
//
// population_stats.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "../../memory/raw_arrays.h"
#include "population_stats.h"
#include "configuration/configuration.h"
#include "data/cats_grid.h"

#include "logging.h"


void print_populated_cell_change_carrying_capacity(struct cats_configuration *conf, struct cats_grid **parent)
{
        float area = (float) conf->geometry.dimension.rows * (float) conf->geometry.dimension.cols;
        for (int i = 0; i < conf->grid_count; i++) {
                struct cats_grid *g = parent[i];
                int64_t old_pop = g->stats.stats[CS_POPULATED_BEFORE_CC];
                int64_t new_pop = g->stats.stats[CS_POPULATED_AFTER_CC];
                float old_perc = ((float) old_pop / area) * 100.0f;
                float new_perc = ((float) new_pop / area) * 100.0f;


                log_message(LOG_INFO,
                            "CC2 : grid %d (%s) populated cells: %"PRIi64" (%.3f%%) -> %"PRId64" (%.3f%%), %"PRId64" lost",
                            i, conf->param[i].species_name,
                            old_pop, old_perc, new_pop, new_perc, old_pop - new_pop);
                if (old_pop < new_pop) exit(1);

        }

}
