// SPDX-License-Identifier: GPL-3.0-or-later
//
// plant_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <math.h>
#include "configuration/configuration.h"
#include "data/cats_datatypes.h"
#include "seeds.h"
#include "data/cats_grid.h"
#include "inline_population.h"
#include "populations/population.h"


cats_dt_rates get_pollination_probability(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col,
                                          struct cats_thread_info *ts)
{
        assert(grid != NULL);
        // makes no sense for asexual species
        if (grid->param.sexuality == SEX_ASEXUAL) return 1.0;

        struct cats_grid **parent = grid->parent;
        assert(parent != NULL);

        cats_dt_population N = get_adult_population(grid, row, col);
        int64_t N_all = get_population_all_classes_sum((const struct cats_grid **) parent, row, col);

        // we need at least 2 plants in the cell for sexual reproduction
        if (N < 2) return 0.0;

        // FIXME magic numbers
        // Number of visits 5...10, capped at N_all

        int max_visits = (int) gsl_rng_uniform_int(ts->rng, 6) + 5;
        int64_t visits = min_int64t(N_all, max_visits);

        long double prob = (float) N / (float) N_all;
        assert(prob <= 1.0 && prob >= 0);
        long double p0 = powl((1.0 - prob), visits);
        long double p1 = prob * powl((1.0 - prob), visits - 1) * visits;
        long double rate = (1.0 - p0 - p1);

        assert(rate >= 0.0);
        assert(rate <= 1.0);

        return max_rates(rate, 0.0); // can be slightly negative
}

