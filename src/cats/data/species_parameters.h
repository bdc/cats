// SPDX-License-Identifier: GPL-3.0-or-later
//
// species_parameters.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include "cats_global.h"
#include "defaults.h"
/// @file cats_structs.h 
/// @brief temporary collection of structs to be used during reorganization 

#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>
#include "cats_enums.h"
#include "cats_datatypes.h"
#include "vital_ages/vital_ages.h"
#include "vital_rates/vital_rates.h"
#include "modules/modules.h"

#define MAX_JUVENILE_TR_STEPS 10001

// forward declarations
struct cats_vital_rate;





/*! \page file_name_patterns File name patterns and variable substitutions
  \tableofcontents
  <!--
  This page describes file name patterns and variable substitutions in file names.
  \section sec_patterns File name patterns -->
  File name patterns are used to specify input files for CATS. You can either specify relative or absolute paths.
  Certain parts of the pattern (starting with %) will be substituted variables at runtime. See \ref fn_patt_vars
  If no percentage signs are present, the file name pattern will be used as is.


  \section fn_patt_vars Variable substitutions

  \subsection fn_patt_year Year: %Y
  \%Y will be substituted with the current simulation year. The format of the year string depends on two configuration values:
  \ref always_show_sign  and \ref zero_pad_to_digits FIXME DOC
  \subsubsection fn_patt_year_ex Example
  <tt>suitability_pattern=suitability/100m-%Y/optimum.tiff</tt>

*/



/// @brief species parameters

enum cats_parametrization_type {
        PARAM_UNDEFINED,
        PARAM_HYBRID,
        PARAM_DIRECT_VITAL_RATE
};

struct cats_initial_population {
        bool adjusted;
        bool set_to_cc;
        //bool prune_under_ZT;
        cats_dt_rates suitability_threshold;
        char *filename; ///< filename for the initial population
};

enum cats_species_preset {
        CATS_P_UNSPECIFIED,
        CATS_P_ANNUAL,
        CATS_P_BIENNIAL,
        CATS_P_TREE
};

struct cats_species_vital_rate_preset {
        bool have_rate_minimum_scale_factor_preset;
        cats_dt_rates rate_minimum_scale_factor_preset;
        bool have_vital_rate_maximum_preset;
        cats_dt_rates vital_rate_maximum_preset;
};

struct cats_species_vital_age_preset {
        bool have_vital_age_preset;
        int32_t vital_age_preset;
};

struct cats_species_param_presets {
        enum cats_species_preset preset;


        struct cats_species_vital_age_preset vital_ages[VA_MAX];

/*
        struct cats_species_vital_rate_preset vital_rates[VR_MAX];

        //bool have_vital_age_preset[VA_MAX];
        //int32_t vital_age_preset[VA_MAX];
        //bool have_rate_minimum_scale_factor_preset[VR_MAX];
        //cats_dt_rates rate_minimum_scale_factor_preset[VR_MAX];
        //bool have_vital_rate_maximum_preset[VR_MAX];
        //cats_dt_rates vital_rate_maximum_preset[VR_MAX];
    */
        bool have_adult_cc_fraction_preset;
        cats_dt_rates adult_cc_fraction_preset;

};
struct cats_ini;
struct lambda_parameters;

struct cats_species_param {
        cats_dt_rates *lambda_cache;
        bool save_lambda_grid;
        bool save_lambda_grid_density;
        bool custom_vital_rates;
        bool custom_vital_ages;
        bool custom_dispersal;
        bool default_demographics;
        cats_dt_rates max_adult_cc_fraction;
        struct cats_species_param_presets presets;
        struct cats_initial_population initial_population;
        enum cats_parametrization_type parametrization;
        struct cats_module *demographic_module;
        //struct cats_rate_information carrying_capacity;
        struct cats_vital_rate carrying_capacity;
        struct cats_vital_rate vital_rates[MAX_VITAL_RATES];

        cats_dt_rates *juvenile_cc_weights;

        int32_t vital_ages[MAX_VITAL_AGES];
        char *preset_name;

        // BOTH ANIMAL AND PLANTS
        //@{

        // HYBRID MODE ONLY
        cats_dt_rates ZT;                      ///< zero threshold - minimum required suitability for FIXME - Warning: when using predictors, OT and ZT have to be changed to fit the multiplier
        cats_dt_rates OT;                      ///< occurrence threshold -  minimum required suitability for FIXME - Warning: when using predictors, OT and ZT have to be changed to fit the multiplier
        cats_dt_rates scale_factor;            ///< scale factor FIXME DOC
        cats_dt_rates demographic_slope;       ///< FIXME DOC
        char *species_name;                    ///< used for display (?)
        char *species_config_section;
        char *species_name_actual;             ///< used for file names (when multiple species use the same set of input data)
        char *suitability_pattern;             ///< file name pattern for suitability files. Certain patterns get replace. See \ref file_name_patterns
        char *suitability_name;
        enum sexuality_type sexuality;         ///< PLANTS ONLY?
        cats_dt_rates hapaxanthy;              ///< FIXME DOC
        cats_dt_rates juvenile_transition_rates[MAX_JUVENILE_TR_STEPS];
        //@}


        /** @name animal specific parameters  */ // ANIMALS
        //@{
        //int32_t eggs_per_female; ///< maximum number of eggs produced per female insect (FIXME DOC under optimal conditions?)
        //int32_t generations_max; ///< maximum number of yearly generations of insects
        //int32_t generations_min; ///< minimum number of yearly generations of insects

        //int32_t animal_dispersal_max_radius; ///< maximal flight/dispersal distance
        //@}


        int32_t plant_dispersal_max_radius;

        struct module_species_data module_data[MAX_MODULES];

};

void init_cats_species_param(struct cats_species_param *param);