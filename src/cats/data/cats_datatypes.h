// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_datatypes.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_DATATYPES_H
#define CATS_DATATYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <float.h>

// scalar and simple composite data types


typedef float cats_dt_dispersal;           ///< data type for dispersal probabilities
typedef long double cats_dt_rates;         ///< data type for suitability-dependent rates

// POPULATIONS
typedef int32_t cats_dt_population;        ///< data type for cell populations of a single class
#define CATS_MAX_POPULATION INT32_MAX

typedef int64_t cats_dt_population_sum;    ///< data type for the sum of class/cell populations
#define CATS_MAX_POPULATION_SUM INT64_MAX

// SEEDS
typedef float cats_dt_seeds;               ///< data type for seeds
#define CATS_MAX_SEEDS FLT_MAX

typedef float cats_dt_environment;         ///< data type for suitabilities

typedef int32_t cats_dt_coord;             ///< data type for coordinates (grid row and column indices)
#define CATS_MAX_COORD INT32_MAX


/// @brief Struct for grid dimensions (number of rows and columns)
struct cats_dimension {
        cats_dt_coord rows;
        cats_dt_coord cols;
};

/// @brief Struct for grid coordinates (row and column)
struct cats_coordinates {
        cats_dt_coord row;
        cats_dt_coord col;
};


// conversion functions
#include <cats_strings/string_converters.h>

#endif