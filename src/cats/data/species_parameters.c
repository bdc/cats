// SPDX-License-Identifier: GPL-3.0-or-later
//
// species_parameters.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#include <string.h>
#include "species_parameters.h"


void init_cats_species_param(struct cats_species_param *param)
{
        struct cats_species_param tmp = {0};
        *param = tmp;
        struct cats_species_param_presets _preset = {0};
        param->presets = _preset;
        for (int32_t i = 0; i < MAX_MODULES; i++) {
                init_module_species_data(&param->module_data[i]);
        }
        param->save_lambda_grid = false;
        param->save_lambda_grid_density = false;
        param->custom_dispersal = false;
        param->custom_vital_ages = false;
        param->custom_vital_rates = false;
        param->lambda_cache = NULL;
        param->scale_factor = 0.5;
        param->max_adult_cc_fraction = 0.8;
        //param->min_reserved_juvenile_cc_fraction = 1.0 - param->max_adult_cc_fraction;
        param->sexuality = SEX_ASEXUAL;
        param->species_name = NULL;
        param->juvenile_cc_weights = NULL;
        param->preset_name = strdup("<no preset specified>");
        param->species_name_actual = NULL;
        param->suitability_pattern = NULL;
        param->initial_population.filename = NULL;
        param->initial_population.suitability_threshold = 0.0;
        param->initial_population.set_to_cc = true;
        param->initial_population.adjusted = false;

        init_cats_vital_rates(param->vital_rates);

}