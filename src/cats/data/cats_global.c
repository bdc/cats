// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_global.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "cats_global.h"
#include "misc/misc.h"
#include "defaults.h"
#include "memory/cats_memory.h"
#include <logging/logging.h>
#include <stdlib.h>


void init_global_info(const char *program_name)
{
        time_now(&global.time_info);

        set_program_name(program_name);
        global.enable_json_output = false;
        global.poisson_dampening_factor = DEFAULT_POISSON_DAMPENING_FACTOR;
        global.poisson_dampening_minimum = DEFAULT_POISSON_DAMPENING_MIN_ROUNDED_TS;
        global.poisson_maximum = DEFAULT_POISSON_MAXIMUM;
        global.poisson_maximum_draw_diff = DEFAULT_POISSON_MAXIMUM_DRAW_DIFF;

        // CHECK FOR IEEE754 support: is 0.0 represented by binary all 0?
        float *f = calloc_or_die(1, sizeof(float));
        double *d = calloc_or_die(1, sizeof(double));
        long double *ld = calloc_or_die(1, sizeof(long double));


        free(f);
        free(d);
        free(ld);

        assert(NULL == 0);

}


#ifdef CATS_ON_WINDOWS
void __declspec(noreturn) exit_cats(int status)
#else


_Noreturn void exit_cats(int status)
#endif
{


#ifdef USEMPI
        if (status != EXIT_SUCCESS) {
                MPI_Abort(MPI_COMM_WORLD, status);

        }
#endif
        if (status != EXIT_SUCCESS) {
                // abort();
        }

        exit(status);


}



