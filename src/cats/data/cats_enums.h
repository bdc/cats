// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_enums.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

/// @brief used to produce timing information and output after an action



/// @brief  provides information about the dispersal vector type

enum dispersal_type {
        DISPERSAL_KERNEL = 1, ///< dispersal is kernel (grid)
        DISPERSAL_CODE = 2 ///< dispersal is function (code))
};

/*
enum simulation_mode {
    SIMMODE_AUTOMAT = 0,
    //SIMMODE_MEGAGRID = 1,
    //SIMMODE_WASSER = 2,
    //SIMMODE_PROPAGATION_SPEED = 3,
    SIMMODE_ANIMALS = 4
};
*/
/*
enum cats_life_type {
    PLANT = 0,
    ANIMAL
};
*/

enum sexuality_type {
        SEX_ASEXUAL = 0,
        SEX_SEXUAL
};

