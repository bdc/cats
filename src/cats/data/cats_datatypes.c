// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_datatypes.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "cats_datatypes.h"
#include "logging.h"


const char *true_values[] = {"1", "y", "t"};
const char *false_values[] = {"0", "n", "f"};
const int true_counts = (int) (sizeof(true_values) / sizeof(char *));
const int false_counts = (int) (sizeof(false_values) / sizeof(char *));


bool string_to_double(char *string, double *value)
{
        if (string == NULL || strlen(string) == 0) return false;

        char *end_pointer = NULL;
        errno = 0;

        double converted = strtod(string, &end_pointer);

        if (strlen(end_pointer) != 0) {
                log_message(LOG_WARNING, "%s: invalid or unused characters when converting '%s' to 'double': '%s'",
                            __func__, string,
                            end_pointer);

        }

        if (errno == 0) {
                *value = converted;
                return true;
        }
        return false;
}


bool string_to_float(char *string, float *value)
{
        if (string == NULL || strlen(string) == 0) return false;

        char *end_pointer = NULL;
        errno = 0;

        float converted = strtof(string, &end_pointer);

        if (strlen(end_pointer) != 0) {
                log_message(LOG_WARNING, "%s: invalid or unused characters when converting '%s' to 'float': '%s'",
                            __func__, string,
                            end_pointer);
                return false;
        }

        if (errno == 0) {
                *value = converted;
                return true;
        }
        return false;
}


bool string_to_long_double(char *string, long double *value)
{
        if (string == NULL || strlen(string) == 0) return false;

        char *end_pointer = NULL;
        errno = 0;

        long double converted = strtold(string, &end_pointer);

        if (strlen(end_pointer) != 0) {
                log_message(LOG_WARNING, "%s: invalid or unused characters when converting '%s' to 'long double': '%s'",
                            __func__, string, end_pointer);
                return false;
        }

        if (errno == 0) {
                *value = (cats_dt_rates) converted;
                return true;
        }
        return false;
}


bool string_to_bool(char *string, bool *value)
{
        if (string == NULL) return false;

        for (int i = 0; i < true_counts; i++) {
                if (!strncmp(string, true_values[i], 1)) {
                        *value = true;
                        return true;
                }
        }

        for (int i = 0; i < false_counts; i++) {
                if (!strncmp(string, false_values[i], 1)) {
                        *value = false;
                        return true;
                }
        }

        return false;
}


bool string_to_integer(char *string, int32_t *value)
{
        if (string == NULL || strlen(string) == 0) return false;

        errno = 0;
        char *end_pointer = NULL;
        long converted = strtol(string, &end_pointer, 10);

        if (strlen(end_pointer) != 0) {
                log_message(LOG_WARNING, "%s: invalid or unused characters when converting '%s' to integer %ld: '%s'",
                            __func__, string, converted, end_pointer);
                return false;
        }

        if (errno == 0) {
                *value = (int32_t) converted;
                return true;
        }
        return false;
}


