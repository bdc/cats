// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_grid.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_GRID_H
#define CATS_CATS_GRID_H

#include <gsl/gsl_rng.h>
#include "data/species_parameters.h"
#include "data/cats_datatypes.h"
#include "stats/statistics.h"
#include "defaults.h"

struct cats_configuration;
struct module_grid_data {
        void *module_data;
};
/// @brief simulation grid
struct cats_grid {
        struct cats_configuration *conf;            ///< pointer to global configuration


        /**  @name metadata **/
        void *parent;       ///< pointer to master grid (id 0, start of struct cats_grid array)
        int32_t id;         ///< unique grid identifier, starts at 0


        /** @name spatial information **/
        const struct cats_dimension dimension;


        struct cats_environment *suitability; /**  @name hybrid mode only: suitability information **/

        /**  @name data grids **/
        cats_dt_population *_population;      ///< actual population data
        cats_dt_population **population;      ///< pointers to the beginning of the grid's rows
        cats_dt_seeds **dispersed_seeds;      ///< freshly dispersed seeds                                [row][col]
        cats_dt_seeds ***seed_bank;           ///< seed bank (excluding first year)                       [row][col][year]
        cats_dt_seeds **seeds_produced;       ///< temporary storage for produced seeds (pre-calculated)  [row][col]
        cats_dt_population ***juveniles;      ///< juvenile plants                                        [row][col][year]

#ifdef USEMPI
        float **seeds_0_all;            ///< global seed grid FIXME MPI VERIFY
#endif

        struct module_grid_data grid_modules[MAX_MODULES];


        /** @name species parameters */
        struct cats_dispersal *dispersal;       ///< dispersal information, points to conf->dispersal[id]
        struct cats_species_param param;        ///< parametrisation information, copied from conf->params

        /** @name other */
        struct statistics stats;                ///< statistics for seeds produced etc
        gsl_rng *rng;                               ///< grid specific random number generator
        struct cats_thread_info *single_thread;     ///< grid specific thread structure for single-threaded tasks

};

#endif