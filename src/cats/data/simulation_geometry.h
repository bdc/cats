// SPDX-License-Identifier: GPL-3.0-or-later
//
// simulation_geometry.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_SIMULATION_GEOMETRY_H
#define CATS_SIMULATION_GEOMETRY_H

#include "cats_datatypes.h"

struct simulation_geometry {
        bool gdal_registered;
        struct cats_dimension dimension;
        struct cats_dimension dimension_orig;
        cats_dt_coord row_offset;
        cats_dt_coord col_offset;
        double origin_x;
        double origin_y;
        double cell_size_x;
        double cell_size_y;
        char *projection_string;
        bool use_sub_grid;
        char *file_format;
        bool ignore_raster_projection;

#ifdef USEMPI
        int *displacements;
        int *cells_per_chunk;
        int *rows_per_chunk;
#endif

};

void init_simulation_geometry(struct simulation_geometry *geo);

#endif //CATS_SIMULATION_GEOMETRY_H
