
// SPDX-License-Identifier: GPL-3.0-or-later
//
// simulation_geometry.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <math.h>
#include "simulation_geometry.h"
#include <logging/logging.h>

void init_simulation_geometry(struct simulation_geometry *geo)
{
        struct simulation_geometry empty = {0};
        *geo = empty;

        geo->origin_x = NAN;
        geo->origin_y = NAN;
        geo->gdal_registered = false;
        geo->cell_size_x = -1;
        geo->cell_size_y = -1;

#ifdef USEMPI
        geo->cells_per_chunk = NULL;
        geo->displacements = NULL;
        geo->rows_per_chunk = NULL;
#endif

        geo->projection_string = NULL;
}


void check_raster_dimensions(struct cats_dimension to_check, struct cats_dimension reference)
{
        if (to_check.cols != reference.cols || to_check.rows != reference.rows) {
                log_message(LOG_ERROR, "GDAL gdal_raster dimensions do not match expected dimensions: "
                                       "%dx%d vs %dx%d", to_check.cols, to_check.rows, reference.cols, reference.rows);
                exit_cats(EXIT_FAILURE);
        }
}
