// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlay_exclusion.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_OVERLAY_EXCLUSION_H
#define CATS_OVERLAY_EXCLUSION_H

#include "../../memory/raw_arrays.h"
#include "../../memory/arrays.h"

struct cats_2d_array_char *translate_exclusion(const struct cats_2d_array_double *data, int64_t *excluded_count);

void destroy_excluded_cells_all_grids(const struct cats_configuration *conf, struct cats_grid *grid);

#endif //CATS_OVERLAY_EXCLUSION_H