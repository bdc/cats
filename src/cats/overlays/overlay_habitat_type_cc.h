// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlay_habitat_type_cc.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_OVERLAY_HABITAT_TYPE_CC_H
#define CATS_OVERLAY_HABITAT_TYPE_CC_H

#include <stdint.h>
#include "../../memory/arrays.h"

#define MAX_HABITAT_TYPE_CODES 1024

/// @brief Auxiliary data for habitat (CC) overlays
struct habitat_layer_cc_aux {
        int32_t min_code;
        int32_t max_code;
        double default_value;
        double habitat_multipliers[MAX_HABITAT_TYPE_CODES];
};

void cleanup_habitat_layer_cc_aux(void **data);

struct cats_2d_array_double *translate_habitat(const struct cats_2d_array_double *data, void *aux);

struct habitat_layer_cc_aux *load_habitat_layer_cc_aux(const char *csv_file, double default_value);

#endif //CATS_OVERLAY_HABITAT_TYPE_CC_H
