// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlay_habitat_type_cc.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <assert.h>

#include "overlay_habitat_type_cc.h"
#include "cats_csv/cats_csv.h"
#include "logging.h"
#include "memory/cats_memory.h"


void cleanup_habitat_layer_cc_aux(void **data)
{
        assert(data != NULL);

        struct habitat_layer_cc_aux *to_delete = (struct habitat_layer_cc_aux *) *data;
        for (int32_t i = 0; i < MAX_HABITAT_TYPE_CODES; i++) {
                to_delete->habitat_multipliers[i] = NAN;
        }

        free(to_delete);
        *data = NULL;
}


struct habitat_layer_cc_aux *load_habitat_layer_cc_aux(const char *csv_file, double default_value)
{
        struct cats_csv *csv = csv_from_filename(csv_file, 2);
        struct habitat_layer_cc_aux *result = calloc_or_die(1, sizeof(struct habitat_layer_cc_aux));

        result->default_value = default_value;
        for (int32_t i = 0; i < MAX_HABITAT_TYPE_CODES; i++) {
                result->habitat_multipliers[i] = default_value;
        }


        int32_t key_field_idx = csv_get_field_idx(csv, "habitat_type");
        int32_t value_field_idx = csv_get_field_idx(csv, "cc_multiplier");

        int32_t min_key = INT32_MAX;
        int32_t max_key = INT32_MIN;
        double min_value = DBL_MAX;
        double max_value = -DBL_MAX;

        for (int32_t row = 0; row < csv->data_row_count; row++) {
                int32_t key = csv_get_int32_field_idx(csv, row, key_field_idx);
                if (key < 0 || key >= MAX_HABITAT_TYPE_CODES) continue;
                if (key < min_key) min_key = key;
                if (key > max_key) max_key = key;
                double value = csv_get_double_field_idx(csv, row, value_field_idx);
                if (value < min_value) min_value = value;
                if (value > max_value) max_value = value;
                result->habitat_multipliers[key] = value;

        }

        if (min_key < 0 || max_key >= MAX_HABITAT_TYPE_CODES) {
                log_message(LOG_ERROR, "habitat type out of range [0, %d): found [%d, %d]", MAX_HABITAT_TYPE_CODES,
                            min_key, max_key);
                exit(EXIT_FAILURE);
        }

        if (min_value < 0 || max_value > 1) {
                log_message(LOG_ERROR,
                            "carrying capacity multiplier are expected to be in range [0, 1]: found [%f, %f]",
                            min_value, max_value);
                exit(EXIT_FAILURE);
        }

        result->min_code = min_key;
        result->max_code = max_key;
        log_message(LOG_IMPORTANT, "loaded habitat carrying capacities from %s: [%d, %d]", csv_file, min_key, max_key);
        csv_free(&csv);
        return result;
}


double get_habitat_cc_multiplier(const struct habitat_layer_cc_aux *aux, int32_t habitat_type)
{
        assert(habitat_type >= 0 && habitat_type < MAX_HABITAT_TYPE_CODES);

        if (habitat_type < aux->min_code || habitat_type > aux->max_code) {
                log_message(LOG_ERROR, "habitat type '%d' outside expected range [%d, %d]", habitat_type, aux->min_code,
                            aux->max_code);
                exit(EXIT_FAILURE);
        }
        double result = aux->habitat_multipliers[habitat_type];
        assert(result >= 0.0);
        return result;
}


struct cats_2d_array_double *translate_habitat(const struct cats_2d_array_double *data, void *aux)
{
        assert(aux != NULL);
        struct habitat_layer_cc_aux *habitat_info = aux;
        struct cats_2d_array_double *result = new_2d_array_double(data->dimension);

        const cats_dt_coord rows = data->dimension.rows;
        const cats_dt_coord cols = data->dimension.cols;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {

                        double value = data->data[row][col];

                        if (isnan(value)) {
                                result->data[row][col] = 0.0;
                        } else {
                                int32_t val = (int32_t) round(value);
                                double multi = get_habitat_cc_multiplier(habitat_info, val);
                                result->data[row][col] = multi;
                        }
                }
        }

        return result;
}
