// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlay_resources.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <math.h>
#include "overlay_resources.h"
#include "memory/arrays.h"


struct cats_2d_array_double *translate_resources(const struct cats_2d_array_double *data)
{
        struct cats_2d_array_double *result = new_2d_array_double(data->dimension);

        const cats_dt_coord rows = data->dimension.rows;
        const cats_dt_coord cols = data->dimension.cols;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {

                        double value = data->data[row][col];

                        if (isnan(value)) {
                                result->data[row][col] = 0.0;
                        } else {

                                if (value <0 ) value = 0;
                                result->data[row][col] = value;
                        }
                }
        }

        return result;
}
