// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlays.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_OVERLAYS_H_
#define CATS_OVERLAYS_H_

#include <stdbool.h>
#include <stdint.h>
#include "data/cats_datatypes.h"
#include "../../memory/arrays.h"

struct cats_configuration;
struct cats_grid;

enum overlay_type {
        OL_NONE = 0,
        OL_EXCLUSION = 1,
        OL_HABITAT_TYPE_CC = 2,
        OL_RESOURCE = 3,
        OL_CUSTOM = 4,
        OL_MAX
};


enum ol_exclusion_type {
        OL_EXCLUSION_NOT_EXCLUDED = 0,
        OL_EXCLUSION_EXCLUDED = 1,
        OL_EXCLUSION_NAN = 2,
};

struct cats_overlay {
    bool enabled;              ///< which of the overlay types are enabled?
    bool is_static;            ///< do the overlays remain constant over time?
    int32_t reload_interval;  ///< how often do overlays change?
    int64_t excluded_cells;
    char *name;                ///< overlay names (as specified in the configuration file)
    char *filename;            ///< overlay filenames or filename patterns
    char *filename_aux;
    char *filename_loaded;
    void *aux_data;

    struct cats_2d_array_char *data_char;
    struct cats_2d_array_double *data_double;

    cats_dt_coord cols; ///< number of rows    (same as grid, for convenience only)
    cats_dt_coord rows; ///< number of columns (same as grid, for convenience only)

};

/**
 @brief Contains information for all overlays
 For each overlay type a number of properties (including whether it is enabled) is stored in arrays of size \ref overlay_type.OL_MAX
 */
struct cats_overlays {

        bool have_overlays;                ///< do we actually have overlays? True if at least one overlay is enabled.
        struct cats_overlay overlay[OL_MAX];

        struct cats_2d_array_char *exclusion;
        struct cats_2d_array_double *habitat_cc;
        struct cats_2d_array_double *resources;

        struct cast_overlay *custom_overlays;
        int32_t custom_overlay_count;

        struct string_array *registered_custom_overlay_names;

        cats_dt_coord cols; ///< number of rows    (same as grid, for convenience only)
        cats_dt_coord rows; ///< number of columns (same as grid, for convenience only)
};


void cleanup_overlays(struct cats_overlays *overlays);

const char *get_overlay_name(enum overlay_type type);

enum overlay_type get_overlay_type_from_name(const char *name, const struct string_array *registered_names);


bool
update_overlay_if_needed(struct cats_configuration *conf, int32_t time, enum overlay_type type);

void reset_overlay(struct cats_overlays *overlay);

void load_overlay_from_file(struct cats_configuration *conf, enum overlay_type type, char *filename);
struct cats_overlay *add_overlay(struct cats_configuration *conf, const char *name);
#endif