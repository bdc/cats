// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlay_exclusion.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>

#include "data/cats_grid.h"
#include "overlays.h"
#include "overlay_exclusion.h"

#include "logging.h"
#include "grids/cats_grid.h"
#include <memory/cats_memory.h>
#include "populations/population.h"
#include "plants/plant_structures.h"
#include "inline_overlays.h"
#include "inline_population.h"


int64_t get_non_excluded_cell_count(struct cats_configuration *conf, struct cats_grid *grid)
{
        int64_t all_count = raster_cell_count(grid->dimension);
        if (conf->overlays.have_overlays && conf->overlays.overlay[OL_EXCLUSION].enabled) {
                all_count -= conf->overlays.overlay[OL_EXCLUSION].excluded_cells;
        }

        return all_count;
}


void destroy_excluded_cells(const struct cats_configuration *conf, struct cats_grid *grid)
{
        int64_t destroyed_cell_count = 0;

        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        if (cell_excluded_by_overlay(conf, row, col) == false) continue;

                        cats_dt_population old_population = get_adult_population(grid, row, col);

                        if (grid->param.default_demographics) {

                                set_population_ignore_cc(grid, row, col, 0); // destroy_excluded_cells
                                destroy_plant_cell(grid, row, col);
                                if (old_population) destroyed_cell_count += 1;
                        } else {
                                bool destroyed = false;
                                for (int32_t i = 0; i < conf->modules.count; i++) {
                                        cats_cell_function destroy = grid->param.module_data[i].cell_destroyed_action;
                                        if (destroy != NULL) {
                                                destroy(grid->conf, grid, row, col, 0);
                                                destroyed = true;
                                        }
                                }
                                if (old_population && destroyed) destroyed_cell_count += 1;
                        }

                }
        }


        log_message(LOG_INFO, "Removed excluded cell for grid %d (%s): %ld cells removed", grid->id, grid->param.species_name,
                    destroyed_cell_count);
}


void destroy_excluded_cells_all_grids(const struct cats_configuration *conf, struct cats_grid *grid)
{
        if (grid->id != 0) return;

        const int32_t grid_count = conf->grid_count;
        struct cats_grid **parent = grid->parent;

        for (int32_t i = 0; i < grid_count; i++) {
                struct cats_grid *g = parent[i];
                destroy_excluded_cells(conf, g);
        }
}


struct cats_2d_array_char *translate_exclusion(const struct cats_2d_array_double *data, int64_t *excluded_count)
{
        struct cats_2d_array_char *result = new_2d_array_char(data->dimension);

        const cats_dt_coord rows = data->dimension.rows;
        const cats_dt_coord cols = data->dimension.cols;
        int64_t count = 0;
        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {

                        const int32_t val = (int32_t) round(data->data[row][col]);

                        if (val == 0) {
                                result->data[row][col] = OL_EXCLUSION_NOT_EXCLUDED;
                        } else if (isnan(data->data[row][col])) {
                                result->data[row][col] = OL_EXCLUSION_NAN;
                                count += 1;
                        } else {
                                result->data[row][col] = OL_EXCLUSION_EXCLUDED;
                                count += 1;
                        }
                }
        }

        *excluded_count = count;
        return result;
}
