// SPDX-License-Identifier: GPL-3.0-or-later
//
// overlays.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdlib.h>

#include <assert.h>
#include "configuration/configuration.h"
#include "logging.h"
#include "memory.h"
#include "overlays.h"
#include "grids/gdal_load.h"
#include "grids/cats_grid.h"
#include "overlay_habitat_type_cc.h"
#include "overlay_exclusion.h"
#include "overlay_resources.h"
#include "paths/path_patterns.h"

const char *get_overlay_name(enum overlay_type type)
{
        switch (type) {
                case OL_EXCLUSION:
                        return "exclusion";
                case OL_HABITAT_TYPE_CC:
                        return "habitat_type_cc";
                case OL_RESOURCE:
                        return "resources";
                        break;
                case OL_NONE:
                case OL_MAX:
                case OL_CUSTOM:
                        break;
        }

        return "unknown overlay type";
}


void cleanup_overlays(struct cats_overlays *overlays)
{
        if (overlays->exclusion) free_cats_grid(&overlays->exclusion);
        if (overlays->habitat_cc) free_cats_grid(&overlays->habitat_cc);
        if (overlays->overlay[OL_HABITAT_TYPE_CC].aux_data) {
                cleanup_habitat_layer_cc_aux(&overlays->overlay[OL_HABITAT_TYPE_CC].aux_data);
        }

        for (int32_t i = 0; i < OL_MAX; i++) {
                free(overlays->overlay[i].filename);
                overlays->overlay[i].filename = NULL;
                free(overlays->overlay[i].filename_aux);
                overlays->overlay[i].filename_aux = NULL;
                free(overlays->overlay[i].filename_loaded);
                overlays->overlay[i].filename_loaded = NULL;
                free(overlays->overlay[i].name);
                overlays->overlay[i].name = NULL;
        }

}

struct cats_overlay *add_overlay(struct cats_configuration *conf, const char *name)
{
        struct cats_overlay *overlay = NULL;
        abort_implemented(__func__);
        return overlay;

}


enum overlay_type get_overlay_type_from_name(const char *name, const struct string_array *registered_names)
{
        if (!name) return OL_NONE;

        if (!strcmp(name, "exclusion")) return OL_EXCLUSION;
        if (!strcmp(name, "habitat carrying capacity")) return OL_HABITAT_TYPE_CC;
        if (!strcmp(name, "resources")) return OL_RESOURCE;

        if (registered_names != NULL) {
                for (int32_t i = 0; i < registered_names->count; i++) {
                        if (!strcmp(name, registered_names->string[i])) {
                                return OL_CUSTOM;
                        }
                }
        }

        log_message(LOG_ERROR, "%s: unknown overlay type '%s'", __func__, name);
        exit(EXIT_FAILURE);
}


void reset_overlay(struct cats_overlays *overlay)
{
        overlay->habitat_cc = NULL;
        overlay->have_overlays = false;
        overlay->exclusion = NULL;
        overlay->cols = 0;
        overlay->rows = 0;


        for (int32_t i = 0; i < OL_MAX; i++) {
                overlay->overlay[i].enabled = false;
                overlay->overlay[i].excluded_cells = 0;

                overlay->overlay[i].is_static = true;
                overlay->overlay[i].reload_interval = 0;
                overlay->overlay[i].filename = NULL;
                overlay->overlay[i].filename_aux = NULL;
                overlay->overlay[i].filename_loaded = NULL;
                overlay->overlay[i].aux_data = NULL;
                overlay->overlay[i].name = NULL;
        }

}


void load_overlay_from_file(struct cats_configuration *conf, enum overlay_type type, char *filename)
{
        assert(conf != NULL);
        if (type <= OL_NONE || type >= OL_MAX) {
                log_message(LOG_ERROR, "overlay type %d out of range", type);
        }
        log_message(LOG_INFO, "gdal: loading grid mask %s", filename);
        char *filename_substituted = filename_pattern_substitution(filename, conf, conf->time.year_current);

        struct cats_2d_array_double *raw_values = get_double_values_from_gdal(&conf->geometry, filename_substituted, false, false);

        check_raster_dimensions(raw_values->dimension, conf->geometry.dimension);

        if (conf->overlays.overlay[type].filename_loaded) free(conf->overlays.overlay[type].filename_loaded);
        conf->overlays.overlay[type].filename_loaded = strdup(filename);

        switch (type) {
                case OL_EXCLUSION:
                        if (conf->overlays.exclusion) free_cats_grid(&conf->overlays.exclusion);
                        conf->overlays.exclusion = translate_exclusion(raw_values, &conf->overlays.overlay[type].excluded_cells);
                        break;
                case OL_HABITAT_TYPE_CC:
                        if (conf->overlays.habitat_cc) free_cats_grid(&conf->overlays.habitat_cc);
                        conf->overlays.habitat_cc = translate_habitat(raw_values, conf->overlays.overlay[type].aux_data);
                        break;
                case OL_CUSTOM:
                        log_message(LOG_ERROR, "OL_CUSTOM not implemented yet");
                        exit(EXIT_FAILURE);
                case OL_RESOURCE:
                        if (conf->overlays.resources) free_cats_grid(&conf->overlays.resources);
                        conf->overlays.resources = translate_resources(raw_values);
                        break;

                case OL_NONE:
                case OL_MAX:
                        break;

        }
        free(filename_substituted);
        free_cats_grid(&raw_values);
}
