// SPDX-License-Identifier: GPL-3.0-or-later
//
// misc.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_MISC_H
#define CATS_MISC_H

#include <string.h>
#include "data/error.h"
#define TEXT_DIVIDER "=================================================================================================="

const char *cats_version(void);


// this is kept as a macro, so we can print calling function
#define ENSURE_FILE_OPENED(FILE_HANDLE, FILENAME)\
if (! (FILE_HANDLE))\
{\
        fprintf(stderr, "ERROR: Couldn't open file %s in %s: %d (%s)\n", FILENAME, __FILE__, __LINE__, __func__);\
        exit(E_UNREADABLE_FILE);\
}


void set_program_name(const char *name);

#endif