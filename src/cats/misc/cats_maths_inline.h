// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_maths_inline.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_MATHS_INLINE_H
#define CATS_CATS_MATHS_INLINE_H

#include <stdint.h>
#include <math.h>
#include "data/cats_datatypes.h"


static inline int64_t max_int64t(int64_t a, int64_t b)
{
        return a > b ? a : b;
}


static inline int64_t min_int64t(int64_t a, int64_t b)
{
        return a < b ? a : b;
}


static inline int32_t max_int32t(int32_t a, int32_t b)
{
        return a > b ? a : b;
}


static inline int32_t min_int32t(int32_t a, int32_t b)
{
        return a < b ? a : b;
}


static inline cats_dt_population max_population_t(cats_dt_population a, cats_dt_population b)
{
        return a > b ? a : b;
}


static inline cats_dt_population min_population_t(cats_dt_population a, cats_dt_population b)
{
        return a < b ? a : b;
}


static inline float min_float(float a, float b)
{
        return a < b ? a : b;
}


static inline float max_float(float a, float b)
{
        return a > b ? a : b;
}


static inline float min_seeds(cats_dt_seeds a, cats_dt_seeds b)
{
        return a < b ? a : b;
}


static inline float max_seeds(cats_dt_seeds a, cats_dt_seeds b)
{
        return a > b ? a : b;
}


static inline cats_dt_rates min_rates(cats_dt_rates a, cats_dt_rates b)
{
        return a < b ? a : b;
}


static inline cats_dt_rates max_rates(cats_dt_rates a, cats_dt_rates b)
{
        return a > b ? a : b;
}


static inline long double min_long_double(long double a, long double b)
{
        return a < b ? a : b;
}


static inline long double max_long_double(long double a, long double b)
{
        return a > b ? a : b;
}


static inline void nan_to_zero(double *data)
{
        if (isnan(*data)) *data = 0.0;
}


static float save_division_nan(float value, float divisor)
{
        if (divisor == 0.0f) return NAN;
        return value / (float) divisor;
}


#endif //CATS_CATS_MATHS_INLINE_H
