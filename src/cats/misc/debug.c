// SPDX-License-Identifier: GPL-3.0-or-later
//
// debug.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "data/cats_grid.h"
#include "misc.h"
#include "inline.h"
#include <memory/cats_memory.h>


void
debug_juveniles(const struct cats_grid *grid, const struct cats_configuration *conf, cats_dt_coord row,
                cats_dt_coord col, const char *action)
{
        if (row == cats_debug.misc_debug_coords.row && col == cats_debug.misc_debug_coords.col) {
                fprintf(cats_debug.misc_debug_file, "%d,%d,%s,juveniles", grid->id, conf->time.year_current, action);

                const int32_t max_age_of_maturity = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);

                for (int32_t i = 0; i < max_age_of_maturity + 1; i++) {
                        if (grid->juveniles[row][col]) {
                                fprintf(cats_debug.misc_debug_file, ",%d", grid->juveniles[row][col][i]);
                        } else {
                                fprintf(cats_debug.misc_debug_file, ",%d", 0);
                        }
                }

                fprintf(cats_debug.misc_debug_file, "\n");
        }
}


void debug_seeds(struct cats_grid *grid, const struct cats_configuration *conf, cats_dt_coord row, cats_dt_coord col,
                 char *action)
{
        if (row == cats_debug.misc_debug_coords.row && col == cats_debug.misc_debug_coords.col) {
                fprintf(cats_debug.misc_debug_file, "%d,%d,%s,seeds,%f", grid->id, conf->time.year_current, action,
                        grid->dispersed_seeds[row][col]);
                const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);
                for (int32_t i = 0; i < seed_persistence; i++) {
                        if (grid->seed_bank[row][col]) {
                                fprintf(cats_debug.misc_debug_file, ",%f", grid->seed_bank[row][col][i]);
                        } else {
                                fprintf(cats_debug.misc_debug_file, ",%d", 0);
                        }
                }

                fprintf(cats_debug.misc_debug_file, "\n");
        }
}


///@ opens the debug_cell file handle and prints a header (single cell debugging)
void start_single_cell_debugging(char **argv)
{
        assert(argv != NULL);

        const char *f = "debug-%s.csv";
        int count = snprintf(NULL, 0, f, argv[1]) + 1;
        char *debug_file_name = malloc_or_die(count);
        snprintf(debug_file_name, count, f, argv[1]);

        cats_debug.misc_debug_file = fopen(debug_file_name, "w");
        cats_debug.misc_debug_coords.row = 0;
        cats_debug.misc_debug_coords.col = 0;
        ENSURE_FILE_OPENED(cats_debug.misc_debug_file, debug_file_name)

        fprintf(cats_debug.misc_debug_file, "grid,year,action,type,values\n");
        free(debug_file_name);
}

