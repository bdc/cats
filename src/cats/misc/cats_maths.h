// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_maths.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_MATHS_H_
#define CATS_MATHS_H_

#include "data/cats_datatypes.h"
#include <gsl/gsl_rng.h>

/// @brief Simple vector data structure of type double
struct cats_vector {
        double *data;
        int32_t length;
};

/// @brief Simple matrix data structure of type double
struct cats_matrix {
        double **data;
        int32_t rows;
        int32_t cols;
};

struct cats_vector *matrix_times_vector(struct cats_matrix *matrix, struct cats_vector *in_vector);

struct cats_matrix *create_matrix(int rows, int cols);

struct cats_vector *create_vector(int32_t length);

void delete_vector(struct cats_vector **vector);

void delete_matrix(struct cats_matrix **matrix);

#endif
