// SPDX-License-Identifier: GPL-3.0-or-later
//
// output.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include <string.h>
#include <stdint.h>


#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "grids/gdal_load.h"
#include "grids/gdal_save.h"


void
save_1d_grid(double *src, struct cats_dimension dimension, char *filename, struct cats_configuration *conf)
{

        struct grid_wrapper data = gridwrapper_1d(src, GDT_Float64, dimension);
        save_grid_to_gdal(&data, GDT_Float64, conf, filename, NULL);
}


void save_2d_grid(float **src, struct cats_dimension dimension, char *filename, struct cats_configuration *conf,
                  struct cats_dimension *offset)
{
        struct grid_wrapper data = gridwrapper(src, dimension);
        if (offset != NULL) {
                data.offset = *offset;
        }
        save_grid_to_gdal(&data, GDT_Float32, conf, filename, NULL);
}

