// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_random.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_RANDOM_H
#define CATS_CATS_RANDOM_H


#include <stdbool.h>
#include <gsl/gsl_rng.h>
#include "data/cats_datatypes.h"

unsigned int get_random_seed(bool show);

gsl_rng *allocate_rng(void);

int32_t poisson(const gsl_rng *rng, cats_dt_rates value);

cats_dt_population poisson_population_capped(const gsl_rng *rng, cats_dt_rates value, cats_dt_population cap);

cats_dt_seeds poisson_seeds(const gsl_rng *rng, cats_dt_rates value);

cats_dt_seeds poisson_seeds_undampened(const gsl_rng *rng, cats_dt_rates value);

cats_dt_seeds poisson_seeds_capped(const gsl_rng *rng, cats_dt_rates value, cats_dt_seeds cap);


uint64_t get_random_seed64(bool show);
#endif //CATS_CATS_RANDOM_H
