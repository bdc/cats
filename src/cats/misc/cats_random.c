// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_random.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include <math.h>
#include <gsl/gsl_randist.h>
#include "logging.h"
#include "cats_random.h"
#include "data/cats_datatypes.h"
#include "inline.h"


unsigned int get_random_seed(bool show)
{
        unsigned int seed;
        struct timeval tv;
#ifdef CATS_ON_WINDOWS
        get_time(&tv);
        seed = tv.tv_sec + tv.tv_usec;
#else
        const char *device = "/dev/urandom";
        FILE *random = fopen(device, "r");

        if (random == NULL) {
                get_time(&tv);
                seed = tv.tv_sec + tv.tv_usec;
        } else {
                size_t read = fread(&seed, sizeof(seed), 1, random);
                if (read <= 0) {
                        log_message(LOG_ERROR, "could not read from %s, using time instead", device);
                        get_time(&tv);
                        seed = tv.tv_sec + tv.tv_usec;
                }

                fclose(random);

                if (show) log_message(LOG_IMPORTANT, "using seed from %s: %d", device, seed);
        }
#endif
        if (show) log_message(LOG_INFO, "random seed value: %d", seed);

        return seed;
}


uint64_t get_random_seed64(bool show)
{
        uint64_t seed;
        struct timeval tv;
#ifdef CATS_ON_WINDOWS
        get_time(&tv);
        seed = tv.tv_sec + tv.tv_usec;
#else
        const char *device = "/dev/urandom";
        FILE *random = fopen(device, "r");

        if (random == NULL) {
                get_time(&tv);
                seed = tv.tv_sec + tv.tv_usec;
        } else {
                size_t read = fread(&seed, sizeof(seed), 1, random);
                if (read <= 0) {
                        log_message(LOG_ERROR, "could not read from %s, using time instead", device);
                        get_time(&tv);
                        seed = tv.tv_sec + tv.tv_usec;
                }

                fclose(random);

                if (show) log_message(LOG_IMPORTANT, "using seed from %s: %"PRId64, device, seed);
        }
#endif
        if (show) log_message(LOG_INFO, "random seed value: %"PRId64, seed);

        return seed;
}


gsl_rng *allocate_rng(void)
{
        gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
        assert(r != NULL);
        gsl_rng_set(r, get_random_seed(false));
        return r;
}


int32_t poisson_undampened(const gsl_rng *rng, cats_dt_rates value)
{
        assert(rng != NULL);
        assert(isnan(value) || value >= 0.0);
        if (value == 0.0) return 0;
        if (isnan(value)) return 0;

        cats_dt_rates result;

        if (value > CATS_MAX_POPULATION) {

                log_message(LOG_WARNING, "poisson called on %f > %d",
                            (double) value, CATS_MAX_POPULATION);
                abort();
        }
        if (value <= global.poisson_maximum) {
                result = gsl_ran_poisson(rng, (double) value);
        } else {
                if (global.poisson_maximum_draw_diff == true) {
                        result = gsl_ran_poisson(rng, (double) global.poisson_maximum) +
                                 roundl(value - global.poisson_maximum);
                } else {
                        result = roundl(value);
                }
        }


        if (result > CATS_MAX_POPULATION) {
                log_message(LOG_WARNING, "poisson called on %f resulted in %f > %d, returning %d",
                            (double) value, (double) result, CATS_MAX_POPULATION, CATS_MAX_POPULATION);

                return CATS_MAX_POPULATION;
        }
        int32_t final = (int32_t) result;
        assert(final >= 0);
        return final;
}


int32_t poisson(const gsl_rng *rng, cats_dt_rates value)
{
        assert(rng != NULL);
        assert(isnan(value) || value >= 0.0);
        cats_dt_rates dampening = global.poisson_dampening_factor;
        assert(dampening >= 0.0 && dampening <= 1.0);


        if (value == 0.0) return 0;
        if (isnan(value)) return 0;

        if (value > CATS_MAX_POPULATION) {

                log_message(LOG_WARNING, "poisson called on %f > %d", (double) value, CATS_MAX_POPULATION);
                abort();
        }

        cats_dt_rates random_part = value;
        cats_dt_rates stable_part = 0.0;

        if (dampening > 0 && roundl(dampening * value) > global.poisson_dampening_minimum) {
                random_part = (1.0 - dampening) * value;
                stable_part = dampening * value;
        }
        cats_dt_rates result;

        if (random_part <= global.poisson_maximum) {
                result = gsl_ran_poisson(rng, (double) random_part) + roundl(stable_part);
        } else {
                if (global.poisson_maximum_draw_diff == true) {
                        result = gsl_ran_poisson(rng, (double) global.poisson_maximum) +
                                 roundl(value - global.poisson_maximum);
                } else {
                        result = roundl(value);
                }
        }

        if (result > CATS_MAX_POPULATION) {
                log_message(LOG_WARNING, "poisson result %f resulted in %f > %d, returning %d",
                            (double) value, (double) result, CATS_MAX_POPULATION, CATS_MAX_POPULATION);
                return CATS_MAX_POPULATION;
        }
        int32_t final = (int32_t) result;
        assert(final >= 0);
        return final;
}


cats_dt_population
poisson_population_capped(const gsl_rng *rng, cats_dt_rates value, cats_dt_population cap)
{
        assert(cap >= 0);
        cats_dt_population p = poisson(rng, value);
        return min_population_t(p, cap);

}


cats_dt_seeds poisson_seeds_undampened(const gsl_rng *rng, cats_dt_rates value)
{
        assert(rng != NULL);
        assert(isnan(value) || value >= 0.0);
        if (value == 0.0) return 0.0f;
        if (isnan(value)) return 0.0f;
        cats_dt_rates result;

        if (value > CATS_MAX_SEEDS) {
                log_message(LOG_WARNING, "poisson called on %f > %f", (double) value, CATS_MAX_SEEDS);
                abort();
        }

        if (value <= global.poisson_maximum) {
                result = gsl_ran_poisson(rng, (double) value);
        } else {
                if (global.poisson_maximum_draw_diff == true) {
                        result = gsl_ran_poisson(rng, (double) global.poisson_maximum) +
                                 roundl(value - global.poisson_maximum);
                } else {
                        result = roundl(value);
                }
        }

        if (result > CATS_MAX_SEEDS) {
                log_message(LOG_WARNING, "poisson called on %f resulted in %f > %f, returning %f",
                            (double) value, (double) result, CATS_MAX_SEEDS, CATS_MAX_SEEDS);
                return CATS_MAX_SEEDS;
        }
        cats_dt_seeds final = (cats_dt_seeds) result;
        assert(final >= 0);
        return final;
}


inline cats_dt_seeds poisson_seeds(const gsl_rng *rng, cats_dt_rates value)
{
        assert(rng != NULL);
        cats_dt_rates dampening = global.poisson_dampening_factor;
        assert(dampening >= 0.0 && dampening <= 1.0);
        assert(isnan(value) || value >= 0.0);
        if (value == 0.0) return 0.0f;
        if (isnan(value)) return 0.0f;

        cats_dt_rates random_part = value;
        cats_dt_rates stable_part = 0.0;

        if (dampening > 0 && roundl(dampening * value) > global.poisson_dampening_minimum) {
                random_part = (1.0 - dampening) * value;
                stable_part = dampening * value;
        }

        cats_dt_rates result;

        if (random_part <= global.poisson_maximum) {
                result = gsl_ran_poisson(rng, (double) random_part) + roundl(stable_part);
        } else {
                if (global.poisson_maximum_draw_diff == true) {
                        result = gsl_ran_poisson(rng, (double) global.poisson_maximum) +
                                 roundl(value - global.poisson_maximum);
                } else {
                        result = roundl(value);
                }
        }

        if (result > CATS_MAX_SEEDS) {
                log_message(LOG_WARNING, "poisson called on %f resulted in %f > %f, returning %f",
                            (double) value, (double) result, CATS_MAX_SEEDS, CATS_MAX_SEEDS);
                return CATS_MAX_SEEDS;
        }

        cats_dt_seeds final = (cats_dt_seeds) result;
        assert(final >= 0);
        return final;
}


cats_dt_seeds poisson_seeds_capped(const gsl_rng *rng, cats_dt_rates value, cats_dt_seeds cap)
{
        assert(cap >= 0);
        cats_dt_seeds p = poisson_seeds(rng, value);
        return min_seeds(p, cap);
}
