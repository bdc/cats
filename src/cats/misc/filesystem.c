// SPDX-License-Identifier: GPL-3.0-or-later
//
// filesystem.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <logging/logging.h>
#include "filesystem.h"
#include "data/error.h"
#ifndef CATS_ON_WINDOWS
#include <unistd.h>
#else
#include <direct.h>
#endif


bool dir_exists(const char *name)
{
        struct stat info;
        int rc = stat(name, &info);
        if (rc == 0 && info.st_mode & S_IFDIR) return true;
        if (rc == 0) {
                fprintf(stderr, "Path %s is not a directory. Exiting.\n", name);
                log_message(LOG_ERROR, "Path %s is not a directory. Exiting.", name);

                exit(E_DIRECTORY_ERROR);
        }
        fprintf(stderr, "Directory '%s' does not exist\n", name);
        log_message(LOG_IMPORTANT, "\tdirectory '%s' does not exist", name);
        return false;
}


bool dir_writeable(const char *name)
{
        int rc = access(name, F_OK);
        if (rc == 0) {
                log_message(LOG_INFO, "\tdirectory %s exists and can be written to.", name);
                return true;
        }

        fprintf(stderr, "directory %s exists but cannot be accessed or written to.\n", name);
        log_message(LOG_ERROR, "\tdirectory %s exists but cannot be accessed or written to.", name);
        return false;
}


void make_directory(const char *path)
{
#ifndef CATS_ON_WINDOWS
        int rc = mkdir(path, S_IRWXU);
#else
        int rc = _mkdir(path);
#endif
        if (rc == 0) return;

        int error = errno;
        fprintf(stderr, "unable to create directory %s: %s\n", path, strerror(error));
        log_message(LOG_ERROR, "\tunable to create directory %s: %s", path, strerror(error));
        exit(E_DIRECTORY_ERROR);
}
