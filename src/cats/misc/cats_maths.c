// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_maths.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>

#include "cats_maths.h"
#include "../../memory/cats_memory.h"
#include "../../memory/raw_arrays.h"
#include "logging.h"


struct cats_vector *matrix_times_vector(struct cats_matrix *matrix, struct cats_vector *in_vector)
{
        if (matrix->cols != in_vector->length) {
                log_message(LOG_ERROR, "matrix cols (%d) do not equal vector length (%d)",
                            matrix->cols, in_vector->length);
                exit(EXIT_FAILURE);
        }

        struct cats_vector *result = create_vector(matrix->rows);

        for (int r = 0; r < matrix->rows; r++) {
                result->data[r] = 0.0f;

                for (int c = 0; c < matrix->cols; c++) {
                        result->data[r] += matrix->data[r][c] * in_vector->data[c];
                }
        }

        return result;
}


struct cats_vector *create_vector(int32_t length)
{
        assert(length > 0);
        struct cats_vector *vector = malloc_or_die(sizeof(struct cats_vector));
        vector->data = calloc_or_die(length, sizeof(double));
        vector->length = length;

        for (int i = 0; i < length; i++) {
                vector->data[i] = 0.0;
        }

        return vector;
}


struct cats_matrix *create_matrix(int rows, int cols)
{
        assert(rows > 0);
        assert(cols > 0);

        struct cats_matrix *matrix = malloc_or_die(sizeof(struct cats_matrix));
        matrix->cols = cols;
        matrix->rows = rows;
        matrix->data = (double **) new_raw_2d_array(rows, cols, sizeof(double));

        return matrix;
}


void delete_vector(struct cats_vector **vector)
{
        free((*vector)->data);
        (*vector)->length = -1;
        free(*vector);
        *vector = NULL;
}


void delete_matrix(struct cats_matrix **matrix)
{
        if (matrix == NULL || *matrix == NULL) return;

        struct cats_matrix *local = *matrix;
        free_grid(&local->data, local->rows);

        free(local->data);
        local->data = NULL;
        local->rows = -1;
        local->cols = -1;
        free(*matrix);
        *matrix = NULL;
}
