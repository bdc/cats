// SPDX-License-Identifier: GPL-3.0-or-later
//
// years.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_YEARS_H
#define CATS_YEARS_H

#include "configuration/configuration.h"
#include "simulation_time.h"

int get_current_year(const struct simulation_time *time);

int get_phase_year_abs(const struct cats_configuration *conf);

void run_phase(struct cats_grid **grids, struct cats_configuration *conf, enum simulation_phase phase);

bool is_output_year(const struct simulation_time *time);

#endif //CATS_YEARS_H
