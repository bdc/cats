// SPDX-License-Identifier: GPL-3.0-or-later
//
// simulation_time.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_SIMULATION_TIME_H
#define CATS_SIMULATION_TIME_H

#include <stdint.h>
#include <stdbool.h>

#define MAX_PHASES 5
struct cats_configuration;

/// @brief simulation phases
enum simulation_phase {
        PHASE_BURN_IN = 1,     ///< simulation phase burn-in
        PHASE_WARM_UP = 2,     ///< simulation phase warm-up - bridging burn-in and main simulation
        PHASE_SIMULATION = 3, ///< main simulation phase
        PHASE_END = 4,
};

enum cats_simulation_stages {
        RUN_SCALE = 1U << 0U,
        RUN_BURN_IN = 1U << 1U,
        RUN_WARM_UP = 1U << 2U,
        RUN_SIM = 1U << 3U
};

struct simulation_time {
        int32_t year_start;             /* start year */
        int32_t year_current;           /* current year */
        int32_t simulation_length;      /* number of years for the simulation phase */
        int32_t burn_in_phase_length;
        int32_t burn_in_phase_year;
        int32_t warm_up_phase_year;
        bool always_show_sign;
        int32_t zero_pad_to_digits;

        bool phase_exists[MAX_PHASES];
        bool phase_done[MAX_PHASES];
        bool initial_cg_skipped;
        enum simulation_phase phase;
        char *phase_name;
        int32_t output_interval;
        char *format_string;

};

void simulation_time_init(struct simulation_time *time);

int32_t setup_current_time_step(struct cats_configuration *conf, int32_t time_step);

void setup_simulation_time(struct cats_configuration *conf, struct simulation_time *time);


#endif //CATS_SIMULATION_TIME_H
