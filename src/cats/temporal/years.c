// SPDX-License-Identifier: GPL-3.0-or-later
//
// years.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <string.h>
#include "logging.h"
#include "years.h"
#include "grids/cats_grid.h"
#include "actions/cats_actions.h"
#include "environment/environment_registry.h"
#include "temporal/phase_names.h"


int get_current_year(const struct simulation_time *time)
{
        switch (time->phase) {
                case PHASE_BURN_IN:
                        return time->year_current - time->burn_in_phase_year;
                default:
                        return time->year_current;
        }
}


int get_phase_year_abs(const struct cats_configuration *conf)
{
        switch (conf->time.phase) {
                case PHASE_BURN_IN:
                        return conf->time.burn_in_phase_year;
                default:
                        return conf->time.year_current;
        }
}


void get_phase_start_and_end_year(struct cats_configuration *conf, enum simulation_phase phase, int *start_year,
                                  int *end_year)
{
        switch (phase) {
                case PHASE_BURN_IN:
                        if (conf->time.burn_in_phase_year == 0 && conf->time.year_start > 0) {
                                conf->time.burn_in_phase_year = conf->time.year_start;
                        }

                        *start_year = 0 + conf->time.burn_in_phase_year;
                        *end_year = conf->time.burn_in_phase_length + conf->time.burn_in_phase_year;
                        break;

                case PHASE_WARM_UP:
                        *start_year = conf->time.warm_up_phase_year;
                        *end_year = conf->time.year_start;
                        break;

                case PHASE_SIMULATION:
                        *start_year = conf->time.year_start;
                        *end_year = conf->time.year_start + conf->time.simulation_length + 1;
                        break;

                default:
                        log_message(LOG_ERROR, "requested phase out ouf bounds");
                        break;
        }
}


void run_phase(struct cats_grid **grids, struct cats_configuration *conf, enum simulation_phase phase)
{
        if (conf->time.phase_exists[phase] == false) {

                log_message(LOG_INFO, "skipping phase %s", get_phase_name(phase));
                return;
        }

        int32_t start_year = 0, end_year = 0;

        conf->time.phase = phase;
        conf->time.phase_name = strdup(get_phase_name(phase));

        log_message(LOG_INFO, "starting phase %s", conf->time.phase_name);

        get_phase_start_and_end_year(conf, phase, &start_year, &end_year);

        for (int32_t time_step = start_year; time_step < end_year; time_step++) {
                run_simulation_phase(grids, conf, time_step);
        }

        // end of phase: unload all suitability
        unload_environment_phase_end(&conf->environment_registry);
        conf->time.phase_done[phase] = true;
}


/*! \brief checks whether output should be written for the current time step
 *
 * @return whether output should be written for the current time step
 * @param time simulation time information
 */
bool is_output_year(const struct simulation_time *time)
{
        if (time->phase == PHASE_BURN_IN) return false;
        if (time->output_interval <= 0) return false;
        if ((time->year_current - time->year_start) % time->output_interval == 0
            && (time->phase == PHASE_SIMULATION || time->phase == PHASE_WARM_UP)) {
                return true;
        }
        return false;
}
