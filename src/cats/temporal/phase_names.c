// SPDX-License-Identifier: GPL-3.0-or-later
//
// phase_names.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "logging.h"
#include "phase_names.h"


const char *get_phase_name(enum simulation_phase phase)
{
        char *phase_name[] = {"unknown",
                              "burnin",
                              "warmup",
                              "year  "};

        if (phase > 0) return phase_name[phase];
        log_message(LOG_ERROR, "requested phase out ouf bounds");
        return "error ";
}


const char *get_phase_shortname(enum simulation_phase phase)
{
        char *phase_name[] = {"U", "B", "W", "S"};
        if (phase > 0) return phase_name[phase];
        log_message(LOG_ERROR, "requested phase out ouf bounds");
        return "E";
}
