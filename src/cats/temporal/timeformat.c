// SPDX-License-Identifier: GPL-3.0-or-later
//
// timeformat.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>

#include "timeformat.h"
#include "configuration/configuration.h"
#include "memory/cats_memory.h"
#include "simulation_time.h"


void set_time_format_string(struct cats_configuration *config)
{
        if (config->time.format_string) free(config->time.format_string);

        char *sign_string = "";
        char *show_sign_string = "+";

        if (config->time.always_show_sign) {
                sign_string = show_sign_string;
        }


        char *format;
        if (config->time.zero_pad_to_digits) {
                format = "%%%s0%dd";
                int count = snprintf(NULL, 0, format, sign_string, config->time.zero_pad_to_digits) + 1;
                config->time.format_string = malloc_or_die(count);
                snprintf(config->time.format_string, count, format, sign_string, config->time.zero_pad_to_digits);

        } else {
                format = "%%%sd";
                int count = snprintf(NULL, 0, format, sign_string) + 1;
                config->time.format_string = malloc_or_die(count);
                snprintf(config->time.format_string, count, format, sign_string);
        }
}


char *formatted_year(const struct simulation_time *time, int32_t year)
{
        char *result = NULL;

        assert(time->format_string);

        int count = snprintf(NULL, 0, time->format_string, year) + 1;
        result = malloc_or_die(count);
        snprintf(result, count, time->format_string, year);

        return result;
}
