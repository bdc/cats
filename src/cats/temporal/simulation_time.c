// SPDX-License-Identifier: GPL-3.0-or-later
//
// simulation_time.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//



#include <stddef.h>
#include <assert.h>
#include "simulation_time.h"
#include "configuration/configuration.h"
#include "timeformat.h"


int32_t setup_current_time_step(struct cats_configuration *conf, int32_t time_step)
{
        conf->time.year_current = time_step;
        int32_t time = conf->time.year_current;

        if (conf->time.phase == PHASE_BURN_IN) {
                time -= conf->time.burn_in_phase_year;
        }

        return time;
}


void setup_simulation_time(struct cats_configuration *conf, struct simulation_time *time)
{
        assert(time != NULL);

        time->phase_exists[PHASE_SIMULATION] = true;

        if (time->burn_in_phase_length > 0) {
                time->phase_exists[PHASE_BURN_IN] = true;
        }

        int32_t diff = abs(time->burn_in_phase_year - time->year_start);
        if (time->phase_exists[PHASE_BURN_IN] && diff > 1) {
                time->phase_exists[PHASE_WARM_UP] = true;
        }


        set_time_format_string(conf);

}


void simulation_time_init(struct simulation_time *time)
{
        for (int32_t i = 0; i < MAX_PHASES; i++) {
                time->phase_exists[i] = false;
                time->phase_done[i] = false;
        }

        time->burn_in_phase_length = 0;
        time->burn_in_phase_year = 0;
        time->initial_cg_skipped = false;
        time->output_interval = 1;
        time->simulation_length = 0;
        time->warm_up_phase_year = 0;
        time->year_current = 0;
        time->year_start = 0;
        time->phase_name = "unknown";
        time->phase = PHASE_SIMULATION;
        time->always_show_sign = false;
        time->zero_pad_to_digits = 0;
        time->format_string = NULL;
}