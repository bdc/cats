// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_registry.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_ENVIRONMENT_REGISTRY_H
#define CATS_ENVIRONMENT_REGISTRY_H

#include <stdint.h>
#include "environment.h"

struct cats_configuration;

void print_environments(struct cats_configuration *conf, int32_t indent);

void reset_environment_registry(struct cats_environment_registry *reg);

char *get_suitability_name_from_species(const char *name);

struct cats_environment_variable *
add_environment_to_registry(struct cats_environment_registry *registry, const char *name,
                            enum environment_variable_type type,
                            int32_t interpolation, int32_t reload_interval, const char *file_name_pattern,
                            const struct cats_dimension *dimension);

struct cats_environment_variable *get_environment_from_registry(struct cats_environment_registry *reg, char *name);

void cleanup_environment_registry(struct cats_environment_registry *reg);

void unload_environment_phase_end(struct cats_environment_registry *reg);

#endif //CATS_ENVIRONMENT_REGISTRY_H
