// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_set.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "cats_global.h"
#include <string.h>
#include "configuration/configuration.h"
#include "environment/environment.h"
#include "environment_set.h"
#include "memory/cats_memory.h"
#include "environment/glm.h"
#include "configuration/print_configuration.h"


const char *get_environment_set_type_name(enum environment_type type)
{
        switch (type) {
                case ENVIRONMENT_TYPE_UNKNOWN:
                        return "unknown";
                case ENVIRONMENT_TYPE_SUITABILITY:
                        return "suitability";
                case ENVIRONMENT_TYPE_GLM:
                        return "glm";
                case ENVIRONMENT_TYPE_DIRECT:
                        return "direct";
                case ENVIRONMENT_TYPE_CONSTANT:
                        return "constant";
        }
        return "error";
}


void print_environment_set(struct cats_environment *set, int32_t indent)
{

        int offset = 0;

        if (set->count > 1) {
                print_string(indent, "Environment set", set->name);
                print_integer(indent, "Environments in set", set->count);
                print_string(indent + 1, "Environment set type", get_environment_set_type_name(set->type));
                offset = 1;

        }
        for (int32_t i = 0; i < set->count; i++) {

                print_environment(set->environments[i], indent + 1 + offset - 1);
                if (set->type == ENVIRONMENT_TYPE_CONSTANT) {
                        print_rate(indent + 1 + offset, "Constant value", set->environments[i]->constant_value);
                }

        }


        print_string(indent + 1, "save environment", bool_to_string(set->save_environment));

        switch (set->type) {
                case ENVIRONMENT_TYPE_UNKNOWN:
                case ENVIRONMENT_TYPE_SUITABILITY:
                case ENVIRONMENT_TYPE_DIRECT:
                case ENVIRONMENT_TYPE_CONSTANT:
                        break;
                case ENVIRONMENT_TYPE_GLM:
                        print_glm_params(&set->glm, indent + 1, set);
                        break;
        }

        log_message(LOG_RAW, "\n");
}


void print_environment_sets(struct cats_configuration *conf)
{
        log_message(LOG_RAW, "Environment sets\n");
        print_integer(1, "Number of sets", conf->environment.count);
        log_message(LOG_RAW, "\n");
        for (int32_t i = 0; i < conf->environment.count; i++) {
                print_environment_set(&conf->environment.environment_sets[i], 1);

        }
        log_message(LOG_RAW, "\n");
        log_message(LOG_RAW, "\n");
}


void init_cats_environment(struct cats_environment *environment)
{
        for (int32_t i = 0; i < MAX_ENVIRONMENTS; i++) {
                environment->environments[i] = NULL;
        }

        environment->count = 0;
        environment->type = ENVIRONMENT_TYPE_UNKNOWN;
        init_glm_params(&environment->glm);
        environment->save_environment = false;
}


struct cats_environment *
add_environment(struct cats_configuration *conf, const char *name, enum environment_type type,
                struct glm_params *glm)
{
        log_message(LOG_INFO, "%s: adding environment set '%s'", __func__, name);
        if (conf->environment.names == NULL) {
                conf->environment.names = new_string_array();
        }
        string_array_add(conf->environment.names, name);

        struct cats_environment *newest = &conf->environment.environment_sets[conf->environment.count];
        init_cats_environment(newest);
        newest->type = type;

        if (newest->name == NULL) {
                newest->name = strdup(name);
        }

        if (type == ENVIRONMENT_TYPE_GLM) {
                if (glm == NULL) {
                        log_message(LOG_ERROR, "%s: environment '%s' has no glm parameter specified", __func__, name);
                        exit_cats(EXIT_FAILURE);
                }
                newest->glm = *glm;
        }

        conf->environment.count += 1;
        return newest;
}


void add_to_environment(struct cats_environment *environment, struct cats_environment_variable *environment_variable)
{
        if (environment == NULL) {
                log_message(LOG_ERROR, "%s: Can't add to empty environment set.", __func__);
                exit(EXIT_FAILURE);
        }

        if (environment_variable == NULL) {
                log_message(LOG_ERROR, "%s: Can't add environment.", __func__);
                exit_cats(EXIT_FAILURE);
        }

        if (environment->type == ENVIRONMENT_TYPE_SUITABILITY && environment->count >= 1) {
                log_message(LOG_ERROR,
                            "%s: Only one environment allowed in a set of ENVIRONMENT_SET_TYPE_SUITABILITY, cannot add '%s' to '%s'",
                            __func__, environment_variable->pattern, environment->name);
                exit_cats(EXIT_FAILURE);
        }


        log_message(LOG_INFO, "%s: adding environment '%s' to environment set '%s'", __func__,
                    environment_variable->pattern,
                    environment->name);

        environment->environments[environment->count] = environment_variable;
        environment->count += 1;
}


void failed_to_find_environment(const struct cats_configuration *conf, const char *name)
{
        log_message(LOG_ERROR, "%s: could not find environment set '%s' in registry", __func__, name);
        if (conf->environment.names == NULL || conf->environment.names->count == 0) {
                log_message(LOG_ERROR, "%s: registry empty ('%s')", __func__, name);
        } else {
                for (int32_t i = 0; i < conf->environment.names->count; i++) {
                        log_message(LOG_INFO, "%s: have registry entry: '%s'", __func__,
                                    conf->environment.names->string[i]);

                }
        }

        exit_cats(EXIT_FAILURE);
}


struct cats_environment *get_environment(const struct cats_configuration *conf, const char *name)
{
        if (conf->environment.names == NULL || conf->environment.names->count == 0) {
                failed_to_find_environment(conf, name);
        }
        int32_t idx = string_array_index(conf->environment.names, name);
        if (idx < 0) failed_to_find_environment(conf, name);


        return (struct cats_environment *) &conf->environment.environment_sets[idx];
}