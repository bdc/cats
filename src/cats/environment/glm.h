// SPDX-License-Identifier: GPL-3.0-or-later
//
// glm.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_GLM_H
#define CATS_GLM_H

#include "cats_global.h"
#include "data/cats_datatypes.h"
#include "defaults.h"

struct cats_environment;
struct cats_configuration;
struct cats_environment_variable;
struct cats_grid;
enum cats_predictor_method {
        PRED_METH_UNKNOWN,
        PRED_METH_GLM_QUADRATIC

};


enum cats_glm_family {
        GLM_FAMILY_UNKNOWN,
        GLM_FAMILY_BINOMIAL,
        GLM_FAMILY_POISSON,
        GLM_FAMILY_BERNOULLI,
        GLM_FAMILY_GAUSSIAN

};


enum cats_glm_type {
        GLM_UNKNOWN,
        GLM_LINEAR,
        GLM_QUADRATIC
};

struct glm_params {
        int32_t count;
        cats_dt_rates linear[MAX_ENVIRONMENTS];
        cats_dt_rates quadratic[MAX_ENVIRONMENTS];
        cats_dt_rates intercept;
        enum cats_glm_type type;
        enum cats_glm_family family;

};

void init_glm_params(struct glm_params *glm);

void print_glm_params(struct glm_params *params, int32_t indent, struct cats_environment *set);

cats_dt_environment
get_suitability_glm(const struct cats_configuration *conf, const struct cats_grid *grid, cats_dt_coord row,
                    cats_dt_coord col);

void allocate_predictor_glm_values(struct cats_environment_variable *predictors, int predictor_count, int param_count);

const char *glm_type_name(enum cats_glm_type type);

#endif //CATS_GLM_H
