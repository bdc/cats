// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_set.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_ENVIRONMENT_SET_H
#define CATS_ENVIRONMENT_SET_H

#include "environment/environment.h"

const char *get_environment_set_type_name(enum environment_type type);

struct cats_environment *
add_environment(struct cats_configuration *conf, const char *name, enum environment_type type,
                struct glm_params *glm);

void add_to_environment(struct cats_environment *environment, struct cats_environment_variable *environment_variable);

struct cats_environment *get_environment(const struct cats_configuration *conf, const char *name);

void print_environment_sets(struct cats_configuration *conf);

#endif
