// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_environment.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#ifndef __MINGW32__

#include <dlfcn.h>

#endif

#include <stdint.h>
#include <math.h>
#include <assert.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "grids/grid_wrapper.h"
#include "actions/cats_actions.h"
#include "logging.h"
#include "environment.h"
#include "misc/cats_maths_inline.h"
#include "../../memory/arrays.h"
#include "../../memory/raw_arrays.h"
#include "grids/gdal_save.h"
#include "inline.h"
#include "paths/paths.h"
#include "paths/output_paths.h"


enum action_status update_environment_burnin(struct cats_configuration *conf, int32_t time)
{
        assert(conf != NULL);

        // are we  past the starting year of the burn in phase?
        if (time != conf->time.burn_in_phase_year) {
                log_message(LOG_INFO, "not changing predictors or suitability during burn in");
                return ACTION_NOT_RUN;
        }

        for (int32_t i = 0; i < conf->environment_registry.count; i++) {
                load_environment_raster(conf, &conf->environment_registry.environment[i], INTERPOLATION_CURRENT, time);
        }

        return ACTION_RUN;
}


enum action_status update_environment_warmup(struct cats_configuration *conf, int32_t time)
{
        assert(conf != NULL);

        int start = conf->time.warm_up_phase_year;
        int end = conf->time.year_start;

        for (int32_t i = 0; i < conf->environment_registry.count; i++) {
                if (conf->environment_registry.environment[i].is_constant == false) {
                        load_environment_interpolated(conf, &conf->environment_registry.environment[i], start, end,
                                                      time);
                } else if (time == start) {
                        log_message(LOG_INFO, " ENV: loading environment %s for year %d as static predictor",
                                    conf->environment_registry.name->string[i],
                                    time);
                        load_environment_raster(conf, &conf->environment_registry.environment[i], INTERPOLATION_CURRENT,
                                                time);
                }
        }

        return ACTION_RUN;
}


bool need_to_load_static_environment_p(struct cats_configuration *conf, int current_year, bool is_constant)
{
        return (current_year == conf->time.year_start && is_constant == true);
}


bool is_environment_yearly_p(bool is_constant, int interpolate)
{
        return (is_constant == false && interpolate <= 1);
}


void load_environment_raster_if_needed(struct cats_environment_variable *environment, int year_wanted,
                                       enum interpolation_type which,
                                       struct cats_configuration *conf)
{
        struct cats_environment_raster *raster = get_raster_from_environment(environment, which);
        if (raster->year == year_wanted) return;
        load_environment_raster(conf, environment, which, year_wanted);
}


void
load_environment_if_needed(struct cats_configuration *conf, struct cats_environment_variable *environment, int32_t time)
{
        char *name = NULL;
        char *short_name = NULL;
        if (environment->environment_type == ENV_VAR_CONSTANT) return;
        get_environment_names(environment, &name, &short_name);
        enum interpolation_type interpolation_type = INTERPOLATION_CURRENT;

        // static suitability load only in year 0
        if (need_to_load_static_environment_p(conf, time, environment->is_constant)) {
                log_message(LOG_INFO, "%s: loading %s %s for year %d as static %s", short_name, name,
                            environment->pattern, time,
                            name);
                load_environment_raster(conf, environment, interpolation_type, time);
        }

        // changing predictor, uninterpolated - load for each year 
        if (is_environment_yearly_p(environment->is_constant, environment->interpolation_interval)) {
                log_message(LOG_INFO, "%s: loading %s %s for year %d", short_name, name, environment->pattern, time);
                load_environment_raster(conf, environment, interpolation_type, time);
        }

        // changing suitabilites, interpolated 
        if (environment->is_constant == false && environment->interpolation_interval > 1) {
                int start = conf->time.year_start +
                            floor((double) (time - conf->time.year_start) /
                                  (double) environment->interpolation_interval) *
                            environment->interpolation_interval;

                int end = start + environment->interpolation_interval;


                log_message(LOG_INFO,
                            "%s: loading interpolated %s %s for year %d (interpolated between %d and %d)",
                            short_name, name, environment->pattern, time, start, end);
                load_environment_interpolated(conf, environment, start, end, time);
       
        }

}


void save_glm(struct cats_configuration *conf, const struct cats_environment *env, int32_t time)
{
        double **values = new_raw_2d_array_from_dimension(conf->geometry.dimension, sizeof(double));
        struct cats_vital_rate rate_info = {0};
        rate_info.environment_set = (struct cats_environment *) env;
        rate_info.density = NO_DENSITY_DEP;
        rate_info.density_ts = 0.0f;

        for (cats_dt_coord row = 0; row < conf->geometry.dimension.rows; row++) {
                for (cats_dt_coord col = 0; col < conf->geometry.dimension.cols; col++) {
                        values[row][col] = (double) get_glm(&rate_info, 0, 1, row, col);
                }
        }

        char *filename = get_environment_output_filename(conf, env);

        struct grid_wrapper data = gridwrapper(values, conf->geometry.dimension);
        save_grid_to_gdal(&data, GDT_Float64, conf, filename, env->name);
        free(filename);
        free_grid(&values, conf->geometry.dimension.rows);
}


enum action_status update_environment_simulation(struct cats_configuration *conf, int32_t time)
{
        assert(conf != NULL);
        for (int32_t i = 0; i < conf->environment_registry.count; i++) {
                load_environment_if_needed(conf, &conf->environment_registry.environment[i], time);
        }

        for (int32_t i = 0; i < conf->environment.count; i++) {
                struct cats_environment *env = &conf->environment.environment_sets[i];
                if (env->save_environment == false) continue;
                if (env->type == ENVIRONMENT_TYPE_GLM) {
                        save_glm(conf, env, time);
                } else if (env->type == ENVIRONMENT_TYPE_SUITABILITY) {
                        save_suitability_to_gdal(conf, env);
                }
        }

        return ACTION_RUN;
}
