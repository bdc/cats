// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_ENVIRONMENT_H
#define CATS_ENVIRONMENT_H

#include "cats_global.h"
#include "../../cats_csv/cats_csv.h"
#include "environment/glm.h"
#include "environment/environment_rasters.h"

/// @brief Single environment variable (e.g. a suitability or a predictor), containing data and information for interpolation
///
/// FIXME DOC long description
struct cats_environment_variable {
        char *name;
        char *pattern;                               //!< name of the environment, e.g. used in file names
        enum environment_variable_type environment_type;   //!< what kind of environment do we have? e.g. predictors, suitability, ...
        struct cats_environment_raster current;   //!< the actual environment raster
        struct cats_environment_raster start;     //!< only used for interpolation: start raster
        struct cats_environment_raster end;       //!< only used for interpolation: end raster
        cats_dt_rates divisor;
        bool is_constant;                         //!< are the values constant over time or interpolated?
        int32_t interpolation_interval;               //!< over how many years will be interpolated (linearly): 0 for disabled
        float interpolation_value;                //!< current interpolation value; in range [0, 1] between start.year and end.year
        cats_dt_rates constant_value;
        int32_t reload_interval;
        bool quiet;                               //!< suppress logging information, eg. during scale factor calculations
};

#define MAX_TOTAL_ENVIRONMENTS 1024
struct cats_environment_registry {
        int32_t count;
        struct string_array *name;
        struct cats_environment_variable environment[MAX_TOTAL_ENVIRONMENTS];
};


struct cats_environment {
        int32_t count;
        char *name;
        struct cats_environment_variable *environments[MAX_ENVIRONMENTS];
        struct glm_params glm;
        enum environment_type type;
        bool save_environment;
};

const char *get_raster_type_name_specific(const struct cats_environment_raster *raster);

void load_environment_raster_if_needed(struct cats_environment_variable *environment, int year_wanted,
                                       enum interpolation_type which,
                                       struct cats_configuration *conf);

struct cats_environment_raster *
get_raster_from_environment(struct cats_environment_variable *environment, enum interpolation_type interpolation_type);


void load_environment_raster(struct cats_configuration *conf, struct cats_environment_variable *environment,
                             enum interpolation_type type, int year);

void unload_environment_raster(struct cats_environment_raster *raster);

void get_environment_names(struct cats_environment_variable *environment, char **name, char **short_name);


void interpolate_environment(const struct cats_configuration *conf,
                             const struct cats_environment_variable *restrict environment);

void
load_environment_interpolated(struct cats_configuration *conf, struct cats_environment_variable *environment,
                              int year_start,
                              int year_end, int year_target);

void load_initial_population(struct cats_grid *grid, struct cats_configuration *conf);

const char *environment_type_name(enum environment_variable_type type);

void print_environment(struct cats_environment_variable *env, int32_t indent);

#endif
