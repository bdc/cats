// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_rasters.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <limits.h>
#include <stdlib.h>
#include "configuration/configuration.h"
#include "logging.h"
#include "environment_rasters.h"
#include "environment.h"


void print_raster_quick_info(struct cats_environment_raster *gr)
{
        const char *type = get_raster_type_name_specific(gr);
        const char *name = gr->filename;

        if (!name) name = "<none>";

        if (gr->year == INT_MIN) {
                log_message(LOG_INFO, "\tcurrently loaded for <%s>: <none> (address: %p)", type, gr->values);
        } else {
                log_message(LOG_INFO, "\tcurrently loaded for <%s>: %d (file name: %s, address: %p)", type, gr->year,
                            name, gr->values);
        }
}


void create_raster_if_needed(struct cats_configuration *conf, struct cats_environment_raster *raster,
                             enum environment_variable_type type)
{
        if (raster->values != NULL) return;

        log_message(LOG_INFO, "\tinitializing empty grid for <%s>", get_raster_type_name_specific(raster));
        log_message(LOG_DEBUG, "%s: ALLOCATING NEW GRID NOW", __func__);
        raster->values = new_raw_2d_array_from_dimension(conf->geometry.dimension, sizeof(cats_dt_environment));
        raster->dimension.rows = conf->geometry.dimension.rows;
        raster->dimension.cols = conf->geometry.dimension.cols;

}


void swap_rasters_maybe(struct cats_environment_raster *from, struct cats_environment_raster *to, int year_wanted)
{
        if (from->year != year_wanted) return; // nothing to do


        log_message(LOG_INFO, "\tmoving %s (%d) to %s (currently %d)",
                    get_raster_type_name_specific(from), from->year, get_raster_type_name_specific(to), to->year);

        // IMPORTANT: do not change order of code

        unload_environment_raster(to);

        // do not change type, as we usually swap the data of end raster and start raster,
        // but not the type


        to->dimension.rows = from->dimension.rows;
        to->dimension.cols = from->dimension.cols;
        to->values = from->values;
        to->year = year_wanted;
        to->quiet = from->quiet;
        from->values = NULL;
        to->filename = from->filename;
        from->filename = NULL;
        // WARNING: never transfer interpolation type, ou might end up with to identical layers (if we access by interpolation type)
        //to->interpolation_type = from->interpolation_type;
        unload_environment_raster(from);

}


const char *get_raster_type_name_short(const struct cats_environment_raster *raster)
{
        switch (raster->environment_type) {
                case ENV_VAR_PREDICTOR:
                        return "PRED";
                case ENV_VAR_SUITABILITY:
                        return "SUIT";
                default:
                        return "UNKN";
        }
}


const char *get_raster_type_name(const struct cats_environment_raster *raster)
{
        switch (raster->environment_type) {
                case ENV_VAR_PREDICTOR:

                        return "predictor";
                case ENV_VAR_SUITABILITY:
                        return "suitability";
                default:
                        return "unknown";
        }
}


const char *get_raster_type_name_specific(const struct cats_environment_raster *raster)
{
        switch (raster->environment_type) {

                case ENV_VAR_UNKNOWN:
                        break;
                case ENV_VAR_CONSTANT:
                        return "constant";
                case ENV_VAR_SUITABILITY:
                        switch (raster->interpolation_type) {
                                case INTERPOLATION_CURRENT:
                                        return "current suitability";
                                case INTERPOLATION_START:
                                        return "start suitability";
                                case INTERPOLATION_END:
                                        return "end suitability";
                                default:
                                        break;
                        }
                        break;
                case ENV_VAR_PREDICTOR:
                        switch (raster->interpolation_type) {
                                case INTERPOLATION_CURRENT:
                                        return "current predictor";
                                case INTERPOLATION_START:
                                        return "start predictor";
                                case INTERPOLATION_END:
                                        return "end predictor";
                                default:
                                        break;
                        }
                        break;

                case ENV_VAR_DIRECT:
                        switch (raster->interpolation_type) {
                                case INTERPOLATION_CURRENT:
                                        return "current variable";
                                case INTERPOLATION_START:
                                        return "start variable";
                                case INTERPOLATION_END:
                                        return "end variable";
                                default:
                                        break;
                        }
                        break;
        }

        return "unknown";
}


struct cats_environment_raster *
get_raster_from_environment(struct cats_environment_variable *environment, enum interpolation_type interpolation_type)
{
        switch (interpolation_type) {
                case INTERPOLATION_CURRENT:
                        return &environment->current;

                case INTERPOLATION_START:
                        return &environment->start;

                case INTERPOLATION_END:
                        return &environment->end;
                default:
                        log_message(LOG_ERROR, "%s: invalid interpolation type selected: %d", __func__,
                                    interpolation_type);
                        exit(EXIT_FAILURE);
        }
}


void unload_environment_raster(struct cats_environment_raster *raster)
{
        if (raster == NULL) {
                log_message(LOG_ERROR, "trying to unload NULL!");
                exit(EXIT_FAILURE);
        }

        if (!raster->quiet) {

                log_message(LOG_INFO, "\tunloading %s: %s", get_raster_type_name_specific(raster), raster->filename);
                log_message(LOG_DEBUG, "values: %p", raster->values);
                log_message(LOG_DEBUG, "rows: %d", raster->dimension.rows);
                log_message(LOG_DEBUG, "cols: %d", raster->dimension.cols);
        }

        if (raster->values == NULL) {
                if (raster->interpolation_type != INTERPOLATION_UNKNOWN) {
                        if (!raster->quiet) {
                                log_message(LOG_INFO, "\t\tno need to unload %s: not loaded",
                                            get_raster_type_name_specific(raster));
                        }
                        return;
                }
        }

        if (!raster->quiet) {
                log_message(LOG_DEBUG, "%s: FREEING GRID NOW %d", __func__, raster->dimension.rows);
                log_message(LOG_INFO, "\tunloading %s: was year %d", get_raster_type_name_specific(raster),
                            raster->year);
        }

        free_grid(&raster->values, raster->dimension.rows);
        raster->values = NULL;
        free(raster->filename);
        raster->filename = NULL;
        raster->year = INT_MIN;
        raster->dimension.rows = -1;
        raster->dimension.cols = -1;
}
