// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#ifndef  _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <assert.h>
#include <logging/logging.h>

#include "configuration/configuration.h"
#include "memory.h"
#include "misc/misc.h"
#include "data/cats_grid.h"
#include "paths/paths.h"
#include "environment.h"
#include "grids/gdal_load.h"
#include "environment/environment_rasters.h"

#ifdef USEMPI
#include "mpi/mpi_save.h"
#endif


float get_interpolation_factor(int year_start_wanted, int year_end_wanted, int year_target, const char *name)
{
        int year_diff = year_end_wanted - year_start_wanted;
        int offset = year_target - year_start_wanted;
        // reshuffle would be done here - either rng or 1.0 at the end of the interpolation window
        float scale = (float) offset / (float) year_diff;
        log_message(LOG_INFO,
                    "\t%s interpolated: <year %d> + ((<year %d>) - (<year %d>)) * %d/%d (=%f)",
                    name, year_start_wanted, year_end_wanted, year_start_wanted, offset, year_diff, scale);
        return scale;
}


void interpolate_environment(const struct cats_configuration *conf,
                             const struct cats_environment_variable *restrict environment)
{
        // warning: grid size not necessarily full grid size,
        // e.g. in MPI simulations, where only a part of the grid is simulated in a single process
        assert(environment->start.values != NULL);
        assert(environment->end.values != NULL);
        assert(environment->current.values != NULL);

        const cats_dt_coord rows = environment->current.dimension.rows;
        const cats_dt_coord cols = environment->current.dimension.cols;

        if (environment->start.dimension.rows != environment->end.dimension.rows
            || environment->start.dimension.cols != environment->end.dimension.cols
            || environment->start.dimension.rows != environment->current.dimension.rows
            || environment->start.dimension.cols != environment->current.dimension.cols) {
                log_message(LOG_ERROR, "%s: dimension mismatch", __func__);
                log_message(LOG_ERROR, "environment (start):   %d x %d", environment->start.dimension.rows,
                            environment->start.dimension.cols);
                log_message(LOG_ERROR, "environment (current): %d x %d", environment->current.dimension.rows,
                            environment->current.dimension.cols);
                log_message(LOG_ERROR, "environment (end):     %d x %d", environment->end.dimension.rows,
                            environment->end.dimension.cols);
                exit_cats(EXIT_FAILURE);

        }

        const cats_dt_environment interpolation_factor = environment->interpolation_value;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        cats_dt_environment start_value = environment->start.values[row][col];
                        cats_dt_environment end_value = environment->end.values[row][col];

                        environment->current.values[row][col] =
                                start_value + (end_value - start_value) * interpolation_factor;
                }
        }
}


void environment_raster_convert_nan_and_less_than_0_to_0(const struct cats_environment_raster *raster)
{
        const cats_dt_coord rows = raster->dimension.rows;
        const cats_dt_coord cols = raster->dimension.cols;

        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        if (isnan(raster->values[row][col]) || raster->values[row][col] < 0) {
                                raster->values[row][col] = 0.0f;

                        }
                }
        }
}


void post_process_suitability(struct cats_environment_variable *environment, struct cats_environment_raster *raster)
{
        if (environment->divisor > 0.0) {

                cats_dt_environment divisor = (cats_dt_environment) environment->divisor;
                log_message(LOG_INFO, "dividing suitability values by %f", divisor);
                for (cats_dt_coord row = 0; row < raster->dimension.rows; row++) {
                        for (cats_dt_coord col = 0; col < raster->dimension.cols; col++) {

                                raster->values[row][col] = raster->values[row][col] / divisor;

                        }
                }
        }
}


void
load_environment_raster(struct cats_configuration *conf, struct cats_environment_variable *environment,
                        enum interpolation_type type, int year)
{
        char *filename = get_environment_filename(conf, environment, year);
        struct cats_environment_raster *raster = get_raster_from_environment(environment, type);
        enum environment_variable_type environment_type = environment->environment_type;
        assert(conf != NULL);
        assert(raster != NULL);
        assert(filename != NULL);
        if (!environment->quiet) {
                log_message(LOG_EMPTY, " ");
                log_message(LOG_INFO, "\t%s: loading <%s> from '%s' (year %d)", __func__,
                            get_raster_type_name_specific(raster),
                            filename, year);
        }

        unload_environment_raster(raster);
        raster->dimension.rows = conf->geometry.dimension.rows;
        raster->dimension.cols = conf->geometry.dimension.cols;
        raster->interpolation_type = type;
        raster->environment_type = environment_type;
        if (raster->values == NULL) {
                raster->values = new_raw_2d_array_from_dimension(conf->geometry.dimension, sizeof(cats_dt_environment));

        }
        struct raster_load_wrapper wrapper = {.geometry = &conf->geometry, .target_environment = raster->values,
                .ignore_geometry = false, .ignore_offset = false, .filename = filename, .type = LOAD_ENVIRONMENT};
        load_gdal_data(&wrapper);

        //raster->values = //;
        //load_environment_gdal(filename, &conf->geometry);

        switch (environment_type) {
                case ENV_VAR_CONSTANT:
                        break;
                case ENV_VAR_PREDICTOR:
                        log_message(LOG_INFO, "leaving NANs for predictors");
                        break;
                case ENV_VAR_SUITABILITY:
                        log_message(LOG_INFO, "converting nan to 0");

                        environment_raster_convert_nan_and_less_than_0_to_0(raster);
                        post_process_suitability(environment, raster);
                        break;
                case ENV_VAR_UNKNOWN:
                        log_message(LOG_ERROR, "%s: unknown raster type for '%s'", __func__, filename);
                        exit(EXIT_FAILURE);
                case ENV_VAR_DIRECT:
                        log_message(LOG_INFO, "converting nan to 0");
                        environment_raster_convert_nan_and_less_than_0_to_0(raster);
                        break;
        }

        raster->year = year;
        raster->filename = strdup(filename);

        free(filename);
}


void save_suitability_from_glm(struct cats_grid *grid, struct cats_configuration *conf)
{
#ifdef USEMPI
        save_predictors_gdal_mpi(grid, conf);

#else

#endif
        log_message(LOG_ERROR, "%s: not implemented", __func__);
}


void get_environment_names(struct cats_environment_variable *environment, char **name, char **short_name)
{
        enum environment_variable_type type = environment->environment_type;
        switch (type) {
                case ENV_VAR_PREDICTOR:
                        *name = "predictor";
                        *short_name = "PRED  ";
                        return;
                case ENV_VAR_SUITABILITY:
                        *name = "suitability";
                        *short_name = "SUIT  ";
                        return;
                case ENV_VAR_DIRECT:
                        *name = "direct parameter";
                        *short_name = "DIRECT";
                        return;
                case ENV_VAR_CONSTANT:
                        *name = "constant";
                        *short_name = "CONST ";
                        return;

                case ENV_VAR_UNKNOWN:
                        // error out
                        break;

        }
        log_message(LOG_ERROR, "%s: unknown type '%s'", __func__,
                    get_raster_type_name_specific(&environment->current));
        exit(EXIT_FAILURE);
}


void
load_environment_interpolated(struct cats_configuration *conf, struct cats_environment_variable *environment,
                              int year_start,
                              int year_end, int year_target)
{
        assert(environment != NULL);
        char *name = NULL;
        char *short_name = NULL;
        int32_t *current_year = &environment->current.year;

        log_message(LOG_INFO, "%s", TEXT_DIVIDER);

        get_environment_names(environment, &name, &short_name);
        log_message(LOG_INFO, "%s: loading %s %s for year %d", short_name, name, environment->pattern, year_target);
        log_message(LOG_INFO, "\tinterpolating %d from %d to %d", year_target, year_start, year_end);

        print_raster_quick_info(&environment->start);
        print_raster_quick_info(&environment->end);
        print_raster_quick_info(&environment->current);

        create_raster_if_needed(conf, &environment->current, environment->environment_type);

        swap_rasters_maybe(&environment->end, &environment->start, year_start);
        load_environment_raster_if_needed(environment, year_end, environment->end.interpolation_type, conf);

        swap_rasters_maybe(&environment->end, &environment->current, year_target);
        load_environment_raster_if_needed(environment, year_start, environment->start.interpolation_type, conf);


        if (*current_year == year_target) {
                log_message(LOG_WARNING, "\tyear is target year, no action needed");
                log_message(LOG_INFO, "%s SUMMARY: now loaded for %s: start %d end %d\n", short_name,
                            environment->pattern,
                            environment->start.year, environment->end.year);
                return;
        }


        log_message(LOG_EMPTY, " ");
        log_message(LOG_INFO, "\tcalculating current %s", name);

        float scale = get_interpolation_factor(year_start, year_end, year_target,
                                               get_raster_type_name_specific(&environment->current));
        environment->interpolation_value = scale;

        if (environment->current.filename) {
                free(environment->current.filename);
                environment->current.filename = NULL;
        }


        interpolate_environment(conf, environment);
        environment->current.year = year_target;
        log_message(LOG_EMPTY, " ");
        log_message(LOG_INFO, "%s SUMMARY: now loaded for %s: start %d end %d", short_name, environment->pattern,
                    environment->start.year, environment->end.year);
        print_raster_quick_info(&environment->start);
        print_raster_quick_info(&environment->end);
        print_raster_quick_info(&environment->current);
        log_message(LOG_INFO, "%s\n", TEXT_DIVIDER);
}


const char *environment_type_name(enum environment_variable_type type)
{
        switch (type) {

                case ENV_VAR_UNKNOWN:
                        return "unknown";
                case ENV_VAR_SUITABILITY:
                        return "suitability";
                case ENV_VAR_PREDICTOR:
                        return "predictor";
                case ENV_VAR_DIRECT:
                        return "variable";
                case ENV_VAR_CONSTANT:
                        return "constant";
        }

        return "ERROR: unknown environment type";
}


void print_environment(struct cats_environment_variable *env, int32_t indent)
{
        print_string(indent, "Environment", "");
        print_string(indent + 1, "name", env->name);
        print_string(indent + 1, "filename pattern", env->pattern);
        print_integer(indent + 1, "interpolation interval", env->interpolation_interval);
        print_integer(indent + 1, "reload interval", env->reload_interval);
        print_string(indent + 1, "constant over time", bool_to_string(env->is_constant));
        print_string(indent + 1, "type", environment_type_name(env->environment_type));
}

