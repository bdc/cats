// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_rasters.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_ENVIRONMENT_RASTERS_H
#define CATS_ENVIRONMENT_RASTERS_H

#include "data/cats_datatypes.h"

struct cats_configuration;

enum interpolation_type {
        INTERPOLATION_UNKNOWN = 0,
        INTERPOLATION_CURRENT = 1,
        INTERPOLATION_START = 2,
        INTERPOLATION_END = 3,
};


enum environment_variable_type {
        ENV_VAR_UNKNOWN,
        ENV_VAR_SUITABILITY,
        ENV_VAR_PREDICTOR,
        ENV_VAR_DIRECT,
        ENV_VAR_CONSTANT

};


enum environment_type {
        ENVIRONMENT_TYPE_UNKNOWN,
        ENVIRONMENT_TYPE_SUITABILITY,
        ENVIRONMENT_TYPE_GLM,
        ENVIRONMENT_TYPE_DIRECT,
        ENVIRONMENT_TYPE_CONSTANT,
};


struct cats_environment_raster {
        cats_dt_environment **values;     //!< the values as 2D-grid
        struct cats_dimension dimension;
        enum interpolation_type interpolation_type;            //!< are we the start, end or current set of values?
        enum environment_variable_type environment_type;
        int year;                         //!< temporal information: which year is loaded in \ref cats_suitability.values? INT_MIN for unknown or not loaded
        char *filename;                   //!< origin filename, for display purposes only. May be empty
        bool quiet;                       //!< suppress logging information, eg. when running scale factor calculations
};


void swap_rasters_maybe(struct cats_environment_raster *from, struct cats_environment_raster *to, int year_wanted);

void create_raster_if_needed(struct cats_configuration *conf, struct cats_environment_raster *raster,
                             enum environment_variable_type type);

void print_raster_quick_info(struct cats_environment_raster *gr);

const char *get_raster_type_name_short(const struct cats_environment_raster *raster);

const char *get_raster_type_name(const struct cats_environment_raster *raster);

const char *get_raster_type_name_specific(const struct cats_environment_raster *raster);

void unload_environment_raster(struct cats_environment_raster *raster);

#endif //CATS_ENVIRONMENT_RASTERS_H
