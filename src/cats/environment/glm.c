// SPDX-License-Identifier: GPL-3.0-or-later
//
// glm.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "glm.h"
#include "overlays/overlays.h"
#include "configuration/configuration.h"


const char *glm_type_name(enum cats_glm_type type)
{
        switch (type) {
                case GLM_UNKNOWN:
                        return "unknown";
                case GLM_LINEAR:
                        return "linear";
                case GLM_QUADRATIC:
                        return "quadratic";
        }
        return "unknown";
}


const char *glm_family_name(enum cats_glm_family family)
{
        switch (family) {

                case GLM_FAMILY_UNKNOWN:
                        return "unknown";
                case GLM_FAMILY_BINOMIAL:
                        return "binomial";
                case GLM_FAMILY_POISSON:
                        return "Poisson";
                case GLM_FAMILY_BERNOULLI:
                        return "Bernoulli";
                case GLM_FAMILY_GAUSSIAN:
                        return "Gaussian";
        }
        return "unknown";
}


void print_glm_params(struct glm_params *params, int32_t indent, struct cats_environment *set)
{
        assert(params != NULL);
        if (params == NULL) {
                log_message(LOG_ERROR, "%s: called with argument params == NULL", __func__);
                exit_cats(EXIT_FAILURE);
        }
        print_string(indent, "GLM parameters", "");
        print_rate(indent + 1, "intercept", params->intercept);
        print_string(indent + 1, "type", glm_type_name(params->type));

        for (int32_t i = 0; i < params->count; i++) {
                print_string(indent + 1, "predictor", "");
                if (set && set->count > i) {
                        print_string(indent + 2, "predictor name", set->environments[i]->name);
                } else {
                        print_string(indent + 2, "predictor name", "unknown");
                }

                print_integer(indent + 2, "predictor index", i);

                print_rate(indent + 3, "coefficient linear", params->linear[i]);
                if (params->type == GLM_QUADRATIC) {
                        print_rate(indent + 3, "coefficient quadratic", params->quadratic[i]);
                }
        }
}


void init_glm_params(struct glm_params *glm)
{
        glm->family = GLM_FAMILY_UNKNOWN;
        glm->intercept = 0.0;

        for (int32_t i = 0; i < MAX_ENVIRONMENTS; i++) {
                glm->quadratic[i] = 0.0;
                glm->linear[i] = 0.0;

        }
        glm->count = 0;
}
