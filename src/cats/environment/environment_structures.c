// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_structures.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include "environment.h"
#include "environment_structures.h"


void init_cats_environment_variable(struct cats_environment_variable *env)
{
        assert(env != NULL);
        unload_environment_raster(&env->current);
        unload_environment_raster(&env->start);
        unload_environment_raster(&env->end);
        env->current.year = INT_MIN;
        env->end.year = INT_MIN;
        env->start.year = INT_MIN;
        env->start.interpolation_type = INTERPOLATION_UNKNOWN;
        env->end.interpolation_type = INTERPOLATION_UNKNOWN;
        env->current.interpolation_type = INTERPOLATION_UNKNOWN;
        free(env->pattern);
        env->pattern = NULL;
        free(env->name);
        env->name = NULL;
        env->divisor = 0.0;

        env->is_constant = false;
        env->interpolation_value = -1.0f;
        env->interpolation_interval = -1;

}


void cleanup_cats_environment_variable(struct cats_environment_variable *env_variable)
{
        assert(env_variable != NULL);

        unload_environment_raster(&env_variable->current);
        unload_environment_raster(&env_variable->start);
        unload_environment_raster(&env_variable->end);


        free(env_variable->pattern);
        env_variable->pattern = NULL;

        free(env_variable->name);
        env_variable->name = NULL;

        env_variable->is_constant = true;
        env_variable->interpolation_interval = -1;
        env_variable->reload_interval = -1;
}
