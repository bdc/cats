// SPDX-License-Identifier: GPL-3.0-or-later
//
// environment_registry.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "environment_registry.h"
#include "environment.h"
#include <string.h>
#include <limits.h>
#include "environment/environment_structures.h"
#include "memory/arrays.h"
#include <logging/logging.h>

void reset_environment_registry(struct cats_environment_registry *reg)
{
        reg->count = 0;
        reg->name = new_string_array();
        for (int32_t i = 0; i < MAX_TOTAL_ENVIRONMENTS; i++) {
        }

}


char *get_suitability_name_from_species(const char *name)
{
        return compound_string("__suitability", name, "-");
}


struct cats_environment_variable *
add_environment_to_registry(struct cats_environment_registry *registry, const char *name,
                            enum environment_variable_type type,
                            int32_t interpolation, int32_t reload_interval,
                            const char *file_name_pattern, const struct cats_dimension *dimension)
{
        if (registry->name == NULL) {
                log_message(LOG_ERROR, "%s: name variable is NULL!", __func__);
                exit_cats(EXIT_FAILURE);
        }

        if (registry->count >= MAX_TOTAL_ENVIRONMENTS - 1) {
                log_message(LOG_ERROR, "%s: too many environments added.", __func__);
                exit_cats(EXIT_FAILURE);
        }


        if (interpolation > 0 && reload_interval > 0) {
                log_message(LOG_ERROR,
                            "%s: environment '%s': only one of interpolation interval/reload interval can be specified",
                            __func__, name);
                exit(EXIT_FAILURE);
        }

        struct cats_environment_variable *env_var = &registry->environment[registry->count];
        string_array_add(registry->name, name);
        if (interpolation < 0) {
                log_message(LOG_ERROR, "%s: environment '%s': invalid interpolation interval < 0: %d",
                            __func__, name, interpolation);
                exit(EXIT_FAILURE);
        }
        if (reload_interval < 0) {
                log_message(LOG_ERROR, "%s: environment '%s': invalid reload interval < 0: %d",
                            __func__, name, reload_interval);
                exit(EXIT_FAILURE);
        }

        env_var->is_constant = interpolation == 0;
        env_var->interpolation_interval = interpolation;
        env_var->reload_interval = reload_interval;
        env_var->environment_type = type;
        if (env_var->name == NULL) { env_var->name = strdup(name); }

        env_var->pattern = strdup(file_name_pattern);

        log_message(LOG_INFO, "%s: added environment '%s' to registry: "
                              "type %s, "
                              "filename pattern: '%s, ",
                    __func__, name,
                    environment_type_name(type),
                    file_name_pattern);

        env_var->current.year = INT_MIN;
        env_var->start.year = INT_MIN;
        env_var->end.year = INT_MIN;

        env_var->current.environment_type = type;
        env_var->end.environment_type = type;
        env_var->start.environment_type = type;

        env_var->current.values = NULL;
        env_var->end.values = NULL;
        env_var->start.values = NULL;
        env_var->quiet = false;

        env_var->current.interpolation_type = INTERPOLATION_CURRENT;
        env_var->start.interpolation_type = INTERPOLATION_START;
        env_var->end.interpolation_type = INTERPOLATION_END;

        env_var->current.dimension.rows = dimension->rows;
        env_var->current.dimension.cols = dimension->cols;
        env_var->current.quiet = false;
        env_var->start.quiet = false;
        env_var->end.quiet = false;
        cats_dt_environment **tmp = new_raw_2d_array_from_dimension(*dimension, sizeof(cats_dt_environment));
        env_var->current.values = tmp;
        env_var->end.values = NULL;
        env_var->start.values = NULL;
        registry->count += 1;
        return env_var;
}


struct cats_environment_variable *get_environment_from_registry(struct cats_environment_registry *reg, char *name)
{

        int32_t idx = string_array_index(reg->name, name);
        if (idx < 0) {
                log_message(LOG_ERROR, "%s: could not find environment '%s' in registry.", __func__, name);
                exit(EXIT_FAILURE);
        }
        if (idx >= reg->count) {
                log_message(LOG_ERROR, "%s: environment '%s' found in registry, but index out of range", __func__,
                            name);
                exit(EXIT_FAILURE);
        }
        log_message(LOG_ERROR, "%s: loaded environment data %s (%d) from registry", __func__, name, idx);
        return &reg->environment[idx];

}


void cleanup_environment_registry(struct cats_environment_registry *reg)
{

        for (int32_t i = 0; i < reg->count; i++) {
                log_message(LOG_INFO, "%s: cleaning up %s", __func__, reg->name->string[i]);
                cleanup_cats_environment_variable(&reg->environment[i]);
        }

        free_string_array(&reg->name);
        reg->count = 0;
}


void unload_environment_phase_end(struct cats_environment_registry *reg)
{
        for (int32_t i = 0; i < reg->count; i++) {
                struct cats_environment_variable *env = &reg->environment[i];
                env->end.year = INT_MIN;
                env->start.year = INT_MIN;
                unload_environment_raster(&env->end);
                unload_environment_raster(&env->start);

        }

}


