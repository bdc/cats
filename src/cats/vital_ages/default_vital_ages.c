// SPDX-License-Identifier: GPL-3.0-or-later
//
// default_vital_ages.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "default_vital_ages.h"
#include <logging/logging.h>

const char *get_vital_age_name(enum cats_vital_age_id idx)
{
        switch (idx) {
                case VA_SEED_PERSISTENCE:
                        return "seed persistence";
                case VA_AGE_OF_MATURITY_MIN:
                        return "minimum age of maturity";
                case VA_AGE_OF_MATURITY_MAX:
                        return "maximum age of maturity";
                case VA_MAX:
                case VA_MIN:
                        break;
        }

        log_message(LOG_ERROR, "%s: unknown vital age with index %d", __func__, idx);
        exit_cats(EXIT_FAILURE);
}

