// SPDX-License-Identifier: GPL-3.0-or-later
//
// inline.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_INLINE_H_
#define CATS_INLINE_H_

#include "defaults.h"
#include "environment/glm.h"
#include <math.h>
#include <assert.h>
#include "cats_global.h"
#include "plants/plant_rates.h"
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "environment/load_environment.h"
#include "misc/misc.h"
#include "logging.h"
#include "overlays/overlays.h"
#include "misc/cats_maths_inline.h"
#include "memory/arrays.h"

#include "inline_carrying_capacity.h"
#include "inline_population.h"
#include "inline_vital_ages.h"


static inline cats_dt_environment
get_suitability_from_env(const struct cats_environment *set, cats_dt_coord row, cats_dt_coord col)
{
        assert(set->type == ENVIRONMENT_TYPE_SUITABILITY);
        assert(set->count == 1);
        assert(row >= 0);
        assert(col >= 0);
        assert(row < set->environments[0]->current.dimension.rows);
        assert(col < set->environments[0]->current.dimension.cols);
        return set->environments[0]->current.values[row][col];
}


static inline cats_dt_rates
age_modified_juvenile_survival_rate(cats_dt_rates juvenile_transition_rate, int32_t age, int32_t mat_min,
                                    int32_t mat_max)
{
        // old behaviour
        return juvenile_transition_rate;
}


static inline cats_dt_rates get_juvenile_tr_from_germination_to_adult_survival(cats_dt_rates germination_to_adult_surv,
                                                                               const struct cats_species_param *species_parameter)
{
        if (isnan(germination_to_adult_surv)) return NAN;
        assert(species_parameter->juvenile_transition_rates != NULL);
        assert(germination_to_adult_surv >= 0.0);
        assert(germination_to_adult_surv <= 1.0);

        int32_t idx = (int32_t) (germination_to_adult_surv * (MAX_JUVENILE_TR_STEPS - 1));


        assert(idx >= 0 && idx < MAX_JUVENILE_TR_STEPS);
        return species_parameter->juvenile_transition_rates[idx];
}


static inline cats_dt_rates
density_multiplier(enum cats_density_dependence dependence,
                   cats_dt_rates N,
                   cats_dt_rates K,
                   cats_dt_rates density_ts)
{

        if (dependence == NO_DENSITY_DEP) return 1.0;

        assert(density_ts >= 0);
        assert(density_ts < 1);
        assert(K >= N);

        if (K <= 0) return 0.0;
        if (N / K < density_ts) return 1.0;

        N = N - K * density_ts;
        K = K - K * density_ts;

        assert(K > 0);
        assert(N >= 0);


        switch (dependence) {
                case NO_DENSITY_DEP:
                        return 1.0;
                case DENSITY_DEP_NEGATIVE:
                        return (K - N) / K;
                case DENSITY_DEP_POSITIVE:
                        return (1.0 - (K - N) / K);
        }
        log_message(LOG_ERROR, "%s: reached the unreachable", __func__);
        exit_cats(EXIT_FAILURE);

}


static inline cats_dt_rates clamp(cats_dt_rates v, cats_dt_rates min, cats_dt_rates max)
{
        if (isinf(v) || isnan(v)) {
                log_message(LOG_ERROR, "calculated rate value is out of range: %f\n", (double) v);
                exit(EXIT_FAILURE);
        }

        const cats_dt_rates t = v < min ? min : v;
        return t > max ? max : t;
}


static inline cats_dt_rates
get_suitability_if_needed(const struct cats_vital_rate *rate_link, const struct cats_grid *grid,
                          const struct link_override_parameters *override,
                          cats_dt_coord row, cats_dt_coord col)
{


        if (rate_link->suitability_cutoff > 0.0 || override != NULL || rate_link->func->type != LINK_CONSTANT) {
                // we need suitability
        } else {
                return NAN;
        }

        if (override != NULL && override->override_suitability > 0.0) return override->override_suitability;


        if (rate_link->environment_set->type == ENVIRONMENT_TYPE_SUITABILITY) {
                return (cats_dt_rates) get_suitability_from_env(rate_link->environment_set, row, col);
        } else {
                log_message(LOG_ERROR, "%s: mismatch suitability requested from non-suitability environment",
                            __func__);
                exit_cats(EXIT_FAILURE);
        }
}


static inline cats_dt_environment get_suitability(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid != NULL);
        assert(grid->conf != NULL);


        assert(grid->suitability != NULL);
        return get_suitability_from_env(grid->suitability, row, col);
}


static inline double get_seed_sum(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid != NULL && grid->juveniles != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        const int32_t max_sp = get_vital_age(grid, VA_SEED_PERSISTENCE); //grid->param.seed_persistence;
        double sum = grid->dispersed_seeds[row][col];
        if (grid->seed_bank[row][col] == NULL) return sum;
        for (int32_t i = 0; i < max_sp; i++) {
                sum += grid->seed_bank[row][col][i];
        }
        return sum;
}


static inline cats_dt_population_sum
get_juvenile_sum(const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col)
{
        assert(grid != NULL && grid->juveniles != NULL);
        assert(row >= 0 && row < grid->dimension.rows);
        assert(col >= 0 && col < grid->dimension.cols);
        assert(grid->juveniles[row] != NULL);

        const cats_dt_population *juveniles = grid->juveniles[row][col];
        if (juveniles == NULL) return 0;

        cats_dt_population_sum juvenile_sum = 0;
        const int32_t max_stage =
                get_vital_age(grid, VA_AGE_OF_MATURITY_MAX) + 1; //grid->param.max_age_of_maturity + 1;
        for (int32_t i = 0; i < max_stage; i++) {
                juvenile_sum += juveniles[i];
        }

        return juvenile_sum;
}


static inline cats_dt_rates
age_specific_maturation_rate(cats_dt_rates age, cats_dt_rates juvenile_survival, int32_t age_min, int32_t age_max)
{
        //printf("%Lf %d %d\n", age, age_min, age_max);
        assert(age_min <= age_max);
        assert(age_min >= 0);
        assert(age <= age_max);
        assert(age >= 0);

        if (age < age_min) return 0.0;
        cats_dt_rates maturation = ((age - age_min + 1.0) / (age_max - age_min + 1.0)) * juvenile_survival;

        assert(maturation >= 0.0 && maturation <= 1.0);
        assert(maturation == juvenile_survival || age < age_max);

        return maturation;
}


#endif
