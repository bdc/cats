// SPDX-License-Identifier: GPL-3.0-or-later
//
// dispersal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

// TODO
// TODO
// TODO

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "cats_global.h"
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <gsl/gsl_rng.h>

#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "logging.h"
#include "dispersal.h"
#include "misc/cats_maths_inline.h"
#include "threading/threading-helpers.h"


#ifdef USEMPI
#include "mpi/mpi_cats.h"
#endif

#include "memory/cats_memory.h"
#include "inline_overlays.h"

// ANSATZPUNKTE
// 3 verschiedene dispersals:
// normal, mixed, linear -> vereinheitlichen


void setup_dispersal_vectordata(const struct cats_configuration *conf, struct cats_dispersal *d, int vec, int grid_id,
                                double **data, int rows, int cols, char *filename)
{
        assert(conf != NULL);
        assert(data != NULL);
        assert(filename != NULL);

        if (rows != cols) {
                log_message(LOG_ERROR, "expected square dispersal data: %s rows (%d) != cols (%d)", filename, rows,
                            cols);
                exit(EXIT_FAILURE);
        }

        if (cols % 2 != 1) {
                log_message(LOG_ERROR, "dispersal must have an odd number of rows/cols");
                exit(EXIT_FAILURE);
        }

        d->radius[vec] = ceil((float) cols / 2.0);
        d->rows[vec] = rows;
        d->cols[vec] = cols;

        int id = grid_id;

        if (d->radius[vec] > conf->param[id].plant_dispersal_max_radius) {
                conf->param[id].plant_dispersal_max_radius = d->radius[vec];
        }

        if (cols > d->max_vector_diameter) {
                d->max_vector_diameter = cols;
                //d->max_vector_radius = ceil((float) d->max_vector_diameter / 2.0);
        }

        float sum = 0.0f;

        for (int64_t r = 0; r < rows; r++) {
                for (int64_t c = 0; c < cols; c++) {
                        sum += d->p[vec][r][c];
                }
        }

        log_message(LOG_IMPORTANT, "dispersal sum for %s: %f", filename, sum);
}


void get_vec_prob(float *prob, const struct cats_configuration *conf, const struct cats_dispersal *dispersal,
                  struct cats_thread_info *ts, bool average) // fixme rename maybe
{
        const int vectors = dispersal->vector_count;
        float prob_total = 0.0f;
        int unused = 0;

        for (int32_t vector = 0; vector < vectors; vector++) {
                if (dispersal->prob_max[vector] >= 0.0) {
                        float range = dispersal->prob_max[vector] - dispersal->prob_min[vector];
                        if (average == false) {
                                prob[vector] = dispersal->prob_min[vector] + (gsl_rng_uniform(ts->rng) * range);

                        } else {
                                prob[vector] = dispersal->prob_min[vector] + 0.5 * range;

                        }
                        prob_total += prob[vector];
                } else {
                        prob[vector] = -1.0f;
                        unused++;
                }
        }

        if (unused > 0) {
                float remaining_per = (1.0f - prob_total) / ((float) unused);
                for (int32_t vector = 0; vector < vectors; vector++) {
                        if (prob[vector] < 0) prob[vector] = remaining_per;
                }
        }
}


void disperse_seeds(const struct cats_grid *restrict grid, cats_dt_coord s_row, cats_dt_coord s_col,
                    struct cats_thread_info *restrict ts)
{

        // s_row and s_col are the source coordinates, i.e. the coordinates of the cells from which we disperse
        assert(grid != NULL);
        assert(grid->dispersal != NULL);
        assert(ts != NULL);
        assert(ts->conf != NULL);
        assert(grid->dispersal->max_vector_diameter > 0);
        assert(grid->seeds_produced != NULL);
        assert(s_row >= 0);

        assert(s_row < grid->dimension.rows);
        assert(s_col >= 0);
        assert(s_col < grid->dimension.cols);


        float seeds = grid->seeds_produced[s_row][s_col];
        assert(seeds >= 0.0);
        if (seeds < 1.0) return;

        const struct cats_configuration *const conf = (struct cats_configuration *) ts->conf;
        if (cell_excluded_by_overlay(conf, s_row,
                                     s_col)) { return; } // should have been checked by the calling function;

        const struct cats_dispersal *const restrict dispersal = grid->dispersal;

        const int32_t vectors = dispersal->vector_count;
#ifdef __STDC_NO_VLA__
        float *prob = malloc_or_die(sizeof(float) * vectors);

#else
        float prob[vectors];
#endif



        // float prob[vectors];
        if (conf->command_line_options.lambda_test) {
                get_vec_prob(prob, conf, dispersal, ts, true);
        } else {
                get_vec_prob(prob, conf, dispersal, ts, false);
        }


        int rows_before = 0; // used only for MPI
        int rows_after = 0;  // used only for MPI

#ifdef USEMPI
#ifndef TESTMPI
        float ** restrict destination = grid->seeds_0_all;
        int radius;
        get_rows_around(conf, &rows_before, &rows_after, &radius, grid->id);
#else
        float **destination = NULL;
        log_message(LOG_ERROR, "MPI SKIPPED DISPERSAL");
        free(prob):
        return;
#endif
#else
        float **restrict destination = grid->dispersed_seeds;
#endif

        assert(destination != NULL);
        s_row = s_row + rows_before; // add offset   

        const cats_dt_coord grid_cols = grid->dimension.cols;
        const cats_dt_coord grid_rows = grid->dimension.rows;

        for (int32_t v = 0; v < vectors; v++) {
                if (prob[v] <= 0.0 || dispersal->types[v] != DISPERSAL_KERNEL) continue;

                const cats_dt_dispersal *const restrict source_probabilities = dispersal->plin[v];
                const int32_t vector_columns = dispersal->cols[v];
                const int32_t width = dispersal->radius[v]; // radius includes source cell

                const float sx = prob[v] * seeds; // seeds to distribute = seeds left in cell times vector probability
                // seeds left = produced minus local and long range dispersal

                const int32_t c_min = max_int32t(0, s_col - width + 1);
                const int32_t c_max = min_int32t(s_col + width, grid_cols);

                const int32_t r_min = max_int32t(0, s_row - width + 1);
                const int32_t r_max = min_int32t(s_row + width, rows_before + grid_rows + rows_after);

                for (int32_t r = r_min; r < r_max; r++) {
                        for (int32_t c = c_min; c < c_max; c++) {

                                const int32_t dist_r = r - s_row;
                                const int32_t dist_c = c - s_col;

                                const int32_t vector_row = width + dist_r - 1;
                                const int32_t vector_col = width + dist_c - 1;
                                destination[r][c] +=
                                        sx * source_probabilities[vector_row * vector_columns + vector_col];

                        }
                }

        }
#ifdef __STDC_NO_VLA__
        free(prob);
#endif
}


