// SPDX-License-Identifier: GPL-3.0-or-later
//
// local_dispersal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "cats_global.h"
#include "threading/threading-helpers.h"
#include "misc/cats_maths_inline.h"
#include "dispersal.h"
#include "configuration/configuration.h"
#include "data/cats_grid.h"
#include "local_dispersal.h"


void cell_local_dispersal(struct cats_configuration *conf, struct cats_grid *grid, cats_dt_coord row,
                          cats_dt_coord col, struct cats_thread_info *ts)
{
        const struct cats_dispersal *const dispersal = grid->dispersal;
        if (dispersal->local_dispersal <= 0.0) return;

        float seeds = grid->seeds_produced[row][col];

        cats_dt_seeds self = (float) (seeds * dispersal->local_dispersal);
        self = min_float(seeds, self);

        grid->dispersed_seeds[row][col] += self;
        grid->seeds_produced[row][col] -= self;
}