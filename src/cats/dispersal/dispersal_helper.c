// SPDX-License-Identifier: GPL-3.0-or-later
//
// dispersal_helper.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <misc/misc.h>
#include <math.h>
#include "logging.h"
#include "dispersal.h"
#include "misc/cats_random.h"
#include "threading/threading-helpers.h"
#include "memory/cats_memory.h"
#include "memory/raw_arrays.h"


cats_dt_rates get_average_local_dispersal(const struct cats_dispersal *dispersal, const struct cats_configuration *conf)
{

        cats_dt_rates direct_local_dispersal = dispersal->local_dispersal;
        cats_dt_rates kernel_local_dispersal = get_kernel_local_dispersal(dispersal,
                                                                          conf, true);
        cats_dt_rates local_dispersal =
                direct_local_dispersal + (1.0 - direct_local_dispersal) * kernel_local_dispersal;

        assert(local_dispersal >= 0);
        assert(local_dispersal <= 1);

        return local_dispersal;
}


void cleanup_dispersal(struct cats_dispersal *dispersal) //!OCLINT
{
        if (!dispersal) return;

        log_message(LOG_IMPORTANT, "cleaning up dispersal '%s'", dispersal->name);
        free(dispersal->name);
        free(dispersal->types);
        free(dispersal->prob_min);
        free(dispersal->prob_max);
        free(dispersal->radius);


        for (int32_t i = 0; i < dispersal->vector_count; i++) {
                if (dispersal->filenames) free(dispersal->filenames[i]);
                if (dispersal->p && dispersal->p[i]) free_grid(&dispersal->p[i], dispersal->rows[i]);
                free(dispersal->plin[i]);
        }

        free(dispersal->geometry);
        free(dispersal->filenames);
        free(dispersal->p);
        free(dispersal->plin);
        free(dispersal->rows);
        free(dispersal->cols);
}


float
get_kernel_local_dispersal(const struct cats_dispersal *dispersal, const struct cats_configuration *conf, bool average)
{
        assert(dispersal != NULL);
        assert(conf != NULL);

        struct cats_thread_info thread;
        thread.rng = allocate_rng();

        int32_t vectors = dispersal->vector_count;
        assert(vectors > 0);
#ifdef __STDC_NO_VLA__
        float *prob = malloc_or_die(sizeof(float) * vectors);

#else
        //float *prob = malloc_or_die(sizeof(float) * vectors);
        float prob[vectors];
#endif

        get_vec_prob(prob, conf, dispersal, &thread, average);

        float own_cell_dispersal = 0.0f;
        float sd = (float) dispersal->local_dispersal;
        own_cell_dispersal += sd;

        for (int32_t v = 0; v < vectors; v++) {
                if (prob[v] <= 0.0 || dispersal->types[v] != DISPERSAL_KERNEL) {
                        continue;
                }

                int32_t midpoint = (int32_t) ceil((float) dispersal->rows[v] / 2.0) - 1;
                assert(midpoint >= 0);
                cats_dt_dispersal **f = dispersal->p[v];
                own_cell_dispersal += f[midpoint][midpoint] * prob[v] * (1.0 - sd);
        }

        gsl_rng_free(thread.rng);
#ifdef __STDC_NO_VLA__
        free(prob);
#endif
        return own_cell_dispersal;
}