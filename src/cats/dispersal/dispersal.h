// SPDX-License-Identifier: GPL-3.0-or-later
//
// dispersal.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_DISPERSAL_H
#define CATS_DISPERSAL_H

struct cats_thread_info;
struct cats_grid;
struct cats_configuration;
struct cats_species_param;

#include "data/cats_datatypes.h"

///@brief stores the dispersal information for a single species
struct cats_dispersal // FIXME DOC  - each  value is an array -> to array of dispersal with single value
{
        int32_t vector_count;
        enum dispersal_type *types;
        char *name;
        double *prob_min;
        double *prob_max;
        int32_t *radius;
        cats_dt_dispersal ***p;
        cats_dt_dispersal **plin;
        int32_t *rows;
        int32_t *cols;
        char **filenames;
        struct simulation_geometry *geometry;

        bool long_range_enabled;
        cats_dt_rates long_range_prob;
        int32_t long_range_radius;
        int32_t long_range_target_count;
        cats_dt_rates local_dispersal;
        int32_t max_vector_diameter; // largest dispersal size
        bool loaded;
};


void disperse_seeds(const struct cats_grid *restrict grid, cats_dt_coord s_row, cats_dt_coord s_col,
                    struct cats_thread_info *restrict thread);

float
get_kernel_local_dispersal(const struct cats_dispersal *dispersal, const struct cats_configuration *conf, bool average);


void setup_dispersal_vectordata(const struct cats_configuration *conf, struct cats_dispersal *d, int vec, int grid_id,
                                double **data, int rows, int cols, char *filename);

void get_vec_prob(float *prob, const struct cats_configuration *conf, const struct cats_dispersal *dispersal,
                  struct cats_thread_info *thread, bool average);

void dispersal_wrapper(struct cats_grid *grid, struct cats_thread_info *ts);

#endif