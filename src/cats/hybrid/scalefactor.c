// SPDX-License-Identifier: GPL-3.0-or-later
//
// scalefactor.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <assert.h>

#ifdef USEMPI
#include "mpi/mpi_cats.h"
#include "mpi/mpi_scalefactor.h"
#endif

#include <cats_ini/cats_ini.h>
#include <logging/logging.h>

#include "cats/command_line/command_line_options.h"
#include "configuration/load_vital_rates.h"
#include "data/cats_datatypes.h"
#include "grids/gdal_load.h"
#include "hybrid/scalefactor.h"
#include "inline_vital_rates.h"
#include "lambda/leslie_matrix.h"
#include "lambda/lambda.h"

struct cats_module *get_demographic_module_for_matrix(struct cats_configuration *conf, int32_t species_id)
{
        struct cats_module *module = NULL;
        struct cats_species_param *param = &conf->param[species_id];
        if (param->demographic_module == NULL) return module;

        int32_t module_id = param->demographic_module->id;

        if (conf->modules.module[module_id].flags & MODULE_ALTERNATE_DEMOGRAPHIC) return &conf->modules.module[module_id];

        return NULL;
}


bool have_leslie_matrix_function_for_module(struct cats_module *module)
{
        if (module == NULL) return true;
        cats_leslie_matrix_func func = module->create_leslie_matrix;
        return func != NULL;
}

cats_dt_rates calculate_scale_factor_from_lambda(struct cats_configuration *conf, int32_t species_id)
{
        assert(species_id < conf->param_count);
        struct cats_species_param *param = &conf->param[species_id];


        struct cats_module *module = get_demographic_module_for_matrix(conf, species_id);
        bool have_func = have_leslie_matrix_function_for_module(module);
        log_message(LOG_IMPORTANT, "Demographic module %p", module);

        if (module != NULL && have_func == false) {

                log_message(LOG_WARNING, "unable to calculate scale factor for %s: custom vital rates or vital ages",
                            param->species_name);
                log_message(LOG_WARNING, "Using scale factor 0.5 for %s", param->species_name);
                return 0.5;

        } else if (module != NULL) {
                log_message(LOG_IMPORTANT, "Grid %d uses module %s for demography: %p", species_id,
                module->canonical_name, module->create_leslie_matrix);
        }


        cats_dt_environment OT = (cats_dt_environment) conf->param[species_id].OT;
        cats_dt_rates low = 0.0;
        cats_dt_rates high = 1.0;
        cats_dt_rates s = 0.5;

        log_message(LOG_IMPORTANT, "calculating scale factor by finding lambda = 1.0 at OT %f", OT);
        if (param->custom_dispersal == false) {
                load_dispersals_gdal(NULL, conf, species_id);
        }


        int32_t iterations = 0;

        struct lambda_parameters l_param = {0};
        l_param.calculate_scale = true;
        l_param.suitability = OT;
        l_param.N = 0;
        l_param.K =
                get_vital_rate_maximum(&conf->param[species_id].carrying_capacity) * conf->param->max_adult_cc_fraction;
        l_param.grid = 0;
        l_param.row = 0;
        l_param.col = 0;
        l_param.param = &conf->param[species_id];
        l_param.species_id = species_id;
        cats_dt_rates threshold = LAMBDA_EPS;
        cats_dt_rates lambda = 0;
        while (true) {
                iterations++;
                conf->param[species_id].scale_factor = s;
                conf->direct_scale_factor = s;
                cats_dt_rates old_s = s;

                lambda = calculate_lambda(conf, &l_param, true, module);
                log_message(LOG_INFO, "iteration %d: scale factor %0.25Lf resulted in lambda %0.25Lf", iterations, s,
                            lambda);


                if (fabsl(lambda - 1.0) < threshold) {
                        conf->lambda_at_OT = lambda;
                        break;
                }

                if (lambda > 1.0) { // population grows
                        high = s;
                        s = (low + s) / 2.0;
                        if (s == old_s) {
                                s -= 1e-6;
                        }
                } else {
                        low = s;
                        s = (high + s) / 2.0;
                        if (s == old_s) {
                                s += 1e-6;
                        }
                }
                //printf("scale factor change: %Le - %Le = %Le\n", old_s, s, old_s-s);

                if (s >= 1.0) {
                        log_message(LOG_ERROR, "lambda < 1 even at scale factor 1");
                        exit(EXIT_FAILURE);
                }
                int32_t max_iterations = 100;
                if (iterations == 50) {
                        threshold = LAMBDA_EPS * 10.0;
                        log_message(LOG_IMPORTANT, "%s: decreasing lambda threshold to %f", __func__,
                                    (double) threshold);
                }
                if (iterations == 75) {
                        threshold = LAMBDA_EPS * 10.0 * 10.0;
                        log_message(LOG_IMPORTANT, "%s: decreasing lambda threshold to %f", __func__,
                                    (double) threshold);
                }
                if (iterations == 95) {
                        threshold = LAMBDA_EPS * 10.0 * 10.0 * 10.0;
                        log_message(LOG_IMPORTANT, "%s: decreasing lambda threshold to %f", __func__,
                                    (double) threshold);
                }
                if (iterations > max_iterations) {
                        log_message(LOG_ERROR, "could not find scale factor after %d iterations", max_iterations);
                        exit(EXIT_FAILURE);
                }

        }
        log_message(LOG_IMPORTANT,
                    "Setting scale factor to %f based on lambda = 1.0 within %g at OT %f after %d iterations",
                    (double) s,
                    (double) threshold,
                    OT,
                    iterations);
        log_message(LOG_IMPORTANT,
                    "SCALE;%f;%g;%g;%d",
                    (double) s,
                    (double) lambda,
                    (double) threshold,
                    iterations);
        conf->param[species_id].scale_factor = s;
        conf->direct_scale_factor = s;
        calculate_lambda(conf, &l_param, false, module);


        return s;
}


void set_scale_factor_all_species(struct cats_configuration *conf, cats_dt_rates scale_factor)
{

        log_message(LOG_INFO, "setting scale factor for all grids directly to %f", (double) scale_factor);

        for (int id = 0; id < conf->grid_count; id++) {
                conf->param[id].scale_factor = scale_factor;
        }
}


void setup_scale_factor(struct cats_configuration *conf, struct program_options *options)
{

        if (conf->param[0].parametrization != PARAM_HYBRID) {
                log_message(LOG_INFO,
                            "Skipping scale factor calculation -- scale factor only available in suitability mode");

                if (options->calculate_lambda_quit) {
                        log_message(LOG_ERROR, "Cannot calculate lambda for direct parametrization");
                        exit_cats(EXIT_FAILURE);
                }

                return;

        }

#ifdef USEMPI
        if (get_mpi_rank() > 0 &&  options->calculate_lambda_quit) { exit_cats(EXIT_SUCCESS);  }
#endif


        if (options->direct_scale_factor > 0 && options->direct_scale_factor < 1) {
                conf->direct_scale_factor = options->direct_scale_factor;

                log_message(LOG_IMPORTANT, "using pre-calculated scale factor %f", (double) conf->direct_scale_factor);


                set_scale_factor_all_species(conf, conf->direct_scale_factor);
                return;
        }

        if (options->calculate_lambda_quit) {
                for (int32_t i = 0; i < conf->param_count; i++) {
                        conf->param[i].scale_factor = calculate_scale_factor_from_lambda(conf, i);
                }
                exit(EXIT_SUCCESS);
        }


        for (int32_t i = 0; i < conf->param_count; i++) {
                conf->param[i].scale_factor = calculate_scale_factor_from_lambda(conf, i);
        }


#ifdef USEMPI
        broadcast_scale_factor(conf);
#endif

        update_hybrid_density_thresholds(conf);

}