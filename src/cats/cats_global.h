// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_global.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_GLOBAL_H_
#define CATS_GLOBAL_H_

#ifndef  _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifndef __STDC_IEC_559__
//#error "CATS assumes IEC 559 support"
#endif

// INCLUDE STATEMENTS
#include <stdio.h>
#include <stdlib.h>

#include "data/cats_datatypes.h"
#include "cats_time/cats_time.h"
#include "cats_defs.h"
#include "cats_windows.h"


// FUNCTIONS
void init_global_info(const char *program_name);
#ifdef CATS_ON_WINDOWS
void __declspec(noreturn) exit_cats(int status);
#else
_Noreturn void exit_cats(int status);
#endif

// GLOBALS
extern struct cats_global global;
extern struct cats_debug_options cats_debug;

#define CATS_DEBUG_THREADING
#undef CATS_DEBUG_THREADING
#define CATS_DEBUG_SINGLE_CELL
#undef CATS_DEBUG_SINGLE_CELL

enum cats_debug_flags {
        DEBUG_NONE = 0,
        DEBUG_MPI = 1 << 1,
        DEBUG_LAMBDA = 1 << 2
};

/// @brief Run-time information stored in a global variable
struct cats_global {
        bool enable_json_output;
        char *program_name;
        int mpi_world_rank;
        struct cats_time time_info;
        float poisson_dampening_factor;
        float poisson_dampening_minimum;
        float poisson_maximum;
        bool poisson_maximum_draw_diff;
};

#ifdef CATS_DEBUG_SINGLE_CELL
#define DBG_SINGLE(X) do {X} while(0);
#else
#define DBG_SINGLE(X)
#endif


struct cats_debug_options {
        int32_t thread_count;
        FILE **file_handles_threads;
        char **file_names_threads;
        FILE *file_handles_main;
        char *file_names_main;
        int32_t rows;
        int32_t cols;
        double **threading_grid;
        struct cats_configuration *conf;
        struct cats_coordinates misc_debug_coords;
        FILE *misc_debug_file; /// @brief global variable: file handle for a debug file documenting a single cells
};

static void abort_implemented(const char *name) {
        fprintf(stderr, "function '%s' is not yet implemented\n", name);
        exit_cats(EXIT_FAILURE);
}

#endif //CATS_GLOBAL_H_
