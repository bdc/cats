// SPDX-License-Identifier: GPL-3.0-or-later
//
// configuration.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CONFIGURATION_H
#define CATS_CONFIGURATION_H

#include "cats_global.h"
#include "defaults.h"

#ifndef CATS_ON_WINDOWS

#include <sys/time.h>

#endif

#ifdef USEMPI
#include <mpi.h>
#endif

#include "stats/statistics.h"
#include "vital_rates/vital_rates.h"
#include "overlays/overlays.h"
#include "cats/command_line/command_line_options.h"
#include "temporal/simulation_time.h"
#include "environment/environment.h"
#include "data/cats_enums.h"
#include "modules/modules.h"
#include "data/simulation_geometry.h"

struct cats_configuration;
struct cats_grid;
struct cats_dispersal;
struct cats_ini;

typedef enum action_status (*action_function)(struct cats_grid *grid, struct cats_configuration *conf);


void cleanup_dispersal(struct cats_dispersal *dispersal);

void update_juvenile_transition_rates(struct cats_configuration *conf);

struct cats_configuration *new_configuration(void);

void print_config_summary(struct cats_configuration *conf);

void fix_zts_and_ots(struct cats_configuration *conf);

void post_process_configuration(struct cats_configuration *conf);


struct simulation_info {
        //char *project;
        char *program_name;
        int32_t replicate;
};

#ifdef USEMPI

struct mpi_info {
        // mpi
        int world_size;
        int world_rank;
        char processor_name[MPI_MAX_PROCESSOR_NAME];
};
#endif

struct cats_vital_rate_hybrid_function;
#define MAX_ACTIONS_PER_CYCLE 256
#define MAX_ACTION_FUNCTIONS 128
struct action_function_registry {
        char *names[MAX_ACTION_FUNCTIONS];
        char *messages[MAX_ACTION_FUNCTIONS];
        char *messages_real[MAX_ACTION_FUNCTIONS];
        action_function functions[MAX_ACTION_FUNCTIONS];
        int32_t count;
};


#define ACTION_MESSAGE_LENGTH 128

struct cats_action {
        char message[ACTION_MESSAGE_LENGTH];
        char message_real[ACTION_MESSAGE_LENGTH];
        action_function func;
        unsigned int stages;
        char *module_name;
};

struct cats_cycle {
        int32_t num_actions;
        int64_t action_counter;
        int32_t current_action;
        struct cats_action *actions;
};


#define MAX_ENVIRONMENT_SETS  1024
struct cats_environment_collection {
        struct cats_environment environment_sets[MAX_ENVIRONMENT_SETS];
        int32_t count;
        struct string_array *names;
};


struct cats_output {
    struct string_array *needed_output_directories;
    struct string_array *needed_module_output_directories;
    bool have_glm;
    bool write_all;
    char *output_directory;
    char *summary_file_name;
    FILE *summary_file;
    char *log_file_name;
    FILE *lambda_stat_file;
    bool param_compression;
    FILE *statsfile_global;
    FILE *performance_file;
    char *file_content;

};

/// @brief Contains the configuration and state of the simulation
struct cats_configuration {


        bool quiet;
        struct cats_global *global;
        struct program_options command_line_options;
        bool debug_enabled;
        struct cats_ini *ini;

        struct cats_stats_registry stats_registry;

        // cats_stats
        cats_dt_population stats_populated_threshold;

        struct statistics global_stats;

        // actions
        struct action_function_registry action_functions;

        // links and vital rates
        //int rate_count;
        struct cats_vital_rate_hybrid_function vital_dependency_registry[MAX_VITAL_RATES];
        int link_count;




        bool all_species_same;

        struct cats_environment_registry environment_registry;

        struct cats_environment_collection environment;

        struct module_registry modules;

        cats_dt_population lambda_pop;
        int32_t lambda_burn_in_time;
        int32_t param_count;
        struct cats_species_param *param;


        // global
        struct simulation_geometry geometry;
        struct simulation_time time;

#ifdef USEMPI
        struct mpi_info mpi;
#endif
        struct cats_dispersal *dispersal;

        struct cats_overlays overlays;



        int32_t grid_count;


        // which actions happen once for each generation?
        int32_t generation_start;
        //int32_t generation_stop;

        //general
        struct simulation_info simulation;


        // control
        int32_t param_max_threads;
        char *run_name;

        // actions


        //enum simulation_mode mode;

        //int32_t generation_current;
        //int32_t generations_max;

        int64_t rng_seed;


        bool timing_info;

        struct cats_cycle cycle;

        //void (*pre_dispersal_func)(void *grid, struct cats_configuration *conf);
        //void (*post_dispersal_func)(void *grid, struct _configuration *conf);

        cats_dt_rates direct_scale_factor;
        cats_dt_rates lambda_at_OT; // FIXME, per parameter set

        enum cats_debug_flags debug;


        struct cats_output output;
};

void cleanup_configuration(struct cats_configuration **conf);

//typedef void (*fptr)(void *data, void *config);
typedef void (*grid_function)(struct cats_grid *grid, struct cats_configuration *conf);

/*
typedef void (*cell_function)(struct cats_grid *restrict grid, struct cats_configuration *conf, cats_dt_coord row,
                              cats_dt_coord col);
*/
#endif