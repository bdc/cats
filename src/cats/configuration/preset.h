

// SPDX-License-Identifier: GPL-3.0-or-later
//
// preset.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_PRESET_H
#define CATS_PRESET_H

#include "data/species_parameters.h"
#include "configuration.h"

void apply_vr_presets(struct cats_species_param *param);

void load_vr_preset(enum cats_species_preset form, struct cats_species_param_presets *preset,
                    struct cats_species_param *param, bool initial);

enum cats_species_preset get_species_preset_from_from_string(const char *string);

const char *get_preset_name(enum cats_species_preset preset);

#endif //CATS_PRESET_H
