// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_species_params.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "defaults.h"

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include <memory/cats_memory.h>
#include <cats_ini/cats_ini.h>
#include <logging/logging.h>

#include "configuration/configuration.h"
#include "load_configuration_dispersal.h"
#include "load_configuration.h"
#include "preset.h"
#include "load_vital_rates.h"


enum sexuality_type get_sexuality_from_string(const char *string)
{
        if (string && !strncmp(string, "sexual", 6)) return SEX_SEXUAL;
        return SEX_ASEXUAL;
}


void preload_default_vital_rates_for_presets(struct cats_configuration *conf, struct cats_ini *ini, char *species_section,
                                             struct cats_species_param *p)
{
        // load values that could be used for presets -- e.g. some presets could depend on these values
        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {
                struct cats_vital_rate *rl = &p->vital_rates[vr_idx];
                char *vr_name = rl->name;
                char *preload_vital_rate_maximum = compound_string(vr_name, "maximum", " ");
                bool req = !rl->preset.have_maximum; //!p->presets.vital_rates[vr_idx].have_vital_rate_maximum_preset;
                load_conf_value(req, ini, species_section, preload_vital_rate_maximum, &rl->max_rate);
                free(preload_vital_rate_maximum);
        }

}


void preload_default_vital_ages_for_presets(struct cats_configuration *conf, struct cats_ini *ini, char *species_section,
                                            struct cats_species_param *p)
{
        for (enum cats_vital_age_id age_id = VA_MIN + 1; age_id < VA_MAX; age_id++) {
                const char *age_name = get_vital_age_name(age_id);
                bool req = !p->presets.vital_ages[age_id].have_vital_age_preset;
                load_conf_value(req, ini, species_section, age_name, &p->vital_ages[age_id]);
        }
}


void load_conf_vital_ages(struct cats_configuration *conf, struct cats_ini *ini, char *species_section,
                          struct cats_species_param *p)
{
        for (enum cats_vital_age_id age_id = VA_MIN + 1; age_id < VA_MAX; age_id++) {
                const char *age_name = get_vital_age_name(age_id);
                bool req = !p->presets.vital_ages[age_id].have_vital_age_preset;

                bool have_age = load_conf_value(req, ini, species_section, age_name, &p->vital_ages[age_id]);
                if (req == false && have_age &&
                    p->vital_ages[age_id] != p->presets.vital_ages[age_id].vital_age_preset) {
                        log_message(LOG_WARNING, "%s: preset %s specifies %s with %d, but user specified value is %d",
                                    __func__, get_preset_name(p->presets.preset), age_name,
                                    p->presets.vital_ages[age_id].vital_age_preset,
                                    p->vital_ages[age_id]);
                }

        }
}


void load_conf_vital_rate(struct cats_vital_rate *vr, struct cats_configuration *conf, struct cats_ini *ini,
                          const char *species_section, struct cats_species_param *p)
{
        char *vr_name = vr->name;
        char *vital_rate_maximum = compound_string(vr_name, "maximum", " ");

        bool req = !vr->preset.have_maximum;

        log_message(LOG_INFO, "%s: Loading vital rate '%s' (required: %d)", __func__, vr->name, req);


        if (vr == &p->carrying_capacity) req = true;
        bool have_max = load_conf_value(req, ini, species_section, vital_rate_maximum, &vr->max_rate);
        if (vr->is_integer_quantity) {
                vr->max_rate = roundl(vr->max_rate);
        }
        if (req == false && have_max &&
            vr->max_rate != vr->preset.value_maximum && !isnan(vr->preset.value_maximum)) {
                log_message(LOG_WARNING, "%s: preset %s specifies %s with %f, but user specified value is %f",
                            __func__, get_preset_name(p->presets.preset), vital_rate_maximum,
                        //p->presets.vital_rates[vr_idx].vital_rate_maximum_preset,
                            (double) vr->preset.value_maximum,
                            (double) vr->max_rate);
        }

        free(vital_rate_maximum);
        conf_load_vital_rate_extended(vr, conf, ini, species_section, p);

}


void load_plant_species_parameter(struct cats_configuration *conf, struct cats_ini *ini,
                                  char *species_name, struct cats_species_param *p)
{

        char *preset_string = NULL;
        enum cats_species_preset preset = CATS_P_UNSPECIFIED;
        char *sexuality = NULL;

        bool have_preset = load_conf_value(false, ini, species_name, "preset", &preset_string);
        if (have_preset) {
                preset = get_species_preset_from_from_string(preset_string);
                free(p->preset_name);
                p->preset_name = strdup(preset_string);
                p->presets.preset = preset;
        }

        // load presets - don't read the actual values, but only check for which parameters we have a preset
        load_vr_preset(preset, &p->presets, p, true);
        preload_default_vital_rates_for_presets(conf, ini, species_name, p);
        preload_default_vital_ages_for_presets(conf, ini, species_name, p);

        load_conf_value(!p->presets.have_adult_cc_fraction_preset, ini, species_name, "maximum adult fraction of carrying capacity", &p->max_adult_cc_fraction);

        load_vr_preset(preset, &p->presets, p, false);

        apply_vr_presets(p);


        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {

                struct cats_vital_rate *rl = &p->vital_rates[vr_idx];
                load_conf_vital_rate(rl, conf, ini, species_name, p);

        }

        load_conf_vital_ages(conf, ini, species_name, p);


        // OPTIONAL
        bool have_max_adult_cc_fraction = load_conf_value(false, ini, species_name, "maximum adult fraction of carrying capacity", &p->max_adult_cc_fraction);

        if (have_max_adult_cc_fraction && p->presets.have_adult_cc_fraction_preset
            && p->max_adult_cc_fraction != p->presets.adult_cc_fraction_preset) {
                log_message(LOG_WARNING,
                            "%s: preset %s specifies 'maximum adult fraction of carrying capacity = %f', but user specified value is %f",
                            __func__, get_preset_name(preset), (double) p->presets.adult_cc_fraction_preset,
                            (double) p->max_adult_cc_fraction);
        }

        load_conf_value(false, ini, species_name, "hapaxanthy", &p->hapaxanthy);
        load_conf_value(false, ini, species_name, "sexuality", &sexuality);
        p->sexuality = get_sexuality_from_string(sexuality);
        free(preset_string);
        free(sexuality);
}


void load_config_species_parameter(struct cats_configuration *conf, struct cats_ini *ini, int species_idx,
                                   char *species_section)
{
        struct cats_species_param *p = &conf->param[species_idx];

        p->species_name = remove_0th_token(species_section, ":");
        p->species_config_section = strdup(species_section);
        p->demographic_slope = DEFAULT_DEMOGRAPHIC_SLOPE;
        char *demographic_module = NULL;
        p->default_demographics = true;
        bool have_demo_module = load_conf_value(false, ini, species_section, "demographic module", &demographic_module);
        if (have_demo_module) {
                p->default_demographics = false;
                for (int32_t i = 0; i < conf->modules.found_count; i++) {
                        struct cats_module *module = &conf->modules.module[i];
                        if (demographic_module != NULL && module != NULL && module->name &&
                            strcmp(demographic_module, module->name) == 0) {
                                p->demographic_module = module;
                                p->custom_vital_rates = true;
                                p->custom_vital_ages = true;
                                p->custom_dispersal = true;
                                break;
                        }
                }
                if (p->demographic_module == NULL) {
                        log_message(LOG_ERROR, "%s: could not find demographic module '%s' for '%s' ", __func__,
                                    demographic_module, species_section);
                        exit_cats(EXIT_FAILURE);
                }
        }
        free(demographic_module);


        // CHECK LIFE TYPE


        // check simulation mode
        bool have_suitability = load_conf_value(false, ini, species_section, "suitability", &p->suitability_name);

        if (have_suitability) {
                p->parametrization = PARAM_HYBRID;
                log_message(LOG_IMPORTANT, "%s: suitability found: using suitability parametrization mode", __func__);
        } else {
                p->parametrization = PARAM_DIRECT_VITAL_RATE;
                log_message(LOG_IMPORTANT, "%s: no suitability found: using direct parametrization mode", __func__);
        }

        bool need_ZT_OT = true;
        bool need_ini_pop = true;

        if (conf->command_line_options.calculate_lambda_quit) {
                need_ZT_OT = false;
        }

        if (conf->command_line_options.no_input_rasters_required) {
                need_ini_pop = false;
        }


        if (p->parametrization == PARAM_HYBRID) {


                load_conf_value(need_ZT_OT, ini, species_section, "OT", &p->OT);
                load_conf_value(need_ZT_OT, ini, species_section, "ZT", &p->ZT);

                if (need_ZT_OT && p->ZT >= p->OT) {
                        log_message(LOG_ERROR, "ZT %f >= OT %f, ZT must be lower than OT!", (double) p->ZT, (double) p->OT);
                        exit_cats(EXIT_FAILURE);
                }

                char *ts = NULL;
                bool ts_loaded = load_conf_value(false, ini, species_section, "initial population suitability threshold",
                                                 &ts);
                if (ts_loaded) {
                        set_suitability_from_string(&p->initial_population.suitability_threshold, p, ts);
                }

                load_conf_value(false, ini, species_section, "increase initial population to carrying capacity",
                                &p->initial_population.set_to_cc);
                free(ts);
        }

        load_conf_value(need_ini_pop, ini, species_section, "initial population filename", &p->initial_population.filename);  //VR::type=string (filename), req=true*::CONF:species:initial population filename
        load_conf_value(false, ini, species_section, "save lambda grid", &p->save_lambda_grid); //VR::type=boolean,req=false,default=false::CONF:species:save lambda grid
        load_conf_value(false, ini, species_section, "save lambda grid density", &p->save_lambda_grid_density); //VR:type=boolean,req=false,default=false::CONF:species:save lambda grid density
        load_conf_value(false, ini, species_section, "demographic slope", &p->demographic_slope); //VR::type=numeric,default=DEFAULT_DEMOGRAPHIC_SLOPE,req=false::CONF:species:demographic slope
        // used to be hard coded to 20
        // END OF ALL

        load_conf_vital_rate(&p->carrying_capacity, conf, ini, species_section, p);

        if (p->custom_vital_rates == true) {
                log_message(LOG_IMPORTANT, "skipping loading of default vital rates for %s", species_section);
        }
        if (p->default_demographics == true && p->custom_vital_rates == false) {
                load_plant_species_parameter(conf, ini, species_section, p);
        }


        for (int32_t i = 0; i < conf->modules.count; i++) {
                printf("%s: %d\n", __func__, i);
                if (p->module_data[i].load_species_param_function) {
                        p->module_data[i].load_species_param_function(conf, ini, species_section, p);
                }
        }

}


void load_parameter_sets_all_same(struct cats_configuration *conf, struct cats_ini *ini, int species_idx, char *name)
{
        assert(species_idx >= 0);
        char *species_name = strdup(conf->param[0].species_name);
        conf->param[0].species_name_actual = strdup(species_name);

        for (int i = 1; i < conf->param_count; i++) {
                load_config_species_parameter(conf, ini, i, name);
                conf->param[i].species_name_actual = strdup(species_name);
                free(conf->param[i].species_name);
                int count = snprintf(NULL, 0, "%s%d", conf->param[i].species_name, i + 1) + 1;
                conf->param[i].species_name = malloc_or_die(count);
                snprintf(conf->param[i].species_name, count, "%s%d", conf->param[i].species_name_actual, i + 1);
        }

        free(species_name);
}


void load_all_species(struct cats_configuration *conf, struct cats_ini *ini)
{
        struct string_array *species = get_sections_with_prefix(ini, "species");

        for (int32_t i = 0; i < species->count; i++) {
                char *species_section = species->string[i];
                load_config_species_parameter(conf, ini, i, species_section);

                if (conf->all_species_same && i == 0) {
                        load_parameter_sets_all_same(conf, ini, i, species_section);
                }

                char *dispersal_name = NULL;
                if (conf->param[i].custom_dispersal == false) {
                        load_conf_value(true, ini, species_section, "dispersal", &dispersal_name);
                } else {
                        log_message(LOG_IMPORTANT, "skipping loading of dispersal for %s", species_section);
                        continue;
                }


                bool loaded = load_dispersal_info(conf, ini, i, dispersal_name);

                if (loaded && conf->all_species_same && i == 0) {
                        for (int j = 1; j < conf->param_count; j++) {
                                load_dispersal_info(conf, ini, j, dispersal_name);
                        }
                }

                free(dispersal_name);
                dispersal_name = NULL;

                if (!conf->all_species_same) {
                        conf->param[i].species_name_actual = strdup(conf->param[i].species_name);
                }
        }

        free_string_array(&species);
}
