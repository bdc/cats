// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_vital_rates.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#ifndef CATS_LOAD_VITAL_RATES_H
#define CATS_LOAD_VITAL_RATES_H

#include "configuration.h"

void update_hybrid_density_thresholds(struct cats_configuration *conf);

void conf_load_vital_rate_extended(struct cats_vital_rate *vr, struct cats_configuration *conf, struct cats_ini *ini,
                                   const char *species_section, struct cats_species_param *param);

#endif //CATS_LOAD_VITAL_RATES_H
