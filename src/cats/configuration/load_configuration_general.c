
// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_general.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <stdbool.h>
#include "cats/command_line/command_line_options.h"
#include "configuration/load_configuration_general.h"
#include <string.h>


void verify_run_name(const char *name, const char *value)
{
        bool error = false;
        int forbidden[] = {'/',};
        int32_t entries = sizeof(forbidden) / sizeof(forbidden[0]);
        for (int32_t entry = 0; entry < entries; entry++) {
                int f = forbidden[entry];
                if (value == NULL) break;
                if (strchr(value, f) != NULL) {
                        log_message(LOG_ERROR, "configuration parameter '%s': '%s' contains illegal character '%c'",
                                    name, value, f);
                        error = true;
                }
        }

        if (error) {
                log_message(LOG_ERROR, "Invalid configuration parameter value found");
                exit_cats(EXIT_FAILURE);
        }
}


void load_general_info(struct cats_configuration *conf, struct cats_ini *ini)
{
        // required
        load_conf_value(true, ini, "general", "run name", &conf->run_name);
        verify_run_name("run_name", conf->run_name);

        //optional
        load_conf_value(false, ini, "general", "timing info", &conf->timing_info);
        char *od;
        bool have_od = load_conf_value(false, ini, "general", "output directory", &od);
        if (have_od) {
                free(conf->output.output_directory);
                conf->output.output_directory = od;
        }

        load_conf_value(false, ini, "general", "debug", &conf->debug_enabled);
        load_conf_value(false, ini, "general", "compression", &conf->output.param_compression);

        load_conf_value(false, ini, "general", "write all", &conf->output.write_all);
        load_conf_value(false, ini, "general", "file format", &conf->geometry.file_format);
        load_conf_value(false, ini, "general", "ignore raster projection", &conf->geometry.ignore_raster_projection);
        load_conf_value(false, ini, "general", "threads", &conf->param_max_threads);
        load_conf_value(false, ini, "general", "stats populated threshold", &conf->stats_populated_threshold);

        float poisson_dampening_factor, poisson_dampening_minimum, poisson_maximum;
        bool poisson_maximum_draw_diff;
        bool have_damp = load_conf_value(false, ini, "general", "poisson dampening factor", &poisson_dampening_factor);
        bool have_mini = load_conf_value(false, ini, "general", "poisson dampening minimum",
                                         &poisson_dampening_minimum);
        bool have_maxi = load_conf_value(false, ini, "general", "poisson maximum", &poisson_maximum);
        bool have_diff = load_conf_value(false, ini, "general", "poisson maximum draw difference",
                                         &poisson_maximum_draw_diff);

        if (have_damp && conf->command_line_options.have_poisson_dampening_factor) {
                log_message(LOG_WARNING,
                            "option 'poisson dampening factor' specified on command line takes precedence over configuration file");
        } else if (have_damp) {
                if (poisson_dampening_factor >= 0.0f || global.poisson_dampening_factor <= 1.0f) {

                        global.poisson_dampening_factor = poisson_dampening_factor;
                } else {
                        log_message(LOG_ERROR, "option 'poisson dampening factor': %f outside allowed range [0, 1]",
                                    poisson_dampening_factor);
                }
        }

        if (have_mini && conf->command_line_options.have_poisson_dampening_minimum) {
                log_message(LOG_WARNING,
                            "option 'poisson dampening minimum' specified on command line takes precedence over configuration file");
        } else if (have_mini) {
                if (poisson_dampening_minimum >= 0.0f) {
                        global.poisson_dampening_minimum = poisson_dampening_minimum;
                } else {
                        log_message(LOG_ERROR,
                                    "option 'poisson dampening minimum' %f outside allowed range [0, Inf),",
                                    poisson_dampening_minimum);
                        exit_cats(EXIT_FAILURE);

                }
        }

        if (have_maxi && conf->command_line_options.have_poisson_maximum) {
                log_message(LOG_WARNING,
                            "option 'poisson maximum' specified on command line takes precedence over configuration file");
        } else if (have_maxi) {
                if (poisson_maximum >= 0.0f) {
                        global.poisson_maximum = poisson_maximum;
                } else {
                        log_message(LOG_ERROR,
                                    "option 'poisson maximum' %f outside allowed range [0, Inf),",
                                    poisson_maximum);
                        exit_cats(EXIT_FAILURE);

                }
        }

        if (have_diff && conf->command_line_options.have_poisson_maximum_draw_diff) {
                log_message(LOG_WARNING, "option %s specified on command line takes precedence over configuration file",
                            OPTION_POISSON_MAX_DRAW_DIFF_NAME);
        } else if (have_diff) {
                global.poisson_maximum_draw_diff = poisson_maximum_draw_diff;
        }

}