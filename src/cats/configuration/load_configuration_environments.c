// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_environments.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <string.h>
#include "environment/environment_registry.h"
#include "load_configuration_environments.h"
#include "environment/environment_set.h"
#include "configuration/load_configuration_glm.h"


enum environment_variable_type get_environment_variable_type_from_string(const char *string)
{
        assert(string != NULL);
        assert(strlen(string) > 0);
        if (strcmp(string, "predictor") == 0) return ENV_VAR_PREDICTOR;
        if (strcmp(string, "suitability") == 0) return ENV_VAR_SUITABILITY;
        if (strcmp(string, "direct rate") == 0) return ENV_VAR_DIRECT;
        if (strcmp(string, "constant") == 0) return ENV_VAR_CONSTANT;

        log_message(LOG_ERROR, "%s: unknown environment variable type '%s', valid values: 'predictor', 'suitability'", __func__, string);
        exit_cats(EXIT_FAILURE);

}


void add_environment_variable_from_conf(struct cats_configuration *conf, struct cats_ini *ini, char *environment_section)
{
        struct cats_dimension *dimension = &conf->geometry.dimension;
        char *name = remove_0th_token(environment_section, ":");

        if (conf->command_line_options.no_input_rasters_required) {
                struct cats_environment *set = add_environment(conf, name, ENVIRONMENT_TYPE_SUITABILITY, NULL);
                struct cats_environment_variable *env = add_environment_to_registry(&conf->environment_registry, "suitability", ENV_VAR_SUITABILITY,
                                                                                    0, 0, "None",
                                                                                    dimension);
                add_to_environment(set, env);
                return;
        }




        bool enabled = true;
        load_conf_value(false, ini, environment_section, "enabled", &enabled);
        if (enabled == false) {
                free(name);
                return;
        }

        //char *type_name = get_nth_token(section_name, ":", 0);

        char *type_name;
        load_conf_value(true, ini, environment_section, "type", &type_name);

        int32_t interpolation = 0;
        int32_t reload = 0;
        char *filename_pattern = NULL;
        bool have_interp = load_conf_value(false, ini, environment_section, "interpolation interval", &interpolation);
        bool have_reload = load_conf_value(false, ini, environment_section, "replacement interval", &reload);

        if (have_reload > 0 && have_interp > 0) {
                log_message(LOG_ERROR,
                            "%s: section [%s]: only one of the interpolation_interval or reload_interval can be specified",
                            __func__, environment_section);

        }
        load_conf_value(true, ini, environment_section, "filename pattern", &filename_pattern);


        enum environment_variable_type type = get_environment_variable_type_from_string(type_name);

        struct cats_environment_variable *env = add_environment_to_registry(&conf->environment_registry, name, type,
                                                                            interpolation, reload, filename_pattern,
                                                                            dimension);


        if (type == ENV_VAR_SUITABILITY) {
                struct cats_environment *set = add_environment(conf, name, ENVIRONMENT_TYPE_SUITABILITY, NULL);
                load_conf_value(false, ini, environment_section, "save environment", &set->save_environment);
                load_conf_value(false, ini, environment_section, "suitability divisor", &env->divisor);
                add_to_environment(set, env);
        }
        if (type == ENV_VAR_CONSTANT) {
                struct cats_environment *set = add_environment(conf, name, ENVIRONMENT_TYPE_CONSTANT, NULL);
                load_conf_value(false, ini, environment_section, "save environment", &set->save_environment);
                load_conf_value(true, ini, environment_section, "value", &env->constant_value);
                add_to_environment(set, env);
        }
        if (type == ENV_VAR_DIRECT) {
                struct cats_environment *set = add_environment(conf, name, ENVIRONMENT_TYPE_DIRECT, NULL);
                load_conf_value(false, ini, environment_section, "save environment", &set->save_environment);
                add_to_environment(set, env);
        }

        free(type_name);
        free(filename_pattern);
        free(name);
}


void load_environments_configuration(struct cats_configuration *conf, struct cats_ini *ini)
{
        struct string_array *env_variables = get_sections_with_prefix(ini, "environment");
        //struct string_array *suitability_variables = get_sections_with_prefix(ini, "suitability");
        struct string_array *glms = get_sections_with_prefix(ini, "glm");


        for (int32_t i = 0; i < env_variables->count; i++) {
                add_environment_variable_from_conf(conf, ini, env_variables->string[i]);
        }


        for (int32_t i = 0; i < glms->count; i++) {
                add_glm_from_conf(conf, ini, glms->string[i]);
        }


        free_string_array(&env_variables);
        free_string_array(&glms);
}
