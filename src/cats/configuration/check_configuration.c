// SPDX-License-Identifier: GPL-3.0-or-later
//
// check_configuration.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "configuration/configuration.h"
#include "logging.h"
#include "vital_rates/vital_rate_ranges.h"
#include "inline_vital_rates.h"
#include "dispersal/dispersal.h"
#include "data/simulation_geometry.h"


int check_string(char *value, char *name)
{
        int error = 0;
        if (!value || strlen(value) < 1) {
                log_message(LOG_ERROR, "parameter '%s' must be set!", name);
                error = 1;
        }
        return error;
}


int32_t int_range_failed(int32_t value, int32_t min_value, int32_t max_value, char *name)
{
        if (value >= min_value && value <= max_value) return 0;
        log_message(LOG_ERROR, "%s: %d is out of range [%d, %d]", name, value, min_value, max_value);
        return 1;
}


int rate_failed(cats_dt_rates value, char *name)
{
        if (value > 0.0 && value <= 1.0) return 0;
        log_message(LOG_ERROR, "%s: %f is out of range (1, 0)", name, (double) value);
        return 1;
}


int range_float_check_at_least(cats_dt_rates value, cats_dt_rates larger_than, char *name)
{
        if (value < larger_than) {

                log_message(LOG_ERROR, "%s out of range: is %Lf, has to be >= %Lf", name, value, larger_than);
                return 1;
        }

        return 0;

}


int range_float_check_range_inclusive(cats_dt_rates value, cats_dt_rates lower, cats_dt_rates upper, char *name)
{
        if (value < lower || value > upper || isnan(value)) {
                log_message(LOG_ERROR, "%s out of range [%f, %f]: %f", name, (double) lower, (double) upper, (double) value);
                return 1;
        }

        return 0;
}


int range_float_check_larger_than(cats_dt_rates value, cats_dt_rates larger_than, char *name)
{
        if (value <= larger_than || isnan(value)) {
                log_message(LOG_ERROR, "%s out of range: is %f, has to be > %f", name, (double) value, (double) larger_than);
                return 1;
        }
        return 0;
}


int check_geometry(struct simulation_geometry *geometry)
{
        int error = 0;
        error += int_range_failed(geometry->dimension.rows, 0, CATS_MAX_COORD, "grid rows");
        error += int_range_failed(geometry->dimension.cols, 0, CATS_MAX_COORD, "grid columns");
        return error;
}


int check_hybrid_parameters(struct cats_species_param *param)
{
        int error = 0;
        if (param->parametrization == PARAM_HYBRID) {
                error += rate_failed(param->OT, "OT");
                error += rate_failed(param->ZT, "ZT");
        }

        error += range_float_check_larger_than(param->demographic_slope, 0.0, "demographic slope");
        return error;
}


int check_plant_vital_ages(struct cats_species_param *param)
{
        if (param->custom_vital_ages == true) return 0;
        if (param->default_demographics == false) return 0;

        int error = 0;

        int32_t seed_persistence = get_vital_age_from_param(param, VA_SEED_PERSISTENCE);
        int32_t min_age_of_maturity = get_vital_age_from_param(param, VA_AGE_OF_MATURITY_MIN);
        int32_t max_age_of_maturity = get_vital_age_from_param(param, VA_AGE_OF_MATURITY_MAX);

        error += int_range_failed(seed_persistence, 1, INT32_MAX,
                                  "seed persistence");

        if (min_age_of_maturity < 0 || max_age_of_maturity < 0) {
                log_message(LOG_ERROR,
                            "minimum (%d) and maximum (%d) age of maturity must be greater than 0",
                            min_age_of_maturity, max_age_of_maturity);
                error += 1;
        }
        if (max_age_of_maturity < min_age_of_maturity) {
                log_message(LOG_ERROR, "minimum age of maturity %d > maximum age of maturity %d",
                            min_age_of_maturity, max_age_of_maturity);
                error += 1;
        }

        return error;
}


int check_species_name(struct cats_species_param *param)
{
        int error = 0;
        error += check_string(param->species_name, "species name");
        return error;
}


int check_plant_dispersal(struct cats_dispersal *dispersal) // TODO EXTEND TO CHECK OTHER TESTS
{
        int error = 0;
        error += range_float_check_range_inclusive(dispersal->local_dispersal, 0.0, 1.0, "self_dispersal");
        return error;
}


int check_plant_vital_rates(struct cats_species_param *param)
{
        int error = 0;
        error += check_vital_rate_all(&param->carrying_capacity);
        if (param->parametrization == PARAM_HYBRID) {
                error += check_vital_rate_hybrid(&param->carrying_capacity);
        }

        if (param->custom_vital_rates == true) return error;
        for (enum cats_vital_rate_id i = VR_MIN + 1; i < VR_MAX; i++) {

                error += check_vital_rate_all(&param->vital_rates[i]);
                if (param->parametrization == PARAM_HYBRID) {
                        error += check_vital_rate_hybrid(&param->vital_rates[i]);
                }

        }
        return error;
}


int verify_configuration(struct cats_configuration *conf)
{
        int error = 0;

        error += check_geometry(&conf->geometry);

        for (int i = 0; i < conf->param_count; i++) {
                error += check_plant_vital_ages(&conf->param[i]);
                // error += check_carrying_capacity_max(&conf->param[i]); FIXME FIXME
                error += check_species_name(&conf->param[i]);
                error += check_plant_dispersal(&conf->dispersal[i]);
                error += check_plant_vital_rates(&conf->param[i]);
        }

        if (error > 0) {
                log_message(LOG_ERROR, "Found %d errors in configuration file '%s'", error,
                            conf->command_line_options.configuration_file);
                log_message(LOG_ERROR, "Exiting.");
                exit(EXIT_FAILURE);
        }

        return error;
}
