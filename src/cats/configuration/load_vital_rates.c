// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_vital_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <math.h>
#include <string.h>

#include <cats_ini/cats_ini.h>

#include "environment/environment_set.h"
#include "vital_rates/setup_rates.h"

#include "preset.h"
#include "load_vital_rates.h"


enum cats_density_dependence get_vital_density_from_string(char *string)
{
        if (string == NULL || strlen(string) == 0) {
                log_message(LOG_ERROR, "%s: invalid density dependence '%s'", __func__, "");
                exit_cats(EXIT_FAILURE);
        }

        char *copy = strdup(string);
        copy = left_trim(right_trim(copy));
        if (copy == NULL || strlen(copy) == 0) {
                log_message(LOG_ERROR, "%s: invalid density dependence '%s'", __func__, "");
                exit_cats(EXIT_FAILURE);
        }


        bool result;
        bool valid = string_to_bool(copy, &result);
        if (!valid) {

                log_message(LOG_ERROR, "%s: could not parse string '%s'", __func__, copy);
                exit_cats(EXIT_FAILURE);
        }
        free(copy);
        if (result) {
                return DENSITY_DEP_NEGATIVE;
        } else {
                return NO_DENSITY_DEP;
        }

}


void conf_load_vital_rates_density(struct cats_vital_rate *vr, struct cats_configuration *conf, struct cats_ini *ini,
                                   const char *species_section, struct cats_species_param *param)
{
        char *vr_name = vr->name;
        //const char *species_name = param->species_name;
        const char *sep = " ";

        char *density_value = NULL;
        char *vital_rate_density = compound_string(vr_name, "density", sep);
        char *vital_rate_density_threshold = compound_string(vr_name, "density threshold", sep);


        bool have_dens = load_conf_value(false, ini, species_section, vital_rate_density,
                                         &density_value);
        if (have_dens) {
                set_vital_density(vr, get_vital_density_from_string(density_value));
                set_vital_density_from_string(vr, density_value);
        }

        cats_dt_rates density_ts = NAN;
        bool have_density_ts = load_conf_value(false, ini, species_section, vital_rate_density_threshold, &density_ts);
        if (have_density_ts) {
                set_vital_density_ts(vr, density_ts);
        }

        free(vital_rate_density);
        free(vital_rate_density_threshold);
        free(density_value);
}


void conf_load_vital_rate_environment(struct cats_vital_rate *vr, struct cats_configuration *conf, struct cats_ini *ini,
                                      const char *species_section, struct cats_species_param *param)
{
        char *vr_name = vr->name;
        const char *species_name = param->species_name;
        const char *sep = " ";

        char *vital_rate_environment = compound_string(vr_name, "environment", sep);
        char *vital_rate_environment_multiplier = compound_string(vr_name, "environment multiplier", sep);
        char *environment_value = NULL;

        load_conf_value(true, ini, species_section, vital_rate_environment, &environment_value);
        bool have_multi = load_conf_value(false, ini, species_section, vital_rate_environment_multiplier,
                                          &vr->environment_multiplier);
        if (have_multi) {

        }
        vr->environment_set = get_environment(conf, environment_value);

        log_message(LOG_IMPORTANT,
                    "using environment '%s' for vital rate '%s' of species '%s'",
                    environment_value, vr_name, species_name);

        free(vital_rate_environment);
        free(environment_value);
        free(vital_rate_environment_multiplier);
}


void conf_load_vital_rate_hybrid_information(struct cats_vital_rate *vr, struct cats_configuration *conf,
                                             struct cats_ini *ini, const char *species_section,
                                             struct cats_species_param *param)
{
        char *vr_name = vr->name;
        const char *species_name = param->species_name;
        const char *sep = " ";

        char *vital_rate_function = compound_string(vr_name, "function", sep);
        char *vital_rate_suitability_cutoff = compound_string(vr_name, "suitability cutoff", sep);
        char *vital_rate_minimum = compound_string(vr_name, "minimum", sep);
        char *function_name = NULL;
        char *cutoff_value = NULL;
        char *vr_minimum_value = NULL;

        bool have_minimum = load_conf_value(false, ini, species_section, vital_rate_minimum,
                                            &vr_minimum_value); // VR::ignored::
        free(vital_rate_minimum);
        free(vr_minimum_value);

        load_conf_value(false, ini, species_section, vital_rate_function,
                        &function_name); // VR::type=string (link function),mode=hybrid::CONF:species:<VITAL RATE>:function

        if (function_name) {
                enum cats_rate_function_type func_type = get_function_type(conf,
                                                                           function_name);
                char *link_name = conf->vital_dependency_registry[func_type].short_name;
                log_message(LOG_IMPORTANT,
                            "'%s': changing link function for '%s' from '%s' to '%s'",
                            species_name,
                            vr_name, vr->func->short_name, link_name);
                vr->func = &conf->vital_dependency_registry[func_type];
                if (vr->func->type == LINK_CONSTANT && have_minimum) {
                        log_message(LOG_WARNING,
                                    "%s: species '%s' vital rate '%s: vital rate minimum is ignored when using '%s' link function', ",
                                    __func__, species_name, vr_name, vr->func->short_name);
                }
        }


        bool have_cutoff = load_conf_value(false, ini, species_section, vital_rate_suitability_cutoff,
                                           &cutoff_value); // VR::type=numeric|ZT|OT,mode=hybrid::CONF:<VITAL RATE>:suitability cutoff
        if (have_cutoff) {
                set_vital_cutoff_from_string(vr, param, cutoff_value);
        }

        free(vital_rate_function);
        free(vital_rate_suitability_cutoff);
        free(function_name);
        free(cutoff_value);

}


void conf_load_vital_rate_scale_information(struct cats_vital_rate *vr, struct cats_configuration *conf,
                                            struct cats_ini *ini, const char *species_section,
                                            struct cats_species_param *param)
{
        char *vr_name = vr->name;
        //const char *species_name = param->species_name;
        const char *sep = " ";

        char *vital_rate_scale_factor = compound_string(vr_name, "scale factor", sep);
        char *vital_rate_minimum_scale_factor = compound_string(vr_name, "minimum scale factor", sep);


        bool have_min_scale_preset = vr->preset.have_minimum_scale_factor;//p->presets.vital_rates[vr_idx].have_rate_minimum_scale_factor_preset;
        bool have_rate_scale = false;
        bool have_rate_scale_min = false;

        have_rate_scale = load_conf_value(false, ini, species_section, vital_rate_scale_factor, &vr->rate_scale_factor);
        have_rate_scale_min = load_conf_value(false, ini, species_section, vital_rate_minimum_scale_factor,
                                              &vr->rate_minimum_scale_factor);

        if (have_rate_scale && have_rate_scale_min) {
                log_message(LOG_ERROR, "%s: cannot both specify '%s' and '%s'",
                            __func__, vital_rate_scale_factor,
                            vital_rate_minimum_scale_factor);
                exit_cats(EXIT_FAILURE);
        }

        if (have_min_scale_preset && have_rate_scale) {
                log_message(LOG_WARNING,
                            "%s: user specified value '%s = %f' takes precedence over value '%s minimum scale factor = %f' "
                            "specified by preset '%s'",
                            __func__, vital_rate_scale_factor,
                            (double) vr->rate_scale_factor, vr_name,
                        //p->presets.vital_rates[vr_idx].rate_minimum_scale_factor_preset,
                            (double) vr->preset.value_minimum_scale_factor,
                            param->preset_name);
                vr->rate_minimum_scale_factor = -1.0;
        }

        if (vr->preset.have_minimum_scale_factor &&
            //p->presets.vital_rates[vr_idx].have_rate_minimum_scale_factor_preset &&
            have_rate_scale_min &&
            vr->rate_minimum_scale_factor !=
            //p->presets.vital_rates[vr_idx].rate_minimum_scale_factor_preset
            vr->preset.value_minimum_scale_factor
                ) {
                log_message(LOG_WARNING, "%s: user specified value '%s' is %f, but "
                                         "preset '%s' is %f ",
                            __func__, vital_rate_minimum_scale_factor,
                            (double) vr->rate_minimum_scale_factor,
                            get_preset_name(param->presets.preset),
                        //p->presets.vital_rates[vr_idx].rate_minimum_scale_factor_preset
                            (double) vr->preset.value_minimum_scale_factor);
        }

        if (have_rate_scale_min || have_rate_scale)
                vr->have_rate_non_default_scale_factor = true;


        free(vital_rate_scale_factor);
        free(vital_rate_minimum_scale_factor);

}


void conf_load_vital_rate_extended(struct cats_vital_rate *vr, struct cats_configuration *conf, struct cats_ini *ini,
                                   const char *species_section, struct cats_species_param *param)
{
        char *vr_name = vr->name;
        const char *sep = " ";

        conf_load_vital_rates_density(vr, conf, ini, species_section, param);

        char *vital_rate_minimum = compound_string(vr_name, "minimum", sep);
        cats_dt_rates min_rate;
        load_conf_value(false, ini, species_section, vital_rate_minimum, &min_rate);
        set_vital_rate_minimum(vr, min_rate);
        free(vital_rate_minimum);

        switch (param->parametrization) {
                case PARAM_UNDEFINED:
                        break;
                case PARAM_HYBRID:
                        conf_load_vital_rate_hybrid_information(vr, conf, ini, species_section, param);

                        conf_load_vital_rate_scale_information(vr, conf, ini, species_section, param);
                        break;
                case PARAM_DIRECT_VITAL_RATE:
                        conf_load_vital_rate_environment(vr, conf, ini, species_section, param);
                        break;
        }
}


void update_vital_rate_hybrid_density_thresholds(struct cats_vital_rate *vr, struct cats_configuration *conf,
                                                 struct cats_ini *ini, const char *species_section,
                                                 struct cats_species_param *param)
{
        const char *sep = " ";
        const char *species_name = param->species_name;


        const char *vr_name = vr->name;
        char *vital_rate_density = compound_string(vr_name, "density", sep);
        char *vital_rate_density_threshold = compound_string(vr_name, "density threshold", sep);
        char *density_value = NULL;

        bool have_dens = load_conf_value(false, ini, species_section, vital_rate_density, &density_value);

        cats_dt_rates density_ts = NAN;
        bool have_density_ts = load_conf_value(false, ini, species_section, vital_rate_density_threshold, &density_ts);
        if (have_dens == true && have_density_ts == false) {
                set_vital_density(vr, param->scale_factor);
                log_message(LOG_IMPORTANT,
                            "species '%s': setting density threshold for '%s' to scale factor %f (no default found)",
                            species_name, vr->name, (double) param->scale_factor);
        }

        free(vital_rate_density);
        free(vital_rate_density_threshold);
        free(density_value);
}


// this function is only called in hybrid mode, after the scale factor has been calculated.
// for all vital rates that are density-dependent and have no threshold specified, replace
// set the threshold with the scale factor

void update_hybrid_density_thresholds(struct cats_configuration *conf)
{
        struct cats_ini *ini = conf->ini;
        for (int32_t sp_idx = 0; sp_idx < conf->param_count; sp_idx++) {
                struct cats_species_param *param = &conf->param[sp_idx];
                const char *section_name = conf->param[sp_idx].species_config_section;
                for (int32_t i = VR_MIN + 1; i < VR_MAX; i++) {

                        struct cats_vital_rate *vr = &conf->param[sp_idx].vital_rates[i];
                        update_vital_rate_hybrid_density_thresholds(vr, conf, ini, section_name, param);
                }
        }
}