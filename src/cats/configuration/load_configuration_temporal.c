// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_temporal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include "configuration/configuration.h"
#include "load_configuration_temporal.h"


void load_temporal_info(struct cats_configuration *conf, struct cats_ini *ini)
{
        // required
        load_conf_value(true, ini, "general", "time steps", &conf->time.simulation_length);
        load_conf_value(true, ini, "general", "output interval", &conf->time.output_interval);
        load_conf_value(true, ini, "general", "starting year", &conf->time.year_start);

        // optional
        load_conf_value(false, ini, "general", "always include sign in year", &conf->time.always_show_sign);
        load_conf_value(false, ini, "general", "zero pad year digits", &conf->time.zero_pad_to_digits);
        bool have_burn_in = load_conf_value(false, ini, "general", "burn-in phase length", &conf->time.burn_in_phase_length);

        if (conf->time.burn_in_phase_length > 0) {
                load_conf_value(have_burn_in, ini, "general", "burn-in phase year", &conf->time.burn_in_phase_year);
        }

        load_conf_value(false, ini, "general", "warm-up phase year", &conf->time.warm_up_phase_year);
}
