// SPDX-License-Identifier: GPL-3.0-or-later
//
// preset.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "preset.h"
#include "vital_ages/default_vital_ages.h"
#include "vital_rates/default_vital_rates.h"
#include "inline_vital_rates.h"


enum cats_species_preset get_species_preset_from_from_string(const char *string)
{
        if (string == NULL || strlen(string) == 0) return CATS_P_UNSPECIFIED;
        if (!strncasecmp(string, "annual", 6)) return CATS_P_ANNUAL;
        if (!strncasecmp(string, "biennial", 7)) return CATS_P_BIENNIAL;
        if (!strncasecmp(string, "tree", 4)) return CATS_P_TREE;
        if (!strncasecmp(string, "other", 4)) return CATS_P_UNSPECIFIED;
        log_message(LOG_WARNING,
                    "%s: unknown preset '%s'. Allowed presets: annual, biennial, tree, other. Defaulting to other (no preset).",
                    __func__, string);
        return CATS_P_UNSPECIFIED;
}


const char *get_preset_name(enum cats_species_preset preset)
{
        switch (preset) {
                case CATS_P_UNSPECIFIED:
                        return "default";
                case CATS_P_ANNUAL:
                        return "annual";
                case CATS_P_BIENNIAL:
                        return "biennial";
                case CATS_P_TREE:
                        return "tree";
                default:
                        return "other (unspecified)";
        }
}


void
set_vital_age_preset(struct cats_species_param_presets *preset, enum cats_vital_age_id id, int32_t value, bool initial)
{
        assert(preset != NULL);
        assert(value >= 0);
        if (initial == false) preset->vital_ages[id].vital_age_preset = value;
        preset->vital_ages[id].have_vital_age_preset = true;
}


void preset_set_vital_rate_maximum(struct cats_vital_rate *vr, cats_dt_rates value, bool initial)
{
        assert(vr != NULL);
        assert(value >= 0.0);
        if (initial == false) vr->preset.value_maximum = value;
        vr->preset.have_maximum = true;
}


void preset_set_vital_rate__minimum_scale_factor(struct cats_vital_rate *vr, cats_dt_rates value, bool initial)
{
        assert(vr != NULL);
        assert(value >= 0.0);
        if (initial == false) vr->preset.value_minimum_scale_factor = value;
        vr->preset.have_minimum_scale_factor = true;
}


void set_adult_cc_fraction_preset(struct cats_species_param_presets *preset, cats_dt_rates value, bool initial)
{
        assert(value >= 0);
        assert(value <= 1.0);
        assert(preset != NULL);

        if (initial == false) preset->adult_cc_fraction_preset = value;
        preset->have_adult_cc_fraction_preset = true;

}


void load_vr_preset(enum cats_species_preset form, struct cats_species_param_presets *preset,
                    struct cats_species_param *param, bool initial)
{

        cats_dt_rates min_scale;
        int32_t mat_max;
        switch (form) {
                case CATS_P_ANNUAL:
                        set_vital_age_preset(preset, VA_AGE_OF_MATURITY_MIN, 0, initial);
                        set_vital_age_preset(preset, VA_AGE_OF_MATURITY_MAX, 0, initial);
                        preset_set_vital_rate_maximum(&param->vital_rates[VR_ADULT_SURVIVAL], 0.0, initial);
                        set_adult_cc_fraction_preset(preset, 1.0, initial);
                        break;
                case CATS_P_BIENNIAL:
                        set_vital_age_preset(preset, VA_AGE_OF_MATURITY_MIN, 1, initial);
                        set_vital_age_preset(preset, VA_AGE_OF_MATURITY_MAX, 1, initial);
                        preset_set_vital_rate_maximum(&param->vital_rates[VR_ADULT_SURVIVAL], 0.0, initial);
                        set_adult_cc_fraction_preset(preset, 0.5, initial);
                        break;
                case CATS_P_TREE:               // trees currently use the default parametrisation
                case CATS_P_UNSPECIFIED:
                        mat_max = get_vital_age_from_param(param, VA_AGE_OF_MATURITY_MAX);
                        min_scale = 1.0 - 1.0 / ((cats_dt_rates) mat_max + 1.0);
                        min_scale = max_rates(0.9 * get_default_vital_rate_maximum_from_param(param, VR_ADULT_SURVIVAL),
                                              min_scale);
                        preset_set_vital_rate__minimum_scale_factor(&param->vital_rates[VR_ADULT_SURVIVAL], min_scale,
                                                                    initial);
                        set_adult_cc_fraction_preset(preset, 0.5, initial);
                        break;
        }

        preset->preset = form;
}


void apply_vr_presets(struct cats_species_param *param)
{

        struct cats_species_param_presets *preset = &param->presets;
        const char *name = get_preset_name(preset->preset);
        log_message(LOG_INFO, "%s: applying preset '%s'", __func__, name);

        if (preset->have_adult_cc_fraction_preset) {
                log_message(LOG_INFO,
                            "\tapplying preset '%s': setting 'maximum adult fraction of carrying capacity' =  %f",
                            name, (double) preset->adult_cc_fraction_preset);
                param->max_adult_cc_fraction = preset->adult_cc_fraction_preset;
        }

        for (enum cats_vital_age_id age_id = VA_MIN; age_id < VA_MAX; age_id++) {
                if (preset->vital_ages[age_id].have_vital_age_preset) {
                        log_message(LOG_INFO, "\tapplying preset '%s': setting '%s' =  %d",
                                    name,
                                    get_vital_age_name(age_id), preset->vital_ages[age_id].vital_age_preset);
                        param->vital_ages[age_id] = preset->vital_ages[age_id].vital_age_preset;
                }
        }

        for (enum cats_vital_rate_id rate_id = VR_MIN; rate_id < VR_MAX; rate_id++) {
                struct cats_vital_rate *vr = &param->vital_rates[rate_id];
                if (vr->preset.have_maximum) {
                        vr->max_rate = vr->preset.value_maximum;
                        log_message(LOG_INFO, "\tapplying preset '%s': setting '%s maximum' = %f",
                                    name,
                                    get_default_vital_rate_name(vr),
                                    (double) vr->preset.value_maximum);
                }

                if (vr->preset.have_minimum_scale_factor) {
                        vr->rate_minimum_scale_factor = vr->preset.value_minimum_scale_factor;
                        param->vital_rates[rate_id].have_rate_non_default_scale_factor = true;
                        log_message(LOG_INFO, "\tapplying preset '%s': setting '%s minimum scale factor' = %f",
                                    name,
                                    get_default_vital_rate_name(&param->vital_rates[rate_id]),
                                    (double) vr->preset.value_minimum_scale_factor);
                }

        }
}