// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_helper.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <string.h>
#include "cats_global.h"
#include <memory/raw_arrays.h>
#include "actions/setup.h"
#include "load_configuration_helper.h"
#include <memory/cats_memory.h>
#include "dispersal/dispersal.h"
#include "data/species_parameters.h"


void create_configuration_structures(struct cats_configuration *conf, struct cats_configuration_counts *count)
{
        conf->param_count = count->species;
        conf->grid_count = count->species;
        conf->param = new_raw_1d_array(conf->param_count, sizeof(struct cats_species_param));
        conf->dispersal = new_raw_1d_array(conf->param_count, sizeof(struct cats_dispersal));

        for (int i = 0; i < conf->param_count; i++) {
                init_cats_species_param(&conf->param[i]);
                setup_dispersal_structures(&conf->dispersal[i], count->dispersals[i]);
        }

        conf->global_stats.populated_by_classes = calloc_or_die(conf->param_count + 1, sizeof(int32_t));

        for (int i = 0; i < conf->param_count; i++) {
                conf->global_stats.populated_by_classes[i] = 0;
        }
}


void reset_same_dispersals(bool all_same, struct cats_configuration_counts *count)
{
        assert(count != NULL);
        if (all_same && count->species > 1) {
                for (int species = 1; species < count->species; species++) {
                        count->dispersals[species] = count->dispersals[0];
                }
        }
}


int32_t count_kernel_dispersal_for_species(struct cats_ini *ini, char *species_section)
{
        int32_t dispersal_count = 0;
        char *dispersal_name = NULL;

        load_conf_value(false, ini, species_section, "dispersal", &dispersal_name);

        if (dispersal_name == NULL || strlen(dispersal_name) == 0) {
                free(dispersal_name);
                return dispersal_count;
        }

        char *dispersal_section_name = full_section_name(dispersal_name, "dispersal");


        struct string_array *dispersal_kernel_filenames = get_char_array_from_conf2(ini, dispersal_section_name,
                                                                                    "kernel filenames");
        if (!dispersal_kernel_filenames->count) {
                log_message(LOG_ERROR,
                            "Error creating configuration structures: could not load dispersal information for species '%s': '%s' [%s]",
                            species_section, dispersal_name, dispersal_section_name);
                exit_cats(EXIT_FAILURE);
        }

        dispersal_count = dispersal_kernel_filenames->count;

        free(dispersal_section_name);
        free_string_array(&dispersal_kernel_filenames);
        free(dispersal_name);
        return dispersal_count;
}


struct cats_configuration_counts get_counts_from_config(struct cats_ini *ini)
{
        struct cats_configuration_counts counts = {0};
        bool all_same = false;

        // required
        struct string_array *species = get_sections_with_prefix(ini, "species");
        counts.species = species->count;


        counts.dispersals = new_raw_1d_array(counts.species, sizeof(int));

        for (int i = 0; i < counts.species; i++) {
                counts.dispersals[i] = count_kernel_dispersal_for_species(ini, species->string[i]);
        }

        free_string_array(&species);

        reset_same_dispersals(all_same, &counts);
        return counts;
}
