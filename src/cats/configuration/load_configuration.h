// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_LOAD_CONFIGURATION_H
#define CATS_LOAD_CONFIGURATION_H

#include "configuration/configuration.h"

struct cats_configuration *
load_configuration_from_file(const char *filename, const struct program_options *command_line_options);

void set_suitability_from_string(cats_dt_rates *value, const struct cats_species_param *param, const char *string);

#endif