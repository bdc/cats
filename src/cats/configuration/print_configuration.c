// SPDX-License-Identifier: GPL-3.0-or-later
//
// print_configuration.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stdint.h>
#include "overlays/overlays.h"
#include "environment/environment.h"
#include "environment/environment_set.h"
#include "configuration.h"
#include "misc/misc.h"
#include "configuration/print_configuration.h"
#include "environment/environment_registry.h"
#include "preset.h"
#include "inline_vital_rates.h"
#include "dispersal/dispersal.h"
#include "modules/load_module.h"

const char *get_dispersal_name(enum dispersal_type type);


void print_overlay_summary(struct cats_configuration *conf)
{
        print_category(0, "overlays", NULL);
        print_string(1, "overlays enabled", bool_to_string(conf->overlays.have_overlays));

        if (!conf->overlays.have_overlays) return;

        for (enum overlay_type ol = OL_NONE + 1; ol < OL_MAX; ol++) {
                print_category(1, "overlay type", get_overlay_name(ol));
                print_string(2, "enabled", bool_to_string(conf->overlays.overlay[ol].enabled));
        }
}


const char *get_parametrization_name(enum cats_parametrization_type type)
{
        switch (type) {
                case PARAM_UNDEFINED:
                        log_message(LOG_ERROR, "%s: unknown species parametrization %d", __func__, type);
                        exit_cats(EXIT_FAILURE);
                case PARAM_HYBRID:
                        return "suitability";
                case PARAM_DIRECT_VITAL_RATE:
                        return "direct parametrization";
        }
        log_message(LOG_ERROR, "%s: unknown species parametrization %d", __func__, type);
        exit_cats(EXIT_FAILURE);
}


void print_environments(struct cats_configuration *conf, int32_t indent) // FIXME MOVE TO REGISTRY
{
        print_string(indent, "Environment registry", "");
        for (int32_t i = 0; i < conf->environment_registry.count; i++) {
                print_environment(&conf->environment_registry.environment[i], indent + 1);
                log_message(LOG_RAW, "\n");
        }
}


void print_config_summary(struct cats_configuration *conf)
{
        if (get_log_level() > LOG_INFO) return;
        log_message(LOG_RAW, "%s\n", TEXT_DIVIDER);

        // GENERAL

        print_category(0, "general", NULL);
        //print_string(1, "NEW FUNCTIONS", bool_to_string(conf->__new_functions));
        print_string(1, "run name", conf->run_name);
        print_integer(1, "replicate", conf->simulation.replicate);
        print_string(1, "write timing information", bool_to_string(conf->timing_info));
        print_string(1, "output file format", conf->geometry.file_format);


        // SPATIAL
        print_category(0, "spatial", NULL);
        print_integer(1, "rows", conf->geometry.dimension.rows);
        print_integer(1, "columns", conf->geometry.dimension.cols);

        // TEMPORAL
        print_category(0, "temporal", NULL);
        print_integer(1, "zero pad to digits", conf->time.zero_pad_to_digits);
        print_string(1, "always show sign", bool_to_string(conf->time.always_show_sign));
        print_string(1, "time format string", conf->time.format_string);
        print_integer(1, "output interval in years", conf->time.output_interval);
        print_integer(1, "starting year", conf->time.year_start);
        print_integer(1, "simulation length", conf->time.simulation_length);

        print_string(1, "have burn-in", bool_to_string(conf->time.phase_exists[PHASE_BURN_IN]));
        if (conf->time.phase_exists[PHASE_BURN_IN]) {
                print_integer(2, "burn-in starts in year", conf->time.burn_in_phase_year);
                print_integer(2, "burn-in runs for years", conf->time.burn_in_phase_length);
        }
        print_string(1, "have warm-up", bool_to_string(conf->time.phase_exists[PHASE_WARM_UP]));
        if (conf->time.phase_exists[PHASE_WARM_UP]) {
                print_integer(2, "warm-up starts in year", conf->time.warm_up_phase_year);
        }

        // OVERLAYS
        print_overlay_summary(conf);

        // SPECIES
        print_category(0, "species", NULL);
        if (conf->param_count > 1) {
                print_string(1, "same params for all species", bool_to_string(conf->all_species_same));
        }

        print_integer(1, "species count", conf->param_count);

        for (int i = 0; i < conf->param_count; i++) {
                struct cats_species_param *param = &conf->param[i];
                print_subcategory(1, "species", i, param->species_name);
                print_integer(2, "species number", i);
                print_string(2, "species name", param->species_name);
                print_string(2, "parametrisation mode", get_parametrization_name(param->parametrization));
                print_string(2, "preset", get_preset_name(param->presets.preset));
                print_string(2, "increase initial population to carrying capacity",
                             bool_to_string(param->initial_population.set_to_cc));
                print_rate(2, "maximum adult fraction of carrying capacity", param->max_adult_cc_fraction);

                char *name = compound_string(param->carrying_capacity.name, "maximum", " ");
                print_rate(2, name, get_vital_rate_maximum(&param->carrying_capacity));
                free(name);
                if (param->custom_vital_rates == false) {
                        for (enum cats_vital_rate_id vr_idx = VR_MIN + 1; vr_idx < VR_MAX; vr_idx++) {
                                char *name2 = compound_string(param->vital_rates[vr_idx].name, "maximum", " ");
                                print_rate(2, name2, get_vital_rate_maximum(&param->vital_rates[vr_idx]));
                                free(name2);
                        }
                }

                if (param->parametrization == PARAM_HYBRID) {

                        print_rate(2, "OT", param->OT);
                        print_rate(2, "ZT", param->ZT);
                        print_rate(2, "initial population suitability threshold",
                                   param->initial_population.suitability_threshold);
                        print_rate(2, "scale factor", param->scale_factor);

                }

                for (enum cats_vital_age_id age_id = VA_MIN + 1; age_id < VA_MAX; age_id++) {
                        print_rate(2, get_vital_age_name(age_id), get_vital_age_from_param(param, age_id));
                }


                print_string(2, "save lambda grid", bool_to_string(param->save_lambda_grid));
                print_string(2, "save lambda grid (density)", bool_to_string(param->save_lambda_grid_density));
                print_rate(2, "hapaxanthy", param->hapaxanthy);
                print_rate(2, "demographic slope", param->demographic_slope);
                log_message(LOG_RAW, "\n");

                // DISPERSAL
                if (param->custom_dispersal == false) {
                        print_category(1, "dispersal", param->species_name);
                        struct cats_dispersal *dispersal = &conf->dispersal[i];
                        print_string(2, "long range dispersal", bool_to_string(dispersal->long_range_enabled));
                        if (dispersal->long_range_enabled) {

                                print_rate(2, "LRD probability", dispersal->long_range_prob);
                                print_integer(2, "LRD radius ", dispersal->long_range_radius);
                                print_integer(2, "LRD target count", dispersal->long_range_target_count);
                                log_message(LOG_RAW, "\n");
                        }


                        print_integer(2, "dispersal vector count", dispersal->vector_count);
                        print_rate(2, "self dispersal", dispersal->local_dispersal);


                        for (int32_t j = 0; j < dispersal->vector_count; j++) {
                                print_subcategory(2, "dispersal vector", j, NULL);
                                print_string(3, "type", get_dispersal_name(dispersal->types[j]));
                                print_string(3, "file name", dispersal->filenames[j]);
                                print_rate(3, "prob min", dispersal->prob_min[j]);
                                print_rate(3, "prob max", dispersal->prob_max[j]);

                                log_message(LOG_RAW, "\n");
                        }
                }
        }

        log_message(LOG_RAW, "%s\n", TEXT_DIVIDER);
        print_environments(conf, 0);
        log_message(LOG_RAW, "%s\n", TEXT_DIVIDER);
        print_environment_sets(conf);

        log_message(LOG_RAW, "%s\n", TEXT_DIVIDER);
        print_modules(conf);
        log_message(LOG_RAW, "%s\n", TEXT_DIVIDER);

}


const char *maybe(const char *string)
{
        if (string) return string;
        return "(not specified)";
}


const char *get_dispersal_name(enum dispersal_type type)
{
        switch (type) {
                case DISPERSAL_KERNEL:
                        return "kernel";
                case DISPERSAL_CODE:
                        return "function";
                default:
                        return "unknown";

        }
}
