// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_glm.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <string.h>
#include "environment/environment_registry.h"
#include "load_configuration_environments.h"
#include "environment/environment_set.h"
#include "load_configuration_glm.h"




enum environment_type get_environment_glm_type(const char *type_name)
{
        const char *allowed_names_string = "'glm-quadratic', 'glm-linear'";
        if (type_name == NULL || strlen(type_name) == 0) {
                log_message(LOG_ERROR, "%s: unknown environment set type '%s. Allowed: %s", __func__, type_name,
                            allowed_names_string);
        }

        if (strcmp(type_name, "glm-quadratic") == 0) {
                return ENVIRONMENT_TYPE_GLM;
        }
        if (strcmp(type_name, "glm-linear") == 0) {
                return ENVIRONMENT_TYPE_GLM;
        }

        log_message(LOG_ERROR, "%s: unknown glm type '%s. Allowed: %s", __func__, type_name, allowed_names_string);
        exit_cats(EXIT_FAILURE);
}


enum cats_glm_family get_glm_family(const char *name)
{
        enum cats_glm_family family = GLM_FAMILY_UNKNOWN;
        const char *str = "%s: unknown glm family '%s. Allowed: 'binomial'";
        if (name == NULL || strlen(name) == 0) {
                log_message(LOG_ERROR, str, __func__, name);
                family = GLM_FAMILY_UNKNOWN;
        } else if (strcasecmp(name, "binomial") == 0) {
                family = GLM_FAMILY_BINOMIAL;
        } else if (strcasecmp(name, "gaussian") == 0) {
                family = GLM_FAMILY_GAUSSIAN;
        } else if (strcasecmp(name, "bernoulli") == 0) {
                family = GLM_FAMILY_BERNOULLI;
        } else if (strcasecmp(name, "poisson") == 0) {
                family = GLM_FAMILY_POISSON;
        }

        switch (family) {

                case GLM_FAMILY_UNKNOWN:
                        break;
                case GLM_FAMILY_BINOMIAL:
                        break;
                case GLM_FAMILY_POISSON:
                        break;
                case GLM_FAMILY_BERNOULLI:
                        break;
                case GLM_FAMILY_GAUSSIAN:
                        break;
        }


        if (family != GLM_FAMILY_UNKNOWN) return family;
        log_message(LOG_ERROR, str, __func__, name);
        return GLM_FAMILY_UNKNOWN;
}


enum cats_glm_type get_glm_type_name(const char *type_name)
{
        const char *allowed = "'quadratic glm', 'linear glm'";
        if (type_name == NULL || strlen(type_name) == 0) {
                log_message(LOG_ERROR, "%s: unknown glm type '%s. Allowed: %s", __func__, type_name, allowed);
                return GLM_UNKNOWN;
        }
        if (strcmp(type_name, "quadratic glm") == 0) return GLM_QUADRATIC;
        if (strcmp(type_name, "linear glm") == 0) return GLM_LINEAR;

        log_message(LOG_ERROR, "%s: unknown glm type '%s. Allowed: %s", __func__, type_name, allowed);
        exit(EXIT_FAILURE);
        return GLM_UNKNOWN;
}


void add_glm_from_conf(struct cats_configuration *conf, struct cats_ini *ini, char *environment_section)
{
        char *name = remove_0th_token(environment_section, ":");
        char *type_name;

        load_conf_value(true, ini, environment_section, "type", &type_name);
        if (type_name == NULL) {
                log_message(LOG_ERROR, "%s: empty glm type in section [%s]", __func__, environment_section);
                exit_cats(EXIT_FAILURE);
        }
        struct glm_params glm = {0};
        struct string_array *predictors = get_char_array_from_conf2(ini, environment_section, "predictors");
        if (predictors->count == 0) {
                log_message(LOG_ERROR, "%s: section [%s] does not contain any predictor names", __func__, environment_section);
                exit_cats(EXIT_FAILURE);
        }
        if (predictors->count > MAX_ENVIRONMENTS) {
                log_message(LOG_ERROR, "%s: section [%s]: more predictors (%d) found than the allowed maximum %d",
                            __func__, environment_section,
                            predictors->count, MAX_ENVIRONMENTS);
                exit_cats(EXIT_FAILURE);
        }
        enum environment_type type = get_environment_glm_type(type_name);
        glm.type = get_glm_type_name(type_name);
        free(type_name);

        char *family_name = NULL;

        load_conf_value(true, ini, environment_section, "intercept", &glm.intercept);
        load_conf_value(true, ini, environment_section, "family", &family_name);
        glm.family = get_glm_family(family_name);
        if (glm.family == GLM_FAMILY_UNKNOWN) {
                log_message(LOG_ERROR, "%s: unknown GLM family '%s' for '%s", __func__, family_name, name);
                exit_cats(EXIT_FAILURE);
        }

        free(family_name);


        for (int32_t i = 0; i < predictors->count; i++) {
                get_environment_from_registry(&conf->environment_registry, predictors->string[i]);
                char *predictor_linear = compound_string(predictors->string[i], "linear", " ");
                load_conf_value(true, ini, environment_section, predictor_linear, &glm.linear[i]);
                free(predictor_linear);
                glm.count += 1;

                if (glm.type == GLM_QUADRATIC) {
                        char *predictor_quadratic = compound_string(predictors->string[i], "quadratic", " ");
                        load_conf_value(true, ini, environment_section, predictor_quadratic, &glm.quadratic[i]);
                        free(predictor_quadratic);
                }

        }
        print_glm_params(&glm, 0, NULL);
        struct cats_environment *set = add_environment(conf, name, type, &glm);
        load_conf_value(false, ini, environment_section, "save environment", &set->save_environment);

        for (int32_t i = 0; i < predictors->count; i++) {
                struct cats_environment_variable *env = get_environment_from_registry(&conf->environment_registry,
                                                                                      predictors->string[i]);
                add_to_environment(set, env);
        }

        conf->output.have_glm = true;
}

