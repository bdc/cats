// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_modules.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#include "load_configuration_modules.h"
#include "configuration.h"
#include <cats_ini/cats_ini.h>
#include <string.h>


void load_configuration_modules(struct cats_configuration *conf, struct cats_ini *ini)
{
        struct string_array *modules = get_sections_with_prefix(ini, "module");
        for (int32_t i = 0; i < modules->count; i++) {

                char *module_section = modules->string[i];
                char *name = remove_0th_token(module_section, ":");
                if (i >= MAX_MODULES) {
                        log_message(LOG_ERROR, "too many modules, limit is %d", MAX_MODULES);
                        exit_cats(EXIT_FAILURE);
                }
                //char *name = remove_0th_token(section_name, ":");
                log_message(LOG_INFO, "%s: found module in section %s", __func__, module_section);
                load_conf_value(true, ini, module_section, "filename", &conf->modules.module[i].filename);
                conf->modules.module[i].name = strdup(name);
                free(name);
                conf->modules.found_count++;
        }
        free_string_array(&modules);

}