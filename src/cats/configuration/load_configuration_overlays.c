// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_overlays.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <logging/logging.h>
#include <cats_strings/cats_strings.h>
#include "load_configuration_overlays.h"
#include "overlays/overlay_habitat_type_cc.h"
#include "grids/grid_setup.h"


void load_overlay_configuration(struct cats_configuration *conf, struct cats_ini *ini)
{
        if (conf->command_line_options.no_input_rasters_required) return;
        struct string_array *overlay_names = get_sections_with_prefix(ini, "overlay");

        for (int32_t i = 0; i < overlay_names->count; i++) {
                char *overlay_section = overlay_names->string[i];
                bool enabled = true;
                load_conf_value(false, ini, overlay_section, "enabled", &enabled);
                if (enabled == false) {
                        continue;
                }

                char *overlay_name = remove_0th_token(overlay_names->string[i], ":");


                char *type_name = NULL;

                load_conf_value(true, ini, overlay_section, "type", &type_name);
                enum overlay_type type = get_overlay_type_from_name(type_name, conf->overlays.registered_custom_overlay_names);

                struct cats_overlay *overlay = NULL;

                if (type < OL_CUSTOM && type > OL_NONE) {
                        overlay = &conf->overlays.overlay[type];
                } else if (type == OL_CUSTOM) {
                        overlay = add_overlay(conf, type_name);
                }



                overlay->enabled = true;


                load_conf_value(true, ini, overlay_section, "filename pattern", &overlay->filename);
                load_conf_value(false, ini, overlay_section, "reload interval", &overlay->reload_interval);

                if (overlay->reload_interval > 0) {
                        overlay->is_static = false;
                } else {
                        overlay->is_static = true;
                }

                conf->overlays.have_overlays = true;

                long double default_value = NAN;
                overlay->name = strdup(overlay_name);

                switch (type) {
                        case OL_EXCLUSION:
                                break;
                        case OL_HABITAT_TYPE_CC:

                                load_conf_value(true, ini, overlay_section, "table filename",
                                                &overlay->filename_aux);
                                load_conf_value(true, ini, overlay_section, "default value", &default_value);
                                overlay->aux_data = load_habitat_layer_cc_aux(
                                        overlay->filename_aux,
                                        (double) default_value);
                                break;
                        case OL_RESOURCE:
                                break;
                        case OL_NONE:
                        case OL_MAX:
                        case OL_CUSTOM:
                                log_message(LOG_ERROR, "unknown or unhandled overlay type '%s'", overlay_name);
                                exit(EXIT_FAILURE);
                }
                free(type_name);
                free(overlay_name);
        }

        free_string_array(&overlay_names);
}

