// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_helper.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_LOAD_CONFIGURATION_HELPER_H
#define CATS_LOAD_CONFIGURATION_HELPER_H

#include "configuration.h"
#include "../../cats_ini/cats_ini.h"

/// @brief Stores species and associated counts when pre-parsing the configuration file.
/** The configuration file is pre-parsed and the number of species, their dispersal counts and the number of predictors
 * are read, so that the data structures can be pre-allocated.
 */
struct cats_configuration_counts {
        int species;        ///< the number of species found in the configuration file
        int *dispersals;    ///< the number of dispersals found for each species
};


void create_configuration_structures(struct cats_configuration *conf, struct cats_configuration_counts *count);

struct cats_configuration_counts get_counts_from_config(struct cats_ini *ini);

#endif //CATS_LOAD_CONFIGURATION_HELPER_H
