// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <math.h>

#include <assert.h>

#include <cats_ini/cats_ini.h>
#include <logging/logging.h>

#ifdef USEMPI
#include "mpi/mpi_cats.h"
#endif

#include "debug/debug.h"
#include "misc/misc.h"
#include "paths/paths.h"
#include "vital_rates/default_vital_rates.h"
#include "vital_rates/setup_rates.h"

#include "configuration.h"
#include "check_configuration.h"
#include "load_configuration_general.h"
#include "load_config_spatial.h"
#include "load_configuration_helper.h"
#include "load_configuration_overlays.h"
#include "load_configuration_environments.h"
#include "load_configuration_temporal.h"
#include "load_configuration_modules.h"
#include "test/test_ini.h"
#include "debug/debug_vital_rates.h"

void load_configuration_elements(struct cats_configuration *conf, struct cats_ini *ini);

void load_all_species(struct cats_configuration *conf, struct cats_ini *ini);


struct cats_configuration *
load_configuration_from_file(const char *filename, const struct program_options *command_line_options)
{
        if (command_line_options->need_conf) {
                assert(filename != NULL);
        }
        int32_t replicate = command_line_options->replicate;

        // create empty configuration
        struct cats_configuration *conf = new_configuration();
        conf->quiet = command_line_options->quiet;

        conf->output.log_file_name = command_line_options->log_file;
        conf->output.summary_file = NULL;
        conf->output.summary_file_name = command_line_options->summary_file;
        if (conf->output.summary_file_name) {
                conf->output.summary_file = fopen(conf->output.summary_file_name, "w");
                ENSURE_FILE_OPENED(conf->output.summary_file, conf->output.summary_file_name)
                fprintf(conf->output.summary_file, "type,path,year\n");
                if (conf->output.log_file_name) fprintf(conf->output.summary_file, "log,%s,\n",conf->output.log_file_name);

        }
#ifdef USEMPI
        mpi_setup(conf);
#endif

        conf->command_line_options = *command_line_options;
        if (command_line_options->need_conf) {
                //conf->command_line_options.configuration_file = strdup(command_line_options->configuration_file);
        } else {
                conf->command_line_options.configuration_file = strdup("auto-generated");
        }
        conf->debug = command_line_options->debug_flags;
        conf->link_count = setup_default_links(conf->vital_dependency_registry);

        if (command_line_options->debug_vr) {
                debug_vital_rates(conf, command_line_options);
                exit_cats(EXIT_SUCCESS);
        }

        // load configuration file into ini data structure
        struct cats_ini *ini = NULL;
        if (command_line_options->need_conf) {
                ini = load_ini_file(filename);
        } else {
                ini = create_test_ini(command_line_options);
        }


        // get number of species, dispersal information etc...
        struct cats_configuration_counts count = get_counts_from_config(ini);

        // ... so we can allocate the corresponding data structures
        create_configuration_structures(conf, &count);
        setup_carrying_capacity(conf);
        setup_default_vital_rates(conf);



        // start to fill data
        conf->simulation.replicate = replicate;

        log_message(LOG_IMPORTANT, "replicate number: %d", conf->simulation.replicate);
        conf->ini = ini;
        // we keep a copy of the configuration file contents in memory
        if (conf->command_line_options.need_conf) {
                conf->output.file_content = slurp_file(filename);
        }

        // load data from loaded ini file into the configuration
        load_configuration_modules(conf, ini);
        load_configuration_elements(conf, ini);
        setup_debugging(&cats_debug, conf);
        postprocess_vital_rates(conf);
        //load_vital_rates(conf);
        verify_configuration(conf);
        update_juvenile_transition_rates(conf);


        // clean up and return
        free(count.dispersals);
        return conf;
}


void load_configuration_elements(struct cats_configuration *conf, struct cats_ini *ini)
{
        if (conf->command_line_options.need_conf) {
                load_simulation_geometry_from_initial_population(conf, ini);
        } else {
                conf->geometry.dimension.cols = 1;
                conf->geometry.dimension.rows = 1;
        }
#ifdef USEMPI
        update_mpi_geometry(conf);
#endif
        load_temporal_info(conf, ini);
        load_general_info(conf, ini);
        load_environments_configuration(conf, ini);
        load_all_species(conf, ini);

        load_overlay_configuration(conf, ini);
}


void set_suitability_from_string(cats_dt_rates *value, const struct cats_species_param *param, const char *string)
{
        if (string == NULL || strlen(string) == 0) return;

        char *copy = strdup(string);

        copy = left_trim(right_trim(copy));
        if (copy == NULL || strlen(copy) == 0) {
                exit_cats(EXIT_FAILURE);
        }

        if (strncasecmp(copy, "OT", 2) == 0) {
                *value = param->OT;
        } else if (strncasecmp(copy, "ZT", 2) == 0) {
                *value = param->ZT;
        } else {
                double temp_value = NAN;
                bool success = string_to_double(copy, &temp_value);
                if (success == false || isnan(temp_value)) {
                        log_message(LOG_ERROR,
                                    "%s: could not interpret '%s' as suitability value",
                                    __func__, copy);
                        exit_cats(EXIT_FAILURE);
                }
                *value = (cats_dt_rates) temp_value;
        }
        free(copy);
}
