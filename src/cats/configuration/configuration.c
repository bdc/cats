

// SPDX-License-Identifier: GPL-3.0-or-later
//
// configuration.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <unistd.h>

#include <string.h>
#include <gsl/gsl_rng.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>

#include <memory/cats_memory.h>
#include <cats_ini/cats_ini.h>

#include "configuration.h"
#include "logging.h"
#include "grids/gdal_helper.h"
#include "overlays/overlays.h"
#include "misc/cats_random.h"
#include "actions/setup_actions.h"
#include "environment/environment_registry.h"
#include "paths/paths.h"
#include "plants/juvenile_survival_calculation.h"
#include "inline_vital_rates.h"
#include "dispersal/dispersal.h"
#include "stats/grid_stats.h"

#ifdef CATS_ON_WINDOWS
#include <windows.h>
#else

#include <unistd.h>

#endif


void init_cats_configuration(struct cats_configuration *conf)
{
        if (conf == NULL) {
                log_message(LOG_ERROR, "conf is NULL in %s", __func__);
                exit(EXIT_FAILURE);
        }
        init_stats_registry(&conf->stats_registry);
        init_module_registry(&conf->modules);
        reset_overlay(&conf->overlays);
        zero_statistics_stats(&conf->global_stats);
        simulation_time_init(&conf->time);
        init_simulation_geometry(&conf->geometry);
        reset_environment_registry(&conf->environment_registry);
        conf->cycle.actions = NULL;
        conf->cycle.num_actions = 0;
        conf->cycle.action_counter = 0;

        conf->environment.count = 0;
        conf->output.output_directory = strdup(DEFAULT_OUTPUT_DIRECTORY);
        conf->stats_populated_threshold = DEFAULT_POPULATED_THRESHOLD;

        conf->param_max_threads = 1;
        conf->output.performance_file = NULL;

        conf->grid_count = 1;

        conf->overlays.custom_overlay_count = 0;
        conf->overlays.custom_overlays = NULL;
        conf->overlays.registered_custom_overlay_names = new_string_array();
        conf->output.needed_output_directories = new_string_array();
        conf->output.needed_module_output_directories = new_string_array();
        conf->output.have_glm = false;
        conf->output.write_all = false;

#ifdef USEMPI
        conf->mpi.world_size = 1;
        conf->mpi.world_rank = 1;
#endif
}


struct cats_configuration *new_configuration(void)
{
        struct cats_configuration *conf = calloc_or_die(1, sizeof(struct cats_configuration));
        init_cats_configuration(conf);
        conf->geometry.gdal_registered = gdal_register_if_necessary(conf->geometry.gdal_registered);
        conf->global = &global;
        return conf;
}


void fix_zts_and_ots(struct cats_configuration *conf)
{
        for (int i = 0; i < conf->param_count; i++) {
                if (conf->param[i].OT <= conf->param[i].ZT) {
                        cats_dt_rates old_zt = conf->param[i].ZT;
                        conf->param[i].ZT = conf->param[i].OT * 0.5;
                        log_message(LOG_IMPORTANT, "reducing ZT for species %d from %f to %f (OT is %f)", i,
                                    (double) old_zt, (double) conf->param[i].ZT, (double) conf->param[i].OT);
                }
        }
}


void update_juvenile_transition_rates(struct cats_configuration *conf)
{
        // does not depend on scale factor
        log_message(LOG_INFO, "calculating juvenile transition rates");

        for (int32_t i = 0; i < conf->param_count; i++) {

                const int32_t min_age_of_maturity = get_vital_age_from_param(&conf->param[i], VA_AGE_OF_MATURITY_MIN);
                const int32_t max_age_of_maturity = get_vital_age_from_param(&conf->param[i], VA_AGE_OF_MATURITY_MAX);
                const cats_dt_rates max_rate = get_default_vital_rate_maximum_from_param(&conf->param[i],
                                                                                         VR_GERMINATION_TO_ADULT_SURVIVAL);
                for (int32_t j = 0; j < MAX_JUVENILE_TR_STEPS; j++) {
                        cats_dt_rates val = (cats_dt_rates) j / (cats_dt_rates) (MAX_JUVENILE_TR_STEPS - 1);
                        if (val > max_rate) {
                                conf->param[i].juvenile_transition_rates[j] = NAN;
                                continue;
                        }

                        conf->param[i].juvenile_transition_rates[j] = calculate_survival_rate(val, min_age_of_maturity,
                                                                                              max_age_of_maturity,
                                                                                              false);
                }
        }
}


int32_t get_max_threads(void)
{
#ifdef CATS_ON_WINDOWS
        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);
        return (int32_t) sysinfo.dwNumberOfProcessors;
#else
        return (int32_t) sysconf(_SC_NPROCESSORS_ONLN);
#endif

}


void post_process_configuration(struct cats_configuration *conf)
{
        // ATTENTION: the replicate number is still 0, even if set on the command line to a different value
        assert(conf != NULL);
        setup_simulation_time(conf, &conf->time);

        // reduce ZT if needed
        fix_zts_and_ots(conf);

        conf->rng_seed = get_random_seed(false);
        srand(conf->rng_seed);

        if (conf->timing_info == true) {
                int count =
                        snprintf(NULL, 0, "performance/%s_%03d.csv", conf->run_name, conf->simulation.replicate) + 1;
                char *filename = malloc_or_die(count);
                snprintf(filename, count, "performance/%s_%03d.csv", conf->run_name, conf->simulation.replicate);

                conf->output.performance_file = fopen(filename, "w");
                if (!conf->output.performance_file) {
                        log_message(LOG_ERROR, "failed to open %s: %s", filename, strerror(errno));
                        exit_cats(EXIT_FAILURE);
                }

                fprintf(conf->output.performance_file, "%s\n", "year,stat,cpu_time,wall_time");
        }

        if (conf->param_max_threads <= 0) {
                conf->param_max_threads = get_max_threads();
        }

        if (conf->geometry.file_format == NULL) {
                conf->geometry.file_format = strdup("tiff");
        }

        if (conf->command_line_options.lambda_test) {
                int32_t years = SCALE_LAMBDA_DEFAULT_YEARS;
                if (conf->command_line_options.lambda_years > 0) {
                        years = conf->command_line_options.lambda_years;
                }
                log_message(LOG_IMPORTANT, "overriding simulation length to %d for lambda test", years);
                conf->time.simulation_length = years;
                conf->overlays.have_overlays = false;
        }

        if (conf->command_line_options.lambda_cc > 0 && conf->command_line_options.lambda_test) {
                for (int32_t i = 0; i < conf->param_count; i++) {
                        conf->param[i].carrying_capacity.max_rate = conf->command_line_options.lambda_cc;
                }
        }


        for (int32_t i = 0; i < conf->param_count; i++) {
                setup_plant_juvenile_weights(&conf->param[i]);
        }
}


void cleanup_configuration(struct cats_configuration **conf_orig)
{
        assert(conf_orig != NULL);
        assert(*conf_orig != NULL);

        log_message(LOG_INFO, "cleaning up configuration");


        struct cats_configuration *conf = *conf_orig;
        // THIS HAS TO BE THE FIRST ENTRY
        cleanup_module_registry(&conf->modules);

        if (conf->output.summary_file) {
                fclose(conf->output.summary_file);
                free(conf->output.summary_file_name);
        }

        if (conf->output.statsfile_global) fclose(conf->output.statsfile_global);
        free(conf->output.file_content);

        /* keep sorted!*/
        free(conf->cycle.actions);
        free(conf->command_line_options.configuration_file);
        free_string_array(&conf->environment.names);
        for (int32_t i = 0; i < MAX_ENVIRONMENT_SETS; i++) {
                struct cats_environment *env = &conf->environment.environment_sets[i];
                free(env->name);
        }

        if (conf->geometry.gdal_registered) {
                gdal_destroy();
        }

        free(conf->geometry.projection_string);
        free(conf->geometry.file_format);

        for (int32_t i = 0; i < conf->grid_count; i++) {
                cleanup_dispersal(&conf->dispersal[i]);
        }

        free(conf->dispersal);

        free(conf->output.output_directory);

        for (int i = 0; i < conf->param_count; i++) {
                free(conf->param[i].preset_name);
                free(conf->param[i].species_name);
                free(conf->param[i].species_name_actual);
                free(conf->param[i].initial_population.filename);
                free(conf->param[i].suitability_pattern);
                free(conf->param[i].lambda_cache);
                free(conf->param[i].suitability_name);
                free(conf->param[i].juvenile_cc_weights);
                free(conf->param[i].species_config_section);
                for (int32_t j = 0; j < MAX_MODULES; j++) {
                        cleanup_module_species_data(&conf->param[i].module_data[j]);
                }
        }

        free(conf->simulation.program_name);
        free(conf->time.format_string);
        free(conf->time.phase_name);

        cleanup_action_functions(&conf->action_functions);

        //free(conf->predictors);
        cleanup_overlays(&conf->overlays);
        cleanup_environment_registry(&conf->environment_registry);
        if (conf->output.performance_file) fclose(conf->output.performance_file);
        conf->output.performance_file = NULL;
        free_ini(conf->ini);

#ifdef USEMPI
        free(conf->geometry.cells_per_chunk);
        free(conf->geometry.displacements);
        free(conf->geometry.rows_per_chunk);
#endif
        free(conf->command_line_options.output_directory);

        free(conf->global_stats.populated_by_classes);
        free_string_array(&conf->output.needed_output_directories);
        free_string_array(&conf->output.needed_module_output_directories);
        free_string_array(&conf->overlays.registered_custom_overlay_names);

        free(conf->param);
        conf->param = NULL;
        free(conf->run_name);
        free(conf);
        *conf_orig = NULL;
}

