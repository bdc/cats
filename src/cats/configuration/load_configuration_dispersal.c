// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_configuration_dispersal.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <cats_global.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <logging/logging.h>
#include <cats_strings/cats_strings.h>

#include "data/species_parameters.h"
#include "load_configuration_helper.h"
#include "dispersal/dispersal.h"


bool load_dispersal_info(struct cats_configuration *conf, struct cats_ini *ini, int species_index, char *name)
{
        char *dispersal_section = full_section_name(name, "dispersal");

        struct string_array *file_names = get_char_array_from_conf2(ini, dispersal_section, "kernel filenames");
        struct string_array *prob_min = get_char_array_from_conf2(ini, dispersal_section, "minimum weights");
        struct string_array *prob_max = get_char_array_from_conf2(ini, dispersal_section, "maximum weights");

        if (!((file_names->count == prob_min->count) && (file_names->count == prob_max->count))) {
                log_message(LOG_ERROR, "CONF: count mismatch: %d file names, %d minimum weights, "
                                       "%d maximum weights. Expected all counts to be equal.", file_names->count,
                            prob_min->count, prob_max->count);
                exit(EXIT_FAILURE);
        }

        struct cats_dispersal *dispersal = &conf->dispersal[species_index];
        assert(dispersal->types != NULL);
        dispersal->name = strdup(name);

        load_conf_value(false, ini, dispersal_section, "local dispersal proportion", &dispersal->local_dispersal);
        load_conf_value(false, ini, dispersal_section, "long range radius", &dispersal->long_range_radius);
        load_conf_value(false, ini, dispersal_section, "long range target count", &dispersal->long_range_target_count);
        load_conf_value(false, ini, dispersal_section, "long range event probability", &dispersal->long_range_prob);

        cats_dt_rates pmax, pmin;

        if (dispersal->long_range_prob > 0 || dispersal->long_range_radius || dispersal->long_range_target_count > 0) {
                dispersal->long_range_enabled = true;
        }

        for (int32_t i = 0; i < file_names->count; i++) {
                dispersal->types[i] = DISPERSAL_KERNEL;
                bool pmin_succ = string_to_long_double(prob_min->string[i], &pmin);
                bool pmax_succ = string_to_long_double(prob_max->string[i], &pmax);

                if (pmin_succ == false || pmax_succ == false) {
                        log_message(LOG_ERROR, "CONF: error reading minimum and maximum probability: "
                                               "'%s' and '%s'",
                                    prob_min->string[i], prob_max->string[i]);
                        exit(EXIT_FAILURE);
                }

                if (file_names->count > i && file_names->string[i]) {
                        dispersal->filenames[i] = strdup(file_names->string[i]);
                }

                if ((pmax >= 0 && pmax <= 1.0 && pmin >= 0 && pmin <= 1.0 && pmax >= pmin) ||
                    (pmax == -1.0 && pmin == -1.0)) {
                        dispersal->prob_max[i] = (double) pmax;
                        dispersal->prob_min[i] = (double) pmin;
                }

        }

        free_string_array(&file_names);
        free_string_array(&prob_min);
        free_string_array(&prob_max);
        free(dispersal_section);

        return true;
}
