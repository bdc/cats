// SPDX-License-Identifier: GPL-3.0-or-later
//
// load_config_spatial.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "load_config_spatial.h"
#include "configuration.h"
#include "misc/misc.h"
#include "logging.h"
#include "cats_ini/cats_ini.h"
#include "cats_strings/cats_strings.h"
#include "load_configuration_temporal.h"
#include "paths/paths.h"
#include "grids/gdal_helper.h"


void load_simulation_geometry_from_initial_population(struct cats_configuration *conf, struct cats_ini *ini)
{
        struct string_array *species_sections = get_sections_with_prefix(ini, "species");
        if (species_sections->count == 0) {
                log_message(LOG_ERROR, "%s: could not load any species information", __func__);
                exit_cats(EXIT_FAILURE);
        }

        if (conf->command_line_options.lambda_test) {
                conf->geometry.dimension.cols = 1;
                conf->geometry.dimension.rows = 1;

                log_message(LOG_INFO, "overriding simulation grid dimensions to 1x1");
                return;

        } else if (conf->command_line_options.lambda_gradient) {
                conf->geometry.dimension.cols = 1001;
                conf->geometry.dimension.rows = 250;

        }
        char *file_name = NULL;
        bool load_ini_pop = ! conf->command_line_options.no_input_rasters_required;
        for (int32_t i = 0; i < species_sections->count; i++) {
                char *species_section = species_sections->string[i];

                load_conf_value(load_ini_pop, ini, species_section, "initial population filename", &file_name); // VR::ignored::
                break;
        }
        free_string_array(&species_sections);


        char *initial_population_file_name = get_initial_population_filename(file_name);
        if (load_ini_pop) {
                load_global_geometry(&conf->geometry, initial_population_file_name);
                free(file_name);
                free(initial_population_file_name);
        }

}