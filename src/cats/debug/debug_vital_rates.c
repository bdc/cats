// SPDX-License-Identifier: GPL-3.0-or-later
//
// debug_vital_rates.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//


#include "debug_vital_rates.h"
#include "data/species_parameters.h"
#include "memory/cats_memory.h"
#include "inline_carrying_capacity.h"
#include "grids/grid_setup.h"
#include "paths/directory_helper.h"


struct cats_environment *minimal_suitability_environment(void)
{
        struct cats_environment *env = calloc_or_die(1, sizeof(struct cats_environment));
        struct cats_environment_variable *var = calloc_or_die(1, sizeof(struct cats_environment_variable));
        cats_dt_environment **env_values = new_raw_2d_array(1, 1, sizeof(cats_dt_environment));
        env->environments[0] = var;
        env->count = 1;
        env->type = ENVIRONMENT_TYPE_SUITABILITY;

        var->current.dimension.rows = 1;
        var->current.dimension.cols = 1;
        var->current.values = env_values;
        return env;
}


void cleanup_minimum_suitability_environment(struct cats_environment **env)
{
        struct cats_environment *e = *env;
        struct cats_environment_variable *var = e->environments[0];

        free_grid(&var->current.values, 1);
        free(var);
        var = NULL;
        free(e);
        *env = NULL;
}


void set_param_values(struct cats_species_param *param, cats_dt_rates OT, cats_dt_rates ZT, cats_dt_rates scale)
{
        param->OT = OT;
        param->ZT = ZT;
        param->scale_factor = scale;
}


struct cats_grid *minimal_grid(struct cats_configuration *conf, struct cats_environment *env)
{
        struct cats_grid *grid = calloc_or_die(1, sizeof(struct cats_grid));
        grid->conf = conf;
        grid->suitability = env;
        cats_dt_coord *r = (cats_dt_coord *) &grid->dimension.rows;
        cats_dt_coord *c = (cats_dt_coord *) &grid->dimension.cols;
        *r = 1;
        *c = 1;
        setup_grid_population_structures(grid);
        grid->param.demographic_slope = DEFAULT_DEMOGRAPHIC_SLOPE;
        grid->param.carrying_capacity.environment_set = env;
        grid->param.carrying_capacity.max_rate =
                10000.0 * 10.0; // is an integer quantity, so large number is needed for variation in N/K
        grid->param.max_adult_cc_fraction = 1.0;
        grid->param.carrying_capacity.func = &conf->vital_dependency_registry[LINK_SUITABILITY_SIGMOID];
        grid->param.carrying_capacity.is_carrying_capacity = true;
        grid->param.carrying_capacity.density = NO_DENSITY_DEP;
        return grid;

}


void set_suitability(struct cats_environment *env, cats_dt_environment suit)
{
        env->environments[0]->current.values[0][0] = suit;
}


FILE *init_debug_vr_file(const struct cats_vital_rate *vr, const struct cats_vital_rate *cc,
                         const struct cats_species_param *param, int32_t dens_idx)
{
        FILE *f = NULL;
        char *filename = NULL;

        // assemble filename and open
        int rc = asprintf(&filename,
                          "debug/vital-rates-test/rate-%s_density-%d_ZT-%Lf_OT-%Lf_scale-%Lf_cc-%s.csv",
                          vr->func->short_name, dens_idx, param->ZT, param->OT,
                          param->scale_factor,
                          cc->func->short_name);

        asprintf_check(rc);
        f = fopen(filename, "w");
        ENSURE_FILE_OPENED(f, filename)
        free(filename);

        // write header
        printf("%s - density %d - ZT %Lf - OT %Lf - scale %Lf - cc %s\n",
               vr->func->short_name,
               dens_idx,
               param->ZT,
               param->OT,
               param->scale_factor,
               cc->func->short_name);

        fprintf(f, "value,scale,suit,N,K,density,K_suit,ZT,OT,density_dependence,demographic_slope\n");

        return f;
}


static inline cats_dt_rates attempt_rate_calculation(cats_dt_environment suit, cats_dt_rates N, cats_dt_rates *K_suit,
                                                     struct cats_grid *grid, struct cats_vital_rate *vr)
{
        if (suit < 0.0 || suit > 1.0) {
                *K_suit = NAN;
                return NAN;
        }


        set_suitability(grid->suitability, suit);
        *K_suit = get_adult_carrying_capacity(grid, 0, 0);
        if (N > *K_suit) return NAN;

        return calculate_rate(vr, N, &grid->param, grid, 0, 0, NULL);
}


static inline void
debug_vr_write_line(FILE *f, cats_dt_rates result, cats_dt_environment suit, cats_dt_rates N, cats_dt_rates K_suit,
                    cats_dt_rates K_max, cats_dt_rates density, int32_t dens_idx, const struct cats_grid *grid)
{
        fprintf(f,
                "%0.6Lf,%0.6Lf,%0.6f,%0.6Lf,%0.6Lf,%0.6Lf,%0.6Lf,%0.6Lf,%0.6Lf,%d,%0.6Lf\n",
                result,
                grid->param.scale_factor,
                suit,
                N,
                K_max,
                density,
                K_suit,
                grid->param.ZT,
                grid->param.OT,
                dens_idx,
                grid->param.demographic_slope);
}


void debug_vital_rate(struct cats_vital_rate *vr, struct cats_vital_rate *cc, struct cats_grid *grid,
                      int32_t steps, int32_t dens_idx)
{
        struct cats_species_param *param = &grid->param;

        FILE *f = init_debug_vr_file(vr, cc, param, dens_idx);

        const cats_dt_rates K_max = param->max_adult_cc_fraction * param->carrying_capacity.max_rate;
        const cats_dt_population N_step = (cats_dt_population) ((float) K_max / (float) steps);
        const cats_dt_environment suit_step = (cats_dt_environment) 1.0 / (cats_dt_environment) steps;

        cats_dt_rates N = 0;
        int32_t N_count = 0;

        do {
                cats_dt_environment suit = 0;
                cats_dt_rates density = N / K_max;
                int32_t suit_count = 0;
                cats_dt_rates result = NAN;
                cats_dt_rates K_suit = NAN;

                do {
                        result = attempt_rate_calculation(suit, N, &K_suit, grid, vr);
                        debug_vr_write_line(f, result, suit, N, K_suit, K_max, density, dens_idx, grid);

                        suit_count += 1;
                        suit += suit_step;

                } while (suit_count <= steps);

                N += N_step;
                N_count += 1;

        } while (N_count <= steps);

        fflush(f);
        fclose(f);
}


void debug_vital_rates(struct cats_configuration *conf, const struct program_options *command_line_options)
{
        struct cats_environment *env = minimal_suitability_environment();
        set_suitability(env, 0.5f);
        struct cats_grid *grid = minimal_grid(conf, env);
        struct cats_species_param *param = &grid->param;
        set_param_values(param, 0.5, 0.25, 0.5);
        struct cats_vital_rate *cc = &param->carrying_capacity;

        enum cats_density_dependence density_dep[] = {NO_DENSITY_DEP, DENSITY_DEP_NEGATIVE};
        int32_t dens_count = sizeof(density_dep) / sizeof(density_dep[0]);

        struct cats_vital_rate vr = {.max_rate = 1.0, .environment_set=env};

        int32_t divisions = 200;

        if (command_line_options->debug_vr_subdivisions > 0) {
                divisions = command_line_options->debug_vr_subdivisions;
        }

        cats_dt_rates scale_start = 0.1;
        cats_dt_rates scale_step = 0.2;
        cats_dt_rates scale_max = 1.0;

        if (command_line_options->debug_vr_scale != -1) {
                scale_step = scale_start = scale_max = command_line_options->debug_vr_scale;
        }

        cats_dt_rates OT_step = 0.2;
        cats_dt_rates OT_start = 0.1;
        cats_dt_rates OT_max = 1.0;

        if (command_line_options->debug_vr_ot != -1) {
                OT_step = OT_start = OT_max = command_line_options->debug_vr_ot;
        }


        struct string_array *output_directory = new_string_array();
        string_array_add(output_directory, "debug");
        string_array_add(output_directory, "vital-rates");
        check_and_create_directory_if_needed(output_directory);
        free_string_array(&output_directory);


        for (int32_t ci = LINK_MIN + 1; ci < conf->link_count - 1; ci++) {
                cc->func = &conf->vital_dependency_registry[ci];
                for (int32_t i = LINK_MIN + 1; i < conf->link_count - 1; i++) {
                        vr.func = &conf->vital_dependency_registry[i];
                        for (int32_t dens_idx = 0; dens_idx < dens_count; dens_idx++) {
                                vr.density = density_dep[dens_idx];


                                param->scale_factor = scale_start;
                                do {
                                        param->OT = OT_start;

                                        do {
                                                debug_vital_rate(&vr, cc, grid, divisions, dens_idx);

                                                param->OT += OT_step;
                                        } while (param->OT < OT_max);
                                        param->scale_factor += scale_step;
                                } while (param->scale_factor <= scale_max);
                        }
                }
        }


        cleanup_minimum_suitability_environment(&env);
}