// SPDX-License-Identifier: GPL-3.0-or-later
//
// debug.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_DEBUG_H
#define CATS_DEBUG_H

#include "configuration/configuration.h"
#include "threading/threading-helpers.h"

void setup_debugging(struct cats_debug_options *dbg, struct cats_configuration *conf);

void debug_msg(int32_t thread_id, const char *fmt, ...);

void debug_bare(int32_t thread_id, const char *fmt, ...);

void debug_threading_cell(struct cats_configuration *conf, struct cats_thread_info *ts, int32_t row, int32_t col);

void cleanup_debugging(struct cats_debug_options *dbg, struct cats_configuration *conf);

void reset_threading_grid(struct cats_debug_options *dbg);

void verify_threading_grid(struct cats_debug_options *dbg);

void mark_cell_done(struct cats_debug_options *dbg, cats_dt_coord row, cats_dt_coord col);

double timestamp(void);

#endif //CATS_DEBUG_H
