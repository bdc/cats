// SPDX-License-Identifier: GPL-3.0-or-later
//
// debug.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdint.h>
#include <stdarg.h>
#include <memory/raw_arrays.h>
#include <memory/cats_memory.h>
#include "debug/debug.h"
#include "threading/threading-helpers.h"
#include <cats_strings/cats_strings.h>
#include "misc/misc.h"


#define DEBUG_DIR "debug"


void debug_threading_cell(struct cats_configuration *conf, struct cats_thread_info *ts, int32_t row, int32_t col)
{
        debug_bare(ts->id,
                   "{\"ts\": %f, \"msg\": \"cell task\", \"action_id\": %Ld, \"action\": %d, \"thread id\": %d, \"cell\": "
                   "{\"row\": %d, \"col\": %d } }\n",
                   timestamp(), conf->cycle.action_counter, conf->cycle.current_action,
                   ts->id, row, col);
}


void reset_threading_grid(struct cats_debug_options *dbg)
{
        for (int32_t row = 0; row < dbg->rows; row++) {
                for (int32_t col = 0; col < dbg->cols; col++) {
                        dbg->threading_grid[row][col] = -1;
                }
        }
}


void mark_cell_done(struct cats_debug_options *dbg, cats_dt_coord row, cats_dt_coord col)
{
        if (dbg->threading_grid[row][col] >= 0) {
                log_message(LOG_ERROR, "cell %d %d has already been done", row, col);
                exit(EXIT_FAILURE);
        }
        dbg->threading_grid[row][col] = timestamp();
}


void verify_threading_grid(struct cats_debug_options *dbg)
{
        int64_t error = 0;
        for (int32_t row = 0; row < dbg->rows; row++) {
                for (int32_t col = 0; col < dbg->cols; col++) {
                        if (dbg->threading_grid[row][col] <= 0.0) {
                                log_message(LOG_ERROR, "%s: %s - cell %d %d was not used in threading", __func__,
                                            dbg->conf->cycle.actions[dbg->conf->cycle.current_action].message, row,
                                            col);
                                error = 1;
                        }
                }
        }
        if (error > 0) {
                log_message(LOG_ERROR, "not all cells were used in threading");
                exit(EXIT_FAILURE);
        }
}


void setup_debugging(struct cats_debug_options *dbg, struct cats_configuration *conf)
{
        if (conf->debug_enabled == false) return;
        size_t max_threads = conf->param_max_threads;
        dbg->thread_count = conf->param_max_threads;
        dbg->file_handles_threads = calloc_or_die(max_threads, sizeof(FILE *));
        dbg->file_names_threads = calloc_or_die(max_threads, sizeof(char **));
        dbg->rows = conf->geometry.dimension.rows;
        dbg->cols = conf->geometry.dimension.cols;
        dbg->threading_grid = new_raw_2d_array(dbg->rows, dbg->cols, sizeof(double));
        reset_threading_grid(dbg);
        dbg->conf = conf;

        struct string_array *dir = new_string_array_init(DEBUG_DIR);
        for (int32_t thread_nr = 0; thread_nr < max_threads; thread_nr++) {
                struct string_array *file_name = new_string_array_init("debug");
                string_array_add(file_name, conf->run_name);
                string_array_add_int(file_name, conf->simulation.replicate, "r-%03d");
                string_array_add_int(file_name, thread_nr, "thread-%03d");
                dbg->file_names_threads[thread_nr] = assemble_filename(dir, file_name, "_", "txt");
                free_string_array(&file_name);
                dbg->file_handles_threads[thread_nr] = fopen(dbg->file_names_threads[thread_nr], "w");
                ENSURE_FILE_OPENED(dbg->file_handles_threads[thread_nr], dbg->file_names_threads[thread_nr])
        }


        struct string_array *file_name = new_string_array_init("debug");
        string_array_add(file_name, conf->run_name);
        string_array_add_int(file_name, conf->simulation.replicate, "r-%03d");
        string_array_add(file_name, "main-thread");
        dbg->file_names_main = assemble_filename(dir, file_name, "_", "txt");
        free_string_array(&file_name);
        dbg->file_handles_main = fopen(dbg->file_names_main, "w");
        ENSURE_FILE_OPENED(dbg->file_handles_main, dbg->file_names_main)
        free_string_array(&dir);
}


void cleanup_debugging(struct cats_debug_options *dbg, struct cats_configuration *conf)
{
        if (conf->debug_enabled == false) return;
        for (int32_t thread_nr = 0; thread_nr < dbg->thread_count; thread_nr++) {
                fclose(dbg->file_handles_threads[thread_nr]);
                free(dbg->file_names_threads[thread_nr]);
        }

        free(dbg->file_names_main);
        fclose(dbg->file_handles_main);
        cleanup_raw_2d_double_array(&dbg->threading_grid, dbg->rows);
}


void debug_msg(int32_t thread_id, const char *fmt, ...)
{

        if (thread_id >= cats_debug.thread_count || thread_id < -1) {
                log_message(LOG_ERROR, "%s: thread id %d out of range", __func__, thread_id);
        }
        FILE *handle = NULL;
        if (thread_id == -1) {
                handle = cats_debug.file_handles_main;
        } else {
                handle = cats_debug.file_handles_threads[thread_id];
        }
        double secs = seconds_monotonic_since(&global.time_info);

#ifdef USEMPI
        fprintf(handle, "(%02d)::%f::", global.mpi_world_rank, secs);
#else
        fprintf(handle, "%f::", secs);
#endif

        va_list argp;
        va_start(argp, fmt);
        vfprintf(handle, fmt, argp);
        va_end(argp);
        fflush(handle);
}


void debug_bare(int32_t thread_id, const char *fmt, ...)
{

        if (thread_id >= cats_debug.thread_count || thread_id < -1) {
                log_message(LOG_ERROR, "%s: thread id %d out of range", __func__, thread_id);
        }
        FILE *handle = NULL;
        if (thread_id == -1) {
                handle = cats_debug.file_handles_main;
        } else {
                handle = cats_debug.file_handles_threads[thread_id];
        }
        va_list argp;
        va_start(argp, fmt);
        vfprintf(handle, fmt, argp);
        va_end(argp);
        fflush(handle);
}


double timestamp(void)
{
        return seconds_monotonic_since(&global.time_info);
}
