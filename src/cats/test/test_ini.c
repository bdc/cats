// SPDX-License-Identifier: GPL-3.0-or-later
//
// test_ini.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <string.h>
#include "cats_ini/cats_ini.h"
#include "test_ini.h"

void intern_maybe_add_key_val(struct cats_ini *ini, const char *string) {
        char *string_ = strdup(string);
        maybe_add_key_val( string_, ini);

}
void intern_maybe_add_section(struct cats_ini *ini, const char *name) {
        char *name_ = strdup(name);
        maybe_add_section(name_, ini);
}

struct cats_ini *create_test_ini(const struct program_options *options)
{
        struct cats_ini *ini = new_ini();
        ini->complement = new_ini();


        intern_maybe_add_section(ini, "[general]");
        intern_maybe_add_key_val(ini, "time steps = 5");
        intern_maybe_add_key_val(ini, "output interval = 5");
        intern_maybe_add_key_val(ini, "starting year = 0");
        intern_maybe_add_key_val(ini, "run name = test");

        intern_maybe_add_section(ini, "[environment: --]");
        intern_maybe_add_key_val(ini, "type = suitability");
        intern_maybe_add_key_val(ini, "filename pattern = None");


        intern_maybe_add_section(ini, "[species: --]");
        intern_maybe_add_key_val(ini, "initial population filename = --");
        intern_maybe_add_key_val(ini, "suitability = --");
        intern_maybe_add_key_val(ini, "OT = 0.5");
        intern_maybe_add_key_val(ini, "ZT = 0.25");

        intern_maybe_add_key_val(ini, "carrying capacity maximum  = 1.0");
        intern_maybe_add_key_val(ini, "clonal growth rate maximum = 1.0");
        intern_maybe_add_key_val(ini, "germination rate maximum = 1.0");
        intern_maybe_add_key_val(ini, "flowering frequency maximum = 1.0");
        intern_maybe_add_key_val(ini, "seed yield maximum = 1.0");
        intern_maybe_add_key_val(ini, "adult survival rate maximum = 1.0");
        intern_maybe_add_key_val(ini, "germination to adult survival rate maximum = 1");
        intern_maybe_add_key_val(ini, "seed survival rate maximum = 1");
        intern_maybe_add_key_val(ini, "seed persistence = 1");
        intern_maybe_add_key_val(ini, "minimum age of maturity = 0");
        intern_maybe_add_key_val(ini, "maximum age of maturity = 0");
        intern_maybe_add_key_val(ini, "dispersal=--");


        intern_maybe_add_section(ini, "[dispersal:--]");
        intern_maybe_add_key_val(ini, "kernel filenames = None");
        intern_maybe_add_key_val(ini, "minimum weights = -1");
        intern_maybe_add_key_val(ini, "maximum weights = -1");
        return ini;
}