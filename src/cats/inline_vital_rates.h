// SPDX-License-Identifier: GPL-3.0-or-later
//
// inline_vital_rates.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_INLINE_VITAL_RATES_H
#define CATS_INLINE_VITAL_RATES_H

#include "inline_population.h"
#include "configuration/configuration.h"
#include "data/cats_grid.h"
#include "vital_rates/glm_functions.h"
#include "vital_rates/direct_functions.h"


static inline cats_dt_rates get_vital_rate_maximum(const struct cats_vital_rate *vr)
{
        return vr->max_rate;
}


static inline cats_dt_rates
get_default_vital_rate_maximum_from_grid(const struct cats_grid *grid, enum cats_vital_rate_id vr)
{
        assert(grid != NULL);
        assert(vr > VR_MIN && vr < VR_MAX);
        return get_vital_rate_maximum(&grid->param.vital_rates[vr]);

}


static inline cats_dt_rates
get_default_vital_rate_maximum_from_param(const struct cats_species_param *param, enum cats_vital_rate_id vr)
{
        assert(param != NULL);
        assert(vr > VR_MIN && vr < VR_MAX);
        return get_vital_rate_maximum(&param->vital_rates[vr]);
}


static inline const struct cats_vital_rate *
get_default_vital_rate(const struct cats_grid *grid, const enum cats_vital_rate_id vr)
{
        assert(grid != NULL);
        assert(grid != NULL);
        assert(vr > VR_MIN && vr < VR_MAX);
        return &grid->param.vital_rates[vr];
}


static inline void
load_information_for_density_maybe(cats_dt_rates *K, cats_dt_rates *N, const struct cats_vital_rate *rate_info,
                                   const struct cats_grid *grid, cats_dt_coord row, cats_dt_coord col,
                                   const struct link_override_parameters *override)
{
        bool req_density = false;
        switch (rate_info->density) {

                case NO_DENSITY_DEP:
                        req_density = false;
                        break;
                case DENSITY_DEP_NEGATIVE:
                case DENSITY_DEP_POSITIVE:
                        req_density = true;
                        break;
        }

        if (req_density == false) return;

        if (override) {
                *N = override->override_population;
                *K = override->override_carrying_capacity;
                return;
        }


        if (K == NULL || N == NULL) {
                log_message(LOG_ERROR, "%s: K or N are NULL", __func__);
                exit_cats(EXIT_FAILURE);
        }


        if (grid == NULL) {
                log_message(LOG_ERROR, "%s: grid is NULL", __func__);
                exit_cats(EXIT_FAILURE);
        }


        if (isnan(*K)) {
                *K = roundl(get_vital_rate_maximum(&grid->param.carrying_capacity) * grid->param.max_adult_cc_fraction);
        }

        if (isnan(*N)) {
                *N = (cats_dt_rates) get_adult_population(grid, row, col);
        }

}


#endif //CATS_INLINE_VITAL_RATES_H
