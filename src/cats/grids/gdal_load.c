// SPDX-License-Identifier: GPL-3.0-or-later
//
// gdal_load.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <gdal.h>
#include <cpl_string.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "paths/paths.h"
#include "gdal_load.h"
#include "overlays/overlays.h"
#include "dispersal/dispersal.h"
#include "gdal_helper.h"
#include "grids/grid_converters.h"
#include "misc/misc.h"

#define EPS 1e-8

#ifdef USEMPI
#include "mpi/mpi_cats.h"
#endif

#include "misc/cats_maths_inline.h"
#include <assert.h>
#include "paths/paths_dispersal.h"
#include "inline_population.h"
#include "populations/population.h"
#include "data/simulation_geometry.h"

struct simulation_geometry load_gdal_geometry(const char *filename) {
        log_message(LOG_INFO, "%s: accessing dataset '%s'", __func__, filename);
        GDALDatasetH dataset = GDALOpen(filename, GA_ReadOnly);
        if (dataset == NULL) {
                log_message(LOG_ERROR, "GDAL: couldn't open file '%s'", filename);
                exit(EXIT_FAILURE);
        }

        struct simulation_geometry geometry = {0};
        geometry.projection_string = strdup((char *) GDALGetProjectionRef(dataset));
        geometry.dimension.rows = GDALGetRasterYSize(dataset);
        geometry.dimension.cols = GDALGetRasterXSize(dataset);
        get_gdal_pixel_size(dataset, &geometry.cell_size_x, &geometry.cell_size_y);

        return geometry;

}

GDALDatasetH load_gdal_dataset(struct simulation_geometry *geometry, const char *filename, bool ignore_geometry)
{
        assert(filename != NULL);
        assert(geometry != NULL);

        log_message(LOG_INFO, "%s: accessing dataset '%s'", __func__, filename);
        GDALDatasetH dataset = GDALOpen(filename, GA_ReadOnly);

        if (dataset == NULL) {
                log_message(LOG_ERROR, "GDAL: couldn't open file '%s'", filename);
                exit(EXIT_FAILURE);
        }

        if (ignore_geometry == false) {
                verify_raster_geometry(dataset, geometry);
        }

        print_gdal_info(dataset);
        return dataset;
}


void load_initial_population_gdal(struct cats_grid *grid, struct simulation_geometry *geometry, const char *filename,
                                  const char *species_name)
{
        assert(grid != NULL);
        assert(filename != NULL);

        const int id = grid->id;
        log_message(LOG_INFO, "loading initial population for grid id %d (species name '%s') from file '%s'", id,
                    species_name, filename);
        get_projection_from_file(geometry, filename);


        struct raster_load_wrapper wrapper = {.geometry = geometry, .filename = (char *) filename, .ignore_offset = false, .ignore_geometry = false, .type = LOAD_POPULATION,
                .target_grid = grid};
        load_gdal_data(&wrapper);

}


void load_gdal_data(const struct raster_load_wrapper *data)
{
        GDALDatasetH dataset = load_gdal_dataset(data->geometry, data->filename, data->ignore_geometry);
        GDALRasterBandH band = GDALGetRasterBand(dataset, 1); // first band hardcoded

        cats_dt_coord rows = GDALGetRasterYSize(dataset);
        cats_dt_coord cols = GDALGetRasterXSize(dataset);

        if (data->geometry->use_sub_grid) {
                if (data->ignore_geometry == false && (data->geometry->dimension.rows > rows ||
                                                       data->geometry->dimension.cols > cols)) {
                        log_message(LOG_ERROR, "%s: %s has invalid size (%dx%d) vs (%dx%d)",
                                    __func__, data->filename,
                                    data->geometry->dimension.rows, data->geometry->dimension.cols,
                                    rows, cols);
                        assert(data->geometry->dimension.rows <= rows);
                        assert(data->geometry->dimension.cols <= cols);
                        exit(EXIT_FAILURE);
                }
                if (data->ignore_offset == false) {
                        rows = data->geometry->dimension.rows;
                        cols = data->geometry->dimension.cols;
                }
        }

        int have_nodata;

        int clamped = 0;
        int rounded = 0;
        GDALDataType dt = GDALGetRasterDataType(band);

        double nodata = GDALGetRasterNoDataValue(band, &have_nodata);
        if (have_nodata) {
                log_message(LOG_INFO, "\t'%s': found nodata value %f ", data->filename, nodata);
        }

        double orig = nodata;
        nodata = GDALAdjustValueToDataType(dt, nodata, &clamped, &rounded);
        if (clamped) {
                log_message(LOG_INFO, "%s: nodata value %f has been clamped, was %f", data->filename, nodata, orig);
        }
        if (rounded) {
                log_message(LOG_INFO, "%s: nodata value %f has been rounded, was %f", data->filename, nodata, orig);
        }


        for (int32_t r = 0; r < rows; r++) {
                double *scanline = (double *) CPLMalloc(sizeof(double) * (cols));
                int x_offset = 0;
                int y_offset = r;

                if (data->geometry->use_sub_grid && data->ignore_offset == false) {
                        x_offset += data->geometry->col_offset;
                        y_offset += data->geometry->row_offset;
                }

                CPLErr error = GDALRasterIO(band, GF_Read, x_offset, y_offset, cols, 1, scanline,
                                            cols, 1, GDT_Float64, 0, 0);
                if (error != CE_None) {
                        log_message(LOG_ERROR, "%s: x offset %d y offset %d", __func__, x_offset, y_offset);
                        log_message(LOG_ERROR, "%s: error reading data from file %s", __func__, data->filename);
                        fflush(stdout);
                        CPLError(CE_Warning, error, NULL);
                        exit(EXIT_FAILURE);
                }
                int32_t c;
                double val;


                switch (data->type) {
                        case LOAD_INVALID:
                                log_message(LOG_ERROR, "no type specified");
                                exit_cats(EXIT_FAILURE);
                        case LOAD_ENVIRONMENT:
                                for (c = 0; c < cols; c++) {
                                        val = scanline[c];

                                        if (have_nodata && fabs(val - nodata) < fabs(nodata * EPS)) {
                                                val = NAN;
                                        }

                                        data->target_environment[r][c] = (cats_dt_environment) val;

                                }
                                break;

                        case LOAD_POPULATION:
                                for (c = 0; c < cols; c++) {
                                        val = scanline[c];
                                        if ((have_nodata && fabs(val - nodata) < EPS) || isnan(val)) {
                                                val = 0;
                                        }

                                        set_population_ignore_cc(data->target_grid, r, c,
                                                                 (cats_dt_population) round(val));
                                }
                                break;
                }


                CPLFree(scanline);
        }

        GDALClose(dataset);

}


struct cats_2d_array_double *
get_double_values_from_gdal(struct simulation_geometry *geometry, const char *filename, bool ignore_geometry,
                            bool ignore_offset)
{
        GDALDatasetH dataset = load_gdal_dataset(geometry, filename, ignore_geometry);
        GDALRasterBandH band = GDALGetRasterBand(dataset, 1); // first band hardcoded

        cats_dt_coord rows = GDALGetRasterYSize(dataset);
        cats_dt_coord cols = GDALGetRasterXSize(dataset);
        struct cats_dimension dim = {.rows = rows, .cols=cols};
        struct cats_2d_array_double *raster = new_2d_array_double(dim);


        if (raster->dimension.cols <= 0 || raster->dimension.rows <= 0) {
                log_message(LOG_ERROR, "file %s malformed: %d rows %d cols", filename, raster->dimension.rows,
                            raster->dimension.cols);
        }

        if (geometry->use_sub_grid) {
                if (ignore_geometry == false && (geometry->dimension.rows > raster->dimension.rows ||
                                                 geometry->dimension.cols > raster->dimension.cols)) {
                        log_message(LOG_ERROR, "%s: %s has invalid size (%dx%d) vs (%dx%d)",
                                    __func__, filename,
                                    geometry->dimension.rows, geometry->dimension.cols,
                                    raster->dimension.rows, raster->dimension.cols);
                        assert(geometry->dimension.rows <= raster->dimension.rows);
                        assert(geometry->dimension.cols <= raster->dimension.cols);
                        exit(EXIT_FAILURE);
                }
                if (ignore_offset == false) {
                        raster->dimension.rows = geometry->dimension.rows;
                        raster->dimension.cols = geometry->dimension.cols;
                }
        }


        int have_nodata;
        int clamped = 0;
        int rounded = 0;
        GDALDataType dt = GDALGetRasterDataType(band);
        double nodata = GDALGetRasterNoDataValue(band, &have_nodata);
        double orig = nodata;
        nodata = GDALAdjustValueToDataType(dt, nodata, &clamped, &rounded);
        if (clamped) {
                log_message(LOG_INFO, "%s: nodata value %f has been clamped, was %f", filename, nodata, orig);
        }
        if (rounded) {
                log_message(LOG_INFO, "%s: nodata value %f has been rounded, was %f", filename, nodata, orig);
        }


        for (int32_t r = 0; r < (raster->dimension.rows); r++) {
                double *scanline = (double *) CPLMalloc(sizeof(double) * (raster->dimension.cols));
                int x_offset = 0;
                int y_offset = r;

                if (geometry->use_sub_grid && ignore_offset == false) {
                        x_offset += geometry->col_offset;
                        y_offset += geometry->row_offset;
                }

                CPLErr error = GDALRasterIO(band, GF_Read, x_offset, y_offset, raster->dimension.cols, 1, scanline,
                                            raster->dimension.cols, 1, GDT_Float64, 0, 0);
                if (error != CE_None) {
                        log_message(LOG_ERROR, "%s: x offset %d y offset %d", __func__, x_offset, y_offset);
                        log_message(LOG_ERROR, "%s: error reading data from file %s", __func__, filename);
                        fflush(stdout);
                        CPLError(CE_Warning, error, NULL);
                        exit(EXIT_FAILURE);
                }

                for (int32_t c = 0; c < raster->dimension.cols; c++) {
                        double val = scanline[c];

                        if (have_nodata && fabs(val - nodata) < EPS) {
                                val = NAN;
                        }

                        raster->data[r][c] = val;
                }

                CPLFree(scanline);
        }

        GDALClose(dataset);
        return raster;
}


void load_dispersals_gdal(struct cats_grid *grid, struct cats_configuration *conf,
                          int32_t species_id) // FIXME rething: split into gdal and non-gdal parts
{
        //int id = grid->id;

        if (species_id > 0 && conf->all_species_same) {
                conf->param[species_id].plant_dispersal_max_radius = conf->param[0].plant_dispersal_max_radius;
                species_id = 0;
        }

        struct cats_dispersal *d = &conf->dispersal[species_id];
        if (d == NULL) return;

        if (d->loaded == true && grid != NULL) {
                grid->dispersal = &conf->dispersal[species_id];
                return;
        }

        if (d->loaded) return;

        assert(d->types != NULL);

        int num_vectors = max_int32t(d->vector_count, 1);
        d->max_vector_diameter = 0;

        for (int vec = 0; vec < num_vectors; vec++) {
                char *filename = NULL;
                if (d->types[vec] != DISPERSAL_KERNEL) {
                        log_message(LOG_WARNING, "Dispersal id %d not a vector dispersal, continuing", vec);
                        continue;
                }

                if (d->filenames[vec]) {
                        filename = get_dispersal_path(d->filenames[vec]);
                } else {
                        log_message(LOG_ERROR, "%s:  no dispersal filename given", __func__);
                        exit(EXIT_FAILURE);
                }

                log_message(LOG_INFO, "Loading dispersal file %s", filename);
                d->geometry[vec] = load_gdal_geometry(filename);
                struct cats_2d_array_double *raster = get_double_values_from_gdal(&conf->geometry, filename, true,
                                                                                  true);

                create_and_fill_vector_data(raster->data, d, vec, raster->dimension.rows, raster->dimension.cols);
                setup_dispersal_vectordata(conf, d, vec, species_id, raster->data, raster->dimension.rows,
                                           raster->dimension.cols, filename);
                free_cats_grid(&raster);

                free(filename);
        }

        d->loaded = true;
        assert(conf->param[species_id].plant_dispersal_max_radius > 0);
}


void load_initial_population(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        assert(conf != NULL);

        const int id = grid->id;
        char *filename = get_initial_population_filename(conf->param[id].initial_population.filename);

        assert(filename != NULL);
        load_initial_population_gdal(grid, &conf->geometry, filename, conf->param[id].species_name);
        free(filename);
}
