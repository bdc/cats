// SPDX-License-Identifier: GPL-3.0-or-later
//
// gdal_helper.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include <stdbool.h>
#include <gdal.h>
#include "configuration/configuration.h"
#include "data/simulation_geometry.h"


bool gdal_register_if_necessary(bool registered);

const char *get_gdal_output_driver_from_extension(const char *extension);


void print_gdal_info(GDALDatasetH dataset);

void verify_raster_geometry(GDALDatasetH dataset, struct simulation_geometry *geometry);


void get_projection_from_file(struct simulation_geometry *geometry, const char *filename);

void set_gdal_band_nodata(GDALRasterBandH band, double nodata);

void set_gdal_dataset_metadata(GDALDatasetH dataset, const char *name, const char *value);

void set_gdal_band_metadata(GDALRasterBandH band, GDALColorInterp colorint, const char *description);

void set_gdal_dataset_geo_info(GDALDatasetH dataset, const struct simulation_geometry *geometry,
                               const struct cats_dimension *offset);

void gdal_destroy(void);

void load_global_geometry(struct simulation_geometry *geometry, const char *filename);

GDALDatasetH
create_gdal_dataset(const struct simulation_geometry *geometry, const char *filename, struct cats_dimension dimension,
                    int bands,
                    GDALDataType datatype, const struct cats_configuration *conf);
