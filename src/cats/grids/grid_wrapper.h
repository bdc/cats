// SPDX-License-Identifier: GPL-3.0-or-later
//
// grid_wrapper.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_GRID_WRAPPER_H
#define CATS_GRID_WRAPPER_H

#include <stdint.h>
#include <gdal.h>
#include "data/cats_datatypes.h"
#include "configuration/configuration.h"


/** @brief wrapper functions for different kinds of grids (in 1- and 2D-representation) */
struct grid_wrapper {
        int32_t **data_int32;
        double **data_double;
        char **data_char;
        float **data_float;
        float *data_float_1d;
        int32_t *data_int32_1d;
        char *data_char_1d;
        double *data_double_1d;

        struct cats_dimension dimension;
        struct cats_dimension offset;

        GDALDataType dt;

        bool data_1d;
        double nodata;
};


struct grid_wrapper gridwrapper_from_float(float **data, struct cats_dimension dimension);

struct grid_wrapper gridwrapper_from_int32t(int32_t **data, struct cats_dimension dimension);

struct grid_wrapper gridwrapper_from_double(double **data, struct cats_dimension dimension);

struct grid_wrapper gridwrapper_1d(void *data, GDALDataType dt, struct cats_dimension dimension);

#define gridwrapper(DATA, DIMENSION) _Generic((DATA), \
 int32_t **:        gridwrapper_from_int32t, \
 double **:        gridwrapper_from_double, \
 float **:        gridwrapper_from_float              \
)(DATA, DIMENSION)

#endif //CATS_GRID_WRAPPER_H
