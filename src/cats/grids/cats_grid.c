// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_grid.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"

#include <string.h>
#include <stdint.h>

#ifndef __MINGW32__

#include <dlfcn.h>

#endif

#include <gsl/gsl_rng.h>
#include <assert.h>
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "cats_grid.h"
#include "misc/misc.h"
#include "plants/juveniles.h"
#include "plants/seeds.h"

#include "stats/grid_stats.h"
#include "gdal_load.h"
#include "grid_setup.h"
#include "memory/cats_memory.h"
#include "misc/cats_random.h"

#ifdef USEMPI
#include "mpi/mpi_cats.h"
#include "mpi/mpi_grid_helpers.h"
#endif

#include "environment/environment_set.h"
#include "dispersal/dispersal.h"
#include "paths/output_paths.h"
#include "stats/global_stats.h"
#include "actions/setup.h"


struct cats_grid **create_and_initialize_grids(struct cats_configuration *conf, const int32_t count)
{
        assert(conf != NULL);
        assert(count > 0);

        struct cats_grid **grids = calloc_or_die(count, sizeof(struct cats_grid *));

        for (int32_t class = 0; class < count; class++) {
                grids[class] = create_and_initialize_grid(conf, class, grids);
                grids[class]->id = class;
        }


        for (int32_t c = 0; c < conf->grid_count; c++) {
                if (conf->command_line_options.lambda_test) {
                        setup_lambda_test_simulation(conf, grids[c]);

                } else if (conf->command_line_options.lambda_gradient) {
                        setup_lambda_gradient_simulation(conf, grids[c]);
                }
                else {
                        load_initial_population(grids[c], conf);
                }
        }


#ifdef USEMPI
        if (conf->mpi.world_rank == 0) {
                do_all_grids(grids, conf, &initialize_grid_stats);
                initialize_global_stats(conf);
        }
#else
        do_all_grids(grids, conf, &initialize_grid_stats);
        initialize_global_stats(conf);
#endif


        return grids;
}


void cleanup_array_of_grids(struct cats_grid ***grid, int grid_count)
{
        assert(*grid != NULL);
        struct cats_grid **array = *grid;

        for (int32_t class = 0; class < grid_count; class++) {
                cleanup_grid(array[class]);
                free(array[class]);
                array[class] = NULL;
        }

        free(array);
        *grid = NULL;
}


struct cats_grid *
create_and_initialize_grid(struct cats_configuration *conf, const int32_t id, struct cats_grid **parent)
{
        assert(conf != NULL);
        assert(id >= 0);

        struct cats_grid *grid = malloc_or_die(sizeof(struct cats_grid));

        initialize_grid(grid, conf, conf->geometry.dimension, id);

        grid->dispersal = &conf->dispersal[id];

        // parent is allowed to be NULL!
        if (parent != NULL) grid->parent = parent;

        if (conf->param[id].default_demographics && conf->param[id].custom_dispersal == false)
                load_dispersals_gdal(grid, conf, id);

#ifdef USEMPI
        int rows_before = 0, rows_after = 0, radius = 0;
        get_rows_around(conf, &rows_before, &rows_after, &radius, id);
        grid->seeds_0_all = (cats_dt_seeds **) new_raw_2d_array(grid->dimension.rows + rows_before + rows_after, grid->dimension.cols, sizeof(cats_dt_seeds));
#endif

        return grid;
}


void initialize_grid(struct cats_grid *grid, struct cats_configuration *conf, struct cats_dimension dimension, int id)
{
        assert(conf != NULL);
        assert(grid != NULL);
        assert(dimension.rows > 0);
        assert(dimension.cols > 0);
        assert(id >= 0);

        memset(grid, 0, sizeof(struct cats_grid));

        // set up basic information

        grid->conf = conf;
        grid->id = id;

        // set this grid to the correct species
        grid->param = conf->param[id];

        // initialise rng
        grid->rng = allocate_rng();

        // grid rows and cols are const, so we have to work around for initialisation
        cats_dt_coord *r = (cats_dt_coord *) &grid->dimension.rows;
        cats_dt_coord *c = (cats_dt_coord *) &grid->dimension.cols;
        *r = dimension.rows;
        *c = dimension.cols;


        // make sure
        for (int32_t i = 0; i < MAX_MODULES; i++) {
                grid->grid_modules[i].module_data = NULL;
        }

        // sometimes we do not want to fully initialize the grid
        // the main use for this is for large grids, where the time
        // to allocate the memory is non-trivial and we want to defer
        // this until after the scale factor has been calculated
        // to fail faster in case the scale factor could not be calculated
        // and to save resources eg in conjunction with --scale-only
        // FIXME: SCALE FACTOR make this unnecessary and decouple scale factor calculations from **grids

        // Attention: we need to call finalize_grid() later if only partially initialised
        init_statistics(&grid->stats, &conf->stats_registry);


        setup_grid_population_structures(grid);

        if (grid->param.parametrization == PARAM_HYBRID) {
                char *suit_name = conf->param[id].suitability_name;
                grid->suitability = get_environment(conf, suit_name);
        }

        for (int32_t i = 0; i < conf->modules.count; i++) {
                grid->param.module_data[i].module = &conf->modules.module[i];
                if (grid->param.module_data[i].grid_init_function) {
                        grid->grid_modules[i].module_data = grid->param.module_data[i].grid_init_function(conf, grid,
                                                                                                          NULL);
                }
        }

        setup_grid_seed_structures(conf, grid, id);
        setup_grid_juvenile_structures_opt(conf, grid, id);


        // this should be all NULL anyway


//        grid->mask.v = NULL;

        grid->single_thread = malloc_or_die(sizeof(struct cats_thread_info));
        initialize_thread(grid->single_thread, grid, conf, 0);

}


int64_t raster_cell_count(struct cats_dimension raster)
{
        assert(raster.rows >= 0);
        assert(raster.cols >= 0);

        const int64_t rows = raster.rows;
        const int64_t cols = raster.cols;

        if (rows > INT64_MAX / cols) {
                log_message(LOG_ERROR, "%s: possible integer overflow in %d * %d. Aborting.", __func__, raster.rows, raster.cols);
                exit(EXIT_FAILURE);
        }
        return rows * cols;
}


void cleanup_grid_population(struct cats_grid *grid)
{
        free(grid->_population);
        free(grid->population);
        grid->population = NULL;
        grid->_population = NULL;
}


void cleanup_grid_seeds_and_juveniles(struct cats_grid *grid)
{
        assert(grid != NULL);

        free_grid(&grid->seeds_produced, grid->dimension.rows);
        free_grid(&grid->dispersed_seeds, grid->dimension.rows);
        if (grid->seed_bank) {
                for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                        for (cats_dt_coord col = 0; col < grid->dimension.cols; col++) {
                                destroy_seed_structure(grid, row, col);
                        }
                        free(grid->seed_bank[row]);
                }
        }


        if (grid->juveniles) {
                for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                        for (cats_dt_coord col = 0; col < grid->dimension.cols; col++) {
                                destroy_juveniles(grid, row, col);
                        }

                        free(grid->juveniles[row]);
                }
       }

        if (grid->seeds_produced) {
                for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                        free(grid->seeds_produced[row]);
                }
        }

        free(grid->seed_bank);
        free(grid->juveniles);
        free(grid->seeds_produced);
        grid->juveniles = NULL;
        grid->seed_bank = NULL;
        grid->seeds_produced = NULL;
}


void cleanup_mpi_seeds(struct cats_configuration *conf, struct cats_grid *grid)
{
#ifdef USEMPI
#ifdef TESTMPI
        log_message(LOG_ERROR, "MPI SKIPPED SEED CLEANUP");
#else
#endif
assert(conf != NULL);
assert(grid != NULL);

int id = grid->id;
int rows_before = 0;
int rows_after = 0;
int radius = 0;

get_rows_around(conf, &rows_before, &rows_after, &radius, id);
if (grid->seeds_0_all) free_grid(&grid->seeds_0_all, grid->dimension.rows + rows_after + rows_before);
#endif
}


/* frees the memory allocated by the grid */
void cleanup_grid(struct cats_grid *grid)
{
        assert(grid != NULL);
        assert(grid->conf != NULL);

        struct cats_configuration *conf = grid->conf;
        cleanup_grid_population(grid);
        cleanup_grid_seeds_and_juveniles(grid);
        cleanup_statistics(&grid->stats);

#ifdef USEMPI
        cleanup_mpi_seeds(conf, grid);
#endif

        for (int32_t i = 0; i < conf->modules.count; i++) {
                if (grid->param.module_data[i].grid_cleanup_function) {
                        grid->param.module_data[i].grid_cleanup_function(conf, grid, grid->grid_modules[i].module_data);
                        grid->grid_modules[i].module_data = NULL;

                }
        }

        cleanup_grid_stats(grid, conf);
        cleanup_threads(grid->single_thread, 1);
        gsl_rng_free(grid->rng);
}


void do_all_grids(struct cats_grid **grids, struct cats_configuration *conf, grid_function function)
{
        assert(conf != NULL);
        assert(grids != NULL);
        assert(function != NULL);

        int count = conf->grid_count;
        for (int32_t class = 0; class < count; class++) {
                function(grids[class], conf);
        }
}


void initialize_grid_stats(struct cats_grid *grid, struct cats_configuration *conf)
{
        assert(grid != NULL);
        init_statistics(&grid->stats, &conf->stats_registry);
        grid->stats.has_been_populated = (char **) new_raw_2d_array_from_dimension(grid->dimension, sizeof(char));

        char *filename = get_grid_stat_filename(grid, conf);

        log_message(LOG_IMPORTANT, "STATS:: opening stats file '%s' (deleted if already existing)", filename);
        remove(filename);
        grid->stats.file = fopen(filename, "a+");
        ENSURE_FILE_OPENED(grid->stats.file, filename)

        if (conf->output.summary_file) fprintf(conf->output.summary_file, "grid-stats,%s,\n",filename);

        free(filename);
        write_grid_stats(conf, grid, true);
        fflush(grid->stats.file);
}