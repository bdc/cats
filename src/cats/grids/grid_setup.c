// SPDX-License-Identifier: GPL-3.0-or-later
//
// grid_setup.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <string.h>
#include "grid_setup.h"

#include "logging.h"
#include <memory/cats_memory.h>


void setup_grid_seed_structures(const struct cats_configuration *conf, struct cats_grid *const grid, int grid_id)
{
        assert(grid_id >= 0);
        assert(grid != NULL);
        assert(conf != NULL);

        // ANIMALS use different mechanisms
        if (conf->param[grid_id].default_demographics == false) return;

        const int32_t rows = grid->dimension.rows;
        const int32_t cols = grid->dimension.cols;

        if (grid->seed_bank || grid->dispersed_seeds || grid->seeds_produced) {
                log_message(LOG_ERROR, "%s: seed structures already created, exiting.", __func__);
                exit(EXIT_FAILURE);
        }

        grid->dispersed_seeds = (cats_dt_seeds **) new_raw_2d_array_from_dimension(grid->dimension,
                                                                                   sizeof(cats_dt_seeds));
        grid->seeds_produced = (cats_dt_seeds **) new_raw_2d_array_from_dimension(grid->dimension,
                                                                                  sizeof(cats_dt_seeds));
        grid->seed_bank = calloc_or_die(rows, sizeof(cats_dt_seeds **));

        for (cats_dt_coord row = 0; row < rows; row++) {
                grid->seed_bank[row] = calloc_or_die(cols, sizeof(cats_dt_seeds *));

                for (cats_dt_coord col = 0; col < cols; col++) {
                        grid->seed_bank[row][col] = NULL;
                }
        }
}


void setup_grid_population_structures(struct cats_grid *grid)
{
        assert(grid != NULL);

        // Make sure we don't leak memory
        assert(grid->_population == NULL);
        assert(grid->population == NULL);

        if (grid->_population || grid->population) {
                log_message(LOG_ERROR, "%s: population structures already created, exiting.", __func__);
                exit(EXIT_FAILURE);
        }

        grid->_population = new_raw_1d_array(grid->dimension.rows * grid->dimension.cols, sizeof(cats_dt_population));
        grid->population = calloc_or_die(grid->dimension.rows, sizeof(cats_dt_population *));

        for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                grid->population[row] = &grid->_population[row * grid->dimension.cols];
        }
}


void setup_grid_juvenile_structures_opt(struct cats_configuration *conf, struct cats_grid *grid, int grid_id)
{
        assert(grid != NULL);
        assert(conf != NULL);
        assert(grid_id >= 0);

        if (grid->juveniles) {
                log_message(LOG_ERROR, "%s: juvenile structures already created, exiting.", __func__);
                exit(EXIT_FAILURE);
        }

        grid->juveniles = calloc_or_die(grid->dimension.rows, sizeof(cats_dt_population **));

        for (cats_dt_coord row = 0; row < grid->dimension.rows; row++) {
                grid->juveniles[row] = calloc_or_die(grid->dimension.cols, sizeof(cats_dt_population *));

                for (cats_dt_coord col = 0; col < grid->dimension.cols; col++) {
                        grid->juveniles[row][col] = NULL;
                }
        }
}

