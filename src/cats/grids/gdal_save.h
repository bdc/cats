// SPDX-License-Identifier: GPL-3.0-or-later
//
// gdal_save.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_GDAL_SAVE_H_
#define CATS_GDAL_SAVE_H_

#include "configuration/configuration.h"
#include "gdal_load.h"

void *save_dispersed_seeds_to_gdal(struct cats_grid *grid, struct cats_configuration *conf);

void *save_population_to_gdal(struct cats_grid *grid, struct cats_configuration *conf);

void *save_juveniles_to_gdal(struct cats_grid *grid, struct cats_configuration *conf);

void *save_seeds_to_gdal(struct cats_grid *grid, struct cats_configuration *conf);

void *
save_grid_to_gdal(struct grid_wrapper *gridwrapper, GDALDataType out_datatype, const struct cats_configuration *conf,
                  const char *filename, char *description);

void *save_suitability_to_gdal(struct cats_configuration *conf, struct cats_environment *env);

#ifdef USEMPI

void *save_population_to_gdal_mpi(struct cats_grid *grid, struct cats_configuration *conf);

#endif

#endif