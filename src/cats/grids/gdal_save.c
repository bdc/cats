// SPDX-License-Identifier: GPL-3.0-or-later
//
// gdal_save.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <gdal.h>
#include <cpl_string.h>
#include <assert.h>
#include "gdal_save.h"
#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include "memory/raw_arrays.h"
#include "logging.h"
#include "paths/paths.h"
#include "gdal_load.h"
#include "gdal_helper.h"
#include "memory/arrays.h"
#include "paths/output_paths.h"
#include "inline.h"
#include "inline_vital_rates.h"


void *save_seeds0_to_gdal_mpi(struct cats_grid *grid, struct cats_configuration *conf);


void *save_population_to_gdal(struct cats_grid *grid, struct cats_configuration *conf)
{
#ifdef USEMPI
        save_population_to_gdal_mpi(grid, conf);
        //save_seeds0_to_gdal_mpi(grid, conf);
        return 0;

#endif
        assert(grid != NULL);
        assert(conf != NULL);

        int id = grid->id;
        assert(id >= 0);
        if (id == 0) log_message(LOG_INFO, "cycle %3d: saving adult population", conf->time.year_current);

        char *filename = get_current_population_filename(grid, conf);

        struct grid_wrapper data = gridwrapper(grid->population, grid->dimension);
        save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
        if (global.enable_json_output) {
                fflush(stdout);
                fprintf(stderr,
                        "JSON::{\"msgtype\": \"population-saved\", \"grid-id\": %d, \"file-name\": \"%s\", \"year\": %d, \"max-cc\": %d}\n;",
                        grid->id, filename, conf->time.year_current,
                        //(cats_dt_population) get_max_rate_from_grid(grid, VR_CARRYING_CAPACITY));
                        (cats_dt_population) get_vital_rate_maximum(&grid->param.carrying_capacity));
                fflush(stderr);
        }

        if (conf->output.summary_file) fprintf(conf->output.summary_file, "population,%s,%d\n",filename, conf->time.year_current);


        free(filename);
        return 0;
}


void *save_suitability_to_gdal(struct cats_configuration *conf, struct cats_environment *env)
{
        if (env->type != ENVIRONMENT_TYPE_SUITABILITY) {
                return 0;
        }
#ifdef USEMPI
        log_message(LOG_UNIMPLEMENTED, "%s: not yet implemented for MPI", __func__);
        abort();
        //save_population_to_gdal_mpi(grid, conf);
        //save_seeds0_to_gdal_mpi(grid, conf);
        return 0;

#endif
        char *filename = get_environment_output_filename(conf, env);


        struct grid_wrapper data = gridwrapper(env->environments[0]->current.values, conf->geometry.dimension);
        save_grid_to_gdal(&data, GDT_Float32, conf, filename, env->name);
        if (global.enable_json_output) {
                fflush(stdout);
                fprintf(stderr,
                        "JSON::{\"msgtype\": \"suitability-saved\", \"name\": %s, \"file-name\": \"%s\", \"year\": %d}\n;",
                        env->name, filename, conf->time.year_current);
                fflush(stderr);
        }
        free(filename);
        return 0;
}


void *save_dispersed_seeds_to_gdal(struct cats_grid *grid, struct cats_configuration *conf)
{
#ifdef USEMPI
        log_message(LOG_UNIMPLEMENTED, "saving seeds not supported in mpi version");
        return 0;

#endif
        assert(grid != NULL);
        assert(conf != NULL);

        int id = grid->id;
        assert(id >= 0);
        if (id == 0) log_message(LOG_INFO, "cycle %3d: saving dispersed seeds", conf->time.year_current);


        char *filename = get_current_dispersed_seeds_filename(grid, conf);

        double **seeds = new_raw_2d_array_from_dimension(grid->dimension,
                                                         sizeof(double));
        const int32_t rows = grid->dimension.rows;
        const int32_t cols = grid->dimension.cols;
        for (cats_dt_coord row = 0; row < rows; row++) {
                for (cats_dt_coord col = 0; col < cols; col++) {
                        seeds[row][col] = grid->dispersed_seeds[row][col];
                }
        }


        struct grid_wrapper data = gridwrapper(seeds, grid->dimension);
        save_grid_to_gdal(&data, GDT_Float64, conf, filename, conf->param[id].species_name);
        free(filename);
        free_grid(&seeds, grid->dimension.rows);


        return 0;
}


void *save_seeds_to_gdal(struct cats_grid *grid, struct cats_configuration *conf)
{
#ifdef USEMPI
        log_message(LOG_UNIMPLEMENTED, "saving seeds not supported in mpi version");
        return 0;

#endif
        assert(grid != NULL);
        assert(conf != NULL);

        int id = grid->id;
        assert(id >= 0);
        if (id == 0) log_message(LOG_INFO, "cycle %3d: saving seeds population", conf->time.year_current);
        const int32_t seed_persistence = get_vital_age(grid, VA_SEED_PERSISTENCE);

        for (int i = 0; i < seed_persistence; i++) {
                char *filename = get_current_seeds_filename(grid, conf, i);

                int32_t **seeds = new_raw_2d_array_from_dimension(grid->dimension,
                                                                  sizeof(int32_t)); //new_int32t_grid(grid->rows, grid->cols);
                const int32_t rows = grid->dimension.rows;
                const int32_t cols = grid->dimension.cols;
                for (cats_dt_coord row = 0; row < rows; row++) {
                        for (cats_dt_coord col = 0; col < cols; col++) {
                                if (i > 0) {
                                        int year = i - 1;
                                        if (grid->seed_bank[row][col]) {
                                                seeds[row][col] = (int32_t) grid->seed_bank[row][col][year];
                                        } else {
                                                seeds[row][col] = 0;
                                        }
                                } else {
                                        seeds[row][col] = (int32_t) grid->dispersed_seeds[row][col];
                                }
                        }
                }


                struct grid_wrapper data = gridwrapper(seeds, grid->dimension);
                save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
                free(filename);
                free_grid(&seeds, grid->dimension.rows);
        }

        return 0;
}


void *save_juveniles_to_gdal(struct cats_grid *grid, struct cats_configuration *conf)
{
#ifdef USEMPI
        log_message(LOG_UNIMPLEMENTED, "saving juveniles not supported in mpi version");
        return 0;

#endif
        assert(grid != NULL);
        assert(conf != NULL);

        int id = grid->id;
        assert(id >= 0);
        if (id == 0) log_message(LOG_INFO, "cycle %3d: saving juveniles population", conf->time.year_current);

        const cats_dt_coord rows = grid->dimension.rows;
        const cats_dt_coord cols = grid->dimension.cols;
        const int32_t max_age_of_maturity = get_vital_age(grid, VA_AGE_OF_MATURITY_MAX);

        for (int i = 0; i < max_age_of_maturity + 1; i++) {

                char *filename = get_current_juvenile_filename(grid, conf, i);

                int32_t **juvs = new_raw_2d_array(rows, cols,
                                                  sizeof(int32_t));//new_int32t_grid(grid->rows, grid->cols);
                for (cats_dt_coord row = 0; row < rows; row++) {
                        for (cats_dt_coord col = 0; col < cols; col++) {
                                if (grid->juveniles[row][col]) {
                                        juvs[row][col] = grid->juveniles[row][col][i];
                                } else {
                                        juvs[row][col] = 0;
                                }
                        }
                }
                struct cats_dimension dim = {.rows = rows, .cols = cols};
                struct grid_wrapper data = gridwrapper(juvs, dim);
                save_grid_to_gdal(&data, GDT_Int32, conf, filename, conf->param[id].species_name);
                free(filename);
                free_grid(&juvs, grid->dimension.rows);
        }

        return 0;
}


CPLErr write_row_chunk(GDALRasterBandH band, const struct grid_wrapper *grid, int row, GDALDataType out_datatype)
{
        int x_offset = 0;
        int y_offset = row;
        int width = grid->dimension.cols;
        int height = 1;

        void *data = get_data_pointer(grid, row, width, out_datatype);
        assert(data != NULL);


        return GDALRasterIO(band, GF_Write, x_offset, y_offset, width, height, data, width, height, out_datatype, 0, 0);
}


void *
save_grid_to_gdal(struct grid_wrapper *gridwrapper, GDALDataType out_datatype, const struct cats_configuration *conf,
                  const char *filename, char *description)
{
        cats_dt_coord rows = gridwrapper->dimension.rows;
#ifndef NDEBUG
        cats_dt_coord cols = gridwrapper->dimension.cols;
        assert(cols > 0);
#endif

        assert(gridwrapper != NULL);
        assert(conf != NULL);
        assert(rows > 0);
        assert(filename != NULL);

        double nodata_value = gridwrapper->nodata;

        GDALDatasetH dataset = create_gdal_dataset(&conf->geometry, filename, gridwrapper->dimension, 1, out_datatype,
                                                   conf);
        GDALRasterBandH band = GDALGetRasterBand(dataset, 1);

        set_gdal_dataset_geo_info(dataset, &conf->geometry, &gridwrapper->offset);
        set_gdal_band_nodata(band, nodata_value);
        set_gdal_band_metadata(band, GCI_GrayIndex, description);
        set_gdal_dataset_metadata(dataset, "configuration", conf->output.file_content);

        band = GDALGetRasterBand(dataset, 1);

        for (cats_dt_coord row = 0; row < rows; row++) {
                CPLErr error = write_row_chunk(band, gridwrapper, row, gridwrapper->dt);

                if (error != CE_None) {
                        log_message(LOG_ERROR, "error writing data to file %s", filename);
                        exit(EXIT_FAILURE);
                }
        }

        GDALClose(dataset);
        return 0;
}
