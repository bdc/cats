// SPDX-License-Identifier: GPL-3.0-or-later
//
// grid_wrapper.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include "cats_global.h"
#include "logging.h"
#include "grid_wrapper.h"


struct grid_wrapper gridwrapper_from_int32t(int32_t **data, struct cats_dimension dimension)
{
        struct grid_wrapper result = {0};
        result.data_int32 = data;
        result.nodata = -9999;
        result.dt = GDT_Int32;
        result.dimension.rows = dimension.rows;
        result.dimension.cols = dimension.cols;
        return result;
}


struct grid_wrapper gridwrapper_from_double(double **data, struct cats_dimension dimension)
{
        struct grid_wrapper result = {0};
        result.data_double = data;
        result.nodata = -9999;
        result.dt = GDT_Float64;
        result.dimension.rows = dimension.rows;
        result.dimension.cols = dimension.cols;
        return result;
}


struct grid_wrapper gridwrapper_from_float(float **data, struct cats_dimension dimension)
{
        struct grid_wrapper result = {0};
        result.offset.cols = 0;
        result.offset.rows = 0;
        result.data_float = data;
        result.nodata = -9999;
        result.dt = GDT_Float32;

        result.dimension.rows = dimension.rows;
        result.dimension.cols = dimension.cols;
        return result;
}


struct grid_wrapper gridwrapper_from_env(cats_dt_environment **data, struct cats_dimension dimension)
{
        struct grid_wrapper result = {0};
        result.data_float = data;
        result.nodata = -9999;
        result.dt = GDT_Float32;

        result.dimension.rows = dimension.rows;
        result.dimension.cols = dimension.cols;
        return result;
}


struct grid_wrapper gridwrapper_1d(void *data, GDALDataType dt, struct cats_dimension dimension)
{
        struct grid_wrapper result = {0};
        result.nodata = -9999;
        result.dt = dt;
        result.data_1d = true;
        result.dimension.rows = dimension.rows;
        result.dimension.cols = dimension.cols;

        switch (dt) {
                case GDT_Float32:
                        result.data_float_1d = data;
                        break;
                case GDT_Float64:
                        result.data_double_1d = data;
                        break;
                case GDT_Int32:
                        result.data_int32_1d = data;
                        break;
                default:
                        log_message(LOG_ERROR, "unknown datatype %d", dt);
                        exit(EXIT_FAILURE);
        }
        return result;
}


void *get_data_pointer_1d(const struct grid_wrapper *grid, cats_dt_coord row, cats_dt_coord cols, GDALDataType datatype)
{
        switch (datatype) {
                case GDT_Byte:
                        return &grid->data_char_1d[row * cols];
                case GDT_Int32:
                        return &grid->data_int32_1d[row * cols];
                case GDT_Float32:
                        return &grid->data_float_1d[row * cols];
                case GDT_Float64:
                        return &grid->data_double_1d[row * cols];
                default:
                        log_message(LOG_ERROR, "unhandled datatype for saving");
                        exit(EXIT_FAILURE);
        }
}


void *get_data_pointer_2d(const struct grid_wrapper *grid, cats_dt_coord row, cats_dt_coord cols, GDALDataType datatype)
{
        assert(cols >= 0);
        switch (datatype) {
                case GDT_Byte:
                        return grid->data_char[row];
                case GDT_Int32:
                        return grid->data_int32[row];
                case GDT_Float32:
                        return grid->data_float[row];
                case GDT_Float64:
                        return grid->data_double[row];
                default:
                        log_message(LOG_ERROR, "unhandled datatype for saving");
                        exit(EXIT_FAILURE);
        }
}


void *get_data_pointer(const struct grid_wrapper *grid, cats_dt_coord row, cats_dt_coord cols, GDALDataType datatype)
{
        if (grid->data_1d) return get_data_pointer_1d(grid, row, cols, datatype);

        return get_data_pointer_2d(grid, row, cols, datatype);
}
