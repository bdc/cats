// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_grid.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include "data/cats_datatypes.h"
#include "data/cats_grid.h"
#include "configuration/configuration.h"

int64_t raster_cell_count(struct cats_dimension raster);

void initialize_grid(struct cats_grid *grid, struct cats_configuration *conf, struct cats_dimension dimension, int id);


void cleanup_grid(struct cats_grid *grid);

struct cats_grid *create_and_initialize_grid(struct cats_configuration *conf, int32_t id, struct cats_grid **parent);

struct cats_grid **
create_and_initialize_grids(struct cats_configuration *conf, int32_t count);

void cleanup_array_of_grids(struct cats_grid ***grid, int grid_count);

void do_all_grids(struct cats_grid **grids, struct cats_configuration *conf, grid_function function);

void check_raster_dimensions(struct cats_dimension to_check, struct cats_dimension reference);

