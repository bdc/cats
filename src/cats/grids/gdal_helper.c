// SPDX-License-Identifier: GPL-3.0-or-later
//
// gdal_helper.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdbool.h>
#include <math.h>
#include <gdal.h>
#include <cpl_string.h>
#include <assert.h>

#include <logging/logging.h>
#include "gdal_load.h"
#include "configuration/configuration.h"
#include "misc/misc.h"
#include "data/simulation_geometry.h"


bool gdal_register_if_necessary(bool registered)
{
        if (!registered) GDALAllRegister();
        return true;
}


void gdal_destroy(void)
{
        GDALDestroy();
}


void get_gdal_pixel_size(GDALDatasetH dataset, double *x_res, double *y_res)
{
        assert(x_res != NULL);
        assert(y_res != NULL);
        double cx, cy, transform[6];

        if (GDALGetGeoTransform(dataset, transform) != CE_None) {
                log_message(LOG_ERROR, "GDAL could not get transform from dataset");
                exit_cats(EXIT_FAILURE);
        }

        cx = transform[1];
        cy = transform[5]; // was - transform

        if (fabs(cx) != fabs(cy)) {
                log_message(LOG_ERROR, "CATS only supports square cells. "
                                       "Found horizontal resolution %f != vertical resolution %f", cx, cy);
                exit_cats(EXIT_FAILURE);
        }

        *x_res = cx;
        *y_res = cy;
}


const char *get_gdal_output_driver_from_extension(const char *extension)
{
        char *driver = NULL;
        char *tiff = "GTiff";

        assert(extension != NULL);

        if (!strncmp(extension, "tiff", 4)) driver = tiff;

        if (!driver) {
                log_message(LOG_ERROR, "unknown output type '%s'. Currently on 'tiff' is supported.", extension);
                exit_cats(EXIT_FAILURE);
        }

        return driver;
}


void print_gdal_info(GDALDatasetH dataset)
{
        assert(dataset);

        /*
        GDALDriverH driver = GDALGetDatasetDriver(dataset);

        int x = GDALGetRasterXSize(dataset);
        int y = GDALGetRasterYSize(dataset);
        int rastercount = GDALGetRasterCount(dataset);

        log_message(LOG_DEBUG, "GDAL: used driver %s/%s\n", GDALGetDriverShortName(driver), GDALGetDriverLongName(driver));
        log_message(LOG_DEBUG, "GDAL: found %d gdal_raster bands, x: %d y: %d", rastercount, x, y);

        if (GDALGetProjectionRef(dataset) != NULL) log_message(LOG_DEBUG, "GDAL: found projection_string '%s'", GDALGetProjectionRef(dataset));


        double transform[6];

        if (GDALGetGeoTransform(dataset, transform) == CE_None) {
                log_message(LOG_DEBUG, "GDAL: origin     = (%.6f,%.6f)", transform[0], transform[3]);
                log_message(LOG_DEBUG, "GDAL: pixel Size = (%.6f,%.6f)", transform[1], transform[5]);
        }

        for (int i = 1; i <= rastercount; i++) {
                GDALRasterBandH band;
                band = GDALGetRasterBand(dataset, i);
                int has_min, has_max, success;
                float nodata = GDALGetRasterNoDataValue(band, &success);
                double min_val = GDALGetRasterMinimum(band, &has_min);
                double max_val = GDALGetRasterMaximum(band, &has_max);

                if ((has_min && has_max)) {
                        log_message(LOG_INFO, "GDAL: band %d gdal_raster minimum value: %.3fd  maximum value: %.3f", min_val, max_val);
                }
                
                if (success) {
                        log_message(LOG_INFO, "GDAL: band %d has nodata value: %f", i, nodata);
                } else {
                        log_message(LOG_INFO, "GDAL: band %d has no nodata value defined", i);
                }
        }
         */
}


void verify_raster_geometry(GDALDatasetH dataset, struct simulation_geometry *geometry)
{
        int32_t errors = 0;
        double x_res, y_res;
        get_gdal_pixel_size(dataset, &x_res, &y_res);

        if (geometry->cell_size_x != x_res || geometry->cell_size_y != y_res) {
                log_message(LOG_ERROR, "%s: GEOM: resolution (%f %f) not equal to global resolution (%f %f)",
                            __func__, x_res, y_res, geometry->cell_size_x, geometry->cell_size_y);
                errors += 1;
        }
        cats_dt_coord rows = GDALGetRasterYSize(dataset);
        cats_dt_coord cols = GDALGetRasterXSize(dataset);
#ifdef USEMPI
        if (geometry->dimension_orig.rows != rows || geometry->dimension_orig.cols != cols) {
                log_message(LOG_WARNING, "GEOM: dimension (%d %d) not equal to original global dimensions (%d %d)",
                            rows, cols, geometry->dimension_orig.rows, geometry->dimension_orig.cols);
                errors += 1;
        }
#else
        if (geometry->dimension.rows != rows || geometry->dimension.cols != cols) {
                log_message(LOG_ERROR, "GEOM: dimension (%d %d) not equal to global dimensions (%d %d)",
                            rows, cols, geometry->dimension.rows, geometry->dimension.cols);
                errors += 1;
        }
#endif

        char *projection = (char *) GDALGetProjectionRef(dataset);
        if (geometry->projection_string == NULL || strlen(geometry->projection_string) == 0) {
                log_message(LOG_INFO, "GEOM: global projection string not set, ignoring projection '%s'", projection);
        } else {
                if (strcmp(projection, geometry->projection_string) != 0) {
                        enum cats_log_level ll;
                        if (geometry->ignore_raster_projection) {
                                ll = LOG_INFO;
                        } else {
                                ll = LOG_ERROR;
                                errors += 1;
                        }
                        // log_message(ll, "GEOM: global projection string '%s' not equal to projection '%s'", geometry->projection_string, projection);
                        log_message(ll, "GEOM: projection mismatch with reference projection");

                }

        }


        if (errors) {
                log_message(LOG_ERROR, "%s: found errors, quitting", __func__);
                exit_cats(EXIT_FAILURE);
        }
}


void get_projection_from_file(struct simulation_geometry *geometry, const char *filename)
{
        assert(geometry != NULL);

        if (geometry->projection_string != NULL) return;

        GDALDatasetH dataset = load_gdal_dataset(geometry, filename, false);
        char *projection = (char *) GDALGetProjectionRef(dataset);

        if (geometry->projection_string == NULL || projection == NULL) {
                log_message(LOG_IMPORTANT, "GDAL file '%s' contains no projection information", filename);
        }

        GDALClose(dataset);
}


GDALDatasetH
create_gdal_dataset(const struct simulation_geometry *geometry, const char *filename, struct cats_dimension dimension,
                    int bands,
                    GDALDataType datatype, const struct cats_configuration *conf)
{
        assert(bands > 0);
        char *file_format = "tiff";
        if (geometry->file_format) file_format = geometry->file_format;

        GDALDriverH driver = GDALGetDriverByName(get_gdal_output_driver_from_extension(file_format));

        char **options = NULL;
        options = CSLSetNameValue(options, "COMPRESS", "DEFLATE");
        options = CSLSetNameValue(options, "PREDICTOR", "1");
        options = CSLSetNameValue(options, "ZLEVEL", "9");
        options = CSLSetNameValue(options, "NUM_THREADS", "ALL_CPUS");


        GDALDatasetH dataset = GDALCreate(driver, filename, dimension.cols, dimension.rows, bands, datatype, options);

        if (dataset == NULL) {
                log_message(LOG_ERROR, "GDAL: could not create dataset '%s'\n", filename);
                exit(EXIT_FAILURE);
        }

        GDALSetMetadataItem(dataset, "TIFFTAG_SOFTWARE", cats_version(), NULL);
        GDALSetMetadataItem(dataset, "TIFFTAG_IMAGEDESCRIPTION", conf->output.file_content, NULL);

        CSLDestroy(options);
        return dataset;
}


void set_gdal_dataset_geo_info(GDALDatasetH dataset, const struct simulation_geometry *geometry,
                               const struct cats_dimension *offset)
{
        assert(geometry != NULL);
        assert(dataset != NULL);

        double cell_size_x = geometry->cell_size_x;
        double cell_size_y = geometry->cell_size_y;

        char *projection_string = geometry->projection_string;
        double left = geometry->origin_x;
        double top = geometry->origin_y;

        if (offset != NULL && offset->rows != 0) {
                top += offset->rows * cell_size_y;
        }

        if (isnan(left)) { left = 0; }
        if (isnan(top)) { top = 0; }
        double transform[6] = {left, cell_size_x, 0, top, 0, cell_size_y};

        CPLErr error = GDALSetGeoTransform(dataset, transform);
        if (error != CE_None) log_message(LOG_WARNING, "GDAL: error setting geo transform");

        if (projection_string == NULL) return;

        error = GDALSetProjection(dataset, projection_string);
        if (error != CE_None) {
                log_message(LOG_WARNING, "GDAL error setting projection '%s': error code %d", projection_string, error);
        }
}


void set_gdal_band_metadata(GDALRasterBandH band, GDALColorInterp colorint, const char *description)
{
        CPLErr error = GDALSetRasterColorInterpretation(band, colorint);

        if (error != CE_None) {
                log_message(LOG_WARNING, "error setting color interpretation of band %d to GCI_GrayIndex", 1);
        }

        if (description) {
                GDALSetDescription(band, description);
        }
}


void set_gdal_dataset_metadata(GDALDatasetH dataset, const char *name, const char *value)
{
        if (value == NULL) return;

        CPLErr rc = GDALSetMetadataItem(dataset, name, value, "TEXT");
        if (rc != CE_None) {
                log_message(LOG_WARNING, "Error setting metadata item '%s': %d", name, rc);
        }
}


void set_gdal_band_nodata(GDALRasterBandH band, double nodata)
{
        CPLErr error = GDALSetRasterNoDataValue(band, nodata);
        if (error != CE_None) {
                log_message(LOG_WARNING, "error setting nodata value of band %d to %f\n", 1, nodata);
        }
}


void load_global_geometry(struct simulation_geometry *geometry, const char *filename)
{
        assert(geometry != NULL);
        assert(filename != NULL);
        log_message(LOG_IMPORTANT, "%s: reading global geometry from '%s'", __func__, filename);
        GDALDatasetH dataset = load_gdal_dataset(geometry, filename, true);
        double transform[6] = {0};

        // LOAD ORIGIN AND RESOLUTION
        if (GDALGetGeoTransform(dataset, transform) == CE_None) {
                if (isnan(transform[0]) || isnan(transform[3])) {
                        log_message(LOG_IMPORTANT,
                                    "GEOM: file '%s' specifies no origin, origin set to upper left x: 0, y: 0",
                                    filename);
                        geometry->origin_x = 0;
                        geometry->origin_y = 0;
                } else {
                        geometry->origin_x = transform[0];
                        geometry->origin_y = transform[3];
                        log_message(LOG_IMPORTANT, "GEOM: setting origin to upper left x: %f y: %f", geometry->origin_x,
                                    geometry->origin_y);
                }
                double cx = transform[1];
                double cy = transform[5]; // was - transform

                if (fabs(cx) != fabs(cy)) {
                        log_message(LOG_ERROR, "GEOM: CATS supports square cells only, cell size of '%s' is %fx%f",
                                    filename, cx, cy);
                        exit(EXIT_FAILURE);
                } else {
                        log_message(LOG_IMPORTANT, "GEOM: setting cell resolution to %f %f", cx, cy);
                        geometry->cell_size_x = cx;
                        geometry->cell_size_y = cy;

                }

        } else {
                log_message(LOG_ERROR, "GEOM: %s: unable to load geometry from file '%s'", __func__, filename);
                exit(EXIT_FAILURE);
        }

        // LOAD ROWS AND COLS
        cats_dt_coord rows = GDALGetRasterYSize(dataset);
        cats_dt_coord cols = GDALGetRasterXSize(dataset);
        geometry->dimension.rows = rows;
        geometry->dimension.cols = cols;
        log_message(LOG_IMPORTANT, "GEOM: setting raster dimensions to %d rows and %d cols", rows, cols);
        char *projection = (char *) GDALGetProjectionRef(dataset);
        if (projection == NULL || strlen(projection) == 0) {
                log_message(LOG_IMPORTANT, "file '%s' contains no projection information, projection will be ignored",
                            filename);
        } else {
                geometry->projection_string = strdup(projection);
                log_message(LOG_IMPORTANT, "GEOM: setting projection to '%s'", projection);
        }


        GDALClose(dataset);
}
