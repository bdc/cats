// SPDX-License-Identifier: GPL-3.0-or-later
//
// grid_converters.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <math.h>

#include "data/cats_grid.h"
#include "configuration/configuration.h"
#include <memory/cats_memory.h>

#include "logging.h"
#include "actions/cats_actions.h"
#include "populations/population.h"
#include "inline_population.h"
#include "dispersal/dispersal.h"


cats_dt_environment **convert_double_to_environment(const struct cats_2d_array_double *in_grid)
{
        log_message(LOG_DEBUG, "%s: ALLOCATING NEW GRID NOW", __func__);
        cats_dt_environment **array = new_raw_2d_array_from_dimension(in_grid->dimension, sizeof(cats_dt_environment));
        const cats_dt_coord rows = in_grid->dimension.rows;
        const cats_dt_coord cols = in_grid->dimension.cols;
        for (int32_t r = 0; r < rows; r++) {
                for (int32_t c = 0; c < cols; c++) {
                        array[r][c] = (float) in_grid->data[r][c];
                }
        }

        return array;
}


void initialize_population(struct cats_grid *grid, const struct cats_2d_array_double *data_grid)
{
        for (cats_dt_coord row = 0; row < data_grid->dimension.rows; row++) {
                for (cats_dt_coord col = 0; col < data_grid->dimension.cols; col++) {
                        double val = data_grid->data[row][col];
                        if (isnan(val) || val < 0) val = 0;
                        set_population_ignore_cc(grid, row, col,
                                                 (cats_dt_population) round(val)); // initialize population
                }
        }
}


void create_and_fill_vector_data(double **data, struct cats_dispersal *d, int vec, int rows, int cols)
{
        d->p[vec] = calloc_or_die(rows, sizeof(cats_dt_dispersal *)); // FIXME WAIT FOR DISPERSAL use grid
        d->plin[vec] = calloc_or_die(rows * cols, sizeof(cats_dt_dispersal));

        for (int32_t r = 0; r < rows; r++) {
                d->p[vec][r] = calloc_or_die(cols, sizeof(cats_dt_dispersal));

                for (int32_t c = 0; c < cols; c++) {
                        nan_to_zero(&data[r][c]);
                        if (data[r][c] < 0.0 || data[r][c] > 1.0) {
                                log_message(LOG_ERROR, "dispersal value at [%d, %d] out of range %f", r, c, data[r][c]);
                                exit(EXIT_FAILURE);
                        }

                        d->p[vec][r][c] = (float) data[r][c];
                        d->plin[vec][r * cols + c] = (float) data[r][c];
                }
        }
}
