// SPDX-License-Identifier: GPL-3.0-or-later
//
// gdal_load.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#pragma once

#include <gdal.h>
#include "data/cats_datatypes.h"
#include "configuration/configuration.h"
#include "grid_wrapper.h"
#include <memory/arrays.h>
#include "data/simulation_geometry.h"

enum RASTER_LOAD_TYPE {
        LOAD_INVALID,
        LOAD_ENVIRONMENT,
        LOAD_POPULATION,

};

struct raster_load_wrapper {

        char *filename;
        struct simulation_geometry *geometry;
        bool ignore_offset;
        bool ignore_geometry;
        cats_dt_environment **target_environment;
        struct cats_grid *target_grid;
        enum RASTER_LOAD_TYPE type;
};

void load_gdal_data(const struct raster_load_wrapper *data);

cats_dt_environment **load_environment_gdal(char *filename, struct simulation_geometry *geometry);

void load_initial_population_gdal(struct cats_grid *grid, struct simulation_geometry *geometry, const char *filename,
                                  const char *speciesname);


GDALDatasetH load_gdal_dataset(struct simulation_geometry *geometry, const char *filename, bool ignore_geometry);

void load_dispersals_gdal(struct cats_grid *grid, struct cats_configuration *conf, int32_t species_id);


void *
get_data_pointer_2d(const struct grid_wrapper *grid, cats_dt_coord row, cats_dt_coord cols, GDALDataType datatype);

void *
get_data_pointer_1d(const struct grid_wrapper *grid, cats_dt_coord row, cats_dt_coord cols, GDALDataType datatype);

void *get_data_pointer(const struct grid_wrapper *grid, cats_dt_coord row, cats_dt_coord cols, GDALDataType datatype);

struct cats_2d_array_double *
get_double_values_from_gdal(struct simulation_geometry *geometry, const char *filename, bool ignore_geometry,
                            bool ignore_offset);
struct simulation_geometry load_gdal_geometry(const char *filename);
void get_gdal_pixel_size(GDALDatasetH dataset, double *x_res, double *y_res);
#ifdef USEMPI

float **get_grid_from_mpi(struct cats_grid *grid, struct cats_configuration *conf, float **in);

#endif



