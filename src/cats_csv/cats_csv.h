// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_csv.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CSV_H_
#define CATS_CSV_H_

#include <stdint.h>
#include <stdio.h>
#include <cats_strings/cats_strings.h>

///@brief Data structure for simple csv files
struct cats_csv {
        int32_t data_row_count;   ///< number of data rows in the CSV file (not including the header)
        int32_t column_count;     ///< number of data fields (columns) in the CSV file
        char **headers;           ///< array of headers with length \ref cats_csv.field_count -- NOTE: could be a string array
        char ***data;             ///< CSV data stored as strings by row and column
};

struct cats_csv *csv_read_file(FILE *input, int expected_fields);


int32_t csv_get_field_idx(struct cats_csv *csv, const char *field_name);

double csv_get_double_field_idx(struct cats_csv *csv, int32_t row, int32_t field_idx);

int32_t csv_get_int32_field_idx(struct cats_csv *csv, int32_t row, int32_t field_idx);

double csv_get_double_field_name(struct cats_csv *csv, int32_t row, const char *field_name);

void csv_free(struct cats_csv **csv);

struct cats_csv *csv_from_filename(const char *filename, int expected_fields);

#ifdef DEBUG_CSV
#define DBGCSV(X) {X} while(0);
#else
#define DBGCSV(X);
#endif

#endif





