// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_windows.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_WINDOWS_H
#define CATS_CATS_WINDOWS_H

#ifdef WIN32
#define CATS_ON_WINDOWS
#endif

#if defined(__MINGW64__) || defined(__MINGW32__)
#define __USE_MINGW_ANSI_STDIO 1
#endif

#ifdef CATS_ON_WINDOWS
#define strncasecmp strnicmp
#define strcasecmp stricmp
#define strtok_r strtok_s
#define access _access
#define F_OK 0

#include <string.h>
#include <memory/cats_memory.h>

static inline char *strndup(const char *s, size_t n)
{
        char *end_pointer = memchr(s, '\0', n);
        if (end_pointer != NULL) {
                n = end_pointer - s;
        }

        char *result = malloc_or_die(n + 1);
        memcpy(result, s, n);
        result[n] = '\0';
        return result;
}

#endif



#endif //CATS_CATS_WINDOWS_H
