// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_ini_read_values.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "../logging/logging.h"
#include "cats_ini.h"
#include "../cats_strings/cats_strings.h"
#include "cats_ini_read_values.h"

#include "../cats_strings/string_helpers.h"
#include "cats_ini_helpers.h"
#include <stdio.h>


char *get_value(const struct cats_ini *ini, const char *section, const char *key)
{
        assert(ini != NULL);
        assert(section != NULL);
        assert(key != NULL);


        int section_idx = get_section_index(ini, section);

        if (section_idx < 0) {
                add_complement_value_read((struct cats_ini *) ini, (char *) section, (char *) key);
                return NULL;
        }

        int key_index = get_key_index(ini, section_idx, key);
        if (key_index < 0) {
                add_complement_value_read((struct cats_ini *) ini, (char *) section, (char *) key);
                return NULL;
        }

        int32_t *sec_count = &ini->sections_accessed[section_idx];

        int32_t *count = &ini->values_accessed[section_idx][key_index];
        *count += 1;
        *sec_count += 1;


        return strdup(ini->values[section_idx][key_index]);
}


struct string_array *get_char_array_from_conf2(const struct cats_ini *ini, char *section, char *key)
{
        char *entry = get_value(ini, section, key);
        if (!entry) return new_string_array();

        struct string_array *tokens = get_all_tokens(entry, ",");
        free(entry);
        return tokens;
}


char *get_value_maybe_die(const struct cats_ini *ini, const char *section, const char *key, bool required)
{
        char *value = get_value(ini, section, key);

        if (!required) return value;
        if (get_section_index(ini, section) == -1) {
                log_message(LOG_ERROR,
                            "CONF: failed to load required value for key '%s' from [%s] -- section does not exist",
                            key, section);

                exit(EXIT_FAILURE);
        }
        if (value == NULL || strlen(value) == 0) {
                log_message(LOG_ERROR, "CONF: failed to load required value for key '%s' from [%s]",
                            key, section);

                exit(EXIT_FAILURE);
        }

        return value;
}


bool
get_int32_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required, int32_t *value)
{
        char *string = get_value_maybe_die(ini, section, key, required);
        bool found = string != NULL;
        bool ok = string_to_integer(string, value);

        if (required && !ok) {
                log_message(LOG_ERROR, "[%s]::%s: error converting string '%s' to integer", section, key, string);
                exit(EXIT_FAILURE);
        }

        free(string);
        return found;
}


bool
get_char_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required, char **value)
{
        char *string = get_value_maybe_die(ini, section, key, required);

        if (!string) return false;
        char *copy = string;
        char *result = strdup(left_trim(right_trim(copy)));
        free(string);
        *value = result;
        return true;
}


bool get_long_double_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required,
                                   long double *value)
{
        char *string = get_value_maybe_die(ini, section, key, required);
        bool found = string != NULL;
        bool ok = string_to_long_double(string, value);

        if (required && !ok) {
                log_message(LOG_ERROR, "[%s]::%s: error converting string '%s' to long double", section, key, string);
                exit(EXIT_FAILURE);
        }

        free(string);
        return found;
}


bool get_float_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required,
                             float *value)
{
        char *string = get_value_maybe_die(ini, section, key, required);
        bool found = string != NULL;
        bool ok = string_to_float(string, value);

        if (required && !ok) {
                log_message(LOG_ERROR, "[%s]::%s: error converting string '%s' to float", section, key, string);
                exit(EXIT_FAILURE);
        }

        free(string);
        return found;
}


bool
get_bool_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required, bool *value)
{
        char *string = get_value_maybe_die(ini, section, key, required);
        bool found = string != NULL;
        bool ok = string_to_bool(string, value);

        if (required && !ok) {
                log_message(LOG_ERROR, "[%s]::%s: error converting string '%s' to bool", section, key, string);
                exit(EXIT_FAILURE);
        }

        free(string);
        return found;
}


