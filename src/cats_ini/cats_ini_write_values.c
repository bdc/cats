// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_ini_write_values.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_ini_helpers.h"
#include "../cats_strings/string_helpers.h"
#include "cats_ini.h"
#include "../logging/logging.h"
#include "../cats_strings/cats_strings.h"
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "cats_ini_write_values.h"


int add_section(struct cats_ini *ini, char *section_name)
{
        assert(section_name != NULL);

        int32_t N = ini->n_sections;

        ini->sections = realloc(ini->sections, (N + 1) * sizeof(char *));
        ini->n_keys = realloc(ini->n_keys, (N + 1) * sizeof(int));
        ini->keys = realloc(ini->keys, (N + 1) * sizeof(char **));
        ini->sections_accessed = realloc(ini->sections_accessed, (N + 1) * sizeof(int32_t));
        ini->values_accessed = realloc(ini->values_accessed, (N + 1) * sizeof(int32_t *));
        ini->values = realloc(ini->values, (N + 1) * sizeof(char **));

        ini->sections[N] = section_name;
        ini->sections_accessed[N] = 0;
        ini->n_keys[N] = 0;
        ini->keys[N] = NULL;
        ini->values_accessed[N] = NULL;
        ini->values[N] = NULL;

        DBG(log_message(LOG_RAW, "SECTION: %s\n", ini->sections[N]);)

        ini->n_sections++;
        return N;
}


void maybe_add_section(char *line, struct cats_ini *ini)
{
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        char *start = line + 1;
        char *end = start;

        while (*end != '\0' && *end != ']') end++;

        if (*end != ']') return;

        size_t length = end - start;
        if (length <= 0) {
                fprintf(stderr, "error: empty section name");
                return;
        }

        char *tmp = calloc((length + 1), sizeof(char));
        bool have_colon = false;
        for (int32_t i = 0; i < length; i++) {
                if (start[i] == ':') {
                        have_colon = true;
                        break;
                }

        }
        if (have_colon == false) {
                strncpy(tmp, start, length);
        }
        else {
                for (size_t i = 0; i < length; i++) {
                        tmp[i] = '\0';
                }
                struct string_array *s = get_all_tokens(start, ":");
                char *new = string_array_paste(s, ":");

                free_string_array(&s);
                strncpy(tmp, new, length);
                free(new);
                for (size_t i = length; i > 0; i--) {
                        if (tmp[i] == ']') {
                                tmp[i] = '\0';
                                break;
                        }
                }


        }

        tmp[length] = '\0';

        add_section(ini, tmp);

}


int32_t add_key_val(struct cats_ini *ini, int32_t section_idx, char *key, char *value)
{

        key = left_trim(right_trim(key));
        value = left_trim(right_trim(value));
        //printf("####\n%s\n####\n", key);
        //printf("####\n%s\n####\n", value);
        // how many keys do we already have in this section?
        int32_t KEY = ini->n_keys[section_idx]; // number of keys in section = index of new key

        // make room for one more
        ini->keys[section_idx] = realloc(ini->keys[section_idx], (KEY + 1) * sizeof(char *));
        ini->values[section_idx] = realloc(ini->values[section_idx], (KEY + 1) * sizeof(char *));
        ini->values_accessed[section_idx] = realloc(ini->values_accessed[section_idx], (KEY + 1) * sizeof(int32_t));

        ini->values_accessed[section_idx][KEY] = 0;
        ini->keys[section_idx][KEY] = key;
        ini->values[section_idx][KEY] = value;
        ini->n_keys[section_idx] += 1;
        DBG(log_message(LOG_RAW, "\tKEY: %s VALUE: %s\n", key, val))
        return KEY;
}


void maybe_add_key_val(char *line, struct cats_ini *ini)
{
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        int32_t N_secs = ini->n_sections;
        int32_t SECTION = N_secs - 1;
        if (N_secs == 0) {
                DBG(log_message(LOG_RAW, "IGNORED: line outside of section \"%s\"\n", line);)
                return;
        }

        // remove comments -- everything after the first '#' is removed
        for (size_t i = 0; i < strlen(line); i++) {
                if (line[i] == '#') {
                        line[i] = '\0';
                        break;
                }
        }


        //char *found = strsep(&line, "=");
        char *found = NULL;
        for (size_t i = 0; i < strlen(line); i++) {


                if (line[i] == '=' && i < strlen(line) - 1) {
                        line[i] = '\0';
                        found = &line[i + 1];
                        break;
                }

        }

        fflush(stdout);
        if (line == NULL || (found && strlen(found) == 0)) {
                DBG(log_message(LOG_RAW, "\tIGNORED: empty or invalid line \"%s\"\n", found);)
                return;
        }
        if (found == NULL) return;

        size_t key_len = strlen(line) + 1; // includes \0
        size_t val_len = strlen(found) + 1; // includes \0
        char *key = strndup(line, key_len);
        char *val = strndup(found, val_len);

        add_key_val(ini, SECTION, key, val);

}