// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_ini_write_values.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_CATS_INI_WRITE_VALUES_H
#define CATS_CATS_INI_WRITE_VALUES_H

#include "data/cats_datatypes.h"

int add_section(struct cats_ini *ini, char *section_name);

void maybe_add_section(char *line, struct cats_ini *ini);

int32_t add_key_val(struct cats_ini *ini, int32_t section_idx, char *key, char *value);

void maybe_add_key_val(char *line, struct cats_ini *ini);

#endif //CATS_CATS_INI_WRITE_VALUES_H
