// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_ini.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include "cats_defs.h"

#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include <cats_strings/cats_strings.h>
#include <logging/logging.h>
#include "cats_ini.h"


#include "cats_ini_write_values.h"



struct cats_ini *new_ini(void)
{
        struct cats_ini empty = {0};
        struct cats_ini *ini = malloc(sizeof(struct cats_ini));
        *ini = empty;

        ini->sections = NULL;
        ini->n_sections = 0;
        ini->n_keys = NULL;
        ini->keys = NULL;
        ini->values_accessed = NULL;
        ini->sections_accessed = NULL;
        ini->values = NULL;
        ini->complement = NULL;
        return ini;
}


void free_ini(struct cats_ini *ini)
{
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        const int32_t SECTIONS = ini->n_sections;

        for (int32_t sec = 0; sec < SECTIONS; sec++) {
                const int32_t KEYS = ini->n_keys[sec];

                for (int key = 0; key < KEYS; key++) {
                        free(ini->keys[sec][key]);
                        free(ini->values[sec][key]);
                        ini->keys[sec][key] = NULL;
                        ini->values[sec][key] = NULL;
                        ini->values_accessed[sec][key] = 0;

                }

                free(ini->keys[sec]);
                free(ini->values[sec]);
                free(ini->values_accessed[sec]);
                ini->keys[sec] = NULL;
                ini->values[sec] = NULL;
                ini->values_accessed[sec] = NULL;
        }

        for (int sec = 0; sec < SECTIONS; sec++) {
                free(ini->sections[sec]);
                ini->sections[sec] = NULL;

                ini->sections_accessed[sec] = -1;
        }
        free(ini->sections_accessed);
        free(ini->keys);
        ini->keys = NULL;

        free(ini->values);
        ini->values = NULL;

        free(ini->values_accessed);
        ini->values_accessed = NULL;

        free(ini->n_keys);
        ini->n_keys = NULL;

        free(ini->sections);
        ini->sections = NULL;

        if (ini->complement) free_ini(ini->complement);

        free(ini);
}


struct cats_ini *load_ini_file(const char *filename)
{
        FILE *f = fopen(filename, "r");

        if (f == NULL) {
                log_message(LOG_ERROR, "could not open file '%s' for reading.\nExiting.", filename);
                exit(EXIT_FAILURE);
        }

        struct cats_ini *ini = new_ini();
        ini->complement = new_ini();


        char line[1024] = {0};

        while (fgets(line, 1024, f)) {

                right_trim(line);

                left_trim(line);

                if (line[0] == '#') continue;
                if (line[0] == '[') {

                        maybe_add_section(line, ini);

                } else {
                        maybe_add_key_val(line, ini);

                }
        }

        fclose(f);


        return ini;
}


int get_section_index(const struct cats_ini *ini, const char *section)
{
        // log_message(LOG_ERROR, "could not find section %s", section);
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        for (int i = 0; i < ini->n_sections; i++) {
                if (strcmp(ini->sections[i], section) == 0) return i;
        }

        return -1;
}


int get_key_index(const struct cats_ini *ini, int section_index, const char *key)
{
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        if (section_index < 0 || key == NULL || strlen(key) == 0) return -1;


        const int key_count = ini->n_keys[section_index];
        char **keys = ini->keys[section_index];

        for (int i = 0; i < key_count; i++) {
                if (keys[i] && strcmp(keys[i], key) == 0) return i;
        }

        return -1;
}


int32_t get_section_prefix_count(const struct cats_ini *ini, const char *section_prefix)
{
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        if (section_prefix == NULL) {
                log_message(LOG_ERROR, "%s: function parameter section == NULL", __func__);
                exit(EXIT_FAILURE);
        }


        size_t len = strlen(section_prefix);

        int32_t count = 0;

        for (int32_t i = 0; i < ini->n_sections; i++) {
                if (strlen(ini->sections[i]) < len + 1) continue;


                if (strncmp(ini->sections[i], section_prefix, len) == 0
                    && ini->sections[i][len] == ':'
                        )
                        count++;
        }


        return count;
}


struct string_array *get_sections_with_prefix(const struct cats_ini *ini, const char *section_prefix)
{
        if (ini == NULL) {
                log_message(LOG_ERROR, "%s: function parameter ini == NULL", __func__);
                exit(EXIT_FAILURE);
        }

        if (section_prefix == NULL) {
                log_message(LOG_ERROR, "%s: function parameter section == NULL", __func__);
                exit(EXIT_FAILURE);
        }


        size_t len = strlen(section_prefix);

        struct string_array *result = new_string_array();

        for (int32_t i = 0; i < ini->n_sections; i++) {
                if (strlen(ini->sections[i]) < len + 1) continue;


                if (strncmp(ini->sections[i], section_prefix, len) == 0
                    && ini->sections[i][len] == ':') {
                        string_array_add(result, ini->sections[i]);
                }

        }


        return result;
}


char *full_section_name(const char *section, const char *prefix)
{
        if (prefix == NULL) return strdup(section);
        char *result = compound_string(prefix, section, ":");
        return result;
}