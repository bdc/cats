// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_ini_helpers.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stddef.h>
#include <string.h>
#include "cats_ini_helpers.h"
#include "cats_ini_write_values.h"
#include "misc/misc.h"
#include <logging/logging.h>

void add_complement_value_read(struct cats_ini *ini, char *section, char *key)
{
        if (ini->complement == NULL) return;

        int32_t c_section = get_section_index(ini->complement, section);
        if (c_section < 0) {
                char *section_name = strdup(section);
                c_section = add_section(ini->complement, section_name);
        }
        ini->complement->sections_accessed[c_section] += 1;

        int32_t c_val = get_key_index((struct cats_ini *) ini->complement, c_section, key);

        if (c_val < 0) {
                c_val = add_key_val(ini->complement, c_section, strdup(key), NULL);
        }
        ini->complement->values_accessed[c_section][c_val] += 1;
}


void print_accesses(struct cats_ini *ini)
{
        const int32_t SECTIONS = ini->n_sections;
        for (int32_t sec = 0; sec < SECTIONS; sec++) {
                const int32_t KEYS = ini->n_keys[sec];
                for (int key = 0; key < KEYS; key++) {
                        log_message(LOG_RAW, "%s::%s has value '%s' and was accessed", ini->sections[sec], ini->keys[sec][key],
                               ini->values[sec][key]);
                        log_message(LOG_RAW, "% d times\n", ini->values_accessed[sec][key]);
                }
        }
}


void print_unused_values(struct cats_ini *ini, const char *filename)
{
        fprintf(stdout, "%s\n", TEXT_DIVIDER);
        fprintf(stdout, "%sConfiguration parameters that were not used in '%s'%s\n\n", C_YELLOW, filename, C_RESET);

        const int32_t SECTIONS = ini->n_sections;
        for (int32_t sec = 0; sec < SECTIONS; sec++) {
                if (ini->sections_accessed[sec] == 0) {
                        fprintf(stdout, "Section was never read: [%s]\n", ini->sections[sec]);
                }
                const int32_t KEYS = ini->n_keys[sec];
                for (int key = 0; key < KEYS; key++) {
                        if (ini->values_accessed[sec][key] > 0) continue;
                        fprintf(stdout, "Value was never read: [%s] %s = %s\n", ini->sections[sec],
                                ini->keys[sec][key],
                                ini->values[sec][key]);
                }
        }
        fprintf(stdout, "\n");
}


void print_complement_values(struct cats_ini *ini, const char *filename)
{

        fprintf(stdout, "%s\n", TEXT_DIVIDER);
        fprintf(stdout, "%s(Optional) configuration parameters that were not found in '%s'%s\n\n", C_YELLOW, filename,
                C_RESET);

        struct cats_ini *c = ini->complement;

        const int32_t SECTIONS = c->n_sections;
        for (int32_t sec = 0; sec < SECTIONS; sec++) {
                const int32_t KEYS = c->n_keys[sec];

                for (int key = 0; key < KEYS; key++) {
                        if (c->values_accessed[sec][key] > 0) {
                                fprintf(stdout, "Optional configuration parameter was not found: [%s] %s\n",
                                        c->sections[sec],
                                        c->keys[sec][key]);
                        }
                }
        }
        fprintf(stdout, "\n\n");
}