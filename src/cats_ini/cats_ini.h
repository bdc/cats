// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_ini.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_INI_H
#define CATS_INI_H

#include "data/cats_datatypes.h"

#ifdef DEBUG_INI
#define DBG(X) {X;}while(0);
#else
#define DBG(X)
#endif

/// @brief Data structure for simple INI files
/**
  * A simple flat ini structure containing N named sections (names: char *) which contain
  * M(N) key-value pairs (key and values: char *)
  * \n
  * \b Attention: section names, key names and value names are not unique! Only the first one inserted will be returned!
 */
struct cats_ini {

        int n_sections;   // number of currently stored sections: N
        char **sections;  // sections contains the names of the sections
        char ***keys;     // array of all keys for all sections
        char ***values;   // array of all values for all sections
        int32_t **values_accessed;  // how often each key has been accessed;
        int32_t *sections_accessed;
        int *n_keys;      // number of stored keys per section
        struct cats_ini *complement;

        // keys[X]      contains the array of keys for the section
        //              with index X in range [0, N)
        //
        // keys[X][y]   contains the key with index Y of the section with index X
        //              with Y in range[0, keys[X]) and X in range [0, N)
        //
        // values analogue to keys
};

void maybe_add_key_val(char *line, struct cats_ini *ini);
void maybe_add_section(char *line, struct cats_ini *ini);
struct cats_ini *load_ini_file(const char *filename);
struct cats_ini *new_ini(void);
int add_section(struct cats_ini *ini, char *section_name);
int32_t add_key_val(struct cats_ini *ini, int32_t section_idx, char *key, char *value);

void free_ini(struct cats_ini *ini);

struct string_array *get_char_array_from_conf2(const struct cats_ini *ini, char *section, char *key);

bool get_int32_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required,
                             int32_t *value);

bool get_long_double_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required,
                                   long double *value);

bool get_float_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required,
                             float *value);

bool
get_char_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required, char **value);

bool
get_bool_config_value(const struct cats_ini *ini, const char *section, const char *key, bool required, bool *value);

//void load_conf_value(bool required, struct cats_ini *ini, char *section, char *key, void *value);


#define load_conf_value(REQUIRED, INI, SECTION, KEY, VALUE) _Generic((VALUE), \
 int32_t *:       get_int32_config_value, \
 long double *:       get_long_double_config_value,                          \
 float *:        get_float_config_value, \
 char **:       get_char_config_value, \
 bool *:       get_bool_config_value \
)(INI, SECTION, KEY, REQUIRED, (VALUE))

int get_section_index(const struct cats_ini *ini, const char *section);

int get_key_index(const struct cats_ini *ini, int section_index, const char *key);

int32_t get_section_prefix_count(const struct cats_ini *ini, const char *section_prefix);

struct string_array *get_sections_with_prefix(const struct cats_ini *ini, const char *section_prefix);

char *full_section_name(const char *section, const char *prefix);

void print_accesses(struct cats_ini *ini);

void print_unused_values(struct cats_ini *ini, const char *filename);

void print_complement_values(struct cats_ini *ini, const char *filename);

#endif