// SPDX-License-Identifier: GPL-3.0-or-later
//
// logging.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_LOGGING_H_
#define CATS_LOGGING_H_
#include <stdbool.h>
#include "cats_defs.h"
#include <time.h>
#include "cats_time/cats_time.h"

#if defined(__MINGW32__) || defined(CATS_ON_WINDOWS)
// no color codes
#define C_RED    ""
#define C_GREEN  ""
#define C_RESET  ""
#define C_YELLOW ""
#define C_WHITE  ""
#else
#define C_RED "\x1B[31m"
#define C_GREEN "\x1B[32m"
#define C_RESET "\033[0m"
#define C_YELLOW "\x1B[33m"
#define C_WHITE  "\x1B[37m"
#endif

enum cats_log_level {
        LOG_DEBUG,
        LOG_INFO,
        LOG_EMPTY,
        LOG_MARK,
        LOG_IMPORTANT,
        LOG_WARNING,
        LOG_ERROR,
        LOG_MPI,
        LOG_RAW,
        LOG_UNIMPLEMENTED,
        LOG_UNKNOWN
};

void logging_initialize(enum cats_log_level level, const struct cats_time *start_time, const char *log_file_name, bool quiet);

void logging_set_mpi_rank(int mpi_world_rank);

void logging_set_module_name(const char *name);

__attribute__ ((__format__ (__printf__, 2, 3)))
void log_message(enum cats_log_level level, const char *fmt, ...);

enum cats_log_level get_log_level(void);

void log_message_simple(enum cats_log_level level, const char *fmt, ...);

void set_log_level(enum cats_log_level new_level);

#endif