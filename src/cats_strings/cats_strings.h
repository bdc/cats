// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_strings.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_STRING_H_
#define CATS_STRING_H_
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "data/error.h"
enum JSON_HINT {
        JSON_STRING = 0,
        JSON_QUOTED = 0,
        JSON_UNQUOTED = 1,
        JSON_NUMERIC = 1
};

struct string_array {
        int count;
        char **string;
        enum JSON_HINT *typehint;
};




#include <stdio.h>

#define MAX_STRING_BUFF_LENGTH 2048

char *read_single_line(FILE *file);

char *slurp_file(const char *filename);

#define ENSURE_FILE_OPENED(FILE_HANDLE, FILENAME)\
if (! (FILE_HANDLE))\
{\
        fprintf(stderr, "ERROR: Couldn't open file %s in %s: %d (%s)\n", FILENAME, __FILE__, __LINE__, __func__);\
        exit(E_UNREADABLE_FILE);\
}


void asprintf_check(int rc);

struct string_array *new_string_array(void);

void free_string_array(struct string_array **array);

int32_t count_fields(const char *line, const char *sep);

struct string_array *get_all_tokens(char *line, const char *seperator);

void string_array_add(struct string_array *array, const char *string);

void string_array_add_int64(struct string_array *array, int64_t number, char *format_string);

char *string_array_paste(const struct string_array *array, const char *sep);

char *assemble_filename(struct string_array *path, struct string_array *filename, char *filesep, char *extension);

void string_array_add_int(struct string_array *array, int32_t number, const char *format_string);

struct string_array *new_string_array_init(const char *entry);


void string_array_add_conditional(struct string_array *array, char *string, bool conditon);

void string_array_add_double(struct string_array *array, double number, const char *format_string);

void
string_array_add_int_conditional(struct string_array *array, int32_t number, const char *format_string, bool condition);
const char *get_nth_string_from_array(const struct string_array *array, int n);
void print_string_array(const struct string_array *array);

char *remove_0th_token(char *line, const char *sep);

char *get_nth_token(char *line, const char *sep, int32_t n);

struct string_array *copy_string_array(const struct string_array *src);

char *json_dict(const struct string_array *header, const struct string_array *data);

int32_t string_array_index(const struct string_array *array, const char *needle);

bool in_string_array(const struct string_array *array, const char *needle);

size_t substring_count(const char *haystack, const char *needle);

char *compound_string(const char *s1, const char *s2, const char *sep);

char *replace_substring(const char *original, const char *search, const char *replacement);

bool string_is_emptyish(const char *string);

char *empty_string(void);

char *right_trim(char *line);

char *left_trim(char *line);

void abort_on_null_token(const char *token, char *orig, int count);

#define PAD_NAME 55
#define PAD_VALUE 35

void print_string(int indent, const char *name, const char *value);

void print_subcategory(int indent, const char *category_name, int64_t category_number, char *name);

void print_category(int indent, const char *category, const char *name);

void print_integer(int indent, const char *name, int64_t value);

void print_rate(int indent, const char *name, long double value);

const char *bool_to_string(bool b);

#ifdef DEBUG_STRING
#define DBG_STRING(X) {X;}while(0);
#else
#define DBG_STRING(X)
#endif

// implemented in string_helpers.h


#endif
