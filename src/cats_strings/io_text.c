// SPDX-License-Identifier: GPL-3.0-or-later
//
// io_text.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <logging/logging.h>
#include "cats_strings.h"


char *read_single_line(FILE *file)
{
        assert(file != NULL);
        char buffer[MAX_STRING_BUFF_LENGTH] = {0};
        fgets(buffer, MAX_STRING_BUFF_LENGTH, file);
        fflush(stdout);
        char *result = right_trim(buffer);

        if (strlen(result)) return strdup(result);
        return NULL;
}


char *slurp_file(const char *filename)
{
        assert(filename != NULL);

        FILE *file = fopen(filename, "r");

        if (!file) {
                log_message(LOG_ERROR, "%s: File '%s' does not exist or could not be opened.", __func__, filename);
                return NULL;
        }

        // get file size
        fseek(file, 0L, SEEK_END);
        long size = ftell(file);
        fseek(file, 0L, SEEK_SET);

        char *buffer = malloc(size + 1);

        if (!buffer) {
                log_message(LOG_ERROR, "%s: failed to allocate memory", __func__);
                exit(EXIT_FAILURE);
        }

        fread(buffer, sizeof(char), size, file);
        fclose(file);
        buffer[size] = '\0';
        return buffer;
}
