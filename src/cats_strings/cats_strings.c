// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_strings.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cats_strings.h"
#include <logging/logging.h>
#include <memory/cats_memory.h>
#include <assert.h>


void asprintf_check(int rc)
{
        if (rc >= 0) return;
        log_message(LOG_ERROR, "error in asprintf_ could not allocate memory or other error. rc: %d", rc);
        exit(EXIT_FAILURE);
}


void string_array_add(struct string_array *array, const char *string)
{
        if (!array) {
                log_message(LOG_ERROR, "%s: empty string array, can't add '%s'", __func__, string);
                exit(EXIT_FAILURE);
        }
        if (!string) return;
        if (!strlen(string)) return;

        size_t new_size = (array->count + 1) * sizeof(char *);

        DBG_STRING(log_message(LOG_RAW, "%s: address:        %p\n", __func__, array);)
        DBG_STRING(log_message(LOG_RAW, "%s: count:          %d\n", __func__, array->count);)
        DBG_STRING(log_message(LOG_RAW, "%s: trying to add:  %s\n", __func__, string);)

        array->string = realloc_or_die(array->string, new_size);
        array->typehint = realloc_or_die(array->typehint, sizeof(enum JSON_HINT) * (array->count + 1));
        array->string[array->count] = strdup(string);
        array->typehint[array->count] = JSON_QUOTED;
        array->count = array->count + 1;

        DBG_STRING(log_message(LOG_RAW, "%s: string addr:    %p\n", __func__, array->string);)
        DBG_STRING(log_message(LOG_RAW, "%s: count:          %d\n", __func__, array->count);)
        DBG_STRING(log_message(LOG_RAW, "%s: newest pointer: %p\n", __func__, array->string[array->count - 1]);)
        DBG_STRING(log_message(LOG_RAW, "%s: newest string:  %s\n", __func__, array->string[array->count - 1]);)
        DBG_STRING(log_message(LOG_RAW, "\n");)
}


void string_array_add_conditional(struct string_array *array, char *string, bool condition)
{
        if (!condition) return;
        string_array_add(array, string);
}


void string_array_add_int(struct string_array *array, int32_t number, const char *format_string)
{
        const char *f = format_string;
        if (!format_string || !strlen(format_string)) f = "%d";

        int count = snprintf(NULL, 0, f, number) + 1;
        char *tmp = malloc_or_die(count);
        snprintf(tmp, count, f, number);

        string_array_add(array, tmp);
        array->typehint[array->count - 1] = JSON_NUMERIC;
        free(tmp);
}


void string_array_add_int64(struct string_array *array, int64_t number, char *format_string)
{
        char *f = format_string;
        if (!format_string || !strlen(format_string)) f = "%ld";

        int count = snprintf(NULL, 0, f, number) + 1;
        char *tmp = malloc_or_die(count);
        snprintf(tmp, count, f, number);

        string_array_add(array, tmp);
        array->typehint[array->count - 1] = JSON_NUMERIC;
        free(tmp);
}


void string_array_add_double(struct string_array *array, double number, const char *format_string)
{
        const char *f = format_string;
        if (!format_string || !strlen(format_string)) f = "%f";
        int count = snprintf(NULL, 0, f, number) + 1;
        char *tmp = malloc_or_die(count);
        snprintf(tmp, count, f, number);
        string_array_add(array, tmp);
        array->typehint[array->count - 1] = JSON_NUMERIC;
        free(tmp);
}


void
string_array_add_int_conditional(struct string_array *array, int32_t number, const char *format_string, bool condition)
{
        if (!condition) return;
        string_array_add_int(array, number, format_string);
        array->typehint[array->count - 1] = JSON_NUMERIC;
}


struct string_array *new_string_array()
{
        struct string_array *result = malloc_or_die(sizeof(struct string_array));
        result->string = NULL;
        result->count = 0;
        result->typehint = NULL;
        return result;
}

const char *get_nth_string_from_array(const struct string_array *array, int n)
{
        assert(array != NULL);
        if (n < 0 && n >= array->count) {
                log_message(LOG_ERROR, "%s: requested string index %d out of range [0, %d]", __func__ , n, array->count);
                exit(EXIT_FAILURE);
        }

        return array->string[n];

}

struct string_array *new_string_array_init(const char *entry)
{
        struct string_array *result = new_string_array();
        string_array_add(result, entry);
        return result;
}


struct string_array *copy_string_array(const struct string_array *src)
{
        assert(src != NULL);
        struct string_array *dst = new_string_array();
        for (int32_t i = 0; i < src->count; i++) {
                string_array_add(dst, src->string[i]);
                dst->typehint[i] = src->typehint[i];
        }
        return dst;

}


void free_string_array(struct string_array **array)
{
        if (array == NULL || *array == NULL) return;

        struct string_array *this = *array;
        DBG_STRING(log_message(LOG_RAW, "%s: address:        %p\n", __func__, this);)
        DBG_STRING(log_message(LOG_RAW, "%s: string:         %p\n", __func__, this->string);)
        DBG_STRING(log_message(LOG_RAW, "%s: count:          %d\n", __func__, this->count);)

        for (int32_t i = 0; i < this->count; i++) {
                DBG_STRING(log_message(LOG_RAW, "%s: string %d:      %s\n", __func__, i, this->string[i]);)
                free(this->string[i]);
        }
        free(this->typehint);
        this->typehint = NULL;
        free(this->string);
        this->string = NULL;
        free(*array);
        *array = NULL;
}


// currently only non-whitespace characters supported
int32_t count_fields(const char *line, const char *sep)
{
        if (string_is_emptyish(line)) return 0;
        size_t length = strlen(line);

        int32_t count = 1;
        size_t sepl = strlen(sep);

        for (size_t i = 0; i < length; i++) {
                if (!strncmp(&line[i], sep, sepl)) count++;
        }

        return count;
}


// untested trivial
void abort_on_null_token(const char *token, char *orig, int count)
{
        if (token != NULL) return;
        log_message(LOG_WARNING, "%s: string '%s' has not enough (%d) tokens", __func__, orig, count);
        abort();
        exit(EXIT_FAILURE);
}


struct string_array *get_all_tokens(char *line, const char *sep)
{
        const int32_t max_count = count_fields(line, sep);

        struct string_array *result = new_string_array();

        if (string_is_emptyish(line) || max_count == 0) return result;

        char *saveptr = NULL;
        char *copy = strdup(line);

        char *token = strtok_r(copy, sep, &saveptr);
        abort_on_null_token(token, line, max_count);
        char *to_add = strdup(token);
        to_add = left_trim(right_trim(to_add));
        string_array_add(result, to_add);
        free(to_add);

        int32_t seen = 1;

        while (token != NULL && seen < max_count) {
                token = strtok_r(NULL, ",", &saveptr);
                abort_on_null_token(token, line, max_count);
                char *token_copy = strdup(token);
                token_copy = left_trim(right_trim(token_copy));
                string_array_add(result, token_copy);
                free(token_copy);
                //string_array_add(result, token);
                DBG_STRING(log_message(LOG_RAW, "-found token: \"%s\"\n", token);)
                seen++;
        }

        free(copy);
        return result;
}


char *get_nth_token(char *line, const char *sep, int32_t n)
{
        char *result = NULL;
        struct string_array *tokens = get_all_tokens(line, sep);
        if (tokens->count < n) return result;

        result = strdup(tokens->string[n]);
        free_string_array(&tokens);
        return result;
}


char *remove_0th_token(char *line, const char *sep)
{
        char *result = NULL;
        struct string_array *tokens = get_all_tokens(line, sep);
        struct string_array *tmp = new_string_array();
        for (int32_t i = 1; i < tokens->count; i++) {
                string_array_add(tmp, tokens->string[i]);
        }

        result = string_array_paste(tmp, sep);
        free_string_array(&tokens);
        free_string_array(&tmp);
        return result;

}


char *json_dict(const struct string_array *header, const struct string_array *data)
{
        const char *quotation = "\"";
        assert(header->count == data->count);
        if (header->count == 0) return NULL;

        size_t len = 3; // 2 * brackets + \0

        // {"a": "b", "c": "3"}
        for (int32_t i = 0; i < header->count; i++) {
                len += strlen(header->string[i]); //
                len += 4; // colon + space + 2x quotation mark

                len += strlen(data->string[i]);
                if (data->typehint[i] == JSON_QUOTED) len += 2; // 2x quotation mark
                if (i < header->count - 1) len += 2;
        }


        char *result = calloc_or_die(1, len);
        size_t written = 0;

        result[0] = '{'; // written: 1
        written = 1;
        result[1] = '\0';

        for (int32_t i = 0; i < header->count; i++) {
                assert(written <= len);
                result = strncat(result, quotation, len - written);
                written += strlen(quotation);

                assert(written <= len);
                result = strncat(result, header->string[i], len - written);
                written += strlen(header->string[i]);

                assert(written <= len);
                result = strncat(result, "\": ", len - written);
                written += 3;
                assert(written <= len);

                if (data->typehint[i] == JSON_QUOTED) {
                        result = strncat(result, "\"", len - written);
                        written += 1;
                }
                assert(written <= len);
                result = strncat(result, data->string[i], len - written);
                written += strlen(data->string[i]);
                assert(written <= len);

                if (data->typehint[i] == JSON_QUOTED) {
                        result = strncat(result, "\"", len - written);
                        written += 1;
                }

                if (i < header->count - 1) {
                        assert(written <= len);
                        result = strncat(result, ", ", len - written);
                        written += 2;
                }
        }

        result[len - 2] = '}';
        result[len - 1] = '\0';
        return result;
}


char *string_array_paste(const struct string_array *array, const char *sep)
{
        if (array->count == 1) return strdup(array->string[0]);

        size_t length = 0;
        for (int32_t i = 0; i < array->count; i++) {
                length += strlen(array->string[i]);
        }

        if (length == 0) {
                log_message(LOG_ERROR, "%s: empty string, exiting", __func__);
                abort();
                exit(EXIT_FAILURE);
        }

        length += (array->count - 1) * strlen(sep);  // space for separator
        length += 1; // space for \0-terminator;

        char *result = calloc_or_die(length, sizeof(char));

        result[0] = '\0';
        for (int32_t i = 0; i < array->count; i++) {
                strcat(result, array->string[i]);
                if (i != array->count - 1) {
                        strcat(result, sep);
                }
        }
        result[length - 1] = '\0';

        return result;
}


void print_string_array(const struct string_array *array)
{
        log_message(LOG_RAW, "string array with %d entries:\n", array->count);
        for (int32_t i = 0; i < array->count; i++) {
                log_message(LOG_RAW, "\t% 2d: '%s'\n", i, array->string[i]);
        }
        log_message(LOG_RAW, "\n");
}


int32_t string_array_index(const struct string_array *array, const char *needle)
{
        if (array == NULL) {
                log_message(LOG_ERROR, "%s: called with empty parameter array", __func__);
                exit(EXIT_FAILURE);
        }

        for (int32_t i = 0; i < array->count; i++) {
                if (strcmp(needle, array->string[i]) == 0) return i;
        }

        return -1;
}


bool in_string_array(const struct string_array *array, const char *needle)
{
        for (int32_t i = 0; i < array->count; i++) {
                if (strcmp(needle, array->string[i]) == 0) return true;
        }
        return false;
}


char *compound_string(const char *s1, const char *s2, const char *sep)
{
        char *result = NULL;

        if (s1 == NULL || s2 == NULL || sep == NULL) {
                if (s1 != NULL) {
                        log_message(LOG_ERROR, "%s: first argument '%s'", __func__, s1);
                } else {
                        log_message(LOG_ERROR, "%s: first argument NULL", __func__);
                }
                if (s2 != NULL) {
                        log_message(LOG_ERROR, "%s: second argument '%s'", __func__, s2);
                } else {
                        log_message(LOG_ERROR, "%s: second argument NULL", __func__);
                }
                if (sep != NULL) {
                        log_message(LOG_ERROR, "%s: separator '%s'", __func__, sep);
                } else {
                        log_message(LOG_ERROR, "%s: separator NULL", __func__);
                }
                log_message(LOG_ERROR, "%s: at least one argument NULL", __func__);
                exit(EXIT_FAILURE);
        }

        size_t len = strlen(s1) + strlen(s2) + strlen(sep) + 1;
        result = malloc_or_die(len);
        snprintf(result, len, "%s%s%s", s1, sep, s2);
        return result;
}