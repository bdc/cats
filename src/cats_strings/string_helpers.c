// SPDX-License-Identifier: GPL-3.0-or-later
//
// string_helpers.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <logging/logging.h>
#include <memory/cats_memory.h>


bool string_is_emptyish(const char *string)
{
        if (string == NULL) return true;

        size_t length = strlen(string);
        if (length == 0) return true;

        for (size_t i = 0; i < length; i++) {
                if (!isspace(string[i])) return false;
        }

        return true;
}


char *right_trim(char *line)
{
        if (!line) return NULL;
        if (!strlen(line)) return line;

        char *last = line + strlen(line);

        while (isspace(*--last) && last != line);

        *(++last) = '\0';

        return line;
}


char *left_trim(char *line)
{
        if (!line) return NULL;
        if (!strlen(line)) return line;
        size_t len = strlen(line);

        size_t start_idx;
        for (start_idx = 0; start_idx < len; start_idx++) {
                if (!isspace(line[start_idx])) break;
        }

        if (start_idx == len) return line;

        memmove(line, &line[start_idx], len - start_idx);
        line[len - start_idx] = '\0';
        return line;

}


size_t substring_count(const char *haystack, const char *needle)
{
        size_t count = 0;

        if (haystack == NULL || needle == NULL) {
                log_message(LOG_WARNING, "error counting substring occurrences: haystack or needle NULL");
                return count;
        }

        if (strlen(haystack) == 0 || strlen(needle) == 0) {
                log_message(LOG_WARNING, "error counting substring occurrences: haystack or needle had length zero");
                return count;
        }


        const char *result = haystack;
        size_t needle_len = strlen(needle);

        do {
                result = strstr(result, needle);
                if (result == NULL) break;
                count += 1;

                // if the remaining string is long enough to contain another needle, advance search position and continue
                // otherwise end search
                if (strlen(result) > needle_len) {
                        result += needle_len;
                } else {
                        break;
                }

        } while (result != NULL);

        return count;
}


char *replace_substring(const char *original, const char *search, const char *replacement)
{
        if (original == NULL || search == NULL || replacement == NULL) {
                log_message(LOG_ERROR,
                            "%s: at least one NULL argument supplied: original '%s', search '%s', replacement '%s' ",
                            __func__, original, search, replacement);
                exit(EXIT_FAILURE);
        }

        size_t search_len = strlen(search);
        size_t replacement_len = strlen(replacement);
        size_t original_len = strlen(original);


        if (original_len == 0 || search_len == 0 || replacement_len == 0) {
                log_message(LOG_ERROR, "at least one zero-length argument to %s supplied", __FUNCTION__);
                exit(EXIT_FAILURE);
        }

        size_t occurrences = substring_count(original, search);
        size_t result_len = strlen(original) + strlen(replacement) * occurrences - strlen(search) * occurrences + 1;

        char *result = calloc_or_die(1, result_len);

        const char *orig_loc = original;
        char *result_loc = result;

        size_t bytes_copied = 0;

        for (size_t i = 0; i < occurrences; i++) {
                char *next_occurrence = strstr(orig_loc, search);
                if (next_occurrence == NULL) { break; }
                size_t bytes_to_copy = strlen(orig_loc) - strlen(next_occurrence);
                if (bytes_to_copy) {
                        bytes_copied += bytes_to_copy;
                        strncpy(result_loc, orig_loc, bytes_to_copy);
                        result_loc += bytes_to_copy;
                        orig_loc += bytes_to_copy + search_len;
                }


                strncpy(result_loc, replacement, replacement_len);
                result_loc += replacement_len;
        }

        if (bytes_copied < original_len) {
                size_t to_copy = original_len - bytes_copied;
                strncat(result_loc, orig_loc, to_copy);
        }

        result[result_len - 1] = '\0';

        return result;
}
