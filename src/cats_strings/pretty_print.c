// SPDX-License-Identifier: GPL-3.0-or-later
//
// pretty_print.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "cats_global.h"
#include <inttypes.h>
#include "cats_strings.h"
#include "logging/logging.h"

void print_string(int indent, const char *name, const char *value)
{
        indent *= 2;
        if (indent > 0) log_message(LOG_RAW, "%*s", indent, "");
        log_message(LOG_RAW, "%-*s%*s\n", PAD_NAME - indent, name, PAD_VALUE, value);
}


void print_subcategory(int indent, const char *category_name, int64_t category_number, char *name)
{
        indent *= 2;
        if (indent > 0) log_message(LOG_RAW, "%*s", indent, "");
        if (name) {
                log_message(LOG_RAW, "[%s %" PRIu64 ": %s]\n", category_name, category_number, name);
        } else {
                log_message(LOG_RAW, "[%s %" PRIu64 "]\n", category_name, category_number);
        }
}


const char *bool_to_string(const bool b)
{
        if (b) return "yes";
        return "no";
}


void print_category(int indent, const char *category, const char *name)
{
        log_message(LOG_RAW, "\n");
        indent *= 2;
        if (indent > 0) log_message(LOG_RAW, "%*s", indent, "");
        if (name) {
                log_message(LOG_RAW, "[%s - %s]\n", category, name);
        } else {
                log_message(LOG_RAW, "[%s]\n", category);
        }
}


void print_integer(int indent, const char *name, int64_t value)
{
        indent *= 2;
        if (indent > 0) log_message(LOG_RAW, "%*s", indent, "");
        log_message(LOG_RAW, "%-*s%*"PRIu64"\n", PAD_NAME - indent, name, PAD_VALUE, value);
}


void print_rate(int indent, const char *name, long double value)
{
        indent *= 2;
        if (indent > 0) log_message(LOG_RAW, "%*s", indent, "");
        log_message(LOG_RAW, "%-*s%*f\n", PAD_NAME - indent, name, PAD_VALUE, (double) value);
}

