// SPDX-License-Identifier: GPL-3.0-or-later
//
// raw_arrays.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_RAW_ARRAYS_H
#define CATS_RAW_ARRAYS_H

#include <stdint.h>
#include <stddef.h>

void cleanup_raw_2d_double_array(double ***data, int64_t rows);

void cleanup_raw_2d_float_array(float ***data, int64_t rows);

void cleanup_raw_2d_int32t_array(int32_t ***data, int64_t rows);

void cleanup_raw_2d_char_array(char ***data, int64_t rows);

void cleanup_raw_2d_uchar_array(unsigned char ***data, int64_t rows);

void zero_raw_1d_float_array(float *array, size_t size);

void zero_raw_1d_long_double_array(long double *array, size_t size);

void zero_raw_1d_int_array(int *array, size_t size);

#define free_grid(PTR_TO_GRID, ROWS) _Generic((PTR_TO_GRID), \
 double ***:        cleanup_raw_2d_double_array, \
 float ***:         cleanup_raw_2d_float_array, \
 int32_t ***:       cleanup_raw_2d_int32t_array, \
 char ***:          cleanup_raw_2d_char_array, \
 unsigned char ***: cleanup_raw_2d_uchar_array\
 )(PTR_TO_GRID, (ROWS))


#define zero_array(ARRAY, ENTRIES) _Generic((ARRAY), \
float *:         zero_raw_1d_float_array, \
long double *:   zero_raw_1d_long_double_array, \
int *:           zero_raw_1d_int_array\
)(ARRAY, (ENTRIES))

void *new_raw_2d_array(size_t rows, size_t cols, size_t size);

void *new_raw_1d_array(size_t count, size_t size);

#endif // CATS_RAW_ARRAYS_H