// SPDX-License-Identifier: GPL-3.0-or-later
//
// raw_arrays.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "raw_arrays.h"
#include "cats_memory.h"


void *new_raw_2d_array(size_t rows, size_t cols, size_t size)
{
        void **grid = calloc_or_die(rows, sizeof(void *));

        for (cats_dt_coord row = 0; row < rows; row++) {
                grid[row] = calloc_or_die(cols, size);
                if (grid[row] == NULL) exit(EXIT_FAILURE); // should never happen, we would exit in calloc_or_die
        }
        return grid;
}


void *new_raw_1d_array(size_t count, size_t size)
{
        return calloc_or_die(count, size);
}


void cleanup_raw_2d_float_array(float ***data, int64_t rows)
{
        if (!*data) return;

        for (int64_t row = 0; row < rows; row++) {
                free((*data)[row]);
                (*data)[row] = NULL;
        }

        free(*data);
        *data = NULL;
}


void cleanup_raw_2d_int32t_array(int32_t ***data, int64_t rows)
{
        if (!*data) return;
        int32_t **local = *data;

        for (int64_t row = 0; row < rows; row++) {
                free(local[row]);
                local[row] = NULL;
        }

        free(local);
        *data = NULL;
}


void cleanup_raw_2d_char_array(char ***data, int64_t rows)
{
        if (!*data) return;
        char **local = *data;

        for (int64_t row = 0; row < rows; row++) {
                free(local[row]);
                local[row] = NULL;
        }
        free(local);
        *data = NULL;
}


void cleanup_raw_2d_uchar_array(unsigned char ***data, int64_t rows)
{
        if (!*data) return;
        unsigned char **local = *data;

        for (int64_t row = 0; row < rows; row++) {
                free(local[row]);
                local[row] = NULL;
        }
        free(local);
        *data = NULL;
}


void cleanup_raw_2d_double_array(double ***data, int64_t rows)
{
        if (!*data) return;
        double **local = *data;

        for (int64_t row = 0; row < rows; row++) {
                free(local[row]);
                local[row] = NULL;
        }
        free(local);
        *data = NULL;
}


void zero_raw_1d_float_array(float *array, size_t size)
{
        for (size_t i = 0; i < size; i++) array[i] = 0.0f;
}


void zero_raw_1d_long_double_array(long double *array, size_t size)
{
        for (size_t i = 0; i < size; i++) array[i] = 0.0;
}


void zero_raw_1d_int_array(int *array, size_t size)
{
        for (size_t i = 0; i < size; i++) array[i] = 0;
}
