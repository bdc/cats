// SPDX-License-Identifier: GPL-3.0-or-later
//
// arrays.h
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#ifndef CATS_ARRAYS_H
#define CATS_ARRAYS_H

#include "data/cats_datatypes.h"
#include "raw_arrays.h"

struct cats_2d_array_float {
        float **data;
        struct cats_dimension dimension;
};

struct cats_2d_array_char {
        char **data;
        struct cats_dimension dimension;
};

struct cats_2d_array_double {
        double **data;
        struct cats_dimension dimension;
};

struct cats_2d_array_char *new_2d_array_char(struct cats_dimension dimension);

struct cats_2d_array_double *new_2d_array_double(struct cats_dimension dimension);

struct cats_2d_array_float *new_2d_array_float(struct cats_dimension dimension);

void cleanup_2d_array_char(struct cats_2d_array_char **grid);

void cleanup_2d_array_float(struct cats_2d_array_float **grid);

void cleanup_2d_array_double(struct cats_2d_array_double **grid);

#define free_cats_grid(PTR_TO_GRID) _Generic((PTR_TO_GRID), \
 struct cats_2d_array_float **:        cleanup_2d_array_float, \
 struct cats_2d_array_double **:       cleanup_2d_array_double, \
 struct cats_2d_array_char **:         cleanup_2d_array_char \
 )(PTR_TO_GRID)


void *new_raw_2d_array_from_dimension(struct cats_dimension dimension, size_t member_size);

#endif //CATS_ARRAYS_H
