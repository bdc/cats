// SPDX-License-Identifier: GPL-3.0-or-later
//
// cats_memory.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "cats_memory.h"

// we do not use logging functions here --
// if there is a memory allocation error, keep everything simple and terminate.

void *malloc_or_die_trace(size_t size, const char *func)
{
        const char *name = func;
        if (!func) name = "unknown";
        if (size > 1000 * 1000 * 1000) {
                fprintf(stderr, "Allocation of %zu bytes\n", size);
                fflush(stderr);
        }

        void *result = malloc(size);

        if (result == NULL) {
                fprintf(stderr, "%s: error allocating %zu bytes. Quitting\n", name, size);
                printf("%s: error allocating %zu bytes. Quitting\n", name, size); // allowed printf
                exit(EXIT_FAILURE);
        }

        return result;
}


void *realloc_or_die(void *ptr, size_t size)
{
        ptr = realloc(ptr, size);

        if (size > 1000 * 1000 * 1000) {
                fprintf(stderr, "Allocation of %zu bytes\n", size);
                fflush(stderr);
        }
        if (ptr == NULL) {
                fprintf(stderr, "%s: error re-allocating %zu bytes. Quitting\n", __func__, size);
                printf("%s: error re-allocating %zu bytes. Quitting\n", __func__, size); // allowed printf
                exit(EXIT_FAILURE);
        }

        return ptr;
}


void *malloc_or_die(size_t size)
{
        if (size > 1000 * 1000 * 1000) {
                fprintf(stderr, "Allocation of %zu bytes\n", size);
                fflush(stderr);
        }

        void *result = malloc(size);
        if (result == NULL) {
                fprintf(stderr, "%s: error allocating %zu bytes. Quitting\n", __func__, size);
                printf("%s: error allocating %zu bytes. Quitting\n", __func__, size); // allowed printf
                exit(EXIT_FAILURE);
        }

        return result;
}


void *calloc_or_die(size_t count, size_t size)
{
        void *result = calloc(count, size);
        if (result == NULL) {
                fprintf(stderr, "%s:error allocating %zu members of %zu bytes. Quitting\n", __func__,
                        count, size);
                printf("%s:error allocating %zu members of %zu bytes. Quitting\n", __func__, count,
                       size); // allowed printf
                exit(EXIT_FAILURE);
        }

        return result;
}