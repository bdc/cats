// SPDX-License-Identifier: GPL-3.0-or-later
//
// arrays.c
//
// Copyright (C) 2011-2024, University of Vienna and Vienna Institute for Nature Conservation & Analyses, Andreas Gattringer.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//

#include "raw_arrays.h"
#include <malloc.h>
#include "cats_memory.h"
#include <assert.h>
#include "arrays.h"


struct cats_2d_array_float *new_2d_array_float(struct cats_dimension dimension)
{
        struct cats_2d_array_float *result = malloc_or_die(sizeof(struct cats_2d_array_float));
        result->data = new_raw_2d_array_from_dimension(dimension, sizeof(float));
        result->dimension = dimension;
        return result;
}


struct cats_2d_array_char *new_2d_array_char(struct cats_dimension dimension)
{
        struct cats_2d_array_char *result = malloc_or_die(sizeof(struct cats_2d_array_char));
        result->data = new_raw_2d_array_from_dimension(dimension, sizeof(char));
        result->dimension = dimension;
        return result;
}


struct cats_2d_array_double *new_2d_array_double(struct cats_dimension dimension)
{
        struct cats_2d_array_double *result = malloc_or_die(sizeof(struct cats_2d_array_double));
        result->data = new_raw_2d_array_from_dimension(dimension, sizeof(double));
        result->dimension = dimension;
        return result;
}


void cleanup_2d_array_char(struct cats_2d_array_char **grid)
{
        assert(grid != NULL);
        struct cats_2d_array_char *g = *grid;
        assert(g != NULL);
        cleanup_raw_2d_char_array(&g->data, g->dimension.rows);
        g->dimension.rows = INT32_MIN;
        g->dimension.cols = INT32_MIN;
        free(g);
        *grid = NULL;
}


void cleanup_2d_array_float(struct cats_2d_array_float **grid)
{
        assert(grid != NULL);
        struct cats_2d_array_float *g = *grid;
        assert(g != NULL);
        cleanup_raw_2d_float_array(&g->data, g->dimension.rows);
        g->dimension.rows = INT32_MIN;
        g->dimension.cols = INT32_MIN;
        free(g);
        *grid = NULL;
}


void cleanup_2d_array_double(struct cats_2d_array_double **grid)
{
        assert(grid != NULL);
        struct cats_2d_array_double *g = *grid;
        assert(g != NULL);
        cleanup_raw_2d_double_array(&g->data, g->dimension.rows);
        g->dimension.rows = INT32_MIN;
        g->dimension.cols = INT32_MIN;
        free(g);
        *grid = NULL;
}


void *new_raw_2d_array_from_dimension(struct cats_dimension dimension, size_t member_size)
{
        return new_raw_2d_array(dimension.rows, dimension.cols, member_size);
}